/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
	  store_id: {
      type: DataTypes.STRING(6),
      allowNull: true
    },
    firstName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    lastName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    username: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    role: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    // subscription: {
    //   type: DataTypes.STRING(200),
    //   allowNull: true
    // },
    // lat: {
    //   type: DataTypes.FLOAT(10,2),
    //   allowNull: true
    // },
    // long: {
    //   type: DataTypes.FLOAT(10,2),
    //   allowNull: true
    // },
    // locationId: {
    //   type: DataTypes.INTEGER(11),
    //   allowNull: true
    // },
    address: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    description: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    // companyId: {
    //   type: DataTypes.INTEGER(11),
    //   allowNull: true
    // },
    createdBy: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM('active','inactive','unapproved','archive'),
      allowNull: true
    },
	postCode: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
	country: {
      type: DataTypes.ENUM('india'),
      allowNull: true
    },
	location: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    tableName: 'users'
  });
};
