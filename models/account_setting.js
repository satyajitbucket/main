module.exports = function(sequelize, DataTypes) {
    return sequelize.define('account_setting', {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      admin_user_id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false
      },
      store_id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false
      },
      account_name: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
      site_short_name: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      country: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      state: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      city: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      street: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      zip: {
        type: DataTypes.INTEGER(5),
        allowNull: true
      },
      mailing_address: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      fax: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
      website: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      email: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      mobile: {
        type: DataTypes.STRING(15),
        allowNull: true
      },
      status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
      },
      image: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'account_setting'
    });
  };
  