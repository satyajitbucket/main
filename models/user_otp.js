module.exports = function(sequelize, DataTypes) {
    return sequelize.define('user_otp', {
      id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {
        type: DataTypes.INTEGER(5),
        allowNull: true
      },
	  otpValue: {
        type: DataTypes.INTEGER(5),
        allowNull: true
      },
      
      status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
      },
       createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
     
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    }, {
      tableName: 'user_otp'
    });
  };
  