module.exports = function(sequelize, DataTypes) {
    return sequelize.define('coupon_value', {
     
      coupon_value: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
     
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
      }
      
    }, {
      tableName: 'couponvalue'
    });
  };