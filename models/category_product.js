module.exports = function(sequelize, DataTypes) {
    return sequelize.define('category_product', {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      category_id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false
      },
      product_id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false
      },
      position: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
      }
    }, {
      tableName: 'category_product'
    });
  };
  