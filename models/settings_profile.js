module.exports = function(sequelize, DataTypes) {
    return sequelize.define('settings_profile', {
      id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      username: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
      email: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
	  phone: {
        type: DataTypes.STRING(12),
        allowNull: true 
      },
	  skype: {
        type: DataTypes.STRING(100),
        allowNull: true 
      },
	   address: {
        type: DataTypes.STRING(200),
        allowNull: true 
      },
      status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'settings_profile'
    });
  };
  