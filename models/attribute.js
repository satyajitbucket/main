module.exports = function(sequelize, DataTypes) {
    return sequelize.define('attribute', {
      id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      label: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      type: {
        type: DataTypes.STRING(50),
        allowNull: true
      },
      is_filterable: {
        type: DataTypes.INTEGER(6).UNSIGNED,
        allowNull: false,
        defaultValue: 1
      },
      is_required: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        defaultValue: 0
      },
      status: {
        type: DataTypes.ENUM('active','inactive','archive'),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'attribute'
    });
  };
  