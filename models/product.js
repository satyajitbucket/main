module.exports = function(sequelize, DataTypes) {
  return sequelize.define('product', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    sku: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    short_description: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT(),
      allowNull: true
    },
    category_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true
    },
    sub_category_id: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    keyword: {
      type: DataTypes.TEXT(),
      allowNull: true
    },
    price: {
    type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00 
    },
    url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    spice_level: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00 
    },
    type: {
      type: DataTypes.ENUM('Veg','Nonveg'),
      allowNull: true
    },
    tax_class_id: {
      type: DataTypes.INTEGER(10),
      allowNull: true
    },
    special_price: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: 0.00
    },
    special_price_from: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    special_price_to: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    weight: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    inventory: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
      defaultValue: 0
    },    
    is_configurable: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      defaultValue: 0
    },
    status: {
      type: DataTypes.ENUM('active','inactive'),
      allowNull: true
    },
    image: {
      type: DataTypes.TEXT(),
      allowNull: true
    },
    meta_title: {
      type: DataTypes.TEXT(),
      allowNull: true
    },
    meta_key: {
      type: DataTypes.TEXT(),
      allowNull: true
    },
    meta_description: {
      type: DataTypes.TEXT(),
      allowNull: true
    },    
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    }
  }, {
    tableName: 'product'
  });
};  