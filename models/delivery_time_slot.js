/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('delivery_time_slot', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    from_time: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    to_time: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    sequence: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    createdBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM('active','inactive','archive'),
      allowNull: true
    }
  }, {
    tableName: 'delivery_time_slot'
  });
};
