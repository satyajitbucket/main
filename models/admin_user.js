module.exports = function(sequelize, DataTypes) {
    return sequelize.define('admin_user', {
      id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      email: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      username: {
        type: DataTypes.STRING(40),
        allowNull: true
      },
      password: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      role:{
          type: DataTypes.STRING(50),
          allowNull:false
      },
      status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
      },
      ip: {
        type: DataTypes.STRING(15),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'admin_user'
    });
  };
  