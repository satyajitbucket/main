module.exports = function(sequelize, DataTypes) {
  return sequelize.define('order', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    order_id: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    store_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      //allowNull: false
      allowNull: true
    },
    order_status: {
      type: DataTypes.ENUM('Processing','Shipped','Delivered','Canceled'),
      allowNull: true
    },
    shipping_method: {
      type: DataTypes.STRING(32),
      allowNull: true
    },
    shipping_description: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    payment_method: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    coupon_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    customer_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    salesman_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    discount_percent: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    discount_amount: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    base_grand_total: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    shipping_amount: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    cgst: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    sgst: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    igst: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    total_tax: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    grand_total: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    amount_paid: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    coupon_amount: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    wallet_amount: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    promotion: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    customer_name: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    customer_email: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    customer_mobile: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    street1: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    street2: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    city: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    state: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    pin: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    country: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    lat: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    long: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    shipping_address: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    gift_message: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    remote_ip: {
      type: DataTypes.STRING(32),
      allowNull: true
    },
    medium: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    delivery_time_slot: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    }
  }, {
    tableName: 'order'
  });
};
