module.exports = function(sequelize, DataTypes) {
    return sequelize.define('cart', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
      },
      customer_id: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: true,
      },
      product_id: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      item_quantity: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    }, {
      tableName: 'cart'
    });
  };
  