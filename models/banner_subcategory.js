module.exports = function(sequelize, DataTypes) {
    return sequelize.define('banner_subcategory', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
      },
      image: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      status:{
        type: DataTypes.ENUM('Active','Inactive'),
        allowNull:true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'banner_subcategory'
    });
  };
