module.exports = function(sequelize, DataTypes) {
    return sequelize.define('users_phone', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
      },
      user_id: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: true,
      },
      user_phone_no: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      is_prime_phone: {
        type: DataTypes.INTEGER(1),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    }, {
      tableName: 'users_phone'
    });
  };
  