
module.exports=function(sequelize,DataTypes){

    return sequelize.define('teams',{

        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
          },
          firstName:{
              type: DataTypes.STRING(100),
              allowNull: true
          },
          lastName:{
              type: DataTypes.STRING(100),
              allowNull:true
          },
         
          profilePicture:{
              type:DataTypes.STRING(300),
              allowNull:true
          },
          description: {
            type: DataTypes.STRING(1000),
            allowNull: true
          }

        },{
            tablename: 'teams'
        });

    };
