module.exports = function(sequelize, DataTypes) {
    return sequelize.define('order_status_history', {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      order_id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false
      },
      status: {
        type: DataTypes.STRING(32),
        allowNull: true
      },
      remarks: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      is_customer_notified: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: false
      },
      customer_sms: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'order_status_history'
    });
  };
  