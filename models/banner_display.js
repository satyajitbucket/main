module.exports = function(sequelize, DataTypes) {
    return sequelize.define('banner_display', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
      },
      title: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      sequence: {
        type: DataTypes.INTEGER(3).UNSIGNED,
        allowNull: true
      },
      status: {
        type: DataTypes.ENUM('active','inactive','archive'),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'banner_display'
    });
  };
  