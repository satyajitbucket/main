
var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var flash = require('connect-flash');

exports.signinview = function(req, res, next){
	res.render('login/index',{ csrfToken: req.csrfToken(), errors: req.flash('errors'), messages: req.flash('message')});
}

exports.signin = function(req, res, next) {

	req.checkBody('email', 'Email is Required').notEmpty();
	req.checkBody('password', 'Password Required').notEmpty();
	req.checkBody('email', 'Email is not Valid').isEmail();

	req.getValidationResult().then(function(result){ 
       if(!result.isEmpty()){
		   	var errors = result.array().map(function (elem) {
                return elem.msg;
           	});
			req.flash('errors',errors);
			return res.redirect('/superpos/admin#signin');
		}else{
			// existingEmail = models.admin_user.findOne({ where: {email:req.body.email} });
			// existingEmail.then(function (users) {
			existingEmail = models.users.findOne({ where: {email:req.body.email} });
			existingEmail.then(function (users) {				
				if(users!=null){
					user = users.toJSON();
					if(!bcrypt.compareSync(req.body.password, user.password)) {	
						req.flash('errors',"Password wrong");
						return res.redirect('/superpos/admin#signin');
					} else {					
						if (req.body.email == users.email) {						
							req.session.user=user;    
							var token = jwt.sign({users}, SECRET, { expiresIn: 18000 });
							req.session.token=token;
							// return res.send(token)						
							return res.redirect('/superpos/dashboard');		 
						}else{							
							req.flash('errors',"No user found");
							return res.redirect('/superpos/admin#signin');						
						}
					} 							
				}else{				
					req.flash('errors',"No user found");
					return res.redirect('/superpos/admin#signin');
				}			
			});
		}	
	});
};
   
exports.signup = function(req, res, next) {
	var user = req.body;
	var users = null;
	
	req.checkBody('firstName', 'First Name  Required').notEmpty();
	req.checkBody('lastName', 'Last Name  Required').notEmpty();
	req.checkBody('email', 'Email Id Required').isEmail();
	req.checkBody('phone', 'Phone No Required').notEmpty();
	req.checkBody('password', 'Password Required').notEmpty();


	req.getValidationResult().then(function(result){ 
		 console.log(result);
		
       if(!result.isEmpty()){
		   console.log(result);
		   var errors = result.array().map(function (elem) {
                return elem.msg;
            });
		   req.flash('errors',errors);
		   return res.redirect('/superpos/admin#signup');
		   
		}else{


				var password = user.password;
				var hash = bcrypt.hashSync(password);
				var duplicate = null;
				duplicate = models.admin_user.findOne({ where: {email:user.email} });
				duplicate.then(function (duplicate) {
				if(duplicate){
					//req.flash('errors','Email Id Already Exists'); 
					res.status(200).send({ message: "Email Id Already Exists" }); 
					return res.redirect('/superpos/admin#signup');
				}else{

					models.admin_user.create({ 
						firstName: user.firstName, 
						lastName: user.lastName,  
						email:user.email,
						password: hash,
						phone:user.phone,
						status:'inactive'
					}).then(function(admin_user) {	
						//req.flash('lmessage','User Successfully Created');  
						res.status(200).send({ message: "User Successfully Created" });					
							return res.redirect('/superpos/admin');
								
					
					})
					.catch(function(error) {
							return res.send(error);
					
					}); 
				}
			}); 	
		}
	
	});
	
};
