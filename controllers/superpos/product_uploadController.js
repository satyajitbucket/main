var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var fs = require('file-system');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const csv = require('fast-csv');

exports.product_upload = function(req, res, next){	
    return res.render('superpos/product_upload/addedit', {title: 'Product Upload',messages: req.flash('info'),errors: req.flash('errors'),arrData:'',});           
};
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//

///////////////////////////// product upload start /////////////////////////////////////
exports.addProduct_upload = function(req, res, next) {  

    var directory ='';
    var filePath ='';
    var formnew = new formidable.IncomingForm();
    formnew.parse(req);
    formnew.on('fileBegin', function (name, file) {
    console.log(file.name)
        if (file.name && file.name != '') {
            directory = __dirname + '/../../public/superpos/Product_upload/';
            filePath = __dirname + '/../../public/superpos/Product_upload/' + file.name;
            if (!fs.existsSync(directory)){
                // fs.mkdirSync(dir);
                fs.mkdirSync(directory, (err) => {
                if (err) throw err;
                });
                file.path = __dirname + '/../../public/superpos/Product_upload/' + file.name;
            }else{
                file.path = __dirname + '/../../public/superpos/Product_upload/' + file.name;
            }
        }
    });	

    formnew.on('end', function () {

        const fileRows = [];

        // open uploaded file
        csv.fromPath(filePath)
        .on("data", function (data) {
            fileRows.push(data); // push each row
        })
        .on("end", function () {
            console.log(fileRows);
            fs.unlinkSync(filePath);   // remove temp file

            const validationError = validateCsvData(fileRows);
            if (validationError) {
                req.flash('errors',validationError);                    
                //console.log(req.flash('info'));
                return res.redirect('back');     
                // return res.status(403).json({ error: validationError });
            }
            //else process "fileRows" and respond
            // return res.json({ message: "valid csv" });
            var i=1;
            fileRows.forEach(function(element,index){
                console.log(index)           
                console.log(element)
                console.log("//////////////////////////////////////////////////////");
                var str = element[0] ?element[0] : '';
                var productName= str.replace(" ", "").substr(0, 5).toUpperCase();
                var skuID = productName;
                //return res.send(skuID);
                if(index!=0){
                    models.product.create({
                        sku: skuID,
                        title: element[0] ?  element[0]: '',
                        short_description: element[1] ?  element[1]: '',
                        description: element[2] ?  element[2]: '',
                        category_id: element[3] ?  element[3]: '',
                        // sub_category_id: element[4] ?  element[4]: '',
                        keyword:element[4] ?  element[4]: '',
                        price: element[5] ?  element[5]: '',                  
                        type: element[6] ?  element[6]: '',
                        special_price: element[7] ?  element[7]: 0.00,
                        special_price_from:element[8] ? element[8]: '',
                        special_price_to:element[9] ? element[9]: '',
                        weight:element[10] ? element[10]: '',
                        inventory:element[11] ? element[11]: 0,
                        image:element[12] ? element[12]: '',
                        is_configurable:'0',
                        status: 'active'
                        
                    }).then(function(product) {
                        var newSkuID = skuID + product.id;
                        models.product.update({
                            sku:newSkuID ? newSkuID : null,
                        },{where:{id:product.id}})
                       
                    });
                }              
        
                i++;
                if(i== fileRows.length){
                    req.flash('info','CSV File, Successfully Uploaded');                    
                    //console.log(req.flash('info'));
                    return res.redirect('back');     
                }   
            },this);
        })
   
    });


};


function validateCsvData(rows) {

    var headerArray =  new Array('Product Name',  'Short Description',  'Description',  'Category Id', 'Keyword',  'Price', 'Type',
    'Special Price','Special Price From','Special Price To','Weight','Inventory','Image');
    // var headerArray =  new Array("name", "roll no", "dob");
    
    console.log(headerArray);    
    if(rows.length > 0){
        if(!compare(rows[0] ,headerArray ))        
        return `Header is not Present or correct. please look into sample csv`;    

        const dataRows = rows.slice(1, rows.length); //ignore header at 0 and get rest of the rows
        for (let i = 0; i < dataRows.length; i++) {
            
            const rowError = validateCsvRow(dataRows[i]);
            if (rowError) {
                return `${rowError} on row ${i + 1}`
            }          
           
        }
        return;
    }else{
        return `empty csv`
    }
}
    
function validateCsvRow(row) {
    // if (!row[0]) {
    //   return "invalid sku"
    // }
    if (!row[0]) {
        return "invalid Product Name"
    }
    if (!row[3]) {
        return "invalid Category Id"
    }
    else if (!Number.isInteger(Number(row[3]))) {
        console.log(row[4])
      return "invalid Category Id"
    }
    if(!row[5] || isNaN(row[5])){
        return "invalid Price"
    }
    // if(!row[9] || (row[7]!='Veg' && row[7] == 'Nonveg' )){
    //     return "invalid Type"
    // }
    // if(!row[16] || isNaN(row[16])){
    //     return "invalid is_configurable"
    // }
    // if(!row[17] || (row[13]!='active' && row[13] == 'inactive' )){
    //     return "invalid status"
    // }
    // else if (!moment(row[2], "YYYY-MM-DD").isValid()) {
    //   return "invalid date of birth"
    // }
    return;
}

function compare(arr1,arr2){ 
    if(!arr1  || !arr2) return        
    let result;    
    // console.log("++++++++++++++++++++++++++++++++++++");
    // console.log(typeof arr1)
    // console.log("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    // console.log(typeof arr2)
    if (JSON.stringify(arr1) === JSON.stringify(arr2)) {
        result = true;
    }else{
        result = false;
    }    
    return result  
}


/////////////////////////// product upload end ///////////////////////////////////////