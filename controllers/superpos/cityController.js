
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var flash = require('connect-flash');


exports.cityList = function(req, res, next){	
	var id = req.params.id;
	var cid = req.params.cid;
	//console.log(cid);	
	//return res.send(cid);
	var arrData = null;
	var countryId = null;	
	countryId = models.country.findOne({ where: {status:'active',id:id}});
	countryId.then(function (country) {	
		//console.log(country.id);	
		//return res.send(country);		
		existingItem = models.city.findAll({ where: {status: {$ne: 'archive'},countryId:id},order: [['name', 'ASC']] });  
		existingItem.then(function (value) {
			if(!cid){			
				//return res.send(value);			
				return res.render('superpos/city/list', {	title: 'City',arrCity:'',arrData: value,arrCountry:country,messages: req.flash('message'),	errors: req.flash('errors')	});			
			}else{
				existingCity = models.city.findOne({ where: {status: {$ne: 'archive'},countryId:id,id:cid} });  
				existingCity.then(function (city) {
					console.log(city);
					return res.render('/city/list', {	title: 'City',arrCity:city,arrData: value,arrCountry:country,messages: req.flash('message'),	errors: req.flash('errors')	});
				});	
			}	
		});
	});
}



// exports.addedit = function(req, res, next){
// 	var id = req.params.id;
// 	var existingItem = null;
// 	var countryId = null;
	
// 	countryId = models.country.findAll({ where: {status:'active'}});
// 	countryId.then(function (country) {
//     if(!id){	
// 	res.render('/city/addedit',{title:'City',arrData:'',arrCountry:country,messages: req.flash('message'),errors: req.flash('errors')});	
// 		}else{
// 			console.log(id);
// 	existingItem = models.city.findOne({ where: {id:id} });
// 	existingItem.then(function (value) {		
//     return res.render('superpos/city/addedit', {
//         title: 'City',
// 		arrData: value,
// 		arrCountry:country,
// 		messages: req.flash('message'),
// 		errors: req.flash('errors')
// 		});
// 	});
	
// }
// });	
// }


exports.add = function(req, res, next) {
	var data = req.body;
	var id = data.update_id;
	//req.checkBody('slag', 'Slag name required').notEmpty();
	// var errors = req.validationErrors(); 
	req.checkBody('name', 'Name Field Required').notEmpty();
	//req.checkBody('slag', 'Slag Name Required').notEmpty();
	
       
    req.getValidationResult().then(function(result){
        if(!result.isEmpty()){
		   var errors = result.array().map(function (elem) {
                return elem.msg;
            });
           req.flash('errors',errors);
		   res.redirect('back');
		}else{
			var duplicate = null;
			//duplicate = models.role.findOne({ where: {name:data.name} });
			duplicate = models.city.findAll({ where: {id: {$ne: id},name: data.name,countryId: data.countryId,status:{$ne:'archive'}} });
			duplicate.then(function (duplicate) {
			if(duplicate.length){
				req.flash('errors',' This Name Is Already Exists');  
				res.redirect('back');	
			}else{
				if(!id){					
					var existingItem=null;
					existingItem = models.city.findOne({ where: {slag:data.slag} });
					existingItem.then(function (value) {
						if(value){
							req.flash('errors','Slag is not unique.');
							return res.redirect('back');
						}else{
							var slug=slugify(data.name);	
							models.city.create({ 
								name: data.name, 
								slag:slug,
								countryId: data.countryId,
								code: data.code,
								sequence: data.sequence,
								status:data.status,
								createdBy:data.user_id,
								updatedBy:""
							}).then(function(city) {					
								req.flash('message','Successfully Created');  
								return res.redirect('/superpos/city/'+data.countryId);
							});
						}
					});	

				}else{	
					//console.log("update")			
					models.city.update({ 
						name: data.name,
						countryId: data.countryId, 
						code: data.code,
						sequence: data.sequence,
						status:data.status,
						updatedBy:data.user_id
						},{where:{id:id}
					}).then(function(city) {
						req.flash('message','Successfully Updated');
						return res.redirect('/superpos/city/'+data.countryId);
					});	
				
				
					}

				}
			}); 
		}	
	
	});
	
};

exports.deleteCity = function(req, res, next) {
	var data = req.body;
	//var id = req.params.id;
	var cid = req.params.cid;
	
	
				
			models.city.update({ 
				status:'archive',
				updatedBy:1
				 },{where:{id:cid}}).then(function(city) {
				
				  req.flash('message','Successfully Deleted');
           res.redirect('back');
				});	
	
};

function slugify(text) {
	 return text.toString().toLowerCase() .replace(/\s+/g, '-') 
	 // Replace spaces with - .replace(/[^\w\-]+/g, '')
	 // Remove all non-word chars .replace(/\-\-+/g, '-') // Replace multiple - with single - .replace(/^-+/, '') 
}

