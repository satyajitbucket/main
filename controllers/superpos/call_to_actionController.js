
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');


exports.call_to_actionList = function(req, res, next){
	var arrData = null;
	existingItem = models.call_to_action.findAll(); 
	existingItem.then(function (value) {
		existingModules = models.modules.findAll({where: {status:{$ne:'archive'}} });
		existingModules.then(function (modules) { 
			return res.render('superpos/call_to_action/list', {title: 'Call to Action',arrModules:modules,arrData: value,delete_message: req.flash('message')});
		});
	});
}

exports.addedit = function(req, res, next){
	var id = req.params.id;
	var existingItem = null; 
	existingModules = models.modules.findAll({ where: {status:{$ne:'archive'}},order: [['name', 'ASC']] });
	existingModules.then(function (modules) { 
		if(!id){	
			res.render('superpos/call_to_action/addedit',{title:'Add Call to Action',arrModules:modules,arrData:'',errors: req.flash('errors'),messages: req.flash('info')});	
		}else{
			//console.log(id);
			existingItem = models.call_to_action.findOne({ where: {id:id} });
			existingItem.then(function (value) {		
				return res.render('superpos/call_to_action/addedit', {title: 'Add Call to Action',arrModules:modules,arrData: value,errors: req.flash('errors'),messages: req.flash('info')});
			});
		}
	});	
}

exports.add = function(req, res, next) {
	var form = new multiparty.Form(); 
	var d = new Date();
	var n = d.getTime();
	var image=null;
	var modId='';
	form.parse(req, function(err, fields, files) {
		req.body=fields;
		//console.log(fields);
		var id =fields.updated_id[0];
		
		req.checkBody('title', 'Title Field Required').notEmpty();						
		req.checkBody('description', 'Description Field Required').notEmpty();
		req.checkBody('moduleId', 'Module Field Required').notEmpty();	
		
		req.getValidationResult().then(function(result){			
			if(!result.isEmpty()){
				var errors = result.array().map(function (elem) {									
					return elem.msg;
				});
				req.flash('errors',errors);
				res.redirect('back');
			}else{
				fields.moduleId.forEach(function(mod){
					modId=modId+mod+',';
				});
				modIdi=modId.lastIndexOf(",");				
				modId=modId.substr(0,modIdi);							
				//return res.send(fields);				
				if(files.image[0].originalFilename!=''){		
					 image=req.app.locals.baseurl+'superpos/images/'+n+files.image[0].originalFilename;
					var ext=image.split(".").reverse();
					//Checking File type 
					if(ext[0]!=='jpg' && ext[0]!=='png'){
						req.flash('errors','Please upload jpg,png file in Image field');
						return res.redirect('back');
					}
				}else{
					 image=fields.update_image[0];
				}
					if(files.backgroungImage[0].originalFilename!=''){		
					 backgroungImage=req.app.locals.baseurl+'superpos/images/'+n+files.backgroungImage[0].originalFilename;
					var ext=backgroungImage.split(".").reverse();
					//Checking File type 
					if(ext[0]!=='jpg' && ext[0]!=='png'){
						req.flash('errors','Please upload jpg,png file in Backgroung Image field');
						return res.redirect('back');
					}
				}else{
					 backgroungImage=fields.update_backgroungImage[0];
				}
				if(!id){
					if(files.image[0].originalFilename==''){
						req.flash('errors','Please upload a image file in Image field');
						return res.redirect('back');
					}
					models.call_to_action.create({ 
						title: fields.title[0] ? fields.title[0] :'',						
						description: fields.description[0] ? fields.description[0] :'',
						backgroungImage: backgroungImage,
						image: image,
						moduleId:modId
					}).then(function(classis) {
					req.flash('info','Insertion Successful');	  
					return res.redirect('addedit');				
					})
					.catch(function(error) {
						return res.send(error);		
					});
				}else{				
					models.call_to_action.update({ 
						title: fields.title[0] ? fields.title[0] :'',						
						description: fields.description[0] ? fields.description[0] :'',
						backgroungImage: backgroungImage,
						image: image,
						moduleId:modId},{where:{id:id}
					}).then(function(classis) {	
						req.flash('info','Updation Successful');	  
						return res.redirect('addedit/'+id);	
					})
					.catch(function(error) {
						return res.send(error);	
					});
				}					
			}	
		});		
	});	
	// uploading files into folder
	var formnew = new formidable.IncomingForm();
	formnew.parse(req);	
	formnew.on('fileBegin', function(field, file) {        
		// console.log("Image  is Here");
		// console.log(file);	
		//Checking File type 
		if(file.name!=''){			
			if(file.type=='image/jpeg' || file.type=='image/png'){
				file.path = __dirname + '/../../public/superpos/images/'+n+ file.name;	
			}else{
				req.flash('errors','Please upload jpg,png file in Image Field');
				return res.redirect('back');
			}	
		}	
	});	
};

exports.deletecall_to_action = function(req, res, next) {	
	var id = req.params.id;		
	//console.log(id);
	models.call_to_action.destroy({where:{id:id}
	}).then(function(call_to_action) {
		req.flash('message','Deleted Successfully');	  
		res.redirect('back');
	});	
};