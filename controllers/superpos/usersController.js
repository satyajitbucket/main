
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser')
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');




exports.usersList = function(req, res, next){
// 	var id = req.params.id;  
// 	var arrData = null;
// 		fetch(req.app.locals.apiurl+'users',{headers: {
// 		    "Content-Type": "application/json; charset=utf-8",
// 		    "token": req.session.token,
// 		}}) .then(function(response) { return response.json() })
// 		    .then(function(value){
// 		        return res.render('superpos/users/list', { title: 'Users',arrData:value.value,	message: req.flash('message')		
// 		});
// 	});
// };

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.users.findAndCountAll({where: {status: {$ne: 'archive'}},limit: limit, offset: offset});            
            existingItem.then(function (results) {
                const itemCount = results.count;
                const pageCount = Math.ceil(results.count / limit);
                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                console.log(startItemsNumber);
                console.log(endItemsNumber);

                // console.log(previousPageLink)
				    return res.render('superpos/users/list', { title: 'Users',arrData:results.value, messages: req.flash('info'),arrData:results.rows,errors:req.flash('errors'),	
                    pageCount,
                    itemCount,
                    currentPage: currPage,
                    previousPage : previousPageLink	,
                    startingNumber: startItemsNumber,
                    endingNumber: endItemsNumber,
                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                }); 
            })
        }	
	});
}


// exports.addeditUser = function(req, res, next){

// 	var id = req.params.id;  
// 	var arrData = null;
// 	var arrRoles = null;
// 		if(!id){
// 			fetch(req.app.locals.apiurl+'users/addedit',{headers: {
// 				"Content-Type": "application/json; charset=utf-8",
// 				"token": req.session.token,
// 			}}) .then(function(response) { return response.json() })
// 				.then(function(value){
// 					res.render('superpos/users/addedit',{title:'Add Users',arrData:'',arrPhone_number:'',arrEmail_id:'',arrAddress:'',arrRoles: value.roles,messages: req.flash('info'),errors: req.flash('errors')});
// 			});
// 		}else{
// 			fetch(req.app.locals.apiurl+'users/addedit/'+id,{headers: {
// 				"Content-Type": "application/json; charset=utf-8",
// 				"token": req.session.token,
// 			}}) .then(function(response) { return response.json() })
// 			.then(function(value){
// 				// console.log('value');
// 				// res.send(value.value);
// 				res.render('superpos/users/addedit',{title:'Edit Users',arrData: value.value,arrPhone_number: value.phone,arrEmail_id:value.email,arrAddress: value.address,arrRoles:value.roles,messages: req.flash('info'),errors: req.flash('errors')});
// 			});
// 		}
// };

exports.addeditUser = function(req, res, next){
	 
    var id = req.params.id;
    var existingItem = null;
    var rolesId = null;
    rolesId = models.role.findAll({ where: {status: 'active'},order: [['name', 'ASC']]} );
    rolesId.then(function (roles) {
        if(!id){	
            //res.status(200).send({ 
            res.render('superpos/users/addedit',{
                title:'Add Users',
                arrData:'',
                arrPhone_number:'',
                arrEmail_id:'',
                arrAddress:'',
                arrRoles: roles,
                messages: req.flash('info'),
                errors: req.flash('errors')
            });
        }else{
            existingItem = models.users.findOne({ where: {id:id} });
            existingItem.then(function (value) {
                existingPhone = models.users_phone.findAll({ where: {user_id:id} });
                existingPhone.then(function (users_phone) {
                    existingEmail = models.users_email.findAll({ where: {user_id:id} });
                    existingEmail.then(function (users_email) {
                        existingAddress = models.users_address.findAll({ where: {user_id:id} });
                        existingAddress.then(function (users_address) {		
                            // res.status(200).send({ 
                            res.render('superpos/users/addedit',{
                                title:'Edit Users',
                                arrData: value,
                                arrPhone_number: users_phone,
                                arrEmail_id:users_email,
                                arrAddress: users_address,
                                arrRoles:roles,
                                messages: req.flash('info'),
                                errors: req.flash('errors')
                            });
                        });    
                    });
                });        
            });
        }
    });
};

exports.usersUqEmail = function(req, res, next){
    console.log(req.body);
    var Email=req.body.email ? req.body.email:'';

    models.users.findAll({where:{email:Email} }).then(function(users){
        if (users.length==0) {
            res.status(200).send({data:{success:true,value:users},errNode:{errMsg:"",errCode:"0"}});
        }else{
            res.status(200).send({data:{success:false,value:users},errNode:{errMsg:"",errCode:"1"}}); 
        }
    })
}

exports.addUser = function(req, res, next) {

    //return res.send(req.body.fields);

    var d = new Date();
    var n = d.getTime();
    var logdetails = req.session.user 
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        var user_phoneArr=fields.user_phone_no;
        var user_emailArr=fields.user_email_id;
        var user_addressArr=fields.user_first_name;

        var image=null;

        if(files.image[0].originalFilename!=''){	
            image = req.app.locals.baseurl+'superpos/myimages/'+files.image[0].originalFilename;
        }else{
            image =fields.update_image[0];
        }	

        //var image =fields.uploadHiddenCropImage && fields.uploadHiddenCropImage[0] !='' ? fields.uploadHiddenCropImage[0] : fields.update_image[0];
        req.body = fields; 
        //return res.send(fields);
            if(!id){

                var password = fields.password[0];
                var hash = bcrypt.hashSync(password);
                models.users.create({ 
                    firstName: fields.firstName ? fields.firstName[0]:null, 
                    lastName: fields.lastName ? fields.lastName[0]:null,
                    email:fields.email ? fields.email[0]:null,
                    username:fields.email ? fields.email[0]:null,
                    password: hash,
                    phone:fields.phone ? fields.phone[0]:null,
					//store_id:fields.store_id[0]?fields.store_id[0]:null,
                    image:image?image:null,
                    //role:fields.role[0]?fields.role[0]:null,
                    //lat:fields.lat[0]?fields.lat[0]:null,
                    //long:fields.long[0]?fields.long[0]:null,
                    address:fields.address ? fields.address[0]:null,
                    postCode:fields.postCode ? fields.postCode[0]:null,
                    country:fields.country ? fields.country[0]:null,
                    location:fields.location ? fields.location[0]:null,
                    //description:fields.description[0]?fields.description[0]:null,
                    status:fields.status ? fields.status[0]:null,
                    createdBy : logdetails ? logdetails.id : '' 
                    }).then(function(users) {

                            
                        if(user_phoneArr){
                            var j=0;

                            var isprimephone= fields.isprimephone ? fields.isprimephone[0].split(",") : [];

                            user_phoneArr.forEach(function(users_phone) {				
                                models.users_phone.create({ 
                                user_id: users.id,
                                user_phone_no: fields.user_phone_no[j],
                                is_prime_phone: isprimephone[j],
                            });
                            j++;
                            }, this);
                        } 
    
                        if(user_emailArr){
                            var k=0;

                            var isprimeemail= fields.isprimeemail ? fields.isprimeemail[0].split(",") : [];

                            user_emailArr.forEach(function(users_email) {				
                                models.users_email.create({ 
                                user_id: users.id,
                                user_email_id: fields.user_email_id[k],
                                is_prime_email: isprimeemail[k],
                            });
                            k++;
                            }, this);
                        } 

                        if(user_addressArr){
                      
                            var i=0;
                            var isprimeaddress= fields.isprimeaddress ? fields.isprimeaddress[0].split(",") : [];
    
                            user_addressArr.forEach(function(element) {				
                                models.users_address.create({ 
                                user_id: users.id,
                                first_name: fields.user_first_name[i],
                                last_name: fields.user_last_name[i],
                                mobile: fields.user_mobile_no[i],
                                address: fields.user_address[i],
                                city: fields.user_city[i],
                                state: fields.user_state[i],
                                pin: fields.user_pin[i],
                                country: fields.user_country[i],
                                is_prime_address: isprimeaddress[i],
                            });
                            i++;
                            }, this);
                        } 

        
                    
                    res.redirect('/superpos/users');
                    req.flash('info','Successfully Created');	 
                    console.log(req.flash('info'));
                    //res.status(200).send({ status:200,users:users });
                    
                    })
                    .catch(function(error) {
                        return res.send(error);
            
                    });
            }else{
                //var password = fields.password[0];
                //var hash = bcrypt.hashSync(password);
                models.users.update({ 
                    firstName: fields.firstName ? fields.firstName[0]:null, 
                    lastName: fields.lastName ? fields.lastName[0]:null,
                    //email:fields.email ? fields.email[0]:null,
                    //username:fields.email ? fields.email[0]:null,
                    //password: hash,
                    //phone:fields.phone ? fields.phone[0]:null,
					//store_id:fields.store_id[0]?fields.store_id[0]:null,
                    image:image?image:null,
                    //role:fields.role[0]?fields.role[0]:null,
                    //lat:fields.lat[0]?fields.lat[0]:null,
                    //long:fields.long[0]?fields.long[0]:null,
                    address:fields.address ? fields.address[0]:null,
                    postCode:fields.postCode ? fields.postCode[0]:null,
                    country:fields.country ? fields.country[0]:null,
                    location:fields.location ? fields.location[0]:null,
                    //description:fields.description[0]?fields.description[0]:null,
                    status:fields.status ? fields.status[0]:null,
                    
                    updatedBy:logdetails ? logdetails.id : ''
                    },{where:{id:id}}).then(function(users) {

                        models.users_phone.destroy({where:{user_id: id}
                        });
                        if(user_phoneArr){
                            var j=0;

                            var isprimephone= fields.isprimephone ? fields.isprimephone[0].split(",") : [];

                            user_phoneArr.forEach(function(users_phone) {				
                                models.users_phone.create({ 
                                user_id: id,
                                user_phone_no: fields.user_phone_no[j],
                                is_prime_phone: isprimephone[j],
                            });
                            j++;
                            }, this);
                        } 
        
                        models.users_email.destroy({where:{user_id: id}
                        });
                        if(user_emailArr){
                            var k=0;

                            var isprimeemail= fields.isprimeemail ? fields.isprimeemail[0].split(",") : [];

                            user_emailArr.forEach(function(users_email) {				
                                models.users_email.create({ 
                                user_id: id,
                                user_email_id: fields.user_email_id[k],
                                is_prime_email: isprimeemail[k],
                            });
                            k++;
                            }, this);
                        } 

                        models.users_address.destroy({	where:{user_id: id}
                        });
                        if(user_addressArr){
                            var i=0;
        
                            var isprimeaddress= fields.isprimeaddress ? fields.isprimeaddress[0].split(",") : [];
        
                            user_addressArr.forEach(function(element) {				
                                models.users_address.create({ 
                                user_id: id,
                                first_name: fields.user_first_name[i],
                                last_name: fields.user_last_name[i],
                                mobile: fields.user_mobile_no[i],
                                address: fields.user_address[i],
                                city: fields.user_city[i],
                                state: fields.user_state[i],
                                pin: fields.user_pin[i],
                                country: fields.user_country[i],
                                is_prime_address: isprimeaddress[i],
                            });
                            i++;
                            }, this);
                        } 

                        req.flash('info','Successfully Updated');	  
                        return res.redirect('/superpos/users');
                        // req.flash('info','Successfully Updated');	
                        //console.log(req.flash('info'));
                    // res.status(200).send({ status:200,users:users });             

                    })
            .catch(function(error) {
                return res.send(error);
            });
        }
	});
};

exports.fileupload = function (req,res) {
	var formnew = new formidable.IncomingForm();
	formnew.parse(req);
	formnew.on('fileBegin', function (name, file) {
        console.log(file.name)
		if (file.name && file.name != '') {
			file.path = __dirname + '/../../public/superpos/myimages/' + file.name;
		}
	});	
};





// exports.deleteUsers = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'users/delete/'+id,{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })	
// }

exports.deleteUsers = function(req, res, next) {
    
    var id = req.params.id;
    models.users.update({ 
        status: 'archive',
    },{where:{id:id}
    }).then(function(value) {
        //res.status(200).send({ status:200,value:value });
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });	
};