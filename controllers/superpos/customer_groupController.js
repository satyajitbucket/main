var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');


exports.customer_groupList = function(req, res, next){
	  
    var arrData = null;
    fetch(req.app.locals.apiurl+'customer_group',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
        
    }}) .then(function(response) { return response.json() })
    .then(function(value){
		return res.render('superpos/customer_group/list', { title: 'Customer Group',arrData: value.value,arrOption:'',message:'',errors:''		
		});
	});
}


exports.addeditCustomer_group = function(req, res, next){
	var id = req.params.id;  
    var arrData = null;
    
    if(!id){
        fetch(req.app.locals.apiurl+'customer_group/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){

            return res.render('superpos/customer_group/addedit', {title: 'Add Customer Group',messages:'',arrData:'',errors:''});
        });
    }else{
        fetch(req.app.locals.apiurl+'customer_group/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/customer_group/addedit', {title: 'Edit Customer Group',messages:'',arrData: value.value,errors:''});
        });
    }
    
};

exports.deleteCustomer_group = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'customer_group/delete/'+id,{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })
		
};


