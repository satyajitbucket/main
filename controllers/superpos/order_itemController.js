var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');


exports.order_itemList = function(req, res, next){
	  
    var arrData = null;
    fetch(req.app.locals.apiurl+'order_item',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
        
    }}) .then(function(response) { return response.json() })
    .then(function(data){
		return res.render('superpos/order_item/list', { title: 'Order Item',arrData:data,arrOption:'',message:'',errors:'',arrOrder:''		
		});
	});
}







