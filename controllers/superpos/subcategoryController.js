var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
const Excel = require('exceljs');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');
var config = require('../../config/config.json');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});


exports.addeditSubcategory = function(req, res, next){
	var id = req.params.id;  
    var arrData = null;
    var arrStore = null;
	var arrCategory = null;
    if(!id){
        fetch(req.app.locals.apiurl+'subcategory/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){

            return res.render('superpos/subcategory/addedit', {title: 'Add Subcategory ',messages:'',arrData:'',arrStore: value.arrstores,arrCategory: value.arrcategory,errors:''});
        });
    }else{
        fetch(req.app.locals.apiurl+'subcategory/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/subcategory/addedit', {title: 'Edit Subcategory ',messages:'',arrData: value.value,arrStore: value.arrstores,arrCategory: value.arrcategory,errors:''});
        });
    }
    
};

exports.subcategoryList =async function(req, res, next){
	  
    var currPage = req.query.page ? req.query.page : 0;
    var limit = req.query.limit ? req.query.limit : 10;
    var offset = currPage!=0 ? (currPage * limit) - limit : 0;
    var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            sequelize.query("SELECT COUNT(*) AS subCategoryCount FROM `subcategory` where status = 'active' ",{ type: Sequelize.QueryTypes.SELECT })
            .then(function (subCategoryCount) {
                if(subCategoryCount ){
                    sequelize.query("select a.title, a.store_id, a.status, a.icon, a.id, b.title 'category' from subcategory as a left join category as b on b.id=a.category where a.status = 'active' order by a.id DESC limit "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT })
                    .then(function (value) {
                        const itemCount = subCategoryCount.length > 0 ? subCategoryCount[0].subCategoryCount : 0;
                        const pageCount =  subCategoryCount.length > 0 ? Math.ceil(subCategoryCount[0].subCategoryCount / limit) : 1;
                        const previousPageLink = paginate.hasNextPages(req)(pageCount);
                        const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                        const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                        return res.render('superpos/subcategory/list', { title: 'Sub Category',arrData: value,arrOption:'',messages:req.flash('info'),errors:req.flash('errors'),		
                            pageCount,
                            itemCount,
                            currentPage: currPage,
                            previousPage : previousPageLink	,
                            startingNumber: startItemsNumber,
                            endingNumber: endItemsNumber,
                            pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                        });
                    }); 
                }else{
                    return res.render('superpos/subcategory/list', { title: 'Sub Category',arrData:'',arrOption:'',message:'',errors:'',		
                        pageCount:0,
                        itemCount:0,
                        currentPage: currPage,
                        previousPage : previousPageLink	,
                        startingNumber: startItemsNumber,
                        endingNumber: endItemsNumber,
                        limit : limit ,
                        pages: paginate.getArrayPages(req)(limit, pageCount, currPage,keyword)	
                    });
                } 
            });  
        }	
    });


    // var currPage = req.query.page ? req.query.page : 0;
    // var limit = req.query.limit ? req.query.limit : 10;
    // var offset = currPage!=0 ? (currPage * limit) - limit : 0;
    // var token= req.session.token;
    // jwt.verify(token, SECRET, function(err, decoded) {
    //     if (err) {
    //         res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
    //     }else{
    //         existingItem = models.subcategory.findAndCountAll({where: {status:{$ne: 'archive'}},limit: limit, offset: offset});            
    //         existingItem.then(function (results) {
    //             //return res.send(results)
    //             existingItem = models.stores.findAll();
    //             existingItem.then(function (stores) {
    //                 const itemCount = results.count;
    //                 const pageCount = Math.ceil(results.count / limit);
    //                 const previousPageLink = paginate.hasNextPages(req)(pageCount);
    //                 const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
    //                 const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
    //                 // console.log(startItemsNumber);
    //                 // console.log(endItemsNumber);
    //                 // console.log(previousPageLink)
    //                 return res.render('superpos/subcategory/list', { title: 'Sub Category',arrData: results,arrOption:'',arrData:results.rows,messages:req.flash('info'),errors:req.flash('errors'),		
    //                     pageCount,
    //                     itemCount,
    //                     currentPage: currPage,
    //                     previousPage : previousPageLink	,
    //                     startingNumber: startItemsNumber,
    //                     endingNumber: endItemsNumber,
    //                     pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
    //                 }); 
    //             })
    //         });  
    //     }	
    // });

}

// exports.subcategoryList =async function(req, res, next){
	  
//     var arrData = null;
//     // fetch(req.app.locals.apiurl+'subcategory',{headers: {
//     //     "Content-Type": "application/json; charset=utf-8",
//     //     "token": req.session.token,
        
//     // }}) .then(function(response) { return response.json() })
//   var  list = await sequelize.query("select b.title 'category', a.title, a.store_id, a.status, a.icon, a.id 'id1' from subcategory as a left join category as b on b.id=a.category order by b.title",{ type: Sequelize.QueryTypes.SELECT });
//     // list.then(function(value){
// 		return res.render('superpos/subcategory/list', { title: 'Sub Category',arrData: list,arrOption:'',message:'',errors:''		
// 		// });
// 	});
// }


exports.deleteSubcategory = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'subcategory/delete/'+id,{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })
		
};
exports.downloadSubCategoryList = async function (req, res, next) {
    var workbook = new Excel.Workbook();

    workbook.creator = 'Me';
    workbook.lastModifiedBy = 'Her';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;

    workbook.views = [
        {
            x: 0, y: 0, width: 10000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ];
    var worksheet = workbook.addWorksheet('My Sheet');
    worksheet.columns = [
        { header: 'Sl. No.', key: 'Slno', width: 10 },
        { header: 'Category Name ', key: 'Catagoryname', width: 30 },
        { header: 'Sub Category Name ', key: 'Subcatagoryname', width: 25 },
        { header: 'Number Of Product ', key: 'product', width: 25 }
       
           ];

    var list = await sequelize.query("select b.title 'category_title', a.title 'sub_category_title', (select count(*) from product where sub_category_id =a.id) 'total_products' from subcategory as a left join category as b on b.id=a.category order by b.title",{ type: Sequelize.QueryTypes.SELECT });
    
   var prv_catg_title = '';
   for (var i = 0; i < list.length; i++) {

        worksheet.addRow({ Slno: i+1, Catagoryname: (list[i].category_title != prv_catg_title ? list[i].category_title : ''), Subcatagoryname:list[i].sub_category_title, product:list[i].total_products  });
        prv_catg_title = list[i].category_title;
    }

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader("Content-Disposition", "attachment; filename=" + "Subcategory-list.xlsx");
    workbook.xlsx.write(res)
        .then(function (data) {
            res.end();
            console.log('File write done........');
        });
};


