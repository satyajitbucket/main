
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');


exports.notification_transList = function(req, res, next){
	  
    var arrData = null;
    fetch(req.app.locals.apiurl+'notification_trans',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
    }}) .then(function(response) { return response.json() })
    .then(function(value){
		return res.render('superpos/notification_trans/list', { title: 'Notification Transaction',arrData: value.value,arrOption:'',message: req.flash('message'),errors:''		
		});
	});
}

exports.addeditNotification_trans = function(req, res, next){
    
    var id = req.params.id;
    var arrData = null;
    var arrOption = null;
    var arrNotification;
    if(!id){
        fetch(req.app.locals.apiurl+'notification_trans/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/notification_trans/addedit', {title: 'Add Notification Transaction',arrData:'',arrOption:'',messages: req.flash('info'),arrNotification: value.notification,errors:''});
        });
    }else{            
        fetch(req.app.locals.apiurl+'notification_trans/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            //res.send(value);	
            return res.render('superpos/notification_trans/addedit', {title: 'Edit Notification Transaction',arrData: value.value,arrOption: value.att_value,messages: req.flash('info'),arrNotification: value.notification,errors:''});

            });
    }	
};

exports.deleteNotification_trans = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'notification_trans/delete/'+id,{headers: {
      "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })	
}

  
