
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');


exports.dropdown_settingsList = function(req, res, next){
	  
//     var arrData = null;
//     fetch(req.app.locals.apiurl+'dropdown_settings',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
//     }}) .then(function(response) { return response.json() })
//     .then(function(value){
// 		return res.render('superpos/dropdown_settings/list', { title: 'Dropdown Settings',arrData: value.value,arrOption:'',message: req.flash('message'),errors:''		
// 		});
// 	});
// }

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.dropdown_settings.findAndCountAll({where: {status:'active'},limit: limit, offset: offset});            
            existingItem.then(function (results) {
                const itemCount = results.count;
                const pageCount = Math.ceil(results.count / limit);
                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                console.log(startItemsNumber);
                console.log(endItemsNumber);

                // console.log(previousPageLink)
                    return res.render('superpos/dropdown_settings/list', { title: 'Dropdown Settings',arrData: results.value,arrOption:'',arrData:results.rows,messages: req.flash('info'),errors:req.flash('errors'),	
                    pageCount,
                    itemCount,
                    currentPage: currPage,
                    previousPage : previousPageLink	,
                    startingNumber: startItemsNumber,
                    endingNumber: endItemsNumber,
                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                }); 
            })
        }	
    });
}


// exports.addeditDropdown_settings = function(req, res, next){
    
//     var id = req.params.id;
//     var arrData = null;
//     var arrOption = null;
//     if(!id){
//         fetch(req.app.locals.apiurl+'dropdown_settings/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/dropdown_settings/addedit', {title: ' Add Dropdown Settings',arrData:'',arrOption:'',messages: req.flash('info'),errors:''});
//         });
//     }else{            
//         fetch(req.app.locals.apiurl+'dropdown_settings/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             //res.send(value.att_value);	
//             return res.render('superpos/dropdown_settings/addedit', {title: ' Edit Dropdown Settings',arrData: value.value,arrOption: value.opt_value,messages: req.flash('info'),errors:''});

//             });
//     }	
// };

exports.addeditDropdown_settings = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    if(!id){	
        // res.status(200).send({ 
        return res.render('superpos/dropdown_settings/addedit', {
            title: ' Add Dropdown Settings',
            arrData:'',
            arrOption:'',
            messages: req.flash('info'),
            errors:req.flash('errors'),
            //status:200
        });
    }else{            
        existingItem = models.dropdown_settings.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
            existingItem = models.dropdown_settings_option.findAll({ where: {dropdown_id:id} });
			existingItem.then(function (opt_value) {	
            //    res.status(200).send({ 
                return res.render('superpos/dropdown_settings/addedit', {
                    title: ' Edit Dropdown Settings',
                    arrData: value,
                    arrOption: opt_value,
                    messages: req.flash('info'),
                    errors:req.flash('errors'),
                   //status:200,
                   //value: value,
                   //opt_value: opt_value
                });
            })
        });	
    }	
};

exports.addDropdown_settings = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        var dropdown_settingsArr=fields.option_value;	
        if(!id){
            models.dropdown_settings.create({ 
                name: fields.name?fields.name[0]:null, 
                identifier: fields.identifier?fields.identifier[0]:null,
                status:fields.status?fields.status[0]:null,
                }).then(function(dropdown_settings) {  
                    if(dropdown_settingsArr){
                        var i=0;
                        dropdown_settingsArr.forEach(function(element) {				
                            models.dropdown_settings_option.create({ 
                            dropdown_id: dropdown_settings.id,
                            option_value: fields.option_value[i],
                            option_label: fields.option_label[i],
                            option_order: fields.option_order[i],
                        });
                        i++;
                        }, this);
                    }  
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/dropdown_settings');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.dropdown_settings.update({ 
                name: fields.name?fields.name[0]:null,
                status:fields.status?fields.status[0]:null,
            },{where:{id:id}}).then(function(dropdown_settings) {

                models.dropdown_settings_option.destroy({	where:{dropdown_id: id}
				});
                if(dropdown_settingsArr){
                    var i=0;
                    dropdown_settingsArr.forEach(function(element) {				
                        models.dropdown_settings_option.create({ 
                        dropdown_id: id,
                        option_value: fields.option_value[i],
                        option_label: fields.option_label[i],
                        option_order: fields.option_order[i],
                    });
                    i++;
                    }, this);
                }  

                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/dropdown_settings');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

// exports.deleteDropdown_settings = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'dropdown_settings/delete/'+id,{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })	
// }

exports.deleteDropdown_settings = function(req, res, next) {

    var id = req.params.id;
    models.dropdown_settings.update({ 
        status:'archive'
            },{where:{id:id}}).then(function(dropdown_settings) {
        models.dropdown_settings_option.destroy({ where: {dropdown_id:id} 
        }).then(function(opt_value){
            //res.status(200).send({ status:200,opt_value:opt_value });
            req.flash('info','Successfully Deleted');
            res.redirect('back');
        })
    });	
};
  