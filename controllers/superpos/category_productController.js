var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');



exports.add = function(req, res, next) {
    //return res.send(req.body);
    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        // return res.send(id);
        
        
        if(!id){
                
                models.category_product.create({ 
                    category_id: fields.category_id ? fields.category_id[0]:null, 
                    product_id: fields.product_id ? fields.product_id[0]:null,
                    position: fields.position[0] ? fields.position[0]:null,			
                    },{where:{id:id}}).then(function(category_product) {

                        return res.redirect('back');
                    
                })
                .catch(function(error) {
                    return res.send(error);
        
                });
            }
        
        else{
        
            models.category_product.update({ 
                category_id: fields.category_id ? fields.category_id[0]:null, 
                product_id: fields.product_id ? fields.product_id[0]:null,
                position: fields.position[0] ? fields.position[0]:null,			
                },{where:{id:id}}).then(function(category_product) {
 
                    return res.redirect('/superpos/category_product');
                })
                .catch(function(error) {
                    return res.send(error);

            });
        }
    
    
    });
    
};
    




exports.addedit = function(req, res, next){
   
    var id = req.params.id;
    var existingItem = null;
    var existingLocation=null;
    existingLocation = models.category.findAll();
    existingLocation.then(function (category) {	
        if(!id){	
            return res.render('superpos/category_product/addedit', {title: 'Category product',messages:'',arrData:'',errors:'',arrCategoryData:category});
        }else{            
            console.log(id);
            existingItem = models.category_product.findOne({ where: {id:id} });
            existingItem.then(function (value) {		
                return res.render('superpos/category_product/addedit', {title: 'Category product',messages:'',arrData:value,errors:'',arrCategoryData:category});
            });	
        }	
    });
}





exports.category_productList = function(req, res, next){
	var arrData = null;
    fetch(req.app.locals.apiurl+'category_product',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
    }}) .then(function(response) { return response.json() })
    .then(function(data){
		return res.render('superpos/category_product/list', { title: 'Category product',arrData:data,arrOption:'',message:'',errors:''		
		});
	});
}




exports.deleteCategory_Product = function(req, res, next) {		
	var id = req.params.id;
	//console.log(req.body);
	models.category_product.destroy({
        where:{id:id}
	}).then(function(category_product) {
        //req.flash('message','Deletion Successful');
        
		res.redirect('back');
	});
};



exports.addedit = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    var existingPro = null;
    existingPro = models.product.findAll();
    existingPro.then(function (product) {
    if(!id){	
            
        return res.render('superpos/category/addedit', {title: 'Category',messages:'',arrData:'',arrOption:'',errors:'',arrProductData:product});
    }else{            
        existingItem = models.category.findOne({ where: {id:id} });
        existingItem.then(function (value) {
            var resultArray = [];
            //var arrCat = value.product.split(',');
            var arrCat = value.products.split(',');
            //console.log('-----------------------------------------');
            //console.log(arrCat);
            product.forEach(i=>{
                var isSelected = 0;
                if(arrCat.includes(i.id)){
                     isSelected = 1;
                }
                else{
                    isSelected = 0;
                }
                resultArray.push({
                "sku":i.sku,
                "title":i.title,
                "short_description":i.short_description,
                "description":i.description,
                "categories":i.cats,
                "price":i.price,
                "url":i.url,
                "spice_level":i.spice_level,
                "type":i.type,
                "tax_class_id":i.tax_class_id,
                "special_price":i.special_price,
                "special_price_from":i.special_price_from,
                "special_price_to":i.special_price_to,
                "weight":i.weight,
                "inventory":i.inventory,
                "status":i.status,
                "image":i.image,
                "meta_title":i.meta_title,
                "meta_key":i.meta_key,
                "meta_description":i.meta_description,
                "isSelected":isSelected
                });
            });
            
            return res.render('superpos/category/addedit', {title: 'Category',messages:'',arrData:value,arrOption:'',errors:'',arrProductData:resultArray});

        });
    }
});	
}
