var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');


// exports.addeditBanner = function(req, res, next){
// 	var id = req.params.id;  
//     var arrData = null;
//     var arrProduct = null;
//     if(!id){
//         fetch(req.app.locals.apiurl+'banner/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){

//             return res.render('superpos/banner/addedit', {title: 'Add Banner',messages:'',arrData:'',arrBannerDisplay: value.arrbanner_display,arrBannerSection: value.arrbanner_section, arrStores: value.arrstore, arrCategory: value.arrcategory,errors:''});
//         });
//     }else{
//         fetch(req.app.locals.apiurl+'banner/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/banner/addedit', {title: 'Edit Banner',messages:'',arrData: value.value,arrBannerDisplay: value.arrbanner_display,arrBannerSection: value.arrbanner_section, arrStores: value.arrstore, arrCategory: value.arrcategory,errors:''});
//         });
//     }
    
// };

exports.addeditBanner = function(req, res, next){
    
    var id = req.params.id;
	category = models.banner_section.findAll({ where: {status:'active'} });
	category.then(function (banner_section) {
        category = models.banner_display.findAll({ where: {status:'active'} });
	    category.then(function (banner_display) {
            category = models.stores.findAll({ where: {status:'active'} });
	        category.then(function (stores) {
                category = models.category.findAll({ where: {status:'active'} });
                category.then(function (category) {	
                    var existingItem = null;
                    if(!id){	
                        //res.status(200).send({ 
                        return res.render('superpos/banner/addedit',{
                            title: 'Add Banner',
                            messages:req.flash('info'),
                            arrData:'',
                            arrBannerDisplay: banner_display,
                            arrBannerSection: banner_section,
                            arrStores: stores,
                            arrCategory: category,
                            errors:req.flash('errors'),
                            //status:200,
                            //arrbanner_section: banner_section,
                            //arrbanner_display: banner_display,
                            //arrstore: stores,
                            //arrcategory: category
                        });
                    }else{            
                        existingItem = models.banner.findOne({ where: {id:id} });
                        existingItem.then(function (value) {	
                           // res.status(200).send({ 
                            return res.render('superpos/banner/addedit',{
                                title: 'Edit Banner',
                                arrData: value,
                                messages:req.flash('info'),
                                arrData:value,
                                arrBannerDisplay: banner_display,
                                arrBannerSection: banner_section,
                                arrStores: stores,
                                arrCategory: category,
                                errors:req.flash('errors'),
                                //status:200,
                                //value: value,
                                //arrbanner_section: banner_section,
                                //arrbanner_display: banner_display,
                                //arrstore: stores,
                                //arrcategory: category 
                            });
                        });	
                    }
                });
            });    
        });    
    });		
};

exports.addBanner = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var logdetails = req.session.user 
        var id = fields.update_id[0];

        var image=null;

        if(files.image[0].originalFilename!=''){	
            image = req.app.locals.baseurl+'superpos/myimages/'+files.image[0].originalFilename;
        }else{
            image =fields.update_image[0];
        }	


        // if(fields.uploadHiddenCropImage && fields.uploadHiddenCropImage[0]!=''){
        //     image = req.app.locals.baseurl+'superpos/myimages/'+fields.uploadHiddenCropImage[0];
        // }else{
        //     image =fields.update_image[0];
        // }

        var categoryArr=fields.optionValue;	
        if(!id){
            models.banner.create({ 
                title: fields.title ? fields.title[0] : null,
                category_id: fields.category_id ? fields.category_id[0] : null,
                store_id: fields.store_id ? fields.store_id[0] : null,
                short_description: fields.short_description ? fields.short_description[0]:null,
                type:fields.type ? fields.type[0] : null,
                image:image?image:null, 
                url: fields.url ? fields.url[0] : null,
                section: fields.section ? fields.section[0] : null,
                display_type: fields.display_type ? fields.display_type[0] : null,
                row: fields.row ? fields.row[0] : null,
                sequence: fields.sequence ? fields.sequence[0] : null,
                createdBy: logdetails ? logdetails.id : null,
                status:fields.status ? fields.status[0] : null,
				
                }).then(function(banner) {
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/banner');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.banner.update({ 
                title: fields.title ? fields.title[0] : null,
                category_id: fields.category_id ? fields.category_id[0] : null,
                store_id: fields.store_id ? fields.store_id[0] : null,
                short_description: fields.short_description ? fields.short_description[0]:null,
                type:fields.type ? fields.type[0] : null,
                image:image?image:null, 
                url: fields.url ? fields.url[0] : null,
                section: fields.section ? fields.section[0] : null,
                display_type: fields.display_type ? fields.display_type[0] : null,
                row: fields.row ? fields.row[0] : null,
                sequence: fields.sequence ? fields.sequence[0] : null,
                updatedBy: logdetails ? logdetails.id : null,
                status:fields.status ? fields.status[0] : null,
            },{where:{id:id}}).then(function(citi) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/banner');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

exports.fileupload = function (req,res) {
    //console.log(11151515151515145)
	var formnew = new formidable.IncomingForm();
	formnew.parse(req);
	formnew.on('fileBegin', function (name, file) {
        //console.log("llllllllllllllllllllllllllllllllll")
        console.log(file.name)
		if (file.name && file.name != '') {
			file.path = __dirname + '/../../public/superpos/myimages/' + file.name;
		}
	});	
};



exports.bannerList = function(req, res, next){

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.banner.findAndCountAll({limit: limit, offset: offset});            
            existingItem.then(function (results) {
                const itemCount = results.count;
                const pageCount = Math.ceil(results.count / limit);
                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                console.log(startItemsNumber);
                console.log(endItemsNumber);

                // console.log(previousPageLink)
                return res.render('superpos/banner/list', { title: 'Banner',arrData: results.value,arrOption:'',arrData:results.rows,messages:req.flash('info'),errors:req.flash('errors'),	
                    pageCount,
                    itemCount,
                    currentPage: currPage,
                    previousPage : previousPageLink	,
                    startingNumber: startItemsNumber,
                    endingNumber: endItemsNumber,
                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                }); 
            })
        }	
    });
}




exports.deleteBanner = function(req, res, next) {
    var id = req.params.id;
    console.log(id)
    models.banner.destroy({ 
        where:{id:id}
    }).then(function(value) {
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });	
	
};