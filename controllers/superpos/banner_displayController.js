var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');


exports.banner_displayList = function(req, res, next){
	  
//     var arrData = null;
//     fetch(req.app.locals.apiurl+'banner_display',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
        
//     }}) .then(function(response) { return response.json() })
//     .then(function(data){
// 		return res.render('superpos/banner_display/list', { title: 'Banner Display',message:req.flash('info'),arrData:data.value,arrOption:'',message:'',errors:''		
// 		});
// 	});
// }

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
jwt.verify(token, SECRET, function(err, decoded) {
    if (err) {
        res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
    }else{
        existingItem = models.banner_display.findAndCountAll({limit: limit, offset: offset});            
        existingItem.then(function (results) {
            const itemCount = results.count;
            const pageCount = Math.ceil(results.count / limit);
            const previousPageLink = paginate.hasNextPages(req)(pageCount);
            const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
            const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
            console.log(startItemsNumber);
            console.log(endItemsNumber);

            // console.log(previousPageLink)
            return res.render('superpos/banner_display/list', { title: 'Banner Display',messages:req.flash('info'),arrData:results.value,arrOption:'',arrData:results.rows,message:'',errors:'',
                pageCount,
                itemCount,
                currentPage: currPage,
                previousPage : previousPageLink	,
                startingNumber: startItemsNumber,
                endingNumber: endItemsNumber,
                pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
            }); 
        })
    }	
});
}


// exports.addeditBanner_display = function(req, res, next){
// 	var id = req.params.id;  
//     var arrData = null;
//     var arrProduct = null;
//     if(!id){
//         fetch(req.app.locals.apiurl+'banner_display/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){

//             return res.render('superpos/banner_display/addedit', {title: 'Add Banner Display',message:req.flash('info'),arrData:'',errors:''});
//         });
//     }else{
//         fetch(req.app.locals.apiurl+'banner_display/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/banner_display/addedit', {title: 'Edit Banner Display',message:req.flash('info'),arrData: value.value,errors:''});
//         });
//     }
    
// };

exports.addeditBanner_display = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    if(!id){	
        // res.status(200).send({
        return res.render('superpos/banner_display/addedit', { 
            title: 'Add Banner Display',
            messages:req.flash('info'),
            arrData:'',
            errors:'',
        });
    }else{            
        existingItem = models.banner_display.findOne({ where: {id:id} });
        existingItem.then(function (value) { 
            // return res.send(value);
            // res.status(200).send({ 
            return res.render('superpos/banner_display/addedit', {
                title: 'Edit Banner Display',
                messages:req.flash('info'),
                arrData: value,
                errors:'',
            });
        });
    }
};



exports.addBanner_display = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        var logdetails = req.session.user 

        if(!id)
        {
            models.banner_display.create({ 
                title: fields.title ? fields.title[0] : null, 
                sequence: fields.sequence ? fields.sequence[0] : null,
                status: fields.status ? fields.status[0] : null, 
                createdBy: logdetails ? logdetails.id : null,
            }).then(function(banner_display) {

                req.flash('info','Successfully Created');	  
                res.redirect('/superpos/banner_display');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }else{
            models.banner_display.update({ 
                title: fields.title ? fields.title[0] : null, 
                sequence: fields.sequence ? fields.sequence[0] : null,
                status: fields.status ? fields.status[0] : null, 
                createdBy: logdetails ? logdetails.id : null, 
            },{where:{id:id}}).then(function(banner_display) {
                req.flash('info','Successfully Updated');	  
                res.redirect('/superpos/banner_display');      
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

// exports.deleteBanner_display = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'banner_display/delete/'+id,{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })
		
// };

exports.deleteBanner_display = function(req, res, next) {
    
    var id = req.params.id;
    models.banner_display.destroy({ 
        where:{id:id}
    }).then(function(value) {
        //res.status(200).send({ status:200,value:value });
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });
};







