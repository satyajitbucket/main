var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');
var flash = require('connect-flash');
var config = require('../../config/config.json');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
    max: 5,
    min: 0,
    idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});

// exports.walletList = function(req, res, next){

//     fetch(req.app.locals.apiurl+'wallet',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,        
//     }}) .then(function(response) { return response.json() })
//     .then(function(data){
//         //return res.send(data);
// 		return res.render('superpos/wallet/list', { title: 'Wallet',arrData:data.value,message:'',errors:''		
// 		});
// 	});
// }

exports.walletList = function(req, res, next){
    var token= req.session.token;
    var currPage = req.query.page ? req.query.page : 0;
    var limit = req.query.limit ? req.query.limit : 10;
    var offset = currPage!=0 ? (currPage * limit) - limit : 0;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
                sequelize.query("SELECT COUNT(*) AS orderCount FROM `wallet`",{ type: Sequelize.QueryTypes.SELECT })
                .then(function (ordercount) {

                    if(ordercount ){

                        sequelize.query("SELECT `wallet`.*, CONCAT(`customer`.`first_name`,' ', `customer`.`first_name`) as `customerName`, `customer`.`phone` as `customerPhone` FROM `wallet` INNER JOIN `customer` ON `wallet`.`cid`= `customer`.`id` WHERE wallet.status='active' ORDER BY `wallet`.`id` DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT })
                        .then(function (value) {
                            const itemCount = ordercount.length > 0 ? ordercount[0].orderCount : 0;
                            const pageCount =  ordercount.length > 0 ? Math.ceil(ordercount[0].orderCount / limit) : 1;
                            const previousPageLink = paginate.hasNextPages(req)(pageCount);
                            const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                            const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                        
                            return res.render('superpos/wallet/list', { title: 'Wallet',arrData:value,messages:req.flash('info'),errors:req.flash('errors'),		
                                pageCount,
                                itemCount,
                                currentPage: currPage,
                                previousPage : previousPageLink	,
                                startingNumber: startItemsNumber,
                                endingNumber: endItemsNumber,
                                pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                            });
                            // console.log(value)
                            //res.status(200).send({ status:200,value:value });
                        });
                }else{
                        return res.render('superpos/wallet/list', { title: 'Wallet',arrData:'',messages:req.flash('info'),errors:req.flash('errors'),		
                            pageCount:0,
                            itemCount:0,
                            currentPage: currPage,
                            previousPage : previousPageLink	,
                            startingNumber: startItemsNumber,
                            endingNumber: endItemsNumber,
                            pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                        });
                }        
                
            });
        }	
    });
};

// exports.addeditwallet = function(req, res, next){    
//     var arrData = null;
//     var logdetails = req.session.user 
//     // if(!logdetails){
//     //     fetch(req.app.locals.apiurl+'wallet/addedit',{headers: {
//     //         "Content-Type": "application/json; charset=utf-8",
//     //         "token": req.session.token,
//     //     }}) .then(function(response) { return response.json() })
//     //     .then(function(value){
//     //         // return res.send(value);
//     //         return res.render('superpos/wallet/addedit', {title: 'Add Wallet',arrData:'', messages: req.flash('info'), arrCustomer: value.customers, errors:''});
//     //     });
//     // }else{            
//     //     fetch(req.app.locals.apiurl+'wallet/addedit/'+id,{headers: {
//     //         "Content-Type": "application/json; charset=utf-8",
//     //         "token": req.session.token,
//     //     }}) .then(function(response) { return response.json() })
//     //     .then(function(value){
//            // return res.send(value);
//             return res.render('superpos/wallet/addedit', {title: 'Edit Wallet',arrData:value.logdetails, messages: req.flash('info'), arrCustomer: value.customers, errors:''});
//     //     });
//     // }	
// };


// exports.addeditwallet = function(req, res, next){ 
//     var arrCustomer = null;
//     var logdetails = req.session.user;
//     fetch(req.app.locals.apiurl+'wallet/addedit/'+logdetails.id,{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
//     }}) .then(function(response) { return response.json() })
//         .then(function(value){
//         // console.log('value');
//         res.send(value);
//         res.render('superpos/wallet/addedit',{title: 'Wallet',arrData:value.value, messages: req.flash('info'), arrCustomer: value.customers, errors:''});
    
//     });
// };

exports.addeditwallet = function(req, res, next){    
    var id = req.params.id;
    var logdetails = req.session.user
    existingCus = models.customer.findAll({ where: {status: 'active'} });
    existingCus.then(function (customers) { 
        // var id = fields.update_id[0];  
        //res.send(customers);
            console.log(customers)                
        //if(!logdetails.id){
            if(!id){	
            res.render('superpos/wallet/addedit',{
                title: 'Wallet',
                arrData:'', 
                messages: req.flash('info'), 
                arrCustomer: customers, 
                errors:''
                //status:200, 
                //value: '', 
                //customers: customers 
            });
        }else{            
                            
            existingItem = models.wallet.findOne({ where: {id:id} });
            existingItem.then(function (value) {                    
                //res.status(200).send({ 
                res.render('superpos/wallet/addedit',{
                    title: 'Wallet',
                    arrData:value, 
                    messages: req.flash('info'), 
                    arrCustomer: customers, 
                    errors:''
                    //status:200, 
                    //value: value, 
                    //customers: customers 
                });
            })
            .catch(function(error) {
                return res.send(error);
            });
            
        }
    })
    .catch(function(error) {
        return res.send(error);
    });
};


exports.addWallet = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    var logdetails = req.session.user 
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        if(fields.amount_type == 'debit'){
            var  amount = -Math.abs(fields.amount)
        }else{
            var  amount = fields.amount
        }
        if(!id)
        {
            models.wallet.create({ 
                cid: fields.cid ? fields.cid[0] : null,
                amount_type: fields.amount_type ? fields.amount_type[0] : null,
                amount: amount,
                status: fields.status ? fields.status[0] : null,
                createdBy : logdetails ? logdetails.id : '' 
            }).then(function(delivery_time_slot) {
                req.flash('info','Successfully Created');	  
                res.redirect('/superpos/wallet');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }else{
            models.wallet.update({ 
                cid: fields.cid ? fields.cid[0] : null,
                amount_type: fields.amount_type ? fields.amount_type[0] : null,
                amount: amount,
                status: fields.status ? fields.status[0] : null,
                updatedBy : logdetails ? logdetails.id : '' 
            },{where:{id:id}}).then(function(delivery_time_slot) {
                req.flash('info','Successfully Updated');	  
                res.redirect('/superpos/wallet');      
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

exports.deleteWallet = function(req, res, next) {    
    var id = req.params.id;
       
    models.wallet.update({ 
        status: 'archive',
    },{where:{id:id}
    }).then(function(value) {
        //res.status(200).send({ status:200,value:value });
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });	
};