var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');


exports.salesmanList = function(req, res, next){
    var currPage = req.query.page ? req.query.page : 0;
    var limit = req.query.limit ? req.query.limit : 10;
    var offset = currPage!=0 ? (currPage * limit) - limit : 0;
    var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.salesman.findAndCountAll({limit: limit, offset: offset,order: [['id', 'DESC']]});            
            existingItem.then(function (results) {
                const itemCount = results.count;
                const pageCount = Math.ceil(results.count / limit);
                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                // console.log(startItemsNumber);
                // console.log(endItemsNumber);
                // console.log(previousPageLink)
                return res.render('superpos/salesman/list', { title: 'Salesman',arrData:results.rows,arrOption:'',messages:req.flash('info'),errors: req.flash('errors'),
                    pageCount,
                    itemCount,
                    currentPage: currPage,
                    previousPage : previousPageLink	,
                    startingNumber: startItemsNumber,
                    endingNumber: endItemsNumber,
                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                }); 
            })
        }	
    });
    // fetch(req.app.locals.apiurl+'salesman',{headers: {
    //     "Content-Type": "application/json; charset=utf-8",
    //     "token": req.session.token,        
    // }}) .then(function(response) { return response.json() })
    // .then(function(data){
	// 	return res.render('superpos/salesman/list', { title: 'Salesman',message:req.flash('info'),arrData:data.value,arrOption:'',message:'',errors:''		
	// 	});
	// });
}

// exports.addeditSalesman = function(req, res, next){
// 	var id = req.params.id;  
//     var arrData = null;
//     var arrProduct = null;
//     if(!id){
//         fetch(req.app.locals.apiurl+'salesman/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){

//             return res.render('superpos/salesman/addedit', {title: 'Add Salesman',messages:req.flash('info'),arrData:'',errors:''});
//         });
//     }else{
//         fetch(req.app.locals.apiurl+'salesman/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/salesman/addedit', {title: 'Edit Salesman',message:req.flash('info'),arrData: value.value,errors:''});
//         });
//     }
    
// };

exports.addeditSalesman = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    if(!id){	
        //res.status(200).send({ 
        return res.render('superpos/salesman/addedit', {
            title: 'Add Salesman',
            messages:req.flash('info'),
            arrData:'',
            errors:'',
            //status:200
        });
    }else{            
        existingItem = models.salesman.findOne({ where: {id:id} });
        existingItem.then(function (value) { 

            // res.status(200).send({ 
            return res.render('superpos/salesman/addedit', {
                title: 'Edit Salesman',
                messages:req.flash('info'),
                arrData: value,
                errors:'',
                //status:200,
                //value: value
            });
        });
    }
};

exports.addSalesman = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        var logdetails = req.session.user 

        if(!id)
        {
            models.salesman.create({ 
                name: fields.name ? fields.name[0] : null, 
                email: fields.email ? fields.email[0] : null,
                phone: fields.phone ? fields.phone[0] : null,
                status: fields.status ? fields.status[0] : null, 
                createdBy: logdetails ? logdetails.id : null,
            }).then(function(salesman) {

                req.flash('info','Successfully Created');	  
                res.redirect('/superpos/salesman');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }else{
            models.salesman.update({ 
                name: fields.name ? fields.name[0] : null, 
                email: fields.email ? fields.email[0] : null,
                phone: fields.phone ? fields.phone[0] : null,
                status: fields.status ? fields.status[0] : null, 
                createdBy: logdetails ? logdetails.id : null, 
            },{where:{id:id}}).then(function(salesman) {
                req.flash('info','Successfully Updated');	  
                res.redirect('/superpos/salesman');      
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};


// exports.deleteSalesman = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'salesman/delete/'+id,{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })
		
// };

exports.deleteSalesman = function(req, res, next) {
    
    var id = req.params.id;
       
    models.salesman.destroy({ 
        where:{id:id}
    }).then(function(value) {
        //res.status(200).send({ status:200,value:value });
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });
};







