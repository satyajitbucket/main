var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
const Excel = require('exceljs');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');
var config = require('../../config/config.json');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});


// exports.addeditCategory = function(req, res, next){
// 	var id = req.params.id;  
//     var arrData = null;
//     var arrProduct = null;
// 	var arrStore = null;
//     if(!id){
//         fetch(req.app.locals.apiurl+'category/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){

//             return res.render('superpos/category/addedit', {title: 'Add Category',messages:'',arrData:'',arrProduct: value.product,arrStore: value.arrData,errors:''});
//         });
//     }else{
//         fetch(req.app.locals.apiurl+'category/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/category/addedit', {title: 'Edit Category',messages:'',arrData: value.value,arrProduct: value.product,arrStore: value.arrData,errors:''});
//         });
//     }
    
// };






exports.downloadCategoryList = async function (req, res, next) {
    var workbook = new Excel.Workbook();

    workbook.creator = 'Me';
    workbook.lastModifiedBy = 'Her';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;

    workbook.views = [
        {
            x: 0, y: 0, width: 10000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ];
    var worksheet = workbook.addWorksheet('My Sheet');
    worksheet.columns = [
        { header: 'Sl. No.', key: 'Slno', width: 10 },
        { header: 'Category Name ', key: 'Catagoryname', width: 32 },
        { header: 'Number Of Products ', key: 'Number', width: 20 },
           ];

   var Category_list = await models.category.findAll({order: [['title']] });  
     
   for (var i = 0; i < Category_list.length; i++) {
    var product_list =  await models.product.findAll({where: {
    
        category_id: Category_list[i].id
      }
    });    
        worksheet.addRow({ Slno: i+1, Catagoryname: Category_list[i].title, Number: product_list.length});
     }
    


    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader("Content-Disposition", "attachment; filename=" + "Category-list.xlsx");
    workbook.xlsx.write(res)
        .then(function (data) {
            res.end();
            console.log('File write done........');
        });
};







exports.categoryList = function(req, res, next){
	  
//     var arrData = null;
//     fetch(req.app.locals.apiurl+'category',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
        
//     }}) .then(function(response) { return response.json() })
//     .then(function(value){
// 		return res.render('superpos/category/list', { title: 'Category',arrData: value.value,arrStores: value.stores,arrOption:'',message:'',errors:''		
// 		});
// 	});
// }

var currPage = req.query.page ? req.query.page : 0;
    var limit = req.query.limit ? req.query.limit : 10;
    var offset = currPage!=0 ? (currPage * limit) - limit : 0;
    var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.category.findAndCountAll({where: {status:{$ne: 'archive'}},limit: limit, offset: offset, order: [['position', 'ASC']],});            
            existingItem.then(function (results) {
                existingItem = models.stores.findAll();
                existingItem.then(function (stores) {
                    const itemCount = results.count;
                    const pageCount = Math.ceil(results.count / limit);
                    const previousPageLink = paginate.hasNextPages(req)(pageCount);
                    const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                    const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                    // console.log(startItemsNumber);
                    // console.log(endItemsNumber);
                    // console.log(previousPageLink)
                        return res.render('superpos/category/list', { title: 'Category',arrData: results.value,arrStores: stores,arrOption:'',arrData:results.rows,messages:req.flash('info'),errors:req.flash('errors')	,	
                        pageCount,
                        itemCount,
                        currentPage: currPage,
                        previousPage : previousPageLink	,
                        startingNumber: startItemsNumber,
                        endingNumber: endItemsNumber,
                        pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                    }); 
                })
            });  
        }	
    });
}

exports.addeditCategory = function(req, res, next){
    
    var id = req.params.id;
	stores = models.stores.findAll({ where: {status:'active'} });
	stores.then(function (stores) {	
        var existingItem = null;
        if(!id){	
            //res.status(200).send({ 
            return res.render('superpos/category/addedit', {
                title: 'Add Category',
                messages:req.flash('info'),
                arrData:'',
                //arrProduct: product,
                arrStore: stores,
                errors:req.flash('errors'),
                //status:200,
                //arrData:stores
            });
        }else{            
            existingItem = models.category.findOne({ where: {id:id} });
            existingItem.then(function (value) {	
                //res.status(200).send({ 
                return res.render('superpos/category/addedit', {
                    title: 'Edit Category',
                    messages:req.flash('info'),
                    arrData: value,
                    //arrProduct: product,
                    arrStore: stores,
                    errors:req.flash('errors'),
                    //status:200,
                    //value: value,
                    //arrData:stores
                });
            });	
        }	
	});
};

exports.addCategory = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
	//console.log(fields)
	//return res.send(files)
        var id = fields.update_id[0];

        var image=null;

        if(files.image[0].originalFilename!=''){	
            image = files.image[0].originalFilename;
        }else{
            image =fields.update_image[0];
        }
       
        if(!id){
            models.category.create({ 
			    store_id: fields.store_id ? fields.store_id[0] : null,
                title: fields.title[0] ? fields.title[0] : null,
                description: fields.description[0]?fields.description[0]:null,
                parent_category_id: fields.parent_category_id[0]?fields.parent_category_id[0]:null,
                position: fields.position[0]?fields.position[0]:null,
                path: fields.path[0]?fields.path[0]:null, 
                url: fields.url[0]?fields.url[0]:null,
                product_count: fields.product_count[0]?fields.product_count[0]:null,
                include_in_menu: fields.include_in_menu[0]?fields.include_in_menu[0]:null,
                meta_title: fields.meta_title[0]?fields.meta_title[0]:null,
                meta_key: fields.meta_key[0]?fields.meta_key[0]:null,
                meta_description: fields.meta_description[0]?fields.meta_description[0]:null,
                status:fields.status[0]?fields.status[0]:null,
                //icon:fields.uploadHiddenCropImage[0] ? fields.uploadHiddenCropImage[0] :fields.update_image[0],
                icon:image?image:null,
                }).then(function(category) {  
                    
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/category');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.category.update({ 
			    store_id: fields.store_id?fields.store_id[0]:null,
                title: fields.title[0]?fields.title[0]:null,
                description: fields.description[0]?fields.description[0]:null,
                parent_category_id: fields.parent_category_id[0]?fields.parent_category_id[0]:null,
                position: fields.position[0]?fields.position[0]:null,
                path: fields.path[0]?fields.path[0]:null, 
                url: fields.url[0]?fields.url[0]:null,
                product_count: fields.product_count[0]?fields.product_count[0]:null,
                include_in_menu: fields.include_in_menu[0]?fields.include_in_menu[0]:null,
                meta_title: fields.meta_title[0]?fields.meta_title[0]:null,
                meta_key: fields.meta_key[0]?fields.meta_key[0]:null,
                meta_description: fields.meta_description[0]?fields.meta_description[0]:null,
                status:fields.status[0]?fields.status[0]:null,
                //icon:fields.uploadHiddenCropImage[0] ? fields.uploadHiddenCropImage[0] :fields.update_image[0],
                icon:image?image:null,
            },{where:{id:id}}).then(function(category) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/category');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

exports.fileupload = function (req,res) {
	var formnew = new formidable.IncomingForm();
	formnew.parse(req);
	formnew.on('fileBegin', function (name, file) {
        console.log(file.name)
		if (file.name && file.name != '') {
			file.path = __dirname + '/../../public/superpos/myimages/' + file.name;
		}
	});	
};



// exports.deleteCategory = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'category/delete/'+id,{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })
		
// };

exports.deleteCategory = function(req, res, next) {
    
    var id = req.params.id;
    // models.category.destroy({ 
    //     where:{id:id}
    // }).then(function(value) {
    //     //res.status(200).send({ status:200,value:value });
    //     req.flash('info','Successfully Deleted');
    //     res.redirect('back');
    // });	

    models.category.update({ 
        status: 'archive',
    },{where:{id:id}
    }).then(function(value) {
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });

};
