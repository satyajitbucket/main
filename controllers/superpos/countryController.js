
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var flash = require('connect-flash');



exports.dashboard = function(req, res, next){
	  
	res.render('superpos/home/dashboard');
}



exports.countryList = function(req, res, next){	
	var id = req.params.id;
	var arrData = null;
	existingItem = models.country.findAll({ where: {status: {$ne: 'archive'}},order: [['name', 'ASC']] });  
    existingItem.then(function (value) {
		if(!id){
    return res.render('superpos/country/list', {
		title: 'Country',
		arrCountry: '',
		arrData: value,
		messages: req.flash('message'),
		errors: req.flash('errors')	
	});
	}else{
		existingCountry = models.country.findOne({ where: {id:id} });  
				existingCountry.then(function (country) {
					//console.log(city);
					//return res.send(city);
					return res.render('superpos/country/list', {	title: 'Country',arrCountry: country,arrData: value,messages: req.flash('message'),	errors: req.flash('errors')	});
							});	
			}	

});
}


// exports.addedit = function(req, res, next){
// 	var id = req.params.id;
//     var existingItem = null;
//     if(!id){	
// 	res.render('superpos/country/addedit',{title:'Country',arrData:'',messages: req.flash('message'),errors: req.flash('errors')});	
// 		}else{
// 			console.log(id);
// 	existingItem = models.country.findOne({ where: {id:id} });
// 	existingItem.then(function (value) {		
//     return res.render('superpos/country/addedit', {
//         title: 'Country',
// 		arrData: value,
// 		messages: req.flash('message'),
// 		errors: req.flash('errors')
// 		});
// 	});
	
// }
	
// }


exports.add = function(req, res, next) {
	var data = req.body;
	var id = data.update_id;
	//req.checkBody('slag', 'Slag name required').notEmpty();
	// var errors = req.validationErrors(); 
	req.checkBody('name', 'Name Field Required').notEmpty();
	//req.checkBody('slag', 'Slag Name Required').notEmpty();
	
	
       
    req.getValidationResult().then(function(result){
       if(!result.isEmpty()){
		   var errors = result.array().map(function (elem) {
                return elem.msg;
            });
           req.flash('errors',errors);
		   res.redirect('back');
		   }else{

			var duplicate = null;
				//duplicate = models.role.findOne({ where: {name:data.name} });
				duplicate = models.country.findAll({ where: {id: {$ne: id},name: data.name,status:{$ne:'archive'}} });
				duplicate.then(function (duplicate) {
				if(duplicate.length){
					req.flash('errors',' This Name Is Already Exists');  
					res.redirect('back');	
				}else{
	 
	
	if(!id){
		
	var existingItem=null;
	existingItem = models.country.findOne({ where: {slag:data.slag} });
	existingItem.then(function (value) {	
		
    if(value){
		   req.flash('errors','Slag is not unique.');
		   res.redirect('back');
		}
	});
		
	var slug=slugify(data.name);		
	models.country.create({ 
				name: data.name, 
				slag:slug,
				code: data.code,
				sequence: data.sequence,
				status:data.status,
				createdBy:data.user_id,
				updatedBy:""
				 }).then(function(country) {
	
				req.flash('message','Successfully Created');  
				return res.redirect('back');		
						
           
				});
			}else{
				
			models.country.update({ 
				name: data.name, 
				code: data.code,
				sequence: data.sequence,
				status:data.status,
				updatedBy:data.user_id
				 },{where:{id:id}}).then(function(country) {
	
					
			req.flash('message','Successfully Updated');	  
            return res.redirect('/superpos/country');
				});	
				
				
				}

			}
			}); 
	}
	
	
});
	
};

exports.deleteCountry = function(req, res, next) {
	var data = req.body;
	var id = req.params.id;
	
	
				
			models.country.update({ 
				status:'archive',
				updatedBy:1
				 },{where:{id:id}}).then(function(country) {
				
				  req.flash('message','Successfully Deleted');
           res.redirect('back');
				});	
	
};


function slugify(text) {
	 return text.toString().toLowerCase() .replace(/\s+/g, '-') 
	 // Replace spaces with - .replace(/[^\w\-]+/g, '')
	 // Remove all non-word chars .replace(/\-\-+/g, '-') // Replace multiple - with single - .replace(/^-+/, '') 
}
