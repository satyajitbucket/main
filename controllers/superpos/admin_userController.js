// var models = require('../../models');
// var passport = require('passport');
// var bcrypt = require('bcrypt-nodejs');
// var cookieParser = require('cookie-parser');
// var flash = require('connect-flash');
// var formidable = require('formidable');
// var multiparty = require('multiparty'); 
// var bodyParser = require('body-parser');
var fetch = require('node-fetch');

exports.dashboard = function(req, res, next){
    //console.log(user)
	var arrData = null;
    var adminId = [];
    fetch(req.app.locals.apiurl+'dashboard',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
    }}).then(function(response) { return response.json() })
    .then(function(data){
        console.log(new Intl.NumberFormat().format(data.totalAmound[0].totalAmound));
    //    var res=format(data.totalAmound[0].totalAmound).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //    console.log(res);
    //console.log(new Intl.NumberFormat('it-IT', {style: 'currency', maximumSignificantDigits: 3 }).format(data.totalAmound[0].totalAmound));
      //  console.log(new Intl.NumberFormat().format(data.totalAmound[0].totalAmound).toLocaleString('en',{ maximumSignificantDigits: 3 }));
      //  return res.send( data.order[0].totalData);
        return res.render('superpos/home/dashboard', {
            title:'dashboard',
            arrProduct: data.product,
            arrtotalData: data.order,
            //arrtodayTotalAmound: data.todayTotalAmound,
            arrtodayTotalAmound: new Intl.NumberFormat().format(data.todayTotalAmound[0].todayTotalAmound),
            arrtotalAmound: new Intl.NumberFormat().format(data.totalAmound[0].totalAmound),
            arrNewOrder: data.newOrder,
            arrData:data,
            req:req,
            messages: req.flash('message'),
            errors: req.flash('errors')	
        });
    });
}

exports.addeditAdmin = function(req, res, next){
    var id = req.params.id;  
    
    if(!id){
        fetch(req.app.locals.apiurl+'admin/addedit',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
    }}) .then(function(response) { return response.json() })
    .then(function(value){
        res.render('superpos/admin_user/addedit',{title:'Add Admin User',arrData:'',arrRoles: value.roles,messages: req.flash('info'),errors: req.flash('errors')});
    });
    }else{
        fetch(req.app.locals.apiurl+'admin/addedit/'+id,{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            //res.send(value.value);
            res.render('superpos/admin_user/addedit',{title:'Edit Admin User',arrData: value.value,arrRoles: value.roles,messages: req.flash('info'),errors: req.flash('errors')});
        });
    }
};

exports.adminList = function(req, res, next){
    fetch(req.app.locals.apiurl+'admin/list/',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
    }}) .then(function(response) { return response.json() })
    .then(function(value){
		return res.render('superpos/admin_user/list', { title: 'Admin User',arrData:value.value,message:'',errors:''		
		});
	});
}

exports.deleteAdmin = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'admin/delete/'+id,{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })	
}
