var models = require('../../models');
var formidable = require('formidable');
var bodyParser = require('body-parser');
var multiparty = require('multiparty'); 
var bcrypt = require('bcrypt-nodejs');








exports.addTeam = function(req, res, next) {
    //return res.send(req.body);
    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        console.log(fields);
        console.log(files);
        //return res.send("dwbagv");
        var id = fields.update_id[0];
        // return res.send(id);
        var image=null;
        if(files.profilePicture[0].originalFilename!=''){
            image=req.app.locals.baseurl+'all/images/'+n+files.profilePicture[0].originalFilename;
        }else{
            image=fields.update_image[0];
        }
        
        if(!id){
                
                models.team.create({ 
                    firstName: fields.firstName[0]?fields.firstName[0]:null, 
                    lastName: fields.lastName[0]?fields.lastName[0]:null,
                    profilePicture:image?image:null,
                    description:fields.description[0]?fields.description[0]:null,
                   
                    
                }).then(function(team) {  
                    return res.redirect('back');
                    
                })
                .catch(function(error) {
                    return res.send(error);
        
                });
            }
        
    else{
        
        models.team.update({ 
            firstName: fields.firstName[0]?fields.firstName[0]:null, 
            lastName: fields.lastName[0]?fields.lastName[0]:null,
            profilePicture:image?image:null,
            description:fields.description[0]?fields.description[0]:null,
        
        },{where:{id:id}}).then(function(team) {

                
                //return res.send('user created');	  
            return res.redirect('/all/teamList');
            })
            .catch(function(error) {
                return res.send(error);

        });
    }
    
    
    });
    

        var formnew = new formidable.IncomingForm();    
        formnew.parse(req);
        formnew.on('fileBegin', function (name, file){
            //return res.send(name);
            console.log(name);
            console.log(file.name);
           //return res.send(file.name);
            if(file.name && file.name!=''){
            file.path = __dirname + '/../../public/all/images/' +n+ file.name;
            }
        });
    };

    




exports.addeditTeam = function(req, res, next){
    //return res.send(req.params);
    var id = req.params.id;
    //return res.send(id);
    var existingItem = null;
    if(!id){	
        ///return res.send('idnot');
         return res.render('all/team/addeditTeam', {title: 'Team',messages:'',arrData:'',errors:''});
    }else{            
        console.log(id);
        existingItem = models.team.findOne({ where: {id:id} });
        existingItem.then(function (value) {		
            return res.render('all/team/addeditTeam', {title: 'Team',messages:'',arrData:value,errors:''});
        });	
    }	
}





exports.teamList = function(req, res, next){
	  
	//res.render('all/users/list');
	var arrData = null;
	existingItem = models.team.findAll();  
    existingItem.then(function (value) {
        console.log(value);
        //res.json(value);
		return res.render('all/team/teamList', {title: 'Team',message:'',arrData:value,errors:''}); 		
	});
}

exports.deleteTeam = function(req, res, next) {		
	var id = req.params.id;
	console.log(req.body);
	models.team.destroy({
        where:{id:id}
	}).then(function(team) {
		res.redirect('back');
	});
};




