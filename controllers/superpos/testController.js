var models = require('../../models');
var formidable = require('formidable');
var bodyParser = require('body-parser');
var multiparty = require('multiparty'); 
var bcrypt = require('bcrypt-nodejs');


exports.deleteTest = function(req, res, next){
	var id = req.params.id;
	models.teams.destroy({
        where:{id:id}
	}).then(function(team) {
		req.flash('message','Delete Sucessfully.');
		res.redirect('back');
	});
};

exports.testList = function(req, res, next){
	var arrData = null;
	models.teams.findAll().then(function (value) {
        console.log(value);
        //res.json(value);
		return res.render('superpos/test/testList', {title: 'Test',message:'',arrData:value,errors:''}); 		
	});
}; 
exports.addeditTeam = function(req, res, next){
	 var id = req.params.id;
	// return res.send(id);
	 if(!id){
		return res.render('superpos/test/addeditTest', {title: 'Test',messages:'',arrData:'',errors:''}); 
	 }else{
		 existingItem = models.teams.findOne({ where: {id:id} });
		 existingItem.then(function (value) {
			 // return res.send(value);
			  return res.render('superpos/test/addeditTest', {title: 'Test',messages:'',arrData:value,errors:''});
		 });
		 
	 }
};


exports.addTest = function(req, res, next) {
	//return res.send(req.body.update_id);
	var id=req.body.update_id;
	if(!id){
	models.teams.create({ 
                    firstName:req.body.firstname, 
                    lastName: req.body.lastname,
                    description:req.body.description,
                   
                    }).then(function(team) {  
                    return res.redirect('back');
                    
                })
                .catch(function(error) {
                    return res.send(error);
        
                });
				
	}else{
		
		models.teams.update({ 
                    firstName:req.body.firstname, 
                    lastName: req.body.lastname,
                    description:req.body.description,
                   
                    },{where:{id:id}}).then(function(team) {  
                    return res.redirect('back');
                    
                })
                .catch(function(error) {
                    return res.send(error);
        
                });
		
		
	}
};