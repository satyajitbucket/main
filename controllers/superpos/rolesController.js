
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');



exports.rolesList = function(req, res, next){
	  
//     var arrData = null;
//     fetch(req.app.locals.apiurl+'roles',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
//     }}) .then(function(response) { return response.json() })
//     .then(function(value){
// 		return res.render('superpos/roles/list', { title: 'Roles',arrData: value.value,arrOption:'',message: req.flash('message'),errors:''		
// 		});
// 	});
// }

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
jwt.verify(token, SECRET, function(err, decoded) {
    if (err) {
        res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
    }else{
        existingItem = models.role.findAndCountAll({limit: limit, offset: offset});            
        existingItem.then(function (results) {
            const itemCount = results.count;
            const pageCount = Math.ceil(results.count / limit);
            const previousPageLink = paginate.hasNextPages(req)(pageCount);
            const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
            const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
            console.log(startItemsNumber);
            console.log(endItemsNumber);

            // console.log(previousPageLink)
            return res.render('superpos/roles/list', { title: 'Roles',arrData: results.value,arrOption:'',arrData:results.rows,messages:req.flash('info'),errors: req.flash('errors'),
                pageCount,
                itemCount,
                currentPage: currPage,
                previousPage : previousPageLink	,
                startingNumber: startItemsNumber,
                endingNumber: endItemsNumber,
                pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
            }); 
        })
    }	
});
}



// exports.Rolesaddedit = function(req, res, next){
// 	var id = req.params.id;  
//     var arrData = null;
    
//     if(!id){
//         fetch(req.app.locals.apiurl+'roles/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){

//             return res.render('superpos/roles/addedit', {title: 'Add Role ',messages:'',arrData:'',errors:''});
//         });
//     }else{
//         fetch(req.app.locals.apiurl+'roles/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/roles/addedit', {title: 'Edit Role ',messages:'',arrData: value.value,errors:''});
//         });
//     }
    
// };

exports.addeditRoles = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    if(!id){	
        //res.status(200).send({
        return res.render('superpos/roles/addedit', {    
            title: 'Add Role ',
            messages:req.flash('info'),
            arrData:'',
            errors:req.flash('info'),
            //status:200
         });
    }else{            
        existingItem = models.role.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
            //return res.send(value);
            // res.status(200).send({
            return res.render('superpos/roles/addedit', {
                title: 'Edit Role ',
                messages:req.flash('info'),
                arrData: value,
                errors:req.flash('info'),
                //status:200,
                //value: value
            });
        })
    }	
};

exports.addRoles = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
   
        var id = req.body.update_id;
        var str  = req.body.name ;
        var lower1 = str.toLowerCase();
        var lower = lower1.replace(" ", "_").replace(/[`~!@#$%^&*()|+\-=?;:'",.<>0-9\{\}\[\]\\\/]/gi, '');
        
    //return res.send(req.body)
       
        if(!id){
            var slug1= lower;
            models.role.create({ 
                name: req.body.name?req.body.name:null, 
                slag: slug1,
                description: req.body.description?req.body.description:null,
				status:req.body.status?req.body.status:null,
                }).then(function(roles) {  
                     
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/roles');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }else{
            models.role.update({ 
                name: req.body.name?req.body.name:null, 
                slag: slug1,
                description: req.body.description?req.body.description:null,
				status:req.body.status?req.body.status:null,
            },{where:{id:id}}).then(function(roles) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/roles');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    
};

// exports.deleteRole = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'roles/delete/'+id,{headers: {
//       "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })	
// }

exports.deleteRole = function(req, res, next) {
    
    var id = req.params.id;
    models.role.destroy({ 
        where:{id:id}
    }).then(function(value) {
        //res.status(200).send({ status:200,value:value });
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });
};
