var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');
var config = require('../../config/config.json');


var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});







exports.sale_reports = function(req, res, next){

var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
                
            return res.render('superpos/sale_reports/addedit', { title: 'Banner',arrData: '',arrOption:'',messages:req.flash('info'),errors:req.flash('errors'),	
                
            });
        }	
    });
}

exports.sale_reportsList = function(req, res, next){
    
    var token= req.session.token;
        jwt.verify(token, SECRET, function(err, decoded) {
            if (err) {
                res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
            }else{
                var form = new multiparty.Form();
                var logdetails = req.session.user 
                form.parse(req, function(err, fields, files) { 
                    var date_from = fields.date_from[0];
                    var date_to = fields.date_to[0];
               
                    sequelize.query("SELECT * FROM `order` WHERE `order_status` ='Delivered' AND `createdAt` >= '"+date_from+"' AND `createdAt` < ('"+date_to+"' + INTERVAL 1 DAY)",{ type: Sequelize.QueryTypes.SELECT }) 
                    .then(function (order) {
                        //return res.send(order);
                        return res.render('superpos/sale_reports/list', { title: 'Banner',arrData: order,arrOption:'',messages:req.flash('info'),errors:req.flash('errors'),	
                            
                        }); 
                    })
                })
            }	
        });
    }




