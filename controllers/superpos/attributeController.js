
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');


exports.attributeList = function(req, res, next){
	  
    var arrData = null;
    fetch(req.app.locals.apiurl+'attribute',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
    }}) .then(function(response) { return response.json() })
    .then(function(value){
		return res.render('superpos/attribute/list', { title: 'Attribute',arrData: value.value,arrOption:'',message: req.flash('message'),errors:''		
		});
	});
}


exports.addeditAttribute = function(req, res, next){
    
    var id = req.params.id;
    var arrData = null;
    var arrOption = null;
    if(!id){
        fetch(req.app.locals.apiurl+'attribute/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/attribute/addedit', {title: 'Add Attribute',arrData:'',arrOption:'',messages: req.flash('info'),errors:''});
        });
    }else{            
        fetch(req.app.locals.apiurl+'attribute/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            //res.send(value.att_value);	
            return res.render('superpos/attribute/addedit', {title: 'Edit Attribute',arrData: value.value,arrOption: value.att_value,messages: req.flash('info'),errors:''});

            });
    }	
};

exports.deleteAttribute = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'attribute/delete/'+id,{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })	
}


// exports.deleteAttribute = function(req, res, next) {
//     var id = req.params.id;    
//     models.attribute.destroy({ 
//         where:{id:id}
//     }).then(function(attribute) {
//         models.attribute_option.destroy({ where: {attribute_id:id} 
//         }).then(function(att_value){
//         req.flash('message','Successfully Deleted');
//         res.redirect('back');
//         })
//     });	
// };        