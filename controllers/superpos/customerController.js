var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var Excel = require('exceljs');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');
var config = require('../../config/config.json');

var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});



exports.customerList = function(req, res, next){
	  
//     var arrData = null;
//     fetch(req.app.locals.apiurl+'customer',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
        
//     }}) .then(function(response) { return response.json() })
//     .then(function(value){
// 		return res.render('superpos/customer/list', { title: 'Customer',arrData: value.value,arrOption:'',message:'',errors:''		
// 		});
// 	});
// }

    var token= req.session.token;
    var currPage = req.query.page ? req.query.page : 0;
    var limit = req.query.limit ? req.query.limit : 10;
    var offset = currPage!=0 ? (currPage * limit) - limit : 0;
    var keyword = req.query.search ? req.query.search.trim() : '';
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            sequelize.query("SELECT COUNT(*) AS customerCount FROM `customer` where phone like '%"+keyword+"%' or first_name like  '%"+keyword+"%' or last_name like  '%"+keyword+"%'",{ type: Sequelize.QueryTypes.SELECT })
            .then(function (customerCount) {
                // console.log(ordercount[0].orderCount)
                if(customerCount ){
                    sequelize.query("SELECT `customer`.* FROM `customer` where phone like  '%"+keyword+"%' or first_name like  '%"+keyword+"%' or last_name like  '%"+keyword+"%' ORDER BY `id` DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT })
                    .then(function (value) {
                        const itemCount = customerCount.length > 0 ? customerCount[0].customerCount : 0;
                        const pageCount =  customerCount.length > 0 ? Math.ceil(customerCount[0].customerCount / limit) : 1;
                        const previousPageLink = paginate.hasNextPages(req)(pageCount);
                        const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                        const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                    
                        return res.render('superpos/customer/list', { title: 'Customer',arrData:value,messages: req.flash('info'), errors: req.flash('errors'),		
                            pageCount,
                            itemCount,
                            currentPage: currPage,
                            previousPage : previousPageLink	,
                            startingNumber: startItemsNumber,
                            endingNumber: endItemsNumber,
                            pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                        });
                    }); 
                }else{
                    return res.render('superpos/customer/list', { title: 'Customer',arrData:'',messages: req.flash('info'), errors: req.flash('errors'),		
                        pageCount:0,
                        itemCount:0,
                        currentPage: currPage,
                        previousPage : previousPageLink	,
                        startingNumber: startItemsNumber,
                        endingNumber: endItemsNumber,
                        pages: paginate.getArrayPages(req)(limit, pageCount, currPage,keyword)	
                    });
                }                   
            });
        }	
    });	


// var currPage = req.query.page ? req.query.page : 0;
// var limit = req.query.limit ? req.query.limit : 10;
// var offset = currPage!=0 ? (currPage * limit) - limit : 0;
// var token= req.session.token;
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingItem = models.customer.findAndCountAll({order: [['id', 'DESC']],limit: limit, offset: offset});            
//             existingItem.then(function (results) {
//                 const itemCount = results.count;
//                 const pageCount = Math.ceil(results.count / limit);
//                 const previousPageLink = paginate.hasNextPages(req)(pageCount);
//                 const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
//                 const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
//                 console.log(startItemsNumber);
//                 console.log(endItemsNumber);

//                 // console.log(previousPageLink)
//                     return res.render('superpos/customer/list', { title: 'Customer',arrData: results.value,arrOption:'',arrData:results.rows,message:'',errors:''	,
//                     pageCount,
//                     itemCount,
//                     currentPage: currPage,
//                     previousPage : previousPageLink	,
//                     startingNumber: startItemsNumber,
//                     endingNumber: endItemsNumber,
//                     pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
//                 }); 
//             })
//         }	
//     });
}


// exports.addeditCustomer = function(req, res, next){ 
//     //return res.send(req)   
//     var id = req.params.id;
//     console.log(id)
//     console.log('omn')
// 	citi = models.citi.findAll({ where: {status:'active'} });
// 	citi.then(function (citi) {	
//         customer_group = models.customer_group.findAll({ where: {status:'active'} });
// 	    customer_group.then(function (customer_group) {
//             var existingItem = null;
//             if(!id){	
//                 //res.status(200).send({
//                 return res.render('superpos/customer/addedit', {
//                     title: 'Add Customer',
//                     messages:'',
//                     arrData:'',
//                     arrAddress:'',
//                     arrPhone_number:'',
//                     arrEmail_id:'',
//                     arrProduct: '',
//                     arrCiti: citi,
//                     arrCustomer_group: customer_group,
//                     errors:'', 
//                 });
//             }else{            
//                 existingItem = models.customer.findOne({ where: {id:id} });
//                 existingItem.then(function (value) {	
//                     existingAddress = models.customer_address.findAll({ where: {customer_id:id} });
//                     existingAddress.then(function (customer_address) {
//                         console.log("ooooooooooooooooooooooooooooooooooooooooooooooooooo")
//                         console.log(customer_address)
//                         existingPhone = models.customer_phone.findAll({ where: {customer_id:id} });
//                         existingPhone.then(function (customer_phone) {
//                             existingEmail = models.customer_email.findAll({ where: {customer_id:id} });
//                             existingEmail.then(function (customer_email) {	
//                                 //res.status(200).send({ 
//                                 return res.render('superpos/customer/addedit', {
//                                     title: 'Edit Customer',
//                                     messages:'',
//                                     arrData: value,
//                                     arrAddress: customer_address,
//                                     arrPhone_number: customer_phone,
//                                     arrEmail_id:customer_email,
//                                     arrProduct: value.product,
//                                     arrCiti: citi,
//                                     arrCustomer_group: customer_group,
//                                     errors:'',
//                                 });
//                             });
//                         });
//                     });
//                 });	
//             }	
//         });
//     });    
// };

exports.addeditCustomer = function(req, res, next){
	var id = req.params.id;  
    var arrData = null;
    var arrProduct = null;
	var arrCiti = null;
    if(!id){
        fetch(req.app.locals.apiurl+'customer/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){

            return res.render('superpos/customer/addedit', {title: 'Add Customer',messages:'',arrData:'',arrAddress:'',arrPhone_number:'',arrEmail_id:'',arrProduct: value.product,arrCiti: value.arrData,arrCustomer_group: value.arrCustomer_gr,errors:''});
        });
    }else{
        fetch(req.app.locals.apiurl+'customer/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            console.log(value.address)
            return res.render('superpos/customer/addedit', {title: 'Edit Customer',messages:'',arrData: value.value,arrAddress: value.address,arrPhone_number: value.phone,arrEmail_id:value.email,arrProduct: value.product,arrCiti: value.arrData,arrCustomer_group: value.arrCustomer_gr,errors:''});
        });
    }
    
};

exports.deleteCustomer = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'customer/delete/'+id,{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })
		
};



exports.downloadCustomerList = async function (req, res, next) {
    var workbook = new Excel.Workbook();

    workbook.creator = 'Me';
    workbook.lastModifiedBy = 'Her';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;

    workbook.views = [
        {
            x: 0, y: 0, width: 10000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ];
    var worksheet = workbook.addWorksheet('My Sheet');
    worksheet.columns = [
        { header: 'Sl. No.', key: 'Slno', width: 10 },
        { header: 'Customer Name ', key: 'Name', width: 30 },
        { header: 'Email Id ', key: 'email', width: 33 },
        { header: 'Phone No ', key: 'Phone', width: 25 }
       
           ];

    var customerlist = await models.customer.findAll()
  
   for (var i = 0; i < customerlist.length; i++) {
       

        worksheet.addRow({ Slno: i+1, Name :(customerlist[i].first_name+' '+customerlist[i].last_name != 'null null' ? customerlist[i].first_name+' '+customerlist[i].last_name : ''), email:customerlist[i].email, Phone:customerlist[i].phone  });
      
    }
   
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader("Content-Disposition", "attachment; filename=" + "Customer-list.xlsx");
    workbook.xlsx.write(res)
        .then(function (data) {
            res.end();
            console.log('File write done........');
        });
};
