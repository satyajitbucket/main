var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');
var config = require('../../config/config.json');


var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});







// exports.customer_reports = function(req, res, next){

// var token= req.session.token;
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
                
//             return res.render('superpos/customer_reports/addedit', { title: 'Customer',arrData: '',arrOption:'',messages:req.flash('info'),errors:req.flash('errors'),	
                
//             });
//         }	
//     });
// }

exports.customer_reportsList = function(req, res, next){
    
    var token= req.session.token;
        jwt.verify(token, SECRET, function(err, decoded) {
            if (err) {
                res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
            }else{
                // with order count and order value by customer ----> SELECT `customer`.*, CONCAT(`customer_address`.first_name,' ',`customer_address`.last_name,', ',`customer_address`.address,', ',`customer_address`.city,', ',`customer_address`.pin,' ',`customer_address`.country) as customerAddress, (SELECT COUNT(*) FROM `order` WHERE `order`.customer_id = `customer`.id AND `order`.`order_status` ='Delivered') as `numberOfOrder`,(SELECT SUM(grand_total) FROM `order` WHERE `order`.customer_id = `customer`.id AND `order`.`order_status` ='Delivered') as totalAmound FROM `customer` LEFT JOIN `customer_address` on `customer_address`.customer_id = `customer`.id WHERE `customer`.`status` ='active' ORDER BY `customer`.`id` DESC
                //with order count by customer---> SELECT `customer`.*, CONCAT(`customer_address`.first_name,' ',`customer_address`.last_name,', ',`customer_address`.address,', ',`customer_address`.city,', ',`customer_address`.pin,' ',`customer_address`.country) as customerAddress, (SELECT COUNT(*) FROM `order` WHERE `order`.customer_id = `customer`.id AND `order`.`order_status` ='Delivered') as `numberOfOrder` FROM `customer` LEFT JOIN `customer_address` on `customer_address`.customer_id = `customer`.id WHERE `customer`.`status` ='active' ORDER BY `customer`.`id` DESC
                //var logdetails = req.session.user // SELECT `customer`.*, CONCAT(`customer_address`.first_name,' ',`customer_address`.last_name,' ',`customer_address`.address,' ',`customer_address`.city,' ',`customer_address`.pin,' ',`customer_address`.country) as customerAddress FROM `customer` LEFT JOIN `customer_address` on `customer_address`.customer_id = `customer`.id WHERE `customer`.`status` ='active' ORDER BY `customer`.`id` DESC
                //form.parse(req, function(err, fields, files) {  SELECT customer.*, customer_address.* FROM `customer` LEFT JOIN customer_address on customer_address.customer_id = customer.id WHERE customer.`status` ='active' ORDER BY customer.`id` DESC
               
                    // sequelize.query("SELECT customer.* FROM `customer` LEFT JOIN customer_address on customer_address.customer_id = customer.id WHERE `status` ='active' ORDER BY `id` DESC",{ type: Sequelize.QueryTypes.SELECT }) 
                    // sequelize.query("SELECT `customer`.*, CONCAT(`customer_address`.first_name,' ',`customer_address`.last_name,', ',`customer_address`.address,', ',`customer_address`.city,', ',`customer_address`.pin,' ',`customer_address`.country) as customerAddress FROM `customer` LEFT JOIN `customer_address` on `customer_address`.customer_id = `customer`.id WHERE `customer`.`status` ='active' ORDER BY `customer`.`id` DESC",{ type: Sequelize.QueryTypes.SELECT }) 
                    sequelize.query("SELECT `customer`.*, CONCAT(`customer_address`.first_name,' ',`customer_address`.last_name,', ',`customer_address`.address,', ',`customer_address`.city,', ',`customer_address`.pin,' ',`customer_address`.country) as customerAddress, (SELECT COUNT(*) FROM `order` WHERE `order`.customer_id = `customer`.id AND `order`.`order_status` ='Delivered') as `numberOfOrder`,(SELECT SUM(grand_total) FROM `order` WHERE `order`.customer_id = `customer`.id AND `order`.`order_status` ='Delivered') as totalAmound FROM `customer` LEFT JOIN `customer_address` on `customer_address`.customer_id = `customer`.id WHERE `customer`.`status` ='active' ORDER BY `customer`.`id` DESC",{ type: Sequelize.QueryTypes.SELECT }) 
                    .then(function (customer) {
                        //return res.send(order);
                        return res.render('superpos/customer_reports/list', { title: 'Customer',arrData: customer,arrOption:'',messages:req.flash('info'),errors:req.flash('errors'),	
                            
                        }); 
                    })
                //})
            }	
        });
    }




