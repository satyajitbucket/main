var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
const Excel = require('exceljs');
var jwt = require('jsonwebtoken');
var date= new Date(Date.now()).toLocaleString();
var fs = require("fs");
var SECRET = 'nodescratch';
const paginate = require('express-paginate');
var config = require('../../config/config.json');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});


// exports.addeditProduct = function(req, res, next){
// 	var id = req.params.id;  
	 
// 	if(!id){
// 		fetch(req.app.locals.apiurl+'product/addedit',{headers: {
// 			"Content-Type": "application/json; charset=utf-8",
// 			"token": req.session.token,
// 		}}) .then(function(response) { return response.json() })
// 		.then(function(value){
// 			// req.flash('info','Successfully /hhhhhhhhhhhhhhhhhlllllll');	
// 			console.log(req.flash('info'))			
// 			return res.render('superpos/product/addedit',{title:'Add Product',arrData:'',arrCategory: value.category,arrSub_category: value.sub_category,arrStore:value.store,arrCiti:value.citi,arrZipcode:value.zip,arrProduct_unit:'', arrtaxData: value.taxData, messages: req.flash('info'),errors: req.flash('errors')});
// 		});
// 	}else{
// 		fetch(req.app.locals.apiurl+'product/addedit/'+id,{headers: {
// 			"Content-Type": "application/json; charset=utf-8",
// 			"token": req.session.token,
// 		}}) .then(function(response) { return response.json() })
// 		.then(function(value){
// 			// req.flash('info','Successfully /lllllll');	
// 			console.log(req.flash('info'))	  
// 			return res.render('superpos/product/addedit',{title:'Edit Product',arrData: value.value,arrCategory: value.category,arrSub_category: value.sub_category,arrProduct_unit:value.prod_unit, arrtaxData: value.taxData, messages: req.flash('info'),errors: req.flash('errors')});
// 		});
// 	}
// };



exports.downloadList1 = async function(req, res, next){


    


    /*sequelize.query("SELECT `product`.* FROM `product` where is_configurable=0 ",{ type: Sequelize.QueryTypes.SELECT })
    .then(function (value) {
        category = models.category.findAll({ where: {status:'active'} });
        category.then(function (category) {
           
            return res.render('superpos/product/exelreport', { title: 'Product',arrData:value,arrCategory: category,messages: req.flash('info'), errors: req.flash('errors'),		
               	
            });
        });    
    });*/ 

var p = Date(Date.now()).toLocaleString();

    var product_list = await sequelize.query("SELECT `product`.* FROM `product` where is_configurable=0 ",{ type: Sequelize.QueryTypes.SELECT })
    
    
    var data='';
    for (var i = 0; i < product_list.length; i++) {
        data += product_list[i].title+'\t'+product_list[i].price+'\n';
    }
    fs.writeFile('./public/reports/'+'product-list.xls', data, (err) => {
        if (err) throw err;
        console.log('File created');
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "Report.xlsx");
        res.send();
    });

}


exports.downloadList = async function (req, res, next) {
        var workbook = new Excel.Workbook();
    
        workbook.creator = 'Me';
        workbook.lastModifiedBy = 'Her';
        workbook.created = new Date(1985, 8, 30);
        workbook.modified = new Date();
        workbook.lastPrinted = new Date(2016, 9, 27);
        workbook.properties.date1904 = true;
    
        workbook.views = [
            {
                x: 0, y: 0, width: 10000, height: 20000,
                firstSheet: 0, activeTab: 1, visibility: 'visible'
            }
        ];
        var worksheet = workbook.addWorksheet('My Sheet');
        worksheet.columns = [
            { header: 'Sl. No.', key: 'Slno', width: 10 },
            { header: 'Product Name', key: 'ProductName', width: 32 },
            { header: 'Price', key: 'Price', width: 10 },
            { header: 'Special Price', key: 'SpecialPrice', width: 10 },
            { header: 'Category', key: 'Category', width: 30 },
            { header: 'Sub Category', key: 'SubCategory', width: 30 },
        ];
       
       //var product_list = await models.product.findAll({order: [['id', 'DESC']] });
       var product_list = await sequelize.query("select a.title, a.price, a.special_price, b.title 'category_title', c.title 'sub_category_title' from product as a "+
                                                "left join category as b on b.id=a.category_id "+
                                                "left join subcategory as c on c.id=a.sub_category_id " +
                                                "order by b.title",{ type: Sequelize.QueryTypes.SELECT });
       
       for (var i = 0; i < product_list.length; i++) {
            worksheet.addRow({ Slno: i+1, ProductName: product_list[i].title, Price: product_list[i].price, SpecialPrice: product_list[i].special_price, Category: product_list[i].category_title, SubCategory: product_list[i].sub_category_title });
         }
        

    
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader("Content-Disposition", "attachment; filename=" + "product-list.xlsx");
        workbook.xlsx.write(res)
            .then(function (data) {
                res.end();
                console.log('File write done........');
            });
    };
    
   
   
    
   
    









exports.productList = function(req, res, next){
//     fetch(req.app.locals.apiurl+'product',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
//     }}) .then(function(response) { return response.json() })
//     .then(function(value){
// 		// req.flash('info','Successfully /lllllll');	
// 		console.log(req.flash('info'))
// 		return res.render('superpos/product/list', { title: 'Product',arrData: value.value,arrOption:'', messages: req.flash('info'), errors: req.flash('errors')
// 		});
// 	});
// }


var token= req.session.token;
    var currPage = req.query.page ? req.query.page : 0;
    var limit = req.query.limit ? req.query.limit : 10;
    var offset = currPage!=0 ? (currPage * limit) - limit : 0;
    var keyword = req.query.search ? req.query.search.trim() : '';
    var cat_filter = req.query.filter ? req.query.filter : '';
    //return res.send(keyword);	
    //return res.send(cat_filter);
    //console.log(keyword)
    //console.log(cat_filter)	
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            if(cat_filter && cat_filter != '' ){
                sequelize.query("SELECT COUNT(*) AS productCount FROM `product` where is_configurable=0 and category_id="+"'"+cat_filter+"'",{ type: Sequelize.QueryTypes.SELECT })
                .then(function (productCount) {
                    // console.log(ordercount[0].orderCount)
                    if(productCount ){
                        sequelize.query("SELECT `product`.* FROM `product` where is_configurable=0 and category_id="+"'"+cat_filter+"' ORDER BY `id` DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT })
                        .then(function (value) {
                            category = models.category.findAll({ where: {status:'active'} });
                            category.then(function (category) {
                                const itemCount = productCount.length > 0 ? productCount[0].productCount : 0;
                                const pageCount =  productCount.length > 0 ? Math.ceil(productCount[0].productCount / limit) : 1;
                                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                            
                                return res.render('superpos/product/list', { title: 'Product',arrData:value,arrCategory: category,messages: req.flash('info'), errors: req.flash('errors'),		
                                    pageCount,
                                    itemCount,
                                    currentPage: currPage,
                                    previousPage : previousPageLink	,
                                    startingNumber: startItemsNumber,
                                    endingNumber: endItemsNumber,
                                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                                });
                            });    
                        }); 
                    }else{
                        return res.render('superpos/product/list', { title: 'Product',arrData:'',messages: req.flash('info'), errors: req.flash('errors'),		
                            pageCount:0,
                            itemCount:0,
                            currentPage: currPage,
                            previousPage : previousPageLink	,
                            startingNumber: startItemsNumber,
                            endingNumber: endItemsNumber,
                            pages: paginate.getArrayPages(req)(limit, pageCount, currPage,keyword)	
                        });
                    }                   
                });
            }else{
                sequelize.query("SELECT COUNT(*) AS productCount FROM `product` where is_configurable=0 and (title like  '%"+keyword+"%')",{ type: Sequelize.QueryTypes.SELECT })
                .then(function (productCount) {
                    // console.log(ordercount[0].orderCount)
                    if(productCount ){
                        sequelize.query("SELECT `product`.* FROM `product` where is_configurable=0 and (title like  '%"+keyword+"%') ORDER BY `id` DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT })
                        .then(function (value) {
                            category = models.category.findAll({ where: {status:'active'} });
                            category.then(function (category) {
                                const itemCount = productCount.length > 0 ? productCount[0].productCount : 0;
                                const pageCount =  productCount.length > 0 ? Math.ceil(productCount[0].productCount / limit) : 1;
                                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                            
                                return res.render('superpos/product/list', { title: 'Product',arrData:value,arrCategory: category,messages: req.flash('info'), errors: req.flash('errors'),		
                                    pageCount,
                                    itemCount,
                                    currentPage: currPage,
                                    previousPage : previousPageLink	,
                                    startingNumber: startItemsNumber,
                                    endingNumber: endItemsNumber,
                                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                                });
                            });    
                        }); 
                    }else{
                        return res.render('superpos/product/list', { title: 'Product',arrData:'',messages: req.flash('info'), errors: req.flash('errors'),		
                            pageCount:0,
                            itemCount:0,
                            currentPage: currPage,
                            previousPage : previousPageLink	,
                            startingNumber: startItemsNumber,
                            endingNumber: endItemsNumber,
                            pages: paginate.getArrayPages(req)(limit, pageCount, currPage,keyword)	
                        });
                    }                   
                });
            }    
        }	
    });	


// var currPage = req.query.page ? req.query.page : 0;
// var limit = req.query.limit ? req.query.limit : 10;
// var offset = currPage!=0 ? (currPage * limit) - limit : 0;
// var token= req.session.token;
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingItem = models.product.findAndCountAll({where: {is_configurable: 0 },limit: limit, offset: offset,order: [['id', 'DESC']]});            
//             existingItem.then(function (results) {
//                 const itemCount = results.count;
//                 const pageCount = Math.ceil(results.count / limit);
//                 const previousPageLink = paginate.hasNextPages(req)(pageCount);
//                 const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
//                 const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
//                 console.log(startItemsNumber);
//                 console.log(endItemsNumber);

//                 // console.log(previousPageLink)
// 					return res.render('superpos/product/list', { title: 'Product',arrData: results.value,arrOption:'',arrData:results.rows, messages: req.flash('info'), errors: req.flash('errors'),
//                     pageCount,
//                     itemCount,
//                     currentPage: currPage,
//                     previousPage : previousPageLink	,
//                     startingNumber: startItemsNumber,
//                     endingNumber: endItemsNumber,
//                     pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
//                 }); 
//             })
//         }	
// 	});
}

exports.addeditProduct = function(req, res, next){    
    var id = req.params.id;
    var existingItem = null;
    var existingCat = null;
         
	category = models.category.findAll({ where: {status:'active'} });
	category.then(function (category) {	
		taxdropdown = sequelize.query("SELECT dropdown_settings_option.id as optionID, dropdown_settings_option.option_label as optionLabel, dropdown_settings.identifier as dropIdentifier FROM dropdown_settings_option LEFT JOIN dropdown_settings on dropdown_settings_option.dropdown_id = dropdown_settings.id where dropdown_settings.identifier='tax'",{ type: Sequelize.QueryTypes.SELECT })  
		taxdropdown.then(function (taxData) {                               
			if(!id){	   
				// res.status(200).send({ 
				return res.render('superpos/product/addedit',{
					title:'Add Product',
					arrData:'',
					arrProduct_unit:'',
					arrCategory: category,
					arrSub_category: '',
					arrtaxData: taxData, 
					messages: req.flash('info'),
					errors: req.flash('errors')
				});
			}else{            
				existingItem = models.product.findOne({ where: {id:id} });
				existingItem.then(function (value) {	
					if(value.sub_category_id && value.sub_category_id!=''){
						subcategory = models.subcategory.findAll({ where: {id: value.sub_category_id,status:'active'} });
						subcategory.then(function (sub_category) {
							existingproduct_unit = models.product.findAll({ where: {is_configurable:id} });
							existingproduct_unit.then(function (product_unit) {
								// res.status(200).send({ 
								return res.render('superpos/product/addedit',{
									title:'Edit Product',
									arrData: value,
									arrCategory: category,
									arrSub_category: sub_category,
									arrProduct_unit:product_unit, 
									arrtaxData: taxData, 
									messages: req.flash('info'),
									errors: req.flash('errors')
								});
							}); 
						}); 
					}else{
						existingproduct_unit = models.product.findAll({ where: {is_configurable:id} });
						existingproduct_unit.then(function (product_unit) {
							// res.status(200).send({ 
							return res.render('superpos/product/addedit',{
								title:'Edit Product',
								arrData: value,
								arrCategory: category,
								arrSub_category: '',
								arrProduct_unit:product_unit, 
								arrtaxData: taxData, 
								messages: req.flash('info'),
								errors: req.flash('errors')
							});
						});                         
					}                          
				});
			}  
		});               
	});
};

exports.addProduct = function(req, res, next) {
    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        // return res.send(fields);	
        var id = fields.update_id ? fields.update_id[0] : '';
        var image='';
        if(fields.uploadHiddenCropImage && fields.uploadHiddenCropImage[0]!=''){
            image = req.app.locals.baseurl+'superpos/myimages/'+fields.uploadHiddenCropImage[0];
        }else{
            image =fields.update_image ? fields.update_image[0] : '';
        }
        var product_unitArr= fields.product_size ? fields.product_size : [];        
        var str = fields.title ? fields.title[0] : '';
        var productName= str.replace(" ", "").substr(0, 5).toUpperCase();
        var skuID = productName;
       // return res.send(skuID);        
        if(!id || id == '')
        {
            models.product.create({ 
				//sku: fields.sku ? fields.sku[0]:null,
                title: fields.title ? fields.title[0]:null,
                description: fields.description ? fields.description[0]:null,
                short_description: fields.short_description ? fields.short_description[0]:null,
                category_id: fields.category_id ? fields.category_id[0]:null,
                sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
                price: fields.price ? fields.price[0]:null,
                keyword: fields.keyword ? fields.keyword[0]:null,
                url:fields.url ? fields.url[0]:null,
                spice_level:fields.spice_level ? fields.spice_level[0]:null,
                type: fields.type ? fields.type[0]:null,
                tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
				special_price: fields.special_price ? fields.special_price[0]:null,
                special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
                special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
                weight:fields.weight ? fields.weight[0]:null,
                inventory:fields.inventory ? fields.inventory[0]:null,
                //is_configurable: fields.is_configurable ? 0 :null,
                is_configurable: 0,
                meta_title:fields.meta_title ? fields.meta_title[0]:null,
                meta_key:fields.meta_key ? fields.meta_key[0]:null,
                meta_description:fields.meta_description ? fields.meta_description[0]:null,
                status:fields.status ? fields.status[0]:null,
                image:image,
            }).then(function(product) {
				var newSkuID = skuID + product.id;
				//console.log('111111111111111111');
				//console.log(product_unitArr.length);
                models.product.update({
                    sku:newSkuID ? newSkuID : null,
                },{where:{id:product.id}}).then(function(product_update){
					//console.log('222222222222222222');
					//console.log(product_unitArr.length);
                    if(product_unitArr.length > 0  ){
						console.log('33333333333333333333s');
						console.log(product_unitArr.length);
                        var i=0;
                        product_unitArr.forEach(function(element) {				
                            models.product.create({ 
                                title: fields.title ? fields.title[0]:null,
                                description: fields.description ? fields.description[0]:null,
                                short_description: fields.short_description ? fields.short_description[0]:null,
                                category_id: fields.category_id ? fields.category_id[0]:null,
                                sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
                                price: fields.product_price[i],
                                keyword: fields.keyword ? fields.keyword[0]:null,
                                url:fields.url ? fields.url[0]:null,
                                spice_level:fields.spice_level ? fields.spice_level[0]:null,
                                type: fields.type ? fields.type[0]:null,
                                tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
                                special_price: fields.product_special_price ? fields.product_special_price[i]:'',
                                // special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
                                // special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
                                weight:fields.product_size[i],
                                is_configurable: fields.is_configurable ? product.id :null,
                                inventory:fields.product_inventory ? fields.product_inventory[i] : 0,
                                meta_title:fields.meta_title ? fields.meta_title[0]:null,
                                meta_key:fields.meta_key ? fields.meta_key[0]:null,
                                meta_description:fields.meta_description ? fields.meta_description[0]:null,
                                status:fields.product_status ? fields.product_status[i]:null,
                                image:image,
                            });
                            i++;
                        }, this);
                    } 
                    req.flash('info','Successfully Created');	  
                    return res.redirect('/superpos/product');  
                    //res.redirect('back');
                }).catch(function(error) {
                    return res.send(error);
                });
            })  
            .catch(function(error) {
                return res.send(error);
            });
        }else{
            models.product.update({ 
               // sku: fields.sku ? fields.sku[0]:null,
                title: fields.title ? fields.title[0]:null,
                description: fields.description ? fields.description[0]:null,
                short_description: fields.short_description ? fields.short_description[0]:null,
                category_id: fields.category_id ? fields.category_id[0]:null,
                sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
                price: fields.price ? fields.price[0]:null,
                keyword: fields.keyword ? fields.keyword[0]:null,
                url:fields.url ? fields.url[0]:null,
                spice_level:fields.spice_level ? fields.spice_level[0]:null,
                type: fields.type ? fields.type[0]:null,
                tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
				special_price: fields.special_price ? fields.special_price[0]:null,
                special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
                special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
                weight:fields.weight ? fields.weight[0]:null,
                inventory:fields.inventory ? fields.inventory[0]:0,
                meta_title:fields.meta_title ? fields.meta_title[0]:null,
                meta_key:fields.meta_key ? fields.meta_key[0]:null,
                meta_description:fields.meta_description ? fields.meta_description[0]:null,
                status:fields.status ? fields.status[0]:null,
                image:image,
            	},{where:{id:id}}).then(function(product) {
                // models.product_unit.destroy({	where:{product_id: id}
                // });
                var product_unitArr=fields.product_size ? fields.product_size : [];
                console.log(fields.updateUnitId)
                var  unique_product_unitArr = fields.updateUnitId.filter(Boolean);
                models.product.destroy({	
                    where:{id: { $notIn: unique_product_unitArr }, is_configurable: id}
                }).then(function(deleteproduct) {
               
                    if(product_unitArr ){
                        var i=0;
                        product_unitArr.forEach(function(element) {	
                            if(fields.updateUnitId[i] && fields.updateUnitId[i] != '' ){
                                models.product.update({ 
                                    title: fields.title ? fields.title[0]:null,
                                    description: fields.description ? fields.description[0]:null,
                                    short_description: fields.short_description ? fields.short_description[0]:null,
                                    category_id: fields.category_id ? fields.category_id[0]:null,
                                    sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
                                    price: fields.product_price[i],
                                    keyword: fields.keyword ? fields.keyword[0]:null,
                                    url:fields.url ? fields.url[0]:null,
                                    spice_level:fields.spice_level ? fields.spice_level[0]:null,
                                    type: fields.type ? fields.type[0]:null,
                                    tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
                                    special_price: fields.product_special_price ? fields.product_special_price[i]:'',
                                    // special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
                                    // special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
                                    weight:fields.product_size[i],
                                    inventory:fields.product_inventory ? fields.product_inventory[i] : 0,
                                    meta_title:fields.meta_title ? fields.meta_title[0]:null,
                                    meta_key:fields.meta_key ? fields.meta_key[0]:null,
                                    meta_description:fields.meta_description ? fields.meta_description[0]:null,
                                    status:fields.product_status ? fields.product_status[i]:null,
                                    image:image,
                                },{where:{id:fields.updateUnitId[i]}})
                            }else{
                                models.product.create({ 
                                    title: fields.title ? fields.title[0]:null,
                                    description: fields.description ? fields.description[0]:null,
                                    short_description: fields.short_description ? fields.short_description[0]:null,
                                    category_id: fields.category_id ? fields.category_id[0]:null,
                                    sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
                                    price: fields.product_price[i],
                                    keyword: fields.keyword ? fields.keyword[0]:null,
                                    url:fields.url ? fields.url[0]:null,
                                    spice_level:fields.spice_level ? fields.spice_level[0]:null,
                                    type: fields.type ? fields.type[0]:null,
                                    tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
                                    special_price: fields.product_special_price ? fields.product_special_price[i]:'',
                                    // special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
                                    // special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
                                    weight:fields.product_size[i],
                                    is_configurable: fields.is_configurable ? id :null,
                                    inventory:fields.product_inventory ? fields.product_inventory[i] : 0,
                                    meta_title:fields.meta_title ? fields.meta_title[0]:null,
                                    meta_key:fields.meta_key ? fields.meta_key[0]:null,
                                    meta_description:fields.meta_description ? fields.meta_description[0]:null,
                                    status:fields.product_status ? fields.product_status[i]:null,
                                    image:image,
                                });
                            }	
                            i++;
                        }, this);
                    }
                    req.flash('info','Successfully Updated');
                    //return res.redirect('/superpos/product'); 
                    res.redirect('back');  
                })
                .catch(function(error) {
                    return res.send(error);
                });
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    }); 
};

exports.fileupload = function (req,res) {
	var formnew = new formidable.IncomingForm();
	formnew.parse(req);
	formnew.on('fileBegin', function (name, file) {
        console.log(file.name)
		if (file.name && file.name != '') {
			file.path = __dirname + '/../../public/superpos/myimages/' + file.name;
		}
	});	
};


// exports.fileupload = function (req,res) {
//     //console.log(11151515151515145)
// 	var formnew = new formidable.IncomingForm();
// 	formnew.parse(req);
// 	formnew.on('fileBegin', function (name, file) {
//         //console.log("llllllllllllllllllllllllllllllllll")
//         console.log(file.name)
// 		if (file.name && file.name != '') {
// 			file.path = __dirname + '/../../public/superpos/myimages/' + file.name;
// 		}
// 	});	
// };

// exports.deleteProductmul = function(req, res, next){
	
// 	// var id = req.params.id;	
// 	console.log('-------------------------------------------------------------------------------')
// 	fetch(req.app.locals.apiurl+'productmul/delete',{method: 'POST',body:req.body,
// 	headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) {
// 		res.redirect('back'); 
// 	})	
// };


exports.excelReport = function(req,res,next){

    return res.render('superpos/product/exelreport');




};



exports.deleteProduct = function(req, res, next) {    
    var id = req.params.id;
	models.product.destroy({ 
		where:{id:id}
	}).then(function(value) {
		models.product_unit.destroy({ 
			where:{product_id:id}
		})
		//res.status(200).send({ status:200,value:value });
		req.flash('info','Successfully Deleted');
        res.redirect('back');
	});	
};
