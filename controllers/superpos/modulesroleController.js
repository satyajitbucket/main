
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');


exports.dashboard = function(req, res, next){
	  
	res.render('superpos/home/dashboard');
}


exports.modulesroleList = function(req, res, next){ 	  
	var arrData = null;

		models.modules_role.belongsTo(models.modules, {foreignKey: 'modulesId'});
		models.modules_role.belongsTo(models.role, {foreignKey: 'roleId'});
		existingItem=models.modules_role.findAll({include: [models.modules, models.role], raw: true });





	//existingItem = models.modules_role.findAll();  
    existingItem.then(function (value) {
		//console.log(value);
		//return res.send(value);
    return res.render('superpos/modulesrole/list', {
        title: 'Modules Role',
		arrData: value,
		messages: req.flash('message'),
		errors: req.flash('errors')	
    });

});
}


exports.addedit = function(req, res, next){
	var id = req.params.id;
	var existingItem = null;
	var rolesId = null;
	var modules = null;
	 rolesId = models.role.findAll({ where: {status:'active'},order: [['name', 'ASC']]});
	  modules = models.modules.findAll({ where: {status:'active'},order: [['name', 'ASC']]});
	rolesId.then(function (roles) {
		modules.then(function (modul) {


    if(!id){	
	res.render('superpos/modulesrole/addedit',{
		title:'Modules Role',
		arrData:'',
		arrRoles:roles,
		arrSelectModules:'',
		arrModules:modul,
		roleId:id,
		messages: req.flash('message'),
		errors: req.flash('errors')	
		});	
	//res.render('superpos/modulesrole/addedit',{title:'Modules Role',arrData:'',arrRoles:roles,arrModules:modul,messages: req.flash('info'),errors: req.flash('errors')});	
	}else{
	//console.log(id);
	existingItem = models.modules_role.findAll({group: ['modulesId']});
	existingItem.then(function (value) {
		// console.log(value);
		existingModul = models.modules_role.findAll({group: ['modulesId'],where: {roleId:id}});
		existingModul.then(function (module) {
			// res.send(module);					
			return res.render('superpos/modulesrole/addedit', {
				title: 'Modules Role',
				arrData: module,
				arrSelectModules:module,
				arrRoles:roles,
				arrModules:modul,
				roleId:id,
				messages: req.flash('message'),
				errors: req.flash('errors')	
			});
		});
	});
	
}
});
});
}


exports.add = function(req, res, next) {
	var data = req.body;
	 //console.log(req.body);
	// console.log(req.body.status[0]);
	// console.log(req.body.status[1]);
	//return res.send(req.body);
	var id = data.update_id;
	req.checkBody('roleId', 'Role Field Required').notEmpty();
	//req.checkBody('modulesId[]', 'This Field Required').notEmpty();

	 req.getValidationResult().then(function(result){
		//console.log(result);
       if(!result.isEmpty()){
		   var errors = result.array().map(function (elem) {
			   //console.log(elem);
                return elem.msg;
            });
            //console.log('There are following validation errors: ' + errors.join('&&'));
           req.flash('errors',errors);
		   res.redirect('back');
		   }else{


	
	if(!id){
		var ArrData=data.modulesId;
		models.modules_role.destroy({ where: {roleId:data.roleId} }).then(function(result){

		if(ArrData){
		var i=0;
		ArrData.forEach(function(element) {

			if(data.permission[i]=='isDelete'){
				var dataisView ='Y';
				var dataisCreate ='Y';
				var dataisUpdate ='Y';
				var dataisDelete ='Y';
			}
			else if(data.permission[i]=='isUpdate'){
				var dataisView ='Y';
				var dataisCreate ='Y';
				var dataisUpdate ='Y';
				var dataisDelete ='N';
			}
			else if(data.permission[i]=='isCreate'){
				var dataisView ='Y';
				var dataisCreate ='Y';
				var dataisUpdate ='N';
				var dataisDelete ='N';
			}
			else if(data.permission[i]=='isView'){
				var dataisView ='Y';
				var dataisCreate ='N';
				var dataisUpdate ='N';
				var dataisDelete ='N';
			}
			else if(!data.permission[i]==0){
				var dataisView ='N';
				var dataisCreate ='N';
				var dataisUpdate ='N';
				var dataisDelete ='N';
			}				
			

			models.modules_role.create({ 
				modulesId: data.modulesId[i], 
				roleId:data.roleId,
				isView:dataisView,
				isCreate:dataisCreate,
				isUpdate:dataisUpdate,
				isDelete:dataisDelete,
				createdBy:data.user_id,
				updatedBy:""
				 })
						//return res.send('user created');
                   
				

		i++;
		}, this);
		}
	})
	// console.log(i);
	// return res.send("bdwabdsbd");
	
		 req.flash('message','Successfully Created');	
                  res.redirect('back');
				
			}else{

			var ArrData=data.modulesId;
		if(ArrData){
		var i=0;
		ArrData.forEach(function(element) {


			if(data.permission[i]=='isDelete'){
				var dataisView ='Y';
				var dataisCreate ='Y';
				var dataisUpdate ='Y';
				var dataisDelete ='Y';
			}
			else if(data.permission[i]=='isUpdate'){
				var dataisView ='Y';
				var dataisCreate ='Y';
				var dataisUpdate ='Y';
				var dataisDelete ='N';
			}
			else if(data.permission[i]=='isCreate'){
				var dataisView ='Y';
				var dataisCreate ='Y';
				var dataisUpdate ='N';
				var dataisDelete ='N';
			}
			else if(data.permission[i]=='isView'){
				var dataisView ='Y';
				var dataisCreate ='N';
				var dataisUpdate ='N';
				var dataisDelete ='N';
			}

			

				models.modules_role.update({ 
				modulesId: data.modulesId[i], 
				roleId:data.roleId,
				isView:dataisView,
				isCreate:dataisCreate,
				isUpdate:dataisUpdate,
				isDelete:dataisDelete,
				//createdBy:1,
				updatedBy:data.user_id
				 },{where:{id:id}})	


		i++;
		}, this);
		}
						
						//return res.send('user created');
                   req.flash('message','Successfully Updated');	
                  res.redirect('back');
						
           
				
		   }

		   }
		});
	
	
};


exports.delete = function(req, res, next) {
	models.modules_role.destroy({ 
				 where:{
					 id:req.params.id
				 }
				 }).then(function(modulesrole) {
	
						
						//return res.send('user created');
                  return res.render('superpos/modulesrole/list');
						
           
				})

};


exports.deleteModulesrole = function(req, res, next) {
	var data = req.body;
	////console.log(req.body);
	//return res.send(req.body);
	var id = req.params.id;
	
	
				
			models.modules_role.destroy(
				
				 {where:{id:id}}).then(function(modulesrole) {
	
					//req.flash('info', 'you must enter your username and password to login');	
						//return res.send(req.baseUrl);
                  //return res.redirect('superpos/roles/addedit/'+id);
				
				  req.flash('message','Successfully Deleted');
           res.redirect('back');
				});	
				
				
				
	
	
	
};
