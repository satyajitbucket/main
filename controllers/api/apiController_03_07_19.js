﻿
"using strict";
var helpers = require('../../helpers/helper_functions.js');
var models = require('../../models');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
var bcrypt = require('bcrypt-nodejs');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var fs = require('file-system');
var path = require('path');
var config = require('../../config/config.json');
// For Mail Send Through MailGun
const emailConfig = require('../../config/email-config')();
// var API_KEY = 'd81fb7d3e5e5947df21a8ab0868288d7-f45b080f-70ae6080';
// var API_KEY = 'key-7813bdd576b3f7642c38a07069041745';// Sumita's Credential
// var DOMAIN = 'sandbox8011640b552e49dc9ace92d39b9bb058.mailgun.org';
// var DOMAIN = 'sandbox009e0d4e4a234d0bbc351b594357b278.mailgun.org';//Sumita's Credential 
const mailgun = require('mailgun-js')(emailConfig);
// var mailgun = require('mailgun-js')({apiKey: API_KEY, domain: DOMAIN});
var crypt = require('./crypt'); //Use crypt.js
var crypto = require('crypto');
var sms_controller = require('../sms/smsController');

function genchecksum(params, key, cb) {
    var flag = params.refund ? true : false;
    var data = params.toString();

    crypt.gen_salt(4, function(err, salt) {
        var sha256 = crypto.createHash('sha256').update(data + salt).digest('hex');
        var check_sum = sha256 + salt;
        var encrypted = crypt.encrypt(check_sum, key);
        if (flag) {
            params.CHECKSUM = encodeURIComponent(encrypted);
            params.CHECKSUM = encrypted;
        } else {
            params.CHECKSUMHASH = encodeURIComponent(encrypted);
            params.CHECKSUMHASH = encrypted;
        }
        cb(undefined, params);
    });
}


var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});


exports.emailCheck = function(req,res,next){ 
    console.log("pppppppppppppppp")
    var cc = 'll';
    promise1.then(function(value) {
        console.log("ooooooooooooooo");
        console.log(value.length);
        // cc = value;
    
    });
    console.log(cc);
    // return res.send(cccc);
    // return new Promise((resolve, reject) => {
    //     var data = {
    //         from: 'Grocery User <me@samples.mailgun.org>',
    //         to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
    //         subject: 'Password Reset',
    //         html: '<!DOCTYPE html>'+
    //         '<html>'+    
    //         '<head>'+
    //         '</head>'+    
    //         '<body>'+
    //             '<div>'+
    //                 '<p>Hello Sir/Madam,</p>'+
    //                 '<p>Thank you for using One Tap Grocery password reset wizard.</p>'+  
    //                 '<p>For any kind of query reach us at: <br />'+
    //                 'Mail: grocery@gmail.com<br />'+
    //                 'Phone: +91 9232456754<br />'+
    //                 'Have a nice shopping.</p>'+
    //                 '<p>Thanks.</p>'+
    //                 '<p>Regards,<br />'+
    //                 'One Tap Grocery<br />'+
    //                 'Midnapore</p>'+								
    //             '</div>'+       
    //         '</body>'+    
    //         '</html>' 
    //     };
    //     mailgun.messages().send(data, function (error, body) {
    //         if (error) {
    //             return reject(error);
    //         }
    //         return resolve(res.send({ status:200,message: "success" }));
    //     });
    // });
}


exports.dashboard = function(req, res, next){
    console.log(req.headers)
    var token= req.headers['token'];
    var today =  js_yyyy_mm_dd_hh_mm_ss();
    //console.log(today)SELECT * FROM `order`ORDER BY `order`.`createdAt` DESC LIMIT 10
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            sequelize.query("SELECT COUNT(*) as totalData FROM `order` WHERE `createdAt` >= '"+today+"' AND `createdAt` < ('"+today+"' + INTERVAL 1 DAY)",{ type: Sequelize.QueryTypes.SELECT }) 
            .then(function (order) {
                sequelize.query("SELECT SUM(grand_total) as todayTotalAmound FROM `order` WHERE `createdAt` >= '"+today+"' AND `createdAt` < ('"+today+"' + INTERVAL 1 DAY)",{ type: Sequelize.QueryTypes.SELECT }) 
                .then(function (todayTotalAmound) {
                    sequelize.query("SELECT SUM(grand_total) as totalAmound FROM `order`",{ type: Sequelize.QueryTypes.SELECT }) 
                    .then(function (totalAmound) {
                        sequelize.query("SELECT * FROM `order`ORDER BY `order`.`createdAt` DESC LIMIT 10",{ type: Sequelize.QueryTypes.SELECT }) 
                        .then(function (newOrder) {
                            sequelize.query("SELECT COUNT(*) as numberOfProduct, product.title, product.type,product.price FROM order_item LEFT JOIN product ON order_item.product_id = product.id GROUP BY order_item.product_id ORDER BY numberOfProduct DESC LIMIT 10",{ type: Sequelize.QueryTypes.SELECT }) 
                            .then(function (product) {
                            //console.log(order)
                                res.status(200).send({ status:200,order:order,todayTotalAmound:todayTotalAmound,totalAmound:totalAmound,newOrder:newOrder,product:product });
                            });    
                        });    
                    });
                });
            });
        }	
    });
}

function js_yyyy_mm_dd_hh_mm_ss () {  
    now = new Date(); 
    year = "" + now.getFullYear();   
    month = "" + (now.getMonth() + 1); 
    if (month.length == 1) { month = "0" + month; }   day = "" + now.getDate(); 
    if (day.length == 1) { day = "0" + day; }   hour = "" + now.getHours(); 
    if (hour.length == 1) { hour = "0" + hour; }   minute = "" + now.getMinutes(); 
    if (minute.length == 1) { minute = "0" + minute; }   second = "" + now.getSeconds(); 
    if (second.length == 1) { second = "0" + second; }   
    return year + "-" + month + "-" + day + "  00:00:00" ;

}

exports.getbanner = function(req, res) {      
   // var token=req.headers['token'];   
  var slug=req.params.slug;   
  //jwt.verify(token, SECRET, function(err, decoded) {    
          //if (err) {                        
// res.json('Invalid Token');      
                  // }else{                 
    //res.json(decoded);        
             sequelize.query("SELECT * FROM modules WHERE slag="+"'"+slug+"'",{ type: Sequelize.QueryTypes.SELECT })  
                   .then(function(bannerdata){   
                       res.json(bannerdata);              
    })         .catch(function(error) {         
    return res.send(error);                     
   });              
    //}  
   //}); 
};


exports.getDropdown = function(req, res) {      
    // var token=req.headers['token'];   
   var slug=req.params.slug;   
   //jwt.verify(token, SECRET, function(err, decoded) {    
           //if (err) {                        
 // res.json('Invalid Token');      
                   // }else{                 
     //res.json(decoded);        
              sequelize.query("SELECT select_option.id as optionID, select_option.optionLabel as optionLabel, dropdown_settings.identifier as dropIdentifier FROM select_option LEFT JOIN dropdown_settings on select_option.dropdownId = dropdown_settings.id where dropdown_settings.identifier="+"'"+slug+"'",{ type: Sequelize.QueryTypes.SELECT })  
                    .then(function(bannerdata){   
                        res.json(bannerdata);              
     })         .catch(function(error) {         
     return res.send(error);                     
    });              
     //}  
    //}); 
 };


exports.getcalltoaction = function(req, res) {      
   // var token=req.headers['token'];   
  var slug=req.params.slug;   
  //jwt.verify(token, SECRET, function(err, decoded) {    
          //if (err) {                        
// res.json('Invalid Token');      
                  // }else{                 
    //res.json(decoded);        

             sequelize.query("SELECT call_to_action.* FROM call_to_action WHERE FIND_IN_SET((select id FROM modules WHERE slag='"+slug+"'),moduleId)",{ type: Sequelize.QueryTypes.SELECT })  
                   .then(function(calltoactiondata){   
                       res.json(calltoactiondata);              
    })         .catch(function(error) {         
    return res.send(error);                     
   });              
    //}  
   //}); 
};

exports.index = function(req, res) {
    var token = req.headers['token'];	
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });  
    jwt.verify(token, SECRET, function(err, decoded) {	 
        if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
        models.users.findAll().then(function(users) {
            if (users.length) {
				//res.status(200).send(decoded);
				res.status(200).send({ status:200,message: "successfully login",users:users, token: token });
				//return res.json(users);               
            }           
        })
        .catch(function(error) {
			return res.send(error);          
        });    
    });   
};

exports.userlogin = function(req, res) {
    // res.status(200).send(req.body.data);    
    models.admin_user.findOne({ where: {'email':req.body.data.email} })
    .then(function(users) {
        //res.send(users);
        if(users!=null){
            user = users.toJSON();	   
            if(!bcrypt.compareSync(req.body.data.password, user.password)) {		   
                res.status(200).send({ message: "password wrong" });
            } else {	  
                if (req.body.data.email == users.email) {              
                var token =    jwt.sign({users}, SECRET, { expiresIn: 18000 });
                res.status(200).send({ status:200,message: "success", token: token,userdetail:users });                    
                }else{				
                res.status(200).send({ message: "No user found" });
                }            
            }//password check end                 
        }else{                
            res.status(200).send({ message: "No user found" });
        }    
    })
    .catch(function(error) {
        return res.send(error);        
    });   
};

// exports.login = function(req, res) {
//     if(req.body.data.phone && req.body.data.phone!=''){
//         models.users.findOne({ where: {'phone':req.body.data.phone} })
//         .then(function(users) {
//             //console.log(users);
//             if(users!=null){
//                 user = users.toJSON();        
//                 if(!bcrypt.compareSync(req.body.data.password, user.password)) {
//                     res.status(200).send({ success: false, message: "password wrong" });
//                 } else {	  
//                     if (req.body.data.phone == users.phone) {              
//                     var token =    jwt.sign({users}, SECRET, { expiresIn: 18000 });
//                     res.status(200).send({ status:200, success: true, message: "successfully login", token: token,userdetail:users });                    
//                     }else{				
//                     res.status(200).send({ success: false, message: "No user found" });
//                     }                
//                 }//password check end            
                
//             }else{				
//                 res.status(200).send({success: false, message: "No user found" });
//             }  

//             //////////////////////
//             // if(users!=null){
//             //     if (req.body.data.phone == users.phone && req.body.data.password == users.password) {
                    
//             //         var token =    jwt.sign({users}, SECRET, { expiresIn: 18000 });
//             //         res.status(200).send({ status:200,message: "successfully login", token: token });
                        
//             //     }else{
                    
//             //         res.status(200).send({ message: "No user found" });
//             //     }
//             // }else{                
//             //     res.status(200).send({ message: "No user found" });
//             // }
//         })
//         .catch(function(error) {
//             return res.send(error);
            
//         });
//     }else if(req.body.data.email && req.body.data.email!=''){
//         models.users.findOne({ where: {'email':req.body.data.email} })
//         .then(function(users) {
//             //console.log(users);
//             if(users!=null){
//                 user = users.toJSON();        
//                 if(!bcrypt.compareSync(req.body.data.password, user.password)) {
//                     res.status(200).send({success: false, message: "password wrong" });
//                 } else {	  
//                     if (req.body.data.email == users.email) {              
//                         var token =    jwt.sign({users}, SECRET, { expiresIn: 18000 });
//                         res.status(200).send({ status:200,success: true, message: "successfully login", token: token,userdetail:users });                    
//                     }else{				
//                         res.status(200).send({ success: false, message: "No user found" });
//                     }                
//                 }//password check end            
                
//             }else{				
//                 res.status(200).send({ success: false, message: "No user found" });
//             }  
//         })
//         .catch(function(error) {
//             return res.send(error);
            
//         });
//     }   
// };
exports.upload= function(req,res) {
	var d = new Date();
		var n = d.getTime();
	var formnew = new formidable.IncomingForm();
		
				formnew.parse(req);

		formnew.on('fileBegin', function (name, file){
			if(file.name && file.name!=''){
				var filenewname=n+ file.name;
			file.path = __dirname + '/../../public/superpos/myimages/' +filenewname;
			return res.status(200).send({filename:filenewname});
			}
		});
	
	
}
// exports.register= function(req,res) {
	
// 		var token = req.headers['token'];
	
//   if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
  
//   jwt.verify(token, SECRET, function(err, decoded) {
	 
//     if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
//     var userdata=req.body.data;
   
//     var usernamePromise = models.users.findAll({ where: {$or: [{email: userdata.email}, {phone: userdata.phone}]}
		
		
		
// 		} );
//     return usernamePromise.then(function(result) {
// 		//console.log(result);
// 			if(result.length){
// 				 res.status(200).send({ message: "user id or phone noalready exists." });
// 			}else
// 			{
// 				 res.send(userdata);
// 			}
// 			});
// });

// };


exports.roledetails = function(req, res) {
   
    var token= req.headers['token'];
    var id=req.params.id;
    
     jwt.verify(token, SECRET, function(err, decoded) {
              if (err) {
                         res.json('Invalid Token');
                         }else{
                   
        models.role.findAll({attributes: ['name','slag'], where: {id:id}}).then(function(data) {                   
                res.json(data);
        })
        .catch(function(error) {
            return res.send(error);
          
            });
        
         }
     });
};


exports.updatenotification = function(req, res) {
   
 //  var token= req.headers['token'];
    var user_id=req.params.user_id;
    var module_id=req.params.module_id;
    //var prod_id=req.params.prod_id;
    var table_name=req.params.table_name;
    
    models.track_notification.destroy({where:{moduleId:module_id}});
    sequelize.query("select id from "+table_name+" where status='active'",{ type: Sequelize.QueryTypes.SELECT })  
                   .then(function(data){   
                      data.forEach(function(val){
                      models.track_notification.create({ 
						userId: user_id ? user_id :0,						
						moduleId: module_id ? module_id :0,
						proId: val.id
					})  
				});           
    });                      
    
    return res.send('true');
    
    
};
exports.myprojects = function(req, res) {
    var token= req.headers['token'];     
    var module_id=req.params.module_id;
    var user_id=req.params.user_id;
    // jwt.verify(token, SECRET, function(err, decoded) {
    //     if (err) {
    //     res.json('Invalid Token');
    //     }else{
    //         res.json(decoded);
            models.myactivity.findAll({attributes: ['proId'], where: {'userId':user_id,'moduleId':module_id}, raw: true }).then(function(projects){
                var exitingPGArray=[];
                projects.forEach(function(val) {                    
                    exitingPGArray.push(val.proId);
                });
                //res.json(exitingPGArray);
                exit=models.project.findAll({ where: {id:{In: exitingPGArray}}})
                
                exit.then(function(proj){
                    res.send(exit);
                })
            })
            .catch(function(error) {
                return res.send(error);

            });
   
    //     }
    // });
}
exports.saveactivity = function(req, res) {
    var token= req.headers['token'];     
    var module_id=req.params.module_id;
    var prod_id=req.params.prod_id;
    var type=req.params.type;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
        res.json('Invalid Token');
        }else{
            if(type=='job'||type=='classified' || type=="project"){                
                models.myactivity.create({ 
                            userId: decoded.users.id ? decoded.users.id :0,						
                            moduleId: module_id ? module_id :0,
                            proId: prod_id ? prod_id :0,
                }).then(function(activity){ 
                    res.json(activity);	
                }).catch(function(error) {
                    return res.send(error);
                });
            }else if(type=='radar'){
                models.locked_on_tuner.create({ 
                    userId: decoded.users.id ? decoded.users.id :0,						
                    tunerValue: prod_id ? prod_id :0,
                    tunerCategory:'project',
                    createdBy: decoded.users.id ? decoded.users.id :0,
                    updatedBy: decoded.users.id ? decoded.users.id :0
                }).then(function(tuner){ 
                    res.json(tuner);	
                }).catch(function(error) {
                    return res.send(error);
                });
            }   
        }
    });
   // return res.send(token);
};

exports.claimowner = function(req, res) {
    var token= req.headers['token'];     
    //var user_id=req.params.user_id;
    var project_id=req.params.project_id;
    // var type=req.params.type;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
        res.json('Invalid Token');
        }else{
            //if(type=='job'||type=='classified' || type=="project"){                
                models.ownership.create({ 
                            userId: decoded.users.id ? decoded.users.id :0,						
                            projectId: project_id ? project_id :0,
                            status: 'inprogress',
                }).then(function(owner){ 
                    res.json(owner);	
                }).catch(function(error) {
                    return res.send(error);
                });
            //}
   
        }
    });
   // return res.send(token);
};


exports.jobpost = function(req, res) {
    var token= req.headers['token']; 
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
        res.json('Invalid Token');
        }else{        
            //res.json(decoded);
            jobpost=models.jobs.create({ 
                title:req.body.data.title,
                slug:req.body.data.slug,
                companyDescription:req.body.data.companyDescription,
                jobDescription:req.body.data.jobDescription,
                jobType:req.body.data.jobType,
                mobilePhone:req.body.data.mobilePhone,
                fax:req.body.data.fax,
                emailId:req.body.data.emailId,
                website:req.body.data.website,
                status:'active',
                shortDesc :'',
                companyId:'',
                location:'',
                address:'',
                publishedAt:'',
                salary:'',
                showAuthorProfile:'',
                owner:'',
                contactPerson:'',
                designation:'',
                workPhone:'',
                isFeatured:'',
                startDate:'',
                endDate:'',
                createdBy:1, 
                updatedBy:1,
            })
            jobpost.then(function(data) {                   
                //return res.send(data);
                models.job_function.create({
                    jobId:data.id,
                    functionId:req.body.jobfunction.functionId,
                    functionName:req.body.jobfunction.functionName
                }).then(function(funs){ 	
                }).catch(function(error) {
                    return res.send(error);
                });	
                models.job_industry.create({
                    jobId:data.id,
                    industryId:req.body.jobindustry.industryId,
                    industryName:req.body.jobindustry.industryName,
                }).then(function(inds){ 
                    return res.send("Done");	
                }).catch(function(error) {
                    return res.send(error);
                });	
            })
            .catch(function(error) {
                return res.send(error);
            }); 
        }
    });   
}; 

exports.adpost = function(req, res) {
    return res.send(req.body);  
}; 
exports.adwrite = function(req, res) {
    return res.send(req.body);  
};
 
exports.userdetails = function(req, res) {
    
    var token=req.query.token;
    var id=req.query.id;
    jwt.verify(token, SECRET, function(err, decoded) {
             if (err) {
                        res.json('Invalid Token');
                        }else{
                    res.json(decoded);
                models.users.findById(id).then(function(users) {                   
                res.json(users);
        })
        .catch(function(error) {
            return res.send(error);
          
            });
        
        }
    });
};
////////////////////////////////////////////Header API Start////////////////////////////////////
exports.getheader = function(req, res) {
	
	// var token= req.headers['token'];
    
    //  jwt.verify(token, SECRET, function(err, decoded) {
    //           if (err) {
    //                      res.json('Invalid Token');
    //                      }else{
    
    models.modules.findAll({attributes: ['name', 'slag','menuIcon','menuHoverIcon','homeIcon','bannerImage','bannerIcon'], where: {status:'active'},order: [
            ['sequence', 'ASC']
        ], }).then(function(data) {                   
                res.json(data);
        })
        .catch(function(error) {
            return res.send(error);
          
            });
    //          }
    // });
};
////////////////////////////////////////////Header API End//////////////////////////////////////

////////////////////////////////////////////Home page Nineicon API Start////////////////////////////////////
exports.gethomeicon = function(req, res) {
	
	var token= req.headers['token'];
    
     jwt.verify(token, SECRET, function(err, decoded) {
              if (err) {
                         res.json('Invalid Token');
                         }else{
				 
  				 
							 
     sequelize.query("SELECT modules.name,modules.relatedTableName,modules.slag,modules.menuIcon,modules.menuHoverIcon,modules.homeIcon,modules.bannerImage,modules.bannerIcon,(SELECT COUNT(*) FROM track_notification WHERE modules.id=track_notification.moduleId AND userId="+decoded.users.id+") as notification FROM modules where modules.usefor='nineicons' and modules.status='active'",{ type: Sequelize.QueryTypes.SELECT })  
                    .then(function(data){
						
						
					 res.json(data);	
						
						 
						}).catch(function(error) {
            return res.send(error);
          
            });
    
    
             }
    });
};

exports.getmodulenoti = function(req, res) {
	
	var token= req.headers['token'];
    var table_name=req.params.table_name;
    
    
     jwt.verify(token, SECRET, function(err, decoded) {
              if (err) {
                         res.json('Invalid Token');
                         }else{
							 
						
							sequelize.query("SELECT COUNT(*) as cnt FROM "+table_name+" WHERE status='active'",{ type: Sequelize.QueryTypes.SELECT })  
                    .then(function(data){
						
						res.json(data[0].cnt);
						}).catch(function(error) {
            return res.send(error);
          
            });
    
    
             }
    });
};



////////////////////////////////////////////Home page Nineicon API End//////////////////////

//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//

////////////////////////////////////////////Admin start/////////////////////////////////////

exports.addAdmin = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        req.body = fields; 
        if(!id){
            var usernamePromise = null;
            usernamePromise = models.admin_user.findOne({ where: {email: fields.email[0]} });
            return usernamePromise.then(function(result) {
                if(result){
                    req.flash('info','Email ID is Already Exists');  
                    res.redirect('back');
                }else
                {
                    var password = fields.password[0];
                    var hash = bcrypt.hashSync(password);
                    models.admin_user.create({ 
                        name: fields.name[0]?fields.name[0]:null, 
                        email:fields.email?fields.email[0]:null,
                        username:fields.username[0]?fields.username[0]:null,
                        password: hash,
                        role:fields.role?fields.role[0]:null,
                        status:fields.status[0]?fields.status[0]:null,
                        ip:fields.ip[0]?fields.ip[0]:null,
                        updatedBy:""
                    }).then(function(admin_user) {

                        req.flash('info','Successfully Created');	  
                        res.redirect('/superpos/admin/list');
                    })
                    .catch(function(error) {
                        return res.send(error);
                    });
                }
            });
        }else{
            var password = fields.password[0];
            var hash = bcrypt.hashSync(password);
            models.admin_user.update({ 
                name: fields.name[0]?fields.name[0]:null, 
                email:fields.email?fields.email[0]:null,
                username:fields.username[0]?fields.username[0]:null,
                password: hash,
                role:fields.role?fields.role[0]:null,
                status:fields.status[0]?fields.status[0]:null,
                ip:fields.ip[0]?fields.ip[0]:null,
            },{where:{id:id}}).then(function(admin_user) {
                req.flash('info','Successfully Updated');	  
                res.redirect('/superpos/admin/list');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};


	 
exports.addeditAdmin = function(req, res, next){

    var id = req.params.id;
    var existingItem = null;
    var rolesId = null;
    var token= req.headers['token'];
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            rolesId = models.role.findAll({ where: {status: 'active'}} );
            rolesId.then(function (roles) {
                if(!id){	
                    res.status(200).send({ status:200,roles: roles });
                }else{
    
                    existingItem = models.admin_user.findOne({ where: {id:id} });
                    existingItem.then(function (value) {		
                        res.status(200).send({ status:200,value: value, roles: roles });
                    });
                }
            });
        }
    });	
};

    

exports.adminList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.admin_user.findAll();
            existingItem.then(function (value) {
                res.status(200).send({ status:200,value:value });
            });
        }	
    });
}



exports.deleteAdmin = function(req, res, next) {
    
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.admin_user.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};


exports.deleteAdminmul = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {      
                models.admin_user.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 });
};

exports.SearchProfile =function(req, res) {
    console.log(req.body.typeahead);

    profile = models.admin_user.findAll({ where: {name:{
        $like:'%'+req.body.typeahead+'%'}} 
    });
    profile.then(function(admin){
        
            console.log(admin);
            res.status(200).send({admin});

       })
  
}
////////////////////////////////////////////Admin End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
///////////////////////////////////////profile start/////////////////////////////////////////////
////
// exports.addeditProfile = function(req,res,next){
//     //return true;
//     //var id = req.params.id;
   
//     var logdetails = req.session.user 
//     //return res.send(logdetails);
//     var existingItem = null;
//     if(!logdetails.id){	
//         res.status(200).send({ status:200});
//     }else{            
//         existingItem = models.admin_user.findOne({ where: {id:logdetails.id} });
//         existingItem.then(function (userDetails) {	
           	
//                res.status(200).send({ status:200,value: userDetails});
         
//         });	
//     }
// };

exports.addProfile = function(req, res, next) {
    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    var logdetails = req.session.user ;
    form.parse(req, function(err, fields, files) { 
        req.body = fields; 
    var oldPassword= fields.oldPassword ? fields.oldPassword[0] : '';
    console.log(oldPassword);
   
    var newPassword= fields.password ? fields.password[0] : '';
    console.log(newPassword);
    models.users.findOne({ where: {id:logdetails.id} })
    .then(function (users) {
        if(users){
            if(oldPassword && oldPassword!=''){
                console.log(oldPassword);   
                if(!bcrypt.compareSync(oldPassword, users.password)) {

                    req.flash('info','Old password wrong');	  
                    res.redirect('back');

                    //console.log("oldPassword");   
                    //res.status(200).send({ success: false, message: "Old password wrong" });
                } else {
                    console.log("ok");   
                    if(oldPassword == newPassword){

                        req.flash('info','New password can not be the old one');	  
                        res.redirect('back');

                        //console.log("ooooooo")
                        //res.status(200).send({ success: false, message: "New password can not be the old one " });   
                    }else{
                        console.log("ooooooo")
                        var hash = bcrypt.hashSync(fields.password);	
                        models.users.update({ 
                            firstName: fields.firstName ? fields.firstName[0]:null,
                            lastName: fields.lastName ? fields.lastName[0]:null, 
                            email:fields.email ? fields.email[0]:null,
                            phone:fields.phone ? fields.phone[0]:null,
                            password: hash,
                            createdBy : logdetails ? logdetails.id : ''   
                        },{where:{id:logdetails.id}}).then(function(users) {
                            req.flash('info','Successfully Updated');	  
                            res.redirect('back');
                        }).catch(function(error) {
                            console.log("llllllllllllllllllll");   
                            return res.send(error);
                            
                        });
                    }
                  
                }//password check end            
                
            }else{
                req.flash('info','No user found');	  
                res.redirect('back');				
                //res.status(200).send({success: false, message: "No user found" });
            }   
        }else{
            req.flash('info','No user found');	  
            res.redirect('back');				
            //res.status(200).send({success: false, message: "No user found" });
         }   

             }).catch(function(error) {
            return res.send(error);
        });
    })
};  


///
//////////////////////////////////////profile ends//////////////////////////////////////////////
//////
//////////---------------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@----------------//////////////////
///////////////////////////////////////account_setting start//////////////////////////////////
//


exports.addAccount_setting = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    var logdetails = req.session.user 
    form.parse(req, function(err, fields, files) { 
	// console.log(files)
    // return res.send(fields);
    var image =fields.uploadHiddenCropImage && fields.uploadHiddenCropImage[0] !='' ? fields.uploadHiddenCropImage[0] : fields.update_image[0];
    // if(files['image'].originalFilename!=''){
    //     console.log("ppppppppppppppppppppppppppp")
    //     image = fields.uploadHiddenCropImage ? fields.uploadHiddenCropImage[0]: '';
    // }else{
    //     console.log("kkkkkkkkkkkkkkkkkkkkkkkkk")
    //     image= fields.update_image ? fields.update_image[0] :'';
    // }
        var id = fields.update_id ? fields.update_id[0] : '';
        models.account_setting.findOne({ where: {admin_user_id:logdetails.id} })
        .then(function (value) {

        if(!value){
           
            models.account_setting.create({ 
                store_id: fields.store_id ? fields.store_id[0] : null,
                admin_user_id: logdetails.id ? logdetails.id : null,
                account_name: fields.account_name[0] ? fields.account_name[0] : null,
                site_short_name: fields.site_short_name[0]?fields.site_short_name[0]:null,
                country: fields.country[0]?fields.country[0]:null,
                state: fields.state[0]?fields.state[0]:null,
                city: fields.city?fields.city[0]:null, 
                street: fields.street[0]?fields.street[0]:null,
                zip: fields.zipcode?fields.zip:null,
                mailing_address: fields.mailing_address[0]?fields.mailing_address[0]:null,
                fax: fields.fax[0]?fields.fax[0]:null,
                website: fields.website[0]?fields.website[0]:null,
                email: fields.email[0]?fields.email[0]:null,
                mobile: fields.mobile[0]?fields.mobile[0]:null,
                status:fields.status[0]?fields.status[0]:null,
                image: image,
                createdBy : logdetails ? logdetails.id : '', 
               
                }).then(function(account_setting) {  
                    
                    req.flash('info','Successfully Created');
                    return res.redirect('back');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }else{
            
            models.account_setting.update({ 
			    store_id: fields.store_id[0]?fields.store_id[0]:null,
                account_name: fields.account_name[0] ? fields.account_name[0] : null,
                site_short_name: fields.site_short_name[0]?fields.site_short_name[0]:null,
                country: fields.country[0]?fields.country[0]:null,
                state: fields.state[0]?fields.state[0]:null,
                city: fields.city[0]?fields.city[0]:null, 
                street: fields.street[0]?fields.street[0]:null,
                zip: fields.zipcode?fields.zipcode:null,
                mailing_address: fields.mailing_address[0]?fields.mailing_address[0]:null,
                fax: fields.fax[0]?fields.fax[0]:null,
                website: fields.website[0]?fields.website[0]:null,
                email: fields.email[0]?fields.email[0]:null,
                mobile: fields.mobile[0]?fields.mobile[0]:null,
                status:fields.status[0]?fields.status[0]:null,
                image: image,
                createdBy : logdetails ? logdetails.id : '' ,
                
            },{where:{id:id}}).then(function(account_setting) {
                req.flash('info','Successfully Updated');
                return res.redirect('back');
            })
            .catch(function(error) {
                return res.send(error);
            });
    
        }
    });
    });
};


exports.fileupload = function (req,res) {
    //console.log(11151515151515145)
	var formnew = new formidable.IncomingForm();
	formnew.parse(req);
	formnew.on('fileBegin', function (name, file) {
        //console.log("llllllllllllllllllllllllllllllllll")
        console.log(file.name)
		if (file.name && file.name != '') {
			file.path = __dirname + '/../../public/superpos/myimages/' + file.name;
		}
	});	
};



exports.addeditAccount_setting = function(req, res, next){   
    var id = req.params.id;
    stores = models.stores.findAll({ where: {status:'active'} });
	stores.then(function (stores) {	
        city = models.citi.findAll({ where: {status:'active'} });
        city.then(function (citi) {	
            zipcode = models.zipcode.findAll({ where: {status:'active'} });
            zipcode.then(function (zipcode) {	
    models.account_setting.findOne({ where: {admin_user_id:id} })
    .then(function (value) {
        console.log(value);	
        // return res.send(value);
        if(value)
        res.status(200).send({ status:200,value: value,arrData:stores,arrcit:citi,arrpin:zipcode});
        else
        res.status(200).send({ status:200,value: '',arrData:stores,arrcit:citi,arrpin:zipcode});
         });	
      });
     });
   });
};



//
//////////////////////////////account_setting ends//////////////////////////////////////////////////
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
//////////////////////Roles start////////////////////////////////////////////////////////////
//
// exports.rolesList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
// 			existingItem = models.role.findAll({ where: {status: {$ne: 'archive'}} });  
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
// 		    });
//         }	
//     });
//};

// exports.addeditRoles = function(req, res, next){
    
//     var id = req.params.id;
//     var existingItem = null;
//     if(!id){	
//         res.status(200).send({ status:200 });
//     }else{            
//         existingItem = models.role.findOne({ where: {id:id} });
//         existingItem.then(function (value) {	
//           res.status(200).send({ status:200,value: value });
//         })
//     }	
// };


// exports.addRoles = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
   
//         var id = req.body.update_id;
//         var str  = req.body.name ;
//         var lower1 = str.toLowerCase();
//         var lower = lower1.replace(" ", "_").replace(/[`~!@#$%^&*()|+\-=?;:'",.<>0-9\{\}\[\]\\\/]/gi, '');
        
//     //return res.send(req.body)
       
//         if(!id){
//             var slug1= lower;
//             models.role.create({ 
//                 name: req.body.name?req.body.name:null, 
//                 slag: slug1,
//                 description: req.body.description?req.body.description:null,
// 				status:req.body.status?req.body.status:null,
//                 }).then(function(roles) {  
                     
//                     req.flash('info','Successfully Created');
//                     return res.redirect('/superpos/roles');
//                 })
//                 .catch(function(error) {
//                     return res.send(error);
//                 });
//         }else{
//             models.role.update({ 
//                 name: req.body.name?req.body.name:null, 
//                 slag: slug1,
//                 description: req.body.description?req.body.description:null,
// 				status:req.body.status?req.body.status:null,
//             },{where:{id:id}}).then(function(roles) {
//                 req.flash('info','Successfully Updated');
//                 return res.redirect('/superpos/roles');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
    
// };

// exports.deleteRoles = function(req, res, next) {
    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.role.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };


exports.RolesMulDelete = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {      
                models.role.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 });
};

exports.SearchRoles =function(req, res) {
    console.log(req.body.typeahead);

    roles = models.role.findAll({ where: {name:{
        $like:'%'+req.body.typeahead+'%'}} 
    });
    roles.then(function(role){
        // searchRoles = sequelize.query('SELECT * from role where name like "%'+req.body.typeahead+'%"')
        // searchRoles.then(function(value){
            console.log(role);
            res.status(200).send({role});

       })
   // })
}

//
////////////////////////Roles Ends///////////////////////////////////////////
//
////////////////////////////////////////////Attribute start////////////////////////////////

exports.addAttribute = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        var attributeArr=fields.optionValue;	
        if(!id){
            models.attribute.create({ 
                name: fields.name?fields.name[0]:null, 
                label: fields.label?fields.label[0]:null,
                type:fields.type?fields.type[0]:null,
                is_filterable:fields.is_filterable?fields.is_filterable[0]:null,
                is_required:fields.is_required?fields.is_required[0]:null,
                status:fields.status?fields.status[0]:null,
                }).then(function(attribute) {  
                    if(attributeArr){
                        var i=0;
                        attributeArr.forEach(function(element) {				
                            models.attribute_option.create({ 
                            attribute_id: attribute.id,
                            value: fields.optionValue[i],
                            label: fields.optionLabel[i],
                            position: fields.optionPosition[i],
                        });
                        i++;
                        }, this);
                    }  
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/attribute');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.attribute.update({ 
                name: fields.name?fields.name[0]:null, 
                label: fields.label?fields.label[0]:null,
                type:fields.type?fields.type[0]:null,
                is_filterable:fields.is_filterable?fields.is_filterable[0]:null,
                is_required:fields.is_required?fields.is_required[0]:null,
                status:fields.status?fields.status[0]:null,
            },{where:{id:id}}).then(function(attribute) {

                models.attribute_option.destroy({	where:{attribute_id: id}
				});
                if(attributeArr){
                    var i=0;
                    attributeArr.forEach(function(element) {				
                        models.attribute_option.create({ 
                        attribute_id: id,
                        value: fields.optionValue[i],
                        label: fields.optionLabel[i],
                        position: fields.optionPosition[i],
                    });
                    i++;
                    }, this);
                }  

                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/attribute');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};


exports.addeditAttribute = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    if(!id){	
        res.status(200).send({ status:200 });
    }else{            
        existingItem = models.attribute.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
            existingItem = models.attribute_option.findAll({ where: {attribute_id:id} });
			existingItem.then(function (att_value) {	
               res.status(200).send({ status:200,value: value,att_value: att_value });
            })
        });	
    }	
};



exports.attributeList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
			existingItem = models.attribute.findAll({ where: {status: {$ne: 'archive'}} });  
            existingItem.then(function (value) {
                res.status(200).send({ status:200,value:value });
		    });
        }	
    });
};



exports.deleteAttribute = function(req, res, next) {

    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.attribute.destroy({ 
                where:{id:id}
            }).then(function(attribute) {
                models.attribute_option.destroy({ where: {attribute_id:id} 
                }).then(function(att_value){
                    res.status(200).send({ status:200,att_value:att_value });
                })
            });	
        }
    });
};


exports.deleteAttributemul = function(req, res, next) {
    //var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];  
        var i=0;
        ch.forEach(function(index,element) {               
            models.attribute.destroy({
                where:{id:index} 
            })
            i++;
        }, this);
        res.status(200).send({ status:200,value:1 });
};



////////////////////////////////////////////Attribute End///////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Citi start////////////////////////////////
//
// exports.addCiti = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
        
//         if(!id){
//             models.citi.create({ 
//                 name: fields.name[0]?fields.name[0]:null, 
//                 //store_id: fields.store_id?fields.store_id[0]:null,
//                 status:fields.status[0]?fields.status[0]:null,
//                 }).then(function(citi) {  
                     
//                     req.flash('info','Successfully Created');
//                     return res.redirect('/superpos/citi');
//                 })
//                 .catch(function(error) {
//                     return res.send(error);
//                 });
//         }
//         else{
//             models.citi.update({ 
//                 name: fields.name[0]?fields.name[0]:null, 
//                 //store_id: fields.store_id[0]?fields.store_id[0]:null,
                
//                 status:fields.status[0]?fields.status[0]:null,
//             },{where:{id:id}}).then(function(citi) {
//                 req.flash('info','Successfully Updated');
//                 return res.redirect('/superpos/citi');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };


// exports.addeditCiti = function(req, res, next){
    
//     var id = req.params.id;
// 	// stores = models.stores.findAll({ where: {status:'active'} });
// 	// stores.then(function (stores) {	
//     var existingItem = null;
//     if(!id){	
//         res.status(200).send({ status:200});
//     }else{            
//         existingItem = models.citi.findOne({ where: {id:id} });
//         existingItem.then(function (value) {	
//             //existingItem = models.citi.findAll({ where: {citi_id:id} });
// //			existingItem.then(function (att_value) {	
//                res.status(200).send({ status:200,value: value });
//             //})
//         });	
//     }
// 	// });	
// };



// exports.citiList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//            existingItem = models.citi.findAll({ where: {status: {$ne: 'archive'}} });  
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
// 		    });
//         }	
//     });
// };

// exports.deleteCiti = function(req, res, next) {
    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.citi.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };

exports.deleteCitymul = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {      
                models.citi.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 });
};

//
////////////////////////////////////////////Citi ends////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Zipcode start////////////////////////////////
//
// exports.addZipcode = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         var zipcodeArr=fields.optionValue;	
//         if(!id){
//             models.zipcode.create({ 
//                 zipcode: fields.zipcode[0]?fields.zipcode[0]:null, 
//                 // store_id: fields.store_id[0]?fields.store_id[0]:null,
//                 status:fields.status[0]?fields.status[0]:null,
//                 }).then(function(zipcode) {  
                    
//                     req.flash('info','Successfully Created');
//                     return res.redirect('/superpos/zipcode');
//                 })
//                 .catch(function(error) {
//                     return res.send(error);
//                 });
//         }
//         else{
//             models.zipcode.update({ 
//                 zipcode: fields.zipcode[0]?fields.zipcode[0]:null, 
//                 // store_id: fields.store_id[0]?fields.store_id[0]:null,
                
//                 status:fields.status[0]?fields.status[0]:null,
//             },{where:{id:id}}).then(function(citi) {
//                 req.flash('info','Successfully Updated');
//                 return res.redirect('/superpos/zipcode');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };


// exports.addeditZipcode = function(req, res, next){    
//     var id = req.params.id;
//     var existingItem = null;
//     if(!id){	
//         res.status(200).send({ status:200 });
//     }else{            
//         existingItem = models.zipcode.findOne({ where: {id:id} });
//         existingItem.then(function (value) {	
//             //existingItem = models.citi.findAll({ where: {citi_id:id} });
// //			existingItem.then(function (att_value) {	
//                res.status(200).send({ status:200,value: value });
//             //})
//         });	
//     }	
// };

// exports.zipcodeList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingItem = models.zipcode.findAll({ where: {status: {$ne: 'archive'}} });  
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
// 		    });
//         }	
//     });
// };

// exports.deleteZipcode = function(req, res, next) {
    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.zipcode.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };



exports.deleteZipcodemul = function(req, res, next) {
    //var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];  
        var i=0;
        ch.forEach(function(index,element) {               
            models.zipcode.destroy({
                where:{id:index} 
            })
            i++;
        }, this);
        res.status(200).send({ status:200,value:1 });
};



//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Zipcode ends////////////////////////////////
//
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Stores start////////////////////////////////
//
// exports.addStores = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         var stores_cityArr=fields.stores_city_id ;	
//         var stores_zipcodeArr=fields.stores_zipcode_id ;

//         var str = fields.name ? fields.name[0] : '';
//         //return res.send(typeof str);
//         var storesName= str.replace(" ", "").substr(0, 3).toUpperCase();
//         //return res.send(storesName);
//         var storesID = storesName;
//         //return res.send(storesID); 
        

//         if(!id){
//             var i=0;
            

//             models.stores.create({ 
// 			    name: fields.name?fields.name[0]:null, 
//                 //store_id: Store_ID,
//                 description: fields.description?fields.description[0]:null,
// 				address: fields.address?fields.address[0]:null, 
// 				//pin: fields.pin?fields.pin:null, 
// 				ph: fields.ph?fields.ph[0]:null, 
// 				persion: fields.persion?fields.persion[0]:null, 
// 				delivery: fields.delivery?fields.delivery[0]:null, 
// 				//type: fields.type[0]?fields.type[0]:null, 
//                 status:fields.status?fields.status[0]:null,
//                 }).then(function(stores) { 
//                     var newStores_id= storesID +stores.id;
//                     //  return res.send(newStores_id);
//                     models.stores.update({ 
//                         store_id: newStores_id ? newStores_id : null,                        
//                     },{where:{id:stores.id}}).then(function(stores_update) {
                     

//                     // store multicity start
//                     if(stores_cityArr){                        
//                         stores_cityArr.forEach(function(sto_city){
//                             sto_city=sto_city.split("~");
//                             sto_cityId=sto_city[0];
//                             sto_cityName=sto_city[1];
//                             models.stores_city.create({
//                                 stores_id:stores.id,
//                                 stores_city_id:sto_cityId,
//                                 stores_city_name:sto_cityName                            
//                             });		
//                         }, this);
//                     }
//                     // store multicity end

//                     // store multizipcode start
//                     if(stores_zipcodeArr){ 
//                         stores_zipcodeArr.forEach(function(sto_zipcode){
//                             sto_zipcode=sto_zipcode.split("~");
//                             sto_zipcodeId=sto_zipcode[0];
//                             sto_zip=sto_zipcode[1];
//                             models.stores_zipcode.create({
//                                 stores_id:stores.id,
//                                 stores_zipcode_id:sto_zipcodeId,
//                                 stores_zipcode:sto_zip                            	
//                             });		
//                         }, this);
//                     }
//                     // store multizipcode end
//                     req.flash('info','Successfully Created');
//                     return res.redirect('/superpos/stores');
//                 }).catch(function(error) {
//                     return res.send(error);
//                 });
//             })
//             .catch(function(error) {
                
//                 console.log(error);
//                 return res.send(error);
//             });
//         }
//         else{
//             models.stores.update({ 
			   
// 				name: fields.name[0]?fields.name[0]:null, 
//                 description: fields.description[0]?fields.description[0]:null,
// 				address: fields.address[0]?fields.address[0]:null, 
// 				//pin: fields.pin?fields.pin[0]:null, 
// 				ph: fields.ph?fields.ph[0]:null, 
// 				persion: fields.persion?fields.persion[0]:null, 
// 				delivery: fields.delivery?fields.delivery[0]:null, 
// 				//type: fields.type[0]?fields.type[0]:null, 
//                 status:fields.status?fields.status[0]:null,
//             },{where:{id:id}}).then(function(stores) {

//                 // store multicity start
//                 models.stores_city.destroy({ where:{stores_id: id}
// 				});
//                 if(stores_cityArr){                        
//                     stores_cityArr.forEach(function(sto_city){
//                         sto_city=sto_city.split("~");
//                         sto_cityId=sto_city[0];
//                         sto_cityName=sto_city[1];
//                         models.stores_city.create({
//                             stores_id:id,
//                             stores_city_id:sto_cityId,
//                             stores_city_name:sto_cityName                            
//                         });		
//                     }, this);
//                 }
//                 // store multicity end

//                 // store multizipcode start
//                 models.stores_zipcode.destroy({ where:{stores_id: id}
//                 });
//                 if(stores_zipcodeArr){ 
//                     stores_zipcodeArr.forEach(function(sto_zipcode){
//                         sto_zipcode=sto_zipcode.split("~");
//                         sto_zipcodeId=sto_zipcode[0];
//                         sto_zip=sto_zipcode[1];
//                         models.stores_zipcode.create({
//                             stores_id:id,
//                             stores_zipcode_id:sto_zipcodeId,
//                             stores_zipcode:sto_zip                            	
//                         });		
//                     }, this);
//                 }
//                 req.flash('info','Successfully Updated');
//                 // res.redirect('back');
//                 return res.redirect('/superpos/stores');
//             })
//             .catch(function(error) {
                
//                 console.log(error);
//                 return res.send(error);
//             });
//         }
//     });
// };


// exports.addeditStores = function(req, res, next){
    
//     var id = req.params.id;
//     var existingItem = null;
//     existingCity = models.citi.findAll({ where: {status:'active'} });
// 	existingCity.then(function (Allcity) {
    
//     zipcode = models.zipcode.findAll({ where: {status:'active'} });
//     zipcode.then(function (zipcode) {	
//         if(!id){	
//             res.status(200).send({ status:200,city: Allcity,arrzipcode:zipcode});
//         }else{            
//             existingItem = models.stores.findOne({ where: {id:id} });
//             existingItem.then(function (value) {	
//                 existingStores_city = models.stores_city.findAll({ where: {stores_id:id} });
//     			existingStores_city.then(function (stores_city) {
                
//                     existingStores_zipcode = models.stores_zipcode.findAll({ where: {stores_id:id} });
//                     existingStores_zipcode.then(function (stores_zipcode) {
//                     res.status(200).send({ status:200,value: value,city: Allcity,arrStores_city: stores_city,arrzipcode:zipcode,arrStores_zipcode:stores_zipcode});
//                 })
//             })
//             });	
//         }
//     }); 
// }); 
 	
// };

// exports.storesList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
// 			existingItem = models.stores.findAll({ where: {status: {$ne: 'archive'}} });  
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
// 		    });
//         }	
//     });
// };

// exports.deleteStores = function(req, res, next) {
    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.stores.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };

exports.deleteStoreemul = function(req, res, next) {
   // var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];  
        var i=0;
        ch.forEach(function(index,element) {               
            models.stores.destroy({
                where:{id:index} 
            })
            i++;
        }, this);
        res.status(200).send({ status:200,value:1 });
};


//
////////////////////////////////////////////Stores ends////////////////////////////////
//

//
////////////////////////////////////////////Catagory Start//////////////////////////////////

// exports.addCategory = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
// 	//console.log(fields)
// 	//return res.send(files)
//         var id = fields.update_id[0];
       
//         if(!id){
//             models.category.create({ 
// 			    store_id: fields.store_id ? fields.store_id[0] : null,
//                 title: fields.title[0] ? fields.title[0] : null,
//                 description: fields.description[0]?fields.description[0]:null,
//                 parent_category_id: fields.parent_category_id[0]?fields.parent_category_id[0]:null,
//                 position: fields.position[0]?fields.position[0]:null,
//                 path: fields.path[0]?fields.path[0]:null, 
//                 url: fields.url[0]?fields.url[0]:null,
//                 product_count: fields.product_count[0]?fields.product_count[0]:null,
//                 include_in_menu: fields.include_in_menu[0]?fields.include_in_menu[0]:null,
//                 meta_title: fields.meta_title[0]?fields.meta_title[0]:null,
//                 meta_key: fields.meta_key[0]?fields.meta_key[0]:null,
//                 meta_description: fields.meta_description[0]?fields.meta_description[0]:null,
//                 status:fields.status[0]?fields.status[0]:null,
// 				icon:fields.uploadHiddenCropImage[0] ? fields.uploadHiddenCropImage[0] :fields.update_image[0],
//                 }).then(function(category) {  
                    
//                     req.flash('info','Successfully Created');
//                     return res.redirect('/superpos/category');
//                 })
//                 .catch(function(error) {
//                     return res.send(error);
//                 });
//         }
//         else{
//             models.category.update({ 
// 			    store_id: fields.store_id?fields.store_id[0]:null,
//                 title: fields.title[0]?fields.title[0]:null,
//                 description: fields.description[0]?fields.description[0]:null,
//                 parent_category_id: fields.parent_category_id[0]?fields.parent_category_id[0]:null,
//                 position: fields.position[0]?fields.position[0]:null,
//                 path: fields.path[0]?fields.path[0]:null, 
//                 url: fields.url[0]?fields.url[0]:null,
//                 product_count: fields.product_count[0]?fields.product_count[0]:null,
//                 include_in_menu: fields.include_in_menu[0]?fields.include_in_menu[0]:null,
//                 meta_title: fields.meta_title[0]?fields.meta_title[0]:null,
//                 meta_key: fields.meta_key[0]?fields.meta_key[0]:null,
//                 meta_description: fields.meta_description[0]?fields.meta_description[0]:null,
//                 status:fields.status[0]?fields.status[0]:null,
// 				icon:fields.uploadHiddenCropImage[0] ? fields.uploadHiddenCropImage[0] :fields.update_image[0],
//             },{where:{id:id}}).then(function(category) {
//                 req.flash('info','Successfully Updated');
//                 return res.redirect('/superpos/category');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };



// exports.addeditCategory = function(req, res, next){
    
//     var id = req.params.id;
// 	stores = models.stores.findAll({ where: {status:'active'} });
// 	stores.then(function (stores) {	
//     var existingItem = null;
//     if(!id){	
//         res.status(200).send({ status:200,arrData:stores });
//     }else{            
//         existingItem = models.category.findOne({ where: {id:id} });
//         existingItem.then(function (value) {	
//             //existingItem = models.citi.findAll({ where: {citi_id:id} });
// //			existingItem.then(function (att_value) {	
//                res.status(200).send({ status:200,value: value,arrData:stores });
//             //})
//         });	
//     }	
// 	});
// };


// exports.categoryList = function(req, res, next){

//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
// 			existingItem = models.category.findAll();
//             existingItem.then(function (value) {
//                 existingItem = models.stores.findAll();
//                 existingItem.then(function (stores) {
//                     res.status(200).send({ status:200,value:value,stores:stores });
//                 });
//             });
//         }	
//     });
// }

exports.appCategoryList = function(req, res, next){
  
    // existingItem = models.category.findAll({order: [['title', 'ASC']]})
    existingItem = models.category.findAll()
    existingItem.then(function (value) {
        if(value)
        console.log(value.length);
        if(value.length > 0){
            var resultArray = [];
            var i = 0;
            value.forEach(function(element,index){ 
                models.product.findAll({ where: {status:'active',category_id:element.id, is_configurable: {$ne: null}} })
                .then(function(product_unit) {
                    var lngh=product_unit.length;
                    console.log(lngh)
                    if(lngh > 0){
                        resultArray.push({
                            "id":element.id,
                            "store_id":element.store_id,
                            "title":element.title,
                            "parent_category_id":element.parent_category_id,
                            "position":element.position,
                            "path":element.path,
                            "url":element.url,
                            "icon":element.icon,
                            "product_count":element.product_count,
                            "include_in_menu":element.include_in_menu,
                            "meta_title":element.meta_title,
                            "meta_key":element.meta_key,
                            "meta_description":element.meta_description,
                            "status":element.status,
                            "createdAt":element.createdAt,
                            "updatedAt":element.updatedAt
                        }); 
                    }
                   
                    i++;
                    if(i== value.length){
                        resultArray.sort(function(a,b){
                        var titleA = a.title.toLowerCase(), titleB = b.title.toLowerCase();
                            if (titleA < titleB) return -1;
                            if (titleA > titleB) return 1;
                            return 0;});
                        res.status(200).send({ status:200,value:resultArray });
                    }            
                });                
            });           
        }else{
            res.status(200).send({ status:200,value:resultArray });
        }        
    }).catch(function(error) {
        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
    });       
        
}


// exports.deleteCategory = function(req, res, next) {
    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.category.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };


exports.deleteCategorymul = function(req, res, next) {
    var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];  
        var i=0;
        ch.forEach(function(index,element) {               
            models.category.destroy({
                where:{id:index} 
            })
            i++;
        }, this);
        res.status(200).send({ status:200,value:1 });
};

       
////////////////////////////////////////////Catagory End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Sub Catagory Start//////////////////////////////////////
exports.addSubcategory = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id ? fields.update_id[0] : null;        	
        if(!id){
            models.subcategory.create({ 
                store_id: fields.store?fields.store[0]:null,
                category: fields.category?fields.category[0]:null, 
                title: fields.title?fields.title[0]:null,
                status:fields.status[0]?fields.status[0]:null,
                icon:fields.uploadHiddenCropImage[0] ? fields.uploadHiddenCropImage[0] :fields.update_image[0],
            }).then(function(category) {                      
                req.flash('info','Successfully Created');
                return res.redirect('/superpos/subcategory');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
        else{
            models.subcategory.update({ 
			    store_id: fields.store?fields.store[0]:null,
                category: fields.category?fields.category[0]:null, 
				title: fields.title?fields.title[0]:null,
                status:fields.status?fields.status[0]:null,
				icon:fields.uploadHiddenCropImage[0] ? fields.uploadHiddenCropImage[0] :fields.update_image[0],
            },{where:{id:id}}).then(function(citi) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/subcategory');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};




exports.addeditSubcategory = function(req, res, next){
   
    var id = req.params.id;
	stores = models.stores.findAll({ where: {status:'active'} });
	stores.then(function (stores) {	
    category = models.category.findAll({ where: {status:'active'} });
	category.then(function (category) {	
    var existingItem = null;
    if(!id){	
        res.status(200).send({ status:200,arrstores:stores,arrcategory:category });
    }else{            
        existingItem = models.subcategory.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
            //existingItem = models.citi.findAll({ where: {citi_id:id} });
//			existingItem.then(function (att_value) {	
               res.status(200).send({ status:200,value: value,arrstores:stores,arrcategory:category });
            //})
        });	
    }	
    });
});
};


exports.subcategoryList = function(req, res, next){

    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
			existingItem = models.subcategory.findAll();
            existingItem.then(function (value) {
                res.status(200).send({ status:200,value:value });
            });
        }	
    });
}

exports.deleteSubcategory = function(req, res, next) {
    
    var id = req.params.id;
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.subcategory.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};

exports.deleteSubcategorymul = function(req, res, next) {
    var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];  
        var i=0;
        ch.forEach(function(index,element) {               
            models.subcategory.destroy({
                where:{id:index} 
            })
            i++;
        }, this);
        res.status(200).send({ status:200,value:1 });
};

////////////////////////////////////////////Sub Catagory End//////////////////////////////////////
//
//
////////////////////////////////////////////Product start/////////////////////////////////////

// exports.addProduct = function(req, res, next) {
//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         // return res.send(fields);	
//         var id = fields.update_id ? fields.update_id[0] : '';
//         var image='';
//         if(fields.uploadHiddenCropImage && fields.uploadHiddenCropImage[0]!=''){
//             image = req.app.locals.baseurl+'superpos/myimages/'+fields.uploadHiddenCropImage[0];
//         }else{
//             image =fields.update_image ? fields.update_image[0] : '';
//         }
//         var product_unitArr= fields.product_size ? fields.product_size : [];        
//         var str = fields.title ? fields.title[0] : '';
//         var productName= str.replace(" ", "").substr(0, 5).toUpperCase();
//         var skuID = productName;
//        // return res.send(skuID);        
//         if(!id || id == '')
//         {
//             models.product.create({ 
// 				//sku: fields.sku ? fields.sku[0]:null,
//                 title: fields.title ? fields.title[0]:null,
//                 description: fields.description ? fields.description[0]:null,
//                 short_description: fields.short_description ? fields.short_description[0]:null,
//                 category_id: fields.category_id ? fields.category_id[0]:null,
//                 sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
//                 price: fields.price ? fields.price[0]:null,
//                 keyword: fields.keyword ? fields.keyword[0]:null,
//                 url:fields.url ? fields.url[0]:null,
//                 spice_level:fields.spice_level ? fields.spice_level[0]:null,
//                 type: fields.type ? fields.type[0]:null,
//                 tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
// 				special_price: fields.special_price ? fields.special_price[0]:null,
//                 special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
//                 special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
//                 weight:fields.weight ? fields.weight[0]:null,
//                 inventory:fields.inventory ? fields.inventory[0]:null,
//                 //is_configurable: fields.is_configurable ? 0 :null,
//                 is_configurable: fields.is_configurable ? fields.is_configurable[0]:0,
//                 meta_title:fields.meta_title ? fields.meta_title[0]:null,
//                 meta_key:fields.meta_key ? fields.meta_key[0]:null,
//                 meta_description:fields.meta_description ? fields.meta_description[0]:null,
//                 status:fields.status ? fields.status[0]:null,
//                 image:image,
//             }).then(function(product) {
//                 var newSkuID = skuID + product.id;
//                 models.product.update({
//                     sku:newSkuID ? newSkuID : null,
//                 },{where:{id:product.id}}).then(function(product_update){
//                     if(product_unitArr &&  fields.is_configurable ){
//                         var i=0;
//                         product_unitArr.forEach(function(element) {				
//                             models.product.create({ 
//                                 title: fields.title ? fields.title[0]:null,
//                                 description: fields.description ? fields.description[0]:null,
//                                 short_description: fields.short_description ? fields.short_description[0]:null,
//                                 category_id: fields.category_id ? fields.category_id[0]:null,
//                                 sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
//                                 price: fields.product_price[i],
//                                 keyword: fields.keyword ? fields.keyword[0]:null,
//                                 url:fields.url ? fields.url[0]:null,
//                                 spice_level:fields.spice_level ? fields.spice_level[0]:null,
//                                 type: fields.type ? fields.type[0]:null,
//                                 tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
//                                 special_price: fields.product_special_price ? fields.product_special_price[i]:'',
//                                 // special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
//                                 // special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
//                                 weight:fields.product_size[i],
//                                 is_configurable: fields.is_configurable ? product.id :null,
//                                 inventory:fields.product_inventory ? fields.product_inventory[i] : '',
//                                 meta_title:fields.meta_title ? fields.meta_title[0]:null,
//                                 meta_key:fields.meta_key ? fields.meta_key[0]:null,
//                                 meta_description:fields.meta_description ? fields.meta_description[0]:null,
//                                 status:fields.product_status ? fields.product_status[i]:null,
//                                 image:image,
//                             });
//                             i++;
//                         }, this);
//                     } 
//                     req.flash('info','Successfully Created');	  
//                     return res.redirect('/superpos/product');  
//                 }).catch(function(error) {
//                     return res.send(error);
//                 });
//             })  
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }else{
//             models.product.update({ 
//                // sku: fields.sku ? fields.sku[0]:null,
//                 title: fields.title ? fields.title[0]:null,
//                 description: fields.description ? fields.description[0]:null,
//                 short_description: fields.short_description ? fields.short_description[0]:null,
//                 category_id: fields.category_id ? fields.category_id[0]:null,
//                 sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
//                 price: fields.price ? fields.price[0]:null,
//                 keyword: fields.keyword ? fields.keyword[0]:null,
//                 url:fields.url ? fields.url[0]:null,
//                 spice_level:fields.spice_level ? fields.spice_level[0]:null,
//                 type: fields.type ? fields.type[0]:null,
//                 tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
// 				special_price: fields.special_price ? fields.special_price[0]:null,
//                 special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
//                 special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
//                 weight:fields.weight ? fields.weight[0]:null,
//                 inventory:fields.inventory ? fields.inventory[0]:null,
//                 meta_title:fields.meta_title ? fields.meta_title[0]:null,
//                 meta_key:fields.meta_key ? fields.meta_key[0]:null,
//                 meta_description:fields.meta_description ? fields.meta_description[0]:null,
//                 status:fields.status ? fields.status[0]:null,
//                 image:image,
//             },{where:{id:id}}).then(function(product) {
//                 // models.product_unit.destroy({	where:{product_id: id}
//                 // });
//                 var product_unitArr=fields.product_size ? fields.product_size : [];
//                 console.log(fields.updateUnitId)
//                 var  unique_product_unitArr = fields.updateUnitId.filter(Boolean);
//                 models.product.destroy({	
//                     where:{id: { $notIn: unique_product_unitArr }, is_configurable: id}
//                 }).then(function(deleteproduct) {
               
//                     if(product_unitArr && fields.is_configurable){
//                         var i=0;
//                         product_unitArr.forEach(function(element) {	
//                             if(fields.updateUnitId[i] && fields.updateUnitId[i] != '' ){
//                                 models.product.update({ 
//                                     title: fields.title ? fields.title[0]:null,
//                                     description: fields.description ? fields.description[0]:null,
//                                     short_description: fields.short_description ? fields.short_description[0]:null,
//                                     category_id: fields.category_id ? fields.category_id[0]:null,
//                                     sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
//                                     price: fields.product_price[i],
//                                     keyword: fields.keyword ? fields.keyword[0]:null,
//                                     url:fields.url ? fields.url[0]:null,
//                                     spice_level:fields.spice_level ? fields.spice_level[0]:null,
//                                     type: fields.type ? fields.type[0]:null,
//                                     tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
//                                     special_price: fields.product_special_price ? fields.product_special_price[i]:'',
//                                     // special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
//                                     // special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
//                                     weight:fields.product_size[i],
//                                     inventory:fields.product_inventory ? fields.product_inventory[i] : '',
//                                     meta_title:fields.meta_title ? fields.meta_title[0]:null,
//                                     meta_key:fields.meta_key ? fields.meta_key[0]:null,
//                                     meta_description:fields.meta_description ? fields.meta_description[0]:null,
//                                     status:fields.product_status ? fields.product_status[i]:null,
//                                     image:image,
//                                 },{where:{id:fields.updateUnitId[i]}})
//                             }else{
//                                 models.product.create({ 
//                                     title: fields.title ? fields.title[0]:null,
//                                     description: fields.description ? fields.description[0]:null,
//                                     short_description: fields.short_description ? fields.short_description[0]:null,
//                                     category_id: fields.category_id ? fields.category_id[0]:null,
//                                     sub_category_id: fields.sub_category_id ? fields.sub_category_id[0]:null,
//                                     price: fields.product_price[i],
//                                     keyword: fields.keyword ? fields.keyword[0]:null,
//                                     url:fields.url ? fields.url[0]:null,
//                                     spice_level:fields.spice_level ? fields.spice_level[0]:null,
//                                     type: fields.type ? fields.type[0]:null,
//                                     tax_class_id: fields.tax_class_id ? fields.tax_class_id[0]:null,
//                                     special_price: fields.product_special_price ? fields.product_special_price[i]:'',
//                                     // special_price_from:fields.special_price_from ? fields.special_price_from[0]:null,
//                                     // special_price_to:fields.special_price_to ? fields.special_price_to[0]:null,
//                                     weight:fields.product_size[i],
//                                     is_configurable: fields.is_configurable ? id :null,
//                                     inventory:fields.product_inventory ? fields.product_inventory[i] : '',
//                                     meta_title:fields.meta_title ? fields.meta_title[0]:null,
//                                     meta_key:fields.meta_key ? fields.meta_key[0]:null,
//                                     meta_description:fields.meta_description ? fields.meta_description[0]:null,
//                                     status:fields.product_status ? fields.product_status[i]:null,
//                                     image:image,
//                                 });
//                             }	
//                             i++;
//                         }, this);
//                     }
//                     req.flash('info','Successfully Updated');	  
//                     console.log(req.flash('info'));
//                     res.redirect('/superpos/product');   
//                 })
//                 .catch(function(error) {
//                     return res.send(error);
//                 });
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     }); 
// };



// exports.addeditProduct = function(req, res, next){    
//     var id = req.params.id;
//     var existingItem = null;
//     var existingCat = null;
//     var token= req.headers['token'];
//     // console.log(req.token)
//     // console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{       
//             category = models.category.findAll({ where: {status:'active'} });
// 	        category.then(function (category) {	
//                 taxdropdown = sequelize.query("SELECT dropdown_settings_option.id as optionID, dropdown_settings_option.option_label as optionLabel, dropdown_settings.identifier as dropIdentifier FROM dropdown_settings_option LEFT JOIN dropdown_settings on dropdown_settings_option.dropdown_id = dropdown_settings.id where dropdown_settings.identifier='tax'",{ type: Sequelize.QueryTypes.SELECT })  
//                 taxdropdown.then(function (taxData) {                               
//                     if(!id){	   
//                         res.status(200).send({ status:200, category:category, sub_category:'', taxData:taxData});
//                     }else{            
//                         existingItem = models.product.findOne({ where: {id:id} });
//                         existingItem.then(function (value) {	
//                             if(value.sub_category_id && value.sub_category_id!=''){
//                                 subcategory = models.subcategory.findAll({ where: {id: value.sub_category_id,status:'active'} });
//                                 subcategory.then(function (sub_category) {
//                                     existingproduct_unit = models.product.findAll({ where: {is_configurable:id} });
//                                     existingproduct_unit.then(function (product_unit) {
//                                         res.status(200).send({ status:200,value:value,category:category,sub_category:sub_category,prod_unit:product_unit, taxData:taxData });
//                                     }); 
//                                 }); 
//                             }else{
//                                 existingproduct_unit = models.product.findAll({ where: {is_configurable:id} });
//                                 existingproduct_unit.then(function (product_unit) {
//                                     res.status(200).send({ status:200,value:value,category:category,sub_category:'',prod_unit:product_unit, taxData:taxData });
//                                 });                         
//                             }                          
//                         });
//                     }  
//                 });               
//             });
//         }
//     });
// };	


// exports.productList = function(req, res, next){
//     var token= req.headers['token'];
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingItem = models.product.findAll({ where: {status:'active',is_configurable: 0 } });
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }	
//     });
// }

exports.appProductList = function(req, res, next){
    console.log(req.body.data);
    var categoryId= req.body.data.categoryId ? req.body.data.categoryId : '';
    //sequelize.query("SELECT *,'5' AS max_pro_availability FROM product WHERE status='active' and is_configurable=0 and category_id="+"'"+categoryId+"'",{ type: Sequelize.QueryTypes.SELECT }) 
    sequelize.query("SELECT product.*, site_settings.max_pro_availability FROM product LEFT JOIN `site_settings` ON site_settings.id = 1 WHERE product.status='active' and product.is_configurable=0 and product.category_id="+"'"+categoryId+"'",{ type: Sequelize.QueryTypes.SELECT }) 
    .then(function (value) {
        if(value)
        console.log(value.length);
        if(value.length > 0){
            var resultArray = [];
            var i = 0;
            value.forEach(function(element,index){ 
                // sequelize.query("SELECT *,'5' AS max_pro_availability FROM product WHERE status='active' and is_configurable="+"'"+element.id+"'",{ type: Sequelize.QueryTypes.SELECT })  
                sequelize.query("SELECT product.*,site_settings.max_pro_availability FROM product LEFT JOIN `site_settings` ON site_settings.id = 1 WHERE product.status='active' and product.is_configurable="+"'"+element.id+"'",{ type: Sequelize.QueryTypes.SELECT })  
                // models.product.findAll({ where: {status:'active',is_configurable:element.id} })
                .then(function(product_unit) {
                    console.log(product_unit)
                    resultArray.push({
                        "id":element.id,
                        "mainId":product_unit ? product_unit.id :'',
                        "title":element.title,
                        "short_description":element.short_description,
                        "description":element.description,
                        "type":element.type,
                        "size":element.weight,
                        "special_price":element.special_price,
                        "price":element.price,
                        "quantity":element.inventory,
                        "image":element.image,
                        "max_pro_availability":element.max_pro_availability,
                        "spice_level":element.spice_level,
                        "unit":product_unit ? product_unit.concat(value[index]) : value[index]
                    });    
                    i++;
                    if(i== value.length){
                        res.status(200).send({ status:200,value:resultArray });
                    }            
                });                
            });           
        }else{
            res.status(200).send({ status:200,value:resultArray });
        }        
    }).catch(function(error) {
        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
    });      
}

// exports.appProductList = function(req, res, next){
//     console.log(req.body.data);
//     var categoryId= req.body.data.categoryId ? req.body.data.categoryId : '';
//     existingItem = models.product.findAll({ where: {status:'active', category_id : categoryId } });
//     existingItem.then(function (value) {
//         if(value)
//         console.log(value.length);
//         if(value.length > 0){
//             var resultArray = [];
//             var i = 0;
//             value.forEach(function(element,index){ 
//                 models.product_unit.findAll({ where: {product_id:element.id} })
//                 .then(function(product_unit) {
//                     // console.log(product_unit)
//                     resultArray.push({
//                         "id":element.id,
//                         "mainId":product_unit ? product_unit[0].id :'',
//                         "title":element.title,
//                         "short_description":element.short_description,
//                         "description":element.description,
//                         "type":element.type,
//                         "size":element.weight,
//                         "special_price":element.special_price,
//                         "price":element.price,
//                         "quantity":element.inventory,
//                         "image":element.image,
//                         "spice_level":element.spice_level,
//                         "unit":product_unit ? product_unit : ''
//                     });    
//                     i++;
//                     if(i== value.length){
//                         res.status(200).send({ status:200,value:resultArray });
//                     }            
//                 });                
//             });           
//         }else{
//             res.status(200).send({ status:200,value:resultArray });
//         }        
//     }).catch(function(error) {
//         res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
//     });       
// }


// exports.deleteProduct = function(req, res, next) {    
//     var id = req.params.id;
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.product.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 models.product_unit.destroy({ 
//                     where:{product_id:id}
//                 })
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };

exports.deleteProductmul = function(req, res, next) {
    var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];
    //console.log(token)
    // jwt.verify(token, SECRET, function(err, decoded) {
    //     if (err) {
    //         console.log('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb'); 
    //         res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
    //     }else{ 
            //console.log(ch);   
            var i=0;
            ch.forEach(function(index,element) {               
                models.product.destroy({
                    where:{id:index} 
                })
                models.product_unit.destroy({ 
                    where:{product_id:index}
                })
                i++;
            }, this);
            // for(i=0;i<ch.length;i++)
            // {
            //     //console.log(i); 
            //     models.product.destroy({
            //         where:{id:ch[i]} 
            //     }).then(function(value){
                    
            //     });
            // }
            res.status(200).send({ status:200,value:1 });
        //}
    // });  
};
////////////////////////////////////////////Product End/////////////////////////////////////
//
//
///@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@///
////////////////////////////////////////////User Start//////////////////////////////////////

// exports.deleteUsers = function(req, res, next) {
    
//     var id = req.params.id;
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.users.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };



// exports.usersList = function(req, res, next){
	  
//     var arrData = null;
//     var token= req.headers['token'];
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
// 	        existingItem = models.users.findAll({ where: {status: {$ne: 'archive'}} });  
//             existingItem.then(function (value) {
// 		        res.status(200).send({ status:200,value:value });
//             });
//         }
//     });
// };

// exports.usersUqEmail = function(req, res, next){
//     console.log(req.body);
//     var Email=req.body.email ? req.body.email:'';

//     models.users.findAll({where:{email:Email} }).then(function(users){
//         if (users.length==0) {
//         //   if(users[0].Email){
//             //   console.log(result);
//             //   console.log('Email already exists, Email: ' + Email);  
//             //   res.status(200).send({data:{success:false,value:users},errNode:{errMsg:"",errCode:"1"}});                         
//             //  }else{
//                 res.status(200).send({data:{success:true,value:users},errNode:{errMsg:"",errCode:"0"}});  
//             //  }                                    
//             //  var err = new Error();
//             // err.status = 310;
//             // return (err);

//         }else{
//             res.status(200).send({data:{success:false,value:users},errNode:{errMsg:"",errCode:"1"}}); 
//         }
//     })
// }
// exports.addeditUser = function(req, res, next){
	 
//     var id = req.params.id;
//     var existingItem = null;
//     var rolesId = null;
//     var token= req.headers['token'];
    
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             rolesId = models.role.findAll({ where: {status: 'active'},order: [['name', 'ASC']]} );
//             rolesId.then(function (roles) {
//                 if(!id){	
//                     res.status(200).send({ status:200,roles: roles });
// 	            }else{
// 		            existingItem = models.users.findOne({ where: {id:id} });
// 			        existingItem.then(function (value) {
//                         existingPhone = models.users_phone.findAll({ where: {user_id:id} });
//                         existingPhone.then(function (users_phone) {
//                             existingEmail = models.users_email.findAll({ where: {user_id:id} });
//                             existingEmail.then(function (users_email) {
//                                 existingAddress = models.users_address.findAll({ where: {user_id:id} });
//                                 existingAddress.then(function (users_address) {		
//                                     res.status(200).send({ status:200,value:value, roles: roles,phone:users_phone,email:users_email,address:users_address, });
//                                 });    
//                             });
//                         });        
// 		            });
//                 }
//             });
//         }
//     });	
// };


// exports.addUser = function(req, res, next) {

//     //return res.send(req.body.fields);

//     var d = new Date();
//     var n = d.getTime();
//     var logdetails = req.session.user 
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         var user_phoneArr=fields.user_phone_no;
//         var user_emailArr=fields.user_email_id;
//         var user_addressArr=fields.user_first_name;

//         var image=null;

//         if(files.image[0].originalFilename!=''){	
//             image = req.app.locals.baseurl+'superpos/myimages/'+files.image[0].originalFilename;
//         }else{
//             image =fields.update_image[0];
//         }	

//         //var image =fields.uploadHiddenCropImage && fields.uploadHiddenCropImage[0] !='' ? fields.uploadHiddenCropImage[0] : fields.update_image[0];
//         req.body = fields; 
//             if(!id){

//                 var password = fields.password[0];
//                 var hash = bcrypt.hashSync(password);
//                 models.users.create({ 
//                     firstName: fields.firstName ? fields.firstName[0]:null, 
//                     lastName: fields.lastName ? fields.lastName[0]:null,
//                     email:fields.email ? fields.email[0]:null,
//                     username:fields.email ? fields.email[0]:null,
//                     password: hash,
//                     phone:fields.phone ? fields.phone[0]:null,
// 					//store_id:fields.store_id[0]?fields.store_id[0]:null,
//                     image:image?image:null,
//                     //role:fields.role[0]?fields.role[0]:null,
//                     //lat:fields.lat[0]?fields.lat[0]:null,
//                     //long:fields.long[0]?fields.long[0]:null,
//                     address:fields.address ? fields.address[0]:null,
//                     postCode:fields.postCode ? fields.postCode[0]:null,
//                     country:fields.country ? fields.country[0]:null,
//                     location:fields.location ? fields.location[0]:null,
//                     //description:fields.description[0]?fields.description[0]:null,
//                     status:fields.status ? fields.status[0]:null,
//                     createdBy : logdetails ? logdetails.id : '' 
//                     }).then(function(users) {

                            
//                         if(user_phoneArr){
//                             var j=0;

//                             var isprimephone= fields.isprimephone ? fields.isprimephone[0].split(",") : [];

//                             user_phoneArr.forEach(function(users_phone) {				
//                                 models.users_phone.create({ 
//                                 user_id: users.id,
//                                 user_phone_no: fields.user_phone_no[j],
//                                 is_prime_phone: isprimephone[j],
//                             });
//                             j++;
//                             }, this);
//                         } 
    
//                         if(user_emailArr){
//                             var k=0;

//                             var isprimeemail= fields.isprimeemail ? fields.isprimeemail[0].split(",") : [];

//                             user_emailArr.forEach(function(users_email) {				
//                                 models.users_email.create({ 
//                                 user_id: users.id,
//                                 user_email_id: fields.user_email_id[k],
//                                 is_prime_email: isprimeemail[k],
//                             });
//                             k++;
//                             }, this);
//                         } 

//                         if(user_addressArr){
                      
//                             var i=0;
//                             var isprimeaddress= fields.isprimeaddress ? fields.isprimeaddress[0].split(",") : [];
    
//                             user_addressArr.forEach(function(element) {				
//                                 models.users_address.create({ 
//                                 user_id: users.id,
//                                 first_name: fields.user_first_name[i],
//                                 last_name: fields.user_last_name[i],
//                                 mobile: fields.user_mobile_no[i],
//                                 address: fields.user_address[i],
//                                 city: fields.user_city[i],
//                                 state: fields.user_state[i],
//                                 pin: fields.user_pin[i],
//                                 country: fields.user_country[i],
//                                 is_prime_address: isprimeaddress[i],
//                             });
//                             i++;
//                             }, this);
//                         } 

        
                    
//                     res.redirect('/superpos/users');
//                     req.flash('info','Successfully Created');	 
//                     console.log(req.flash('info'));
//                     //res.status(200).send({ status:200,users:users });
                    
//                     })
//                     .catch(function(error) {
//                         return res.send(error);
            
//                     });
//             }else{
//                 //var password = fields.password[0];
//                 //var hash = bcrypt.hashSync(password);
//                 models.users.update({ 
//                     firstName: fields.firstName ? fields.firstName[0]:null, 
//                     lastName: fields.lastName ? fields.lastName[0]:null,
//                     //email:fields.email ? fields.email[0]:null,
//                     //username:fields.email ? fields.email[0]:null,
//                     //password: hash,
//                     //phone:fields.phone ? fields.phone[0]:null,
// 					//store_id:fields.store_id[0]?fields.store_id[0]:null,
//                     image:image?image:null,
//                     //role:fields.role[0]?fields.role[0]:null,
//                     //lat:fields.lat[0]?fields.lat[0]:null,
//                     //long:fields.long[0]?fields.long[0]:null,
//                     address:fields.address ? fields.address[0]:null,
//                     postCode:fields.postCode ? fields.postCode[0]:null,
//                     country:fields.country ? fields.country[0]:null,
//                     location:fields.location ? fields.location[0]:null,
//                     //description:fields.description[0]?fields.description[0]:null,
//                     status:fields.status ? fields.status[0]:null,
                    
//                     updatedBy:logdetails ? logdetails.id : ''
//                     },{where:{id:id}}).then(function(users) {

//                         models.users_phone.destroy({where:{user_id: id}
//                         });
//                         if(user_phoneArr){
//                             var j=0;

//                             var isprimephone= fields.isprimephone ? fields.isprimephone[0].split(",") : [];

//                             user_phoneArr.forEach(function(users_phone) {				
//                                 models.users_phone.create({ 
//                                 user_id: id,
//                                 user_phone_no: fields.user_phone_no[j],
//                                 is_prime_phone: isprimephone[j],
//                             });
//                             j++;
//                             }, this);
//                         } 
        
//                         models.users_email.destroy({where:{user_id: id}
//                         });
//                         if(user_emailArr){
//                             var k=0;

//                             var isprimeemail= fields.isprimeemail ? fields.isprimeemail[0].split(",") : [];

//                             user_emailArr.forEach(function(users_email) {				
//                                 models.users_email.create({ 
//                                 user_id: id,
//                                 user_email_id: fields.user_email_id[k],
//                                 is_prime_email: isprimeemail[k],
//                             });
//                             k++;
//                             }, this);
//                         } 

//                         models.users_address.destroy({	where:{user_id: id}
//                         });
//                         if(user_addressArr){
//                             var i=0;
        
//                             var isprimeaddress= fields.isprimeaddress ? fields.isprimeaddress[0].split(",") : [];
        
//                             user_addressArr.forEach(function(element) {				
//                                 models.users_address.create({ 
//                                 user_id: id,
//                                 first_name: fields.user_first_name[i],
//                                 last_name: fields.user_last_name[i],
//                                 mobile: fields.user_mobile_no[i],
//                                 address: fields.user_address[i],
//                                 city: fields.user_city[i],
//                                 state: fields.user_state[i],
//                                 pin: fields.user_pin[i],
//                                 country: fields.user_country[i],
//                                 is_prime_address: isprimeaddress[i],
//                             });
//                             i++;
//                             }, this);
//                         } 

//                         req.flash('info','Successfully Updated');	  
//                         res.redirect('/superpos/users');
//                         // req.flash('info','Successfully Updated');	
//                         console.log(req.flash('info'));
//                     // res.status(200).send({ status:200,users:users });             

//                     })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
// 	});
// };

exports.deleteUsersmul = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {
                models.users.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 }); 
};

////////////////////////////////////////////User End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Customer Start//////////////////////////////////////


exports.addCustomer = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
	//console.log(fields)
	//return res.send(fields)
        var id = fields.update_id[0];
        var customer_addressArr=fields.address;
        var customer_phoneArr=fields.customer_phone_no;
        var customer_emailArr=fields.customer_email_id;
        var image='';
        if(fields.uploadHiddenCropImage && fields.uploadHiddenCropImage[0]!=''){
            image = req.app.locals.baseurl+'superpos/myimages/'+fields.uploadHiddenCropImage[0];
        }else{
            image =fields.update_image[0];
        }

        //var password = fields.password[0];
        //var hash = bcrypt.hashSync(password);
              
        if(!id){
            models.customer.create({ 
			    
                first_name: fields.first_name ? fields.first_name[0]:null,
                last_name: fields.last_name ? fields.last_name[0]:null,
                email: fields.email ? fields.email[0]:null,
                phone: fields.phone ? fields.phone[0]:null,
                //password: hash,
                //dob: fields.dob ? fields.dob[0]:null, 
                //doa: fields.doa ? fields.doa[0]:null,
                //gender: fields.gender ? fields.gender[0]:null,
                //group_id: fields.group_id ? fields.group_id[0]:null,
                // device: fields.device ? fields.device[0]:null,
                // city: fields.citi?fields.citi[0]:null,
                // last_visited: fields.last_visited ? fields.last_visited[0]:null,
                // total_purchase: fields.total_purchase ? fields.total_purchase[0]:null,
                // order_count: fields.order_count ? fields.order_count[0]:null,
                status:fields.status ? fields.status[0]:null,
                image:image,
                
                }).then(function(customer) { 
                    
                    if(customer_addressArr){
                      
                        var i=0;
                        var isprimeaddress= fields.isprimeaddress ? fields.isprimeaddress[0].split(",") : [];

                        customer_addressArr.forEach(function(element) {				
                            models.customer_address.create({ 
                            customer_id: customer.id,
                            //is_primary: fields.is_primary[i],
                            first_name: fields.address_first_name[i],
                            last_name: fields.address_last_name[i],
                            mobile: fields.address_mobile_no[i],
                            address: fields.address[i],
                            // street1: fields.street1[i],
                            // street2: fields.street2[i],
                            city: fields.city[i],
                            state: fields.state[i],
                            pin: fields.pin[i],
                            country: fields.country[i],
                            // lat: fields.lat[i],
                            // long: fields.long[i],
                            is_prime_address: isprimeaddress[i],
                        });
                        i++;
                        }, this);
                    } 

                    // if(customer_phoneArr){
                    //     var j=0;
                    //     var isprimephone= fields.isprimephone ? fields.isprimephone[0].split(",") : [];

                    //     customer_phoneArr.forEach(function(customer_phone) {				
                    //         models.customer_phone.create({ 
                    //         customer_id: customer.id,
                    //         customer_phone_no: fields.customer_phone_no[j],
                    //         is_prime_phone: isprimephone[j],
                    //     });
                    //     j++;
                    //     }, this);
                    // } 

                    // if(customer_emailArr){
                    //     //console.log("11")
                    //     //console.log(fields.isprimeemail)
                    //     var k=0;
                    //     var isprimeemail= fields.isprimeemail ? fields.isprimeemail[0].split(",") : [];
                    //     //console.log(isprimeemail)
                    //     customer_emailArr.forEach(function(customer_email) {                            
                    //         console.log(isprimeemail[k])				
                    //         models.customer_email.create({ 
                    //         customer_id: customer.id,
                    //         customer_email_id: fields.customer_email_id[k],
                    //         is_prime_email: isprimeemail[k],
                    //     });
                    //     k++;
                    //     }, this);
                    // } 
                     
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/customer');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.customer.update({ 

                first_name: fields.first_name ? fields.first_name[0]:null,
                last_name: fields.last_name ? fields.last_name[0]:null,
                email: fields.email ? fields.email[0]:null,
                phone: fields.phone ? fields.phone[0]:null,
                //dob: fields.dob ? fields.dob[0]:null, 
                //doa: fields.doa ? fields.doa[0]:null,
                //gender: fields.gender ? fields.gender[0]:null,
                //group_id: fields.group_id ? fields.group_id[0]:null,
                // device: fields.device ? fields.device[0]:null,
                // city: fields.citi?fields.citi[0]:null,
                // last_visited: fields.last_visited ? fields.last_visited[0]:null,
                // total_purchase: fields.total_purchase ? fields.total_purchase[0]:null,
                // order_count: fields.order_count ? fields.order_count[0]:null,
                status:fields.status ? fields.status[0]:null,
                image:image,
                
            },{where:{id:id}}).then(function(customer) {

                models.customer_address.destroy({	where:{customer_id: id}
				});
                if(customer_addressArr){
                    var i=0;

                    var isprimeaddress= fields.isprimeaddress ? fields.isprimeaddress[0].split(",") : [];

                    customer_addressArr.forEach(function(element) {				
                        models.customer_address.create({ 
                        customer_id: id,
                        //is_primary: fields.is_primary[i],
                        first_name: fields.address_first_name[i],
                        last_name: fields.address_last_name[i],
                        mobile: fields.address_mobile_no[i],
                        address: fields.address[i],
                        // street1: fields.street1[i],
                        // street2: fields.street2[i],
                        city: fields.city[i],
                        state: fields.state[i],
                        pin: fields.pin[i],
                        country: fields.country[i],
                        // lat: fields.lat[i],
                        // long: fields.long[i],
                        is_prime_address: isprimeaddress[i],
                    });
                    i++;
                    }, this);
                } 

                // models.customer_phone.destroy({	where:{customer_id: id}
				// });
                // if(customer_phoneArr){
                //     var j=0;
                //     var isprimephone= fields.isprimephone ? fields.isprimephone[0].split(",") : [];

                //     customer_phoneArr.forEach(function(customer_phone) {				
                //         models.customer_phone.create({ 
                //         customer_id: id,
                //         customer_phone_no: fields.customer_phone_no[j],
                //         is_prime_phone: isprimephone[j],
                //     });
                //     j++;
                //     }, this);
                // } 

                // models.customer_email.destroy({	where:{customer_id: id}
				// });
                // if(customer_emailArr){
                //     var k=0;
                //     var isprimeemail= fields.isprimeemail ? fields.isprimeemail[0].split(",") : [];
                //         //console.log(isprimeemail)

                //     customer_emailArr.forEach(function(customer_email) {				
                //         models.customer_email.create({ 
                //         customer_id: id,
                //         customer_email_id: fields.customer_email_id[k],
                //         //is_prime_email: fields.isprimeemail[k],
                //         is_prime_email: isprimeemail[k],
                //     });
                //     k++;
                //     }, this);
                // } 

                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/customer');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

exports.addeditCustomer = function(req, res, next){ 
    //return res.send(req)   
    var id = req.params.id;
    console.log(id)
    console.log('omn')
	citi = models.citi.findAll({ where: {status:'active'} });
	citi.then(function (citi) {	
        customer_group = models.customer_group.findAll({ where: {status:'active'} });
	    customer_group.then(function (customer_group) {
            var existingItem = null;
            if(!id){	
                res.status(200).send({ status:200,arrData:citi,arrCustomer_gr:customer_group });
            }else{            
                existingItem = models.customer.findOne({ where: {id:id} });
                existingItem.then(function (value) {	
                    existingAddress = models.customer_address.findAll({ where: {customer_id:id} });
                    existingAddress.then(function (customer_address) {
                        console.log("ooooooooooooooooooooooooooooooooooooooooooooooooooo")
                        console.log(customer_address)
                        existingPhone = models.customer_phone.findAll({ where: {customer_id:id} });
                        existingPhone.then(function (customer_phone) {
                            existingEmail = models.customer_email.findAll({ where: {customer_id:id} });
                            existingEmail.then(function (customer_email) {	
                                res.status(200).send({ status:200,value: value,arrData:citi,arrCustomer_gr:customer_group,address:customer_address,phone:customer_phone,email:customer_email  });
                            });
                        });
                    });
                });	
            }	
        });
    });    
};

exports.customerList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
			existingItem = models.customer.findAll();
            existingItem.then(function (value) {
                res.status(200).send({ status:200,value:value });
		    });
        }	
    });
};


exports.deleteCustomer = function(req, res, next) {
    
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.customer.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};


exports.appCustomerAdd = function(req, res, next) {
        var id = req.body.data.update_id;
       
        if(!id){
            models.customer.create({ 
			    
                name: req.body.data.firstName ? req.body.data.firstName+' '+req.body.data.lastName:null,
                email: req.body.data.email ? req.body.data.email:null,
                mobile: req.body.data.mobile ? req.body.data.mobile:null,
                password: req.body.data.password ? req.body.data.password:null,
                dob: req.body.data.dob ? req.body.data.dob:null, 
                doa: req.body.data.doa ? req.body.data.doa:null,
                gender: req.body.data.gender ? req.body.data.gender:null,
                group_id: req.body.data.group_id ? req.body.data.group_id:null,
                device: req.body.data.device ? req.body.data.device:null,
                city: req.body.data.citi ? req.body.data.citi:null,
                last_visited: req.body.data.last_visited ? req.body.data.last_visited:null,
                total_purchase: req.body.data.total_purchase ? req.body.data.total_purchase:null,
                order_count: req.body.data.order_count ? req.body.data.order_count:null,
                status:req.body.data.status ? req.body.data.status:'active',
                //image:req.body.data.uploadHiddenCropImage  ?  req.body.data.uploadHiddenCropImage :req.body.data.update_image,
                }).then(function(customer) { 

                    models.customer_address.create({ 
                        customer_id: customer.id,
                        mobile: req.body.data.mobile ? req.body.data.mobile:null,
                        street1: req.body.data.address ? req.body.data.address:null,
                        street2: req.body.data.street2 ? req.body.data.street2:null,
                        city: req.body.data.city ? req.body.data.city:null,
                        state: req.body.data.state ? req.body.data.state:null,
                        pin: req.body.data.postCode ? req.body.data.postCode:null,
                        country: req.body.data.country ? req.body.data.country:null,
                        lat: req.body.data.lat ? req.body.data.lat:null,
                        long: req.body.data.long ? req.body.data.long:null,
                        is_prime_address: req.body.data.is_prime_address ? req.body.data.is_prime_address:1,
                    });	

                    models.customer_phone.create({ 
                        customer_id: customer.id,
                        customer_phone_no: req.body.data.mobile ? req.body.data.mobile:null,
                        is_prime_phone: req.body.data.is_prime_phone ? req.body.data.is_prime_phone:1,
                    });	

                    models.customer_email.create({ 
                        customer_id: customer.id,
                        customer_email_id: req.body.data.email ? req.body.data.email:null,
                        is_prime_email: req.body.data.is_prime_email ? req.body.data.is_prime_email:1,
                    });

                    res.status(200).send({ status:200, success: true, message: "success", });
                })
                .catch(function(error) {
                    return res.send( {success: false, error});
                });
                
        }
        else{
            models.customer.update({ 

                name: req.body.data.firstName ? req.body.data.firstName+' '+req.body.data.lastName:null,
                email: req.body.data.email ? req.body.data.email:null,
                mobile: req.body.data.mobile ? req.body.data.mobile:null,
                password: req.body.data.password ? req.body.data.password:null,
                dob: req.body.data.dob ? req.body.data.dob:null, 
                doa: req.body.data.doa ? req.body.data.doa:null,
                gender: req.body.data.gender ? req.body.data.gender:null,
                group_id: req.body.data.group_id ? req.body.data.group_id:null,
                device: req.body.data.device ? req.body.data.device:null,
                city: req.body.data.citi ? req.body.data.citi:null,
                last_visited: req.body.data.last_visited ? req.body.data.last_visited:null,
                total_purchase: req.body.data.total_purchase ? req.body.data.total_purchase:null,
                order_count: req.body.data.order_count ? req.body.data.order_count:null,
                status:req.body.data.status ? req.body.data.status:'active',
                image:req.body.data.uploadHiddenCropImage  ?  req.body.data.uploadHiddenCropImage :req.body.data.update_image,
            },{where:{id:id}}).then(function(customer) {

                models.customer_address.destroy({	where:{customer_id: id}
				});		
                models.customer_address.create({ 
                    customer_id: id,
                    mobile: req.body.data.mobile ? req.body.data.mobile:null,
                    street1: req.body.data.address ? req.body.data.address:null,
                    street2: req.body.data.street2 ? req.body.data.street2:null,
                    city: req.body.data.city ? req.body.data.city:null,
                    state: req.body.data.state ? req.body.data.state:null,
                    pin:req.body.data.postCode ? req.body.data.postCode:null,
                    country: req.body.data.country ? req.body.data.country:null,
                    lat: req.body.data.lat ? req.body.data.lat:null,
                    long: req.body.data.long ? req.body.data.long:null,
                    is_prime_address: req.body.data.is_prime_address ? req.body.data.is_prime_address:1,
                });

                models.customer_phone.destroy({	where:{customer_id: id}
				});			
                models.customer_phone.create({ 
                    customer_id: id,
                    customer_phone_no: req.body.data.mobile ? req.body.data.mobile:null,
                    is_prime_phone: req.body.data.is_prime_phone ? req.body.data.is_prime_phone:1,
                });

                models.customer_email.destroy({	where:{customer_id: id}
				});			
                models.customer_email.create({ 
                    customer_id: id,
                    customer_email_id: req.body.data.email ? req.body.data.email:null,
                    is_prime_email: req.body.data.is_prime_email ? req.body.data.is_prime_email:1,
                });
                res.status(200).send({ status:200, success: true, message: "success", });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });
        }
    //});
};

exports.deleteCoustomermul = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {
                models.customer.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 }); 
};

////////////////////////////////////////////Customer End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//


//
////////////////////////////////////////////Customer_fav Start//////////////////////////////////////
//
exports.addCustomer_fav = function(req, res, next) {
   // return res.send(req.body);
    var d = new Date();
    var n = d.getTime();
    
        var id = req.body.update_id;
        
        if(!id){
            models.customer_fav.create({ 
                customer_id: req.body.customer?req.body.customer:null,
                store_id: req.body.store?req.body.store:null,
                
                }).then(function(customer_fav) {  
                    
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/customer_fav');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.customer_fav.update({ 
			    customer_id: req.body.customer?req.body.customer:null,
                store_id: req.body.store?req.body.store:null,
            },{where:{id:id}}).then(function(category) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/customer_fav');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    
};



exports.addeditCustomer_fav = function(req, res, next){
   
    var id = req.params.id;
	stores = models.stores.findAll({ where: {status:'active'} });
	stores.then(function (stores) {	
    customer = models.customer.findAll({ where: {status:'active'} });
	customer.then(function (customer) {	
    var existingItem = null;
    if(!id){	
        res.status(200).send({ status:200,arrstores:stores,arrcustomer:customer });
    }else{            
        existingItem = models.customer_fav.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
            //existingItem = models.citi.findAll({ where: {citi_id:id} });
//			existingItem.then(function (att_value) {	
               res.status(200).send({ status:200,value: value,arrstores:stores,arrcustomer:customer });
            //})
        });	
    }	
    });
});
};


exports.customer_favList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            stores = models.stores.findAll({ where: {status:'active'} });
	        stores.then(function (stores) {	
                customer = models.customer.findAll({ where: {status:'active'} });
                customer.then(function (customer) {	
                    existingItem = models.customer_fav.findAll();
                    existingItem.then(function (value) {
                        res.status(200).send({ status:200, value:value, stores:stores, customer:customer });
                    });
                });
            });        
        }	
    });
};


exports.deleteCustomer_fav = function(req, res, next) {
    
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.customer_fav.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};
//
////////////////////////////////////////////Customer_fav Ends//////////////////////////////////////
//
////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/////
//
/////////////////////////////////////////////Customer group api start////////////////////////////////
//
exports.addCustomer_group = function(req, res, next) {
    // return res.send(req.body);
     var d = new Date();
     var n = d.getTime();
     
         var id = req.body.update_id;
         
         if(!id){
             models.customer_group.create({ 
                label: req.body.label?req.body.label:null,
                status: req.body.status?req.body.status:null
                
                 
                 }).then(function(customer_fav) {  
                     
                     req.flash('info','Successfully Created');
                     return res.redirect('/superpos/customer_group');
                 })
                 .catch(function(error) {
                     return res.send(error);
                 });
         }
         else{
             models.customer_group.update({ 
                label: req.body.label?req.body.label:null,
                status: req.body.status?req.body.status:null
                
             },{where:{id:id}}).then(function(category) {
                 req.flash('info','Successfully Updated');
                 return res.redirect('/superpos/customer_group');
             })
             .catch(function(error) {
                 return res.send(error);
             });
         }
     
 };

exports.addeditCustomer_group = function(req, res, next){
    
    var id = req.params.id;
	
    var existingItem = null;
    if(!id){	
        res.status(200).send({ status:200});
    }else{            
        existingItem = models.customer_group.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
            //existingItem = models.citi.findAll({ where: {citi_id:id} });
//			existingItem.then(function (att_value) {	
               res.status(200).send({ status:200,value: value});
            //})
        });	
    }	

};

 exports.customer_groupList = function(req, res, next){
     //console.log(req.headers)
     var token= req.headers['token'];
     //console.log(token)
     jwt.verify(token, SECRET, function(err, decoded) {
         if (err) {
             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
         }else{
             existingItem = models.customer_group.findAll();
             existingItem.then(function (value) {
                 res.status(200).send({ status:200,value:value });
             });
         }	
     });
 };
 
 exports.deleteCustomer_group = function(req, res, next) {
    
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.customer_group.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};

exports.deleteCoustomerGroupmul = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {
                models.customer_group.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 }); 
};
 
/////////////////////////////////////////////Customer group api ends////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Customer Address Start//////////////////////////////////////

// exports.customer_address = function(req, res) {
//     //console.log(req.body);
//     var token= req.headers['token']; 
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{        
//             customer_address=models.customer_address.create({ 
//                 customer_id:req.body.data.customer_id,
//                 is_prime_address:req.body.data.is_prime_address,
//                 mobile:req.body.data.mobile,
//                 street1:req.body.data.street1,
//                 street2:req.body.data.street2,
//                 city:req.body.data.city,
//                 state:req.body.data.state,
//                 pin:req.body.data.pin,
//                 country:req.body.data.country,
//                 lat:req.body.data.lat,
//                 long:req.body.data.long, 
//             })
//             customer_address.then(function(value) { 
//                 res.status(200).send({ status:200,value:value });             
//             }).catch(function(error) {
//                 return res.send(error);
//             }); 
//         }
//      });   
// }; 


// exports.customer_addressList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingItem = models.customer_address.findAll();
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }	
//     });
// };



////////////////////////////////////////////Customer Address End////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//

////////////////////////////////////////////customer_address Start//////////////////////////////////

exports.addCustomer_address = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        // var image=null;
        // if(fields.uploadHiddenCropImage && fields.uploadHiddenCropImage[0]!=''){
        //     image = req.app.locals.baseurl+'superpos/myimages/'+fields.uploadHiddenCropImage[0];
        // }else{
        //     image =fields.update_image[0];
        // }
        
        //var prod = fields.products;
       // return res.send(prod);
        //var prod = prod.join();
        //req.body = fields; 
        if(!id)
        {
            models.customer_address.create({ 
                customer_id: fields.customer_id?fields.customer_id[0]:null, 
                is_prime_address: fields.is_prime_address?fields.is_prime_address[0]:null,
                mobile: fields.mobile?fields.mobile[0]:null, 
                street1: fields.street1?fields.street1[0]:null, 
                street2: fields.street2?fields.street2[0]:null, 
                city: fields.city?fields.city[0]:null, 
                state: fields.state?fields.state[0]:null, 
                pin: fields.pin?fields.pin[0]:null, 
                country: fields.country?fields.country[0]:null, 
                lat: fields.lat?fields.lat[0]:null, 
                long: fields.long?fields.long[0]:null,
            }).then(function(customer_address) {

                req.flash('info','Successfully Created');	  
                res.redirect('back');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }else{
            models.customer_address.update({ 
                customer_id: fields.customer_id?fields.customer_id[0]:null, 
                is_prime_address: fields.is_prime_address?fields.is_prime_address[0]:null,
                mobile: fields.mobile?fields.mobile[0]:null, 
                street1: fields.street1?fields.street1[0]:null, 
                street2: fields.street2?fields.street2[0]:null, 
                city: fields.city?fields.city[0]:null, 
                state: fields.state?fields.state[0]:null, 
                pin: fields.pin?fields.pin[0]:null, 
                country: fields.country?fields.country[0]:null, 
                lat: fields.lat?fields.lat[0]:null, 
                long: fields.long?fields.long[0]:null, 
            },{where:{id:id}}).then(function(customer_address) {
                req.flash('info','Successfully Updated');	  
                res.redirect('back');      
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};


exports.addeditCustomer_address = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    var existingPro = null;
    var existingCat = null;
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingCus = models.customer.findAll({ where: {status: 'active'} });
            existingCus.then(function (customer) {
                //existingCat = models.category.findAll();
                //existingCat.then(function (category) {
                    if(!id){	
                        res.status(200).send({ status:200,customer:customer });
                    }else{            
                        existingItem = models.customer_address.findOne({ where: {id:id} });
                        existingItem.then(function (value) {
                            var resultArray = [];
                            // var arrCat = value.products.split(",");
                            // var c = arrCat.map(Number);
                            // product.forEach(i=>{
                            //     var isSelected = 0;
                            //     if(c.indexOf(i.id) > -1){
                            //         isSelected = 1;
                            //     }
                            //     else{
                            //         isSelected = 0;
                            //     }
                                // resultArray.push({
                                //     "id":i.id,
                                //     "sku":i.sku,
                                //     "title":i.title,
                                //     "status":i.status,
                                //     "image":i.image,
                                // });
                            //});
                            res.status(200).send({ status:200,value: value,customer:customer });
                        });
                    }
                //});    
            });	
        }
    });
};


// exports.category_productList = function(req, res, next){

//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingPro = models.product.findAll();
//             existingPro.then(function (product) {
//                 existingCat = models.category.findAll();
//                 existingCat.then(function (category) {
//                     existingItem = models.customer_address.findAll();
//                     existingItem.then(function (value) {
//                         //return res.send(value);
//                         res.status(200).send({ status:200,value:value,product:product,category:category });
//                     });
//                 });
//             });        
//         }	
//     });
// }

exports.customer_addressList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingCus = models.customer.findAll();
            existingCus.then(function (customer) {
                existingItem = models.customer_address.findAll();
                existingItem.then(function (value) {
                    res.status(200).send({ status:200,value:value,customer:customer });
                });
            });    
        }	
    });
};


exports.deleteCustomer_address = function(req, res, next) {
    
    var id = req.params.id;
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.customer_address.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};


// exports.deletecustomer_addressmul = function(req, res, next) {
//     var id = req.params.id;
//     var ch= req.body.check;
//     //console.log(req.body);
//     var token= req.headers['token'];  
//         var i=0;
//         ch.forEach(function(index,element) {               
//             models.customer_address.destroy({
//                 where:{id:index} 
//             })
//             i++;
//         }, this);
//         res.status(200).send({ status:200,value:1 });
// };

       
////////////////////////////////////////////customer_address End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Order Start//////////////////////////////////////

exports.addOrder = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
	//console.log(fields)
    // return res.send(req.session.user);
    var logdetails = req.session.user 
    var id = req.body.update_id;       
        if(!id){
            models.order.create({ 			    
                store_id: req.body.store ? req.body.store:null,
                order_status: req.body.order_status?req.body.order_status:null,
                shipping_method: req.body.shipping_method?req.body.shipping_method:null,
                shipping_description: req.body.shipping_description?req.body.shipping_description:null,
                payment_method: req.body.payment_method?req.body.payment_method:null, 
                coupon_code: req.body.coupon_code?req.body.coupon_code:null,
                salesman_id : req.body.salesman_id ? req.body.salesman_id :null,
                // customer_id: req.body.customer?req.body.customer:null,
                discount_percent: req.body.discount_percent?req.body.discount_percent:null,
                discount_amount: req.body.discount_amount?req.body.discount_amount:null,
                base_grand_total: req.body.base_grand_total?req.body.base_grand_total:null,
                shipping_amount: req.body.shipping_amount?req.body.shipping_amount:null,
                cgst: req.body.cgst?req.body.cgst:null,
                sgst: req.body.sgst?req.body.sgst:null,
                igst:req.body.igst?req.body.igst:null,
                total_tax:req.body.total_tax?req.body.total_tax:null,
                grand_total:req.body.grand_total?req.body.grand_total:null,
                amount_paid:req.body.amount_paid?req.body.amount_paid:null,
                promotion:req.body.promotion?req.body.promotion:null,
                customer_name:req.body.customer_name?req.body.customer_name:null,
                customer_email:req.body.customer_email?req.body.customer_email:null,
                customer_mobile:req.body.customer_mobile?req.body.customer_mobile:null,
                street1:req.body.street1?req.body.street1:null,
                street2:req.body.street2?req.body.street2:null,
                city:req.body.city?req.body.city:null,
                state:req.body.state?req.body.state:null,
                pin:req.body.pin?req.body.pin:null,
                country:req.body.country?req.body.country:null,
                lat:req.body.lat?req.body.lat:null,
                long:req.body.long?req.body.long:null,
                gift_message:req.body.gift_message?req.body.gift_message:null,
                remote_ip:req.body.remote_ip?req.body.remote_ip:null,
                medium:req.body.medium?req.body.medium:null,
                delivery_time_slot : req.body.delivery_time_slot ? req.body.delivery_time_slot : null,                
                createdBy : logdetails ? logdetails.id : ''               
                }).then(function(order) { 
                    if(req.body.is_customer && req.body.is_customer==0){
                        models.customer.create({ 	
                            name:req.body.customer_name ? req.body.customer_name : null,
                            email:req.body.customer_email ? req.body.customer_email : null,
                            mobile:req.body.customer_mobile ? req.body.customer_mobile : null,
                            group_id: 1,
                            status:'active',
                            createdBy : logdetails ? logdetails.id : '' 
                        }).then(function(customer) { 
                            req.flash('info','Successfully Created');
                            return res.redirect('/superpos/order');
                        })
                        .catch(function(error) {
                            return res.send(error);
                        });
                    }else{
                        if(req.body.is_customer_id && req.body.is_customer_id!=''){
                            models.customer.update({ 	
                                name:req.body.customer_name ? req.body.customer_name : null,
                                email:req.body.customer_email ? req.body.customer_email : null,
                                mobile:req.body.customer_mobile ? req.body.customer_mobile : null,                               
                                updatedBy : logdetails ? logdetails.id : '' 
                            },{where:{id:req.body.is_customer_id,status: 'active'}}).then(function(customer) {
                            }).then(function(customer) { 
                                req.flash('info','Successfully Created');
                                return res.redirect('/superpos/order');
                            })
                            .catch(function(error) {
                                return res.send(error);
                            });
                        }else{
                            req.flash('info','Successfully Created');
                            return res.redirect('/superpos/order');
                        }
                        
                    }                    
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.order.update({ 
                store_id: req.body.store?req.body.store:null,
                order_status: req.body.order_status?req.body.order_status:null,
                shipping_method: req.body.shipping_method?req.body.shipping_method:null,
                shipping_description: req.body.shipping_description?req.body.shipping_description:null,
                payment_method: req.body.payment_method?req.body.payment_method:null, 
                coupon_code: req.body.coupon_code?req.body.coupon_code:null,
                salesman_id : req.body.salesman_id ? req.body.salesman_id :null,
                // customer_id: req.body.customer?req.body.customer:null,
                discount_percent: req.body.discount_percent?req.body.discount_percent:null,
                discount_amount: req.body.discount_amount?req.body.discount_amount:null,
                base_grand_total: req.body.base_grand_total?req.body.base_grand_total:null,
                shipping_amount: req.body.shipping_amount?req.body.shipping_amount:null,
                cgst: req.body.cgst?req.body.cgst:null,
                sgst: req.body.sgst?req.body.sgst:null,
                igst:req.body.igst?req.body.igst:null,
                total_tax:req.body.total_tax?req.body.total_tax:null,
                grand_total:req.body.grand_total?req.body.grand_total:null,
                amount_paid:req.body.amount_paid?req.body.amount_paid:null,
                promotion:req.body.promotion?req.body.promotion:null,
                customer_name:req.body.customer_name?req.body.customer_name:null,
                customer_email:req.body.customer_email?req.body.customer_email:null,
                customer_mobile:req.body.customer_mobile?req.body.customer_mobile:null,
                street1:req.body.street1?req.body.street1:null,
                street2:req.body.street2?req.body.street2:null,
                city:req.body.city?req.body.city:null,
                state:req.body.state?req.body.state:null,
                pin:req.body.pin?req.body.pin:null,
                country:req.body.country?req.body.country:null,
                lat:req.body.lat?req.body.lat:null,
                long:req.body.long?req.body.long:null,
                gift_message:req.body.gift_message?req.body.gift_message:null,
                remote_ip:req.body.remote_ip?req.body.remote_ip:null,
                medium:req.body.medium?req.body.medium:null,
                delivery_time_slot : req.body.delivery_time_slot ? req.body.delivery_time_slot : null,             
                updatedBy : logdetails ? logdetails.id : ''                
            },{where:{id:id}}).then(function(order) {
                if(req.body.is_customer && req.body.is_customer==0){
                    models.customer.create({ 	
                        name:req.body.customer_name ? req.body.customer_name : null,
                        email:req.body.customer_email ? req.body.customer_email : null,
                        mobile:req.body.customer_mobile ? req.body.customer_mobile : null,
                        group_id: 1,
                        status:'active',
                        createdBy : logdetails ? logdetails.id : '' 
                    }).then(function(customer) { 
                        req.flash('info','Successfully Updated');
                        return res.redirect('/superpos/order');
                    })
                    .catch(function(error) {
                        return res.send(error);
                    });
                }else{
                    if(req.body.is_customer_id && req.body.is_customer_id!=''){
                        models.customer.update({ 	
                            name:req.body.customer_name ? req.body.customer_name : null,
                            email:req.body.customer_email ? req.body.customer_email : null,
                            mobile:req.body.customer_mobile ? req.body.customer_mobile : null,                               
                            updatedBy : logdetails ? logdetails.id : '' 
                        },{where:{id:req.body.is_customer_id,status: 'active'}}).then(function(customer) {
                        }).then(function(customer) { 
                            req.flash('info','Successfully Updated');
                            return res.redirect('/superpos/order');
                        })
                        .catch(function(error) {
                            return res.send(error);
                        });
                    }else{
                        req.flash('info','Successfully Updated');
                        return res.redirect('/superpos/order');
                    }
                }                
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

exports.addeditOrder = function(req, res, next){   
    var id = req.params.id;
    // users = models.users.findAll({ where: {status:'active'} });
	stores = models.stores.findAll({ where: {status:'active'} });
	stores.then(function (stores) {	
        // customer = models.customer.findAll({ where: {status:'active'} });
        // customer.then(function (customer) {            
            //var existingItem = null;
            models.salesman.findAll()
            .then(function (salesman) {	
                orderStatus = sequelize.query("SELECT dropdown_settings_option.id as optionID, dropdown_settings_option.option_value as optionValue, dropdown_settings_option.option_label as optionLabel, dropdown_settings.identifier as dropIdentifier FROM dropdown_settings_option LEFT JOIN dropdown_settings on dropdown_settings_option.dropdown_id = dropdown_settings.id where dropdown_settings.identifier='order_status'",{ type: Sequelize.QueryTypes.SELECT })  
                shippingMethod = sequelize.query("SELECT dropdown_settings_option.id as optionID, dropdown_settings_option.option_label as optionLabel, dropdown_settings.identifier as dropIdentifier FROM dropdown_settings_option LEFT JOIN dropdown_settings on dropdown_settings_option.dropdown_id = dropdown_settings.id where dropdown_settings.identifier='shipping_method'",{ type: Sequelize.QueryTypes.SELECT })  
                paymentMethod = sequelize.query("SELECT dropdown_settings_option.id as optionID, dropdown_settings_option.option_label as optionLabel, dropdown_settings.identifier as dropIdentifier FROM dropdown_settings_option LEFT JOIN dropdown_settings on dropdown_settings_option.dropdown_id = dropdown_settings.id where dropdown_settings.identifier='payment_method'",{ type: Sequelize.QueryTypes.SELECT })  
                orderStatus.then(function (orderStatus) {
                    shippingMethod.then(function (shippingMethod) {
                        paymentMethod.then(function (paymentMethod) {
                            models.delivery_time_slot.findAll({ where:{status:'active'}, order: [['sequence', 'ASC']]
                            }).then(function (time_slot) {
                                models.category.findAll({ attributes: ['id','title','status'],where: {status:'active'} })
                                .then(function (category) {  
                                    // console.log(category);
                                    if(!id){	
                                        res.status(200).send({ status:200,arrstores:stores,arrcustomer:'', arrsalesman: salesman, arrTimeSlot: time_slot, arrOrderStatus: orderStatus, arrShippingMethod: shippingMethod, arrPaymentMethod: paymentMethod, arrCategory: category});
                                    }else{  
                                        sequelize.query("SELECT `order`.*, CONCAT(customer.first_name,' ',customer.last_name) as customerName, `customer`.`email` as `customerEmail` FROM `order` LEFT JOIN `customer` ON `order`.`customer_id`= `customer`.`id` where `order`.id ="+id,{ type: Sequelize.QueryTypes.SELECT })
                                        .then(function(value) {          
                                        // existingItem = models.order.findOne({ where: {id:id} });
                                        // existingItem.then(function (value) {                                            
                                            sequelize.query("SELECT * FROM `order_item` where `order_id` = "+value[0].id,{ type: Sequelize.QueryTypes.SELECT })
                                            .then(function (orderitems) {
                                                // return res.send(value)
                                                res.status(200).send({ status:200,value: value[0],arrstores:stores,arrcustomer:'', arrsalesman: salesman, arrTimeSlot: time_slot, arrOrderStatus: orderStatus, arrShippingMethod: shippingMethod, arrPaymentMethod: paymentMethod, arrCategory: category, arrOrderItems: orderitems});           
                                            });	
                                        });
                                    }	
                                }).catch(function(error) {
                                    return res.send(error);
                                });	
                            }).catch(function(error) {
                                return res.send(error);
                            });	
                        }).catch(function(error) {
                            return res.send(error);
                        });
                    }).catch(function(error) {
                        return res.send(error);
                    });
                }).catch(function(error) {
                    return res.send(error);
                });	
            }).catch(function(error) {
                return res.send(error);
            });	

        //});
	
    });
};

exports.orderList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(req.token)
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            // sequelize.query("SELECT `order`.*, `dropdown_settings_option`.`option_label` as `orderStatusTitle` FROM `order` INNER JOIN `dropdown_settings_option` ON `order`.`order_status`= `dropdown_settings_option`.`option_value`",{ type: Sequelize.QueryTypes.SELECT })
            sequelize.query("SELECT `order`.*, (SELECT COUNT(*) FROM order_item WHERE order_item.order_id = order.id) as `numberOfOrder`, `dropdown_settings_option`.`option_label` as `orderStatusTitle` FROM `order` LEFT JOIN `dropdown_settings_option` ON `order`.`order_status`= `dropdown_settings_option`.`option_value` ORDER BY `id` DESC",{ type: Sequelize.QueryTypes.SELECT })
            .then(function (value) {
                orderStatus = sequelize.query("SELECT dropdown_settings_option.id as optionID, dropdown_settings_option.option_value as optionValue, dropdown_settings_option.option_label as optionLabel, dropdown_settings.identifier as dropIdentifier FROM dropdown_settings_option LEFT JOIN dropdown_settings on dropdown_settings_option.dropdown_id = dropdown_settings.id where dropdown_settings.identifier='order_status'",{ type: Sequelize.QueryTypes.SELECT })  
                orderStatus.then(function (orderStatus) {
                // existingItem = models.order.findAll();
                // existingItem.then(function (value) {
                    res.status(200).send({ status:200,value:value,orderStatus:orderStatus });
                });
            });    
        }	
    });
}




exports.deleteOrder = function(req, res, next) {
     
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.order.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};

exports.customerBymobile = function(req, res, next){
    console.log(12)
    console.log(req.params.mobile);
    var mobile = req.params.mobile ? req.params.mobile : '';
    models.customer.findOne({ where: {mobile:mobile} 
            
    }).then(function(customer){ 
        if(customer){
            res.status(200).send({data:{success:true,value:customer},errNode:{errMsg:"",errCode:"0"}});
        }else{
            res.status(200).send({data:{success:false,value:customer},errNode:{errMsg:"",errCode:"0"}});
        }        
    }).catch(function(error) {
        res.status(200).send({data:{success:false,value:''},errNode:{errMsg:error,errCode:"1"}});
        // return res.send(error);
    });	

}

exports.product_Subcat_Bycategory = function(req, res, next){
    console.log(111111111)
    console.log(req.params.id);
    var id = req.params.id ? req.params.id : '';
    sequelize.query("SELECT `product`.*, `dropdown_settings_option`.`option_label`, `dropdown_settings_option`.`option_value` FROM `product` LEFT JOIN `dropdown_settings_option` ON `product`.`tax_class_id`= `dropdown_settings_option`.`id` where `product`.`category_id` = "+id,{ type: Sequelize.QueryTypes.SELECT })
    // models.product.findAll({ where: {category_id:id}             
    .then(function(product){ 
        models.subcategory.findAll({ where: {category:id}             
        }).then(function(subcategory){
            res.status(200).send({data:{success:true, product:product, subcategory:subcategory},errNode:{errMsg:"",errCode:"0"}});
        }).catch(function(error) {
            res.status(200).send({data:{success:false,value:''},errNode:{errMsg:error,errCode:"1"}});
        });	     
    }).catch(function(error) {
        res.status(200).send({data:{success:false,value:''},errNode:{errMsg:error,errCode:"1"}});
    });	

}

exports.deleteoredermulsmul = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {
                models.order.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 }); 
};


////////////////////////////////////////////Order End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Order Item Start///////////////////////////////
exports.order_item = function(req, res) {
    console.log(req.body);
    var token= req.headers['token']; 
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{        
            order_item=models.order_item.create({ 
                qty:req.body.data.qty,
                row_total:req.body.data.row_total,
            })
            order.then(function(data) {
                models.category.create({
                    store_id:req.body.data.store_id,   
                }).then(function(category){ 	
                }).catch(function(error) {
                    return res.send(error);
                });	 
                models.order.create({
                    order_id:req.body.data.order_id,
                    discount_percent:req.body.data.discount_percent,
                    discount_amount:req.body.data.discount_amount,
                    cgst:req.body.data.cgst,
                    sgst:req.body.data.sgst,
                    igst:req.body.data.igst,    
                }).then(function(order){ 	
                }).catch(function(error) {
                    return res.send(error);
                });	         
                models.product.create({
                    product_id:req.body.data.product_id,
                    name:req.body.data.name,
                    sku:req.body.data.sku,
                    description:req.body.data.description,
                    price:req.body.data.price,
                }).then(function(product){ 
                    return res.send("Done");	
                }).catch(function(error) {
                    return res.send(error);
                });	
            })
            .catch(function(error) {
                return res.send(error);
 
            }); 
        }
    });   
}; 


exports.order_itemList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(req.token)
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
              if (err) {
                res.json('Invalid Token');
              }else{
				existingItem = models.order_item.findAll();
                existingItem.then(function (value) {
                    res.status(200).send({ status:200,value:value });
             });
        }	
    });
}

////////////////////////////////////////////Order Item End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////Order Status History Start//////////////////////////


exports.order_status_historyList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(req.token)
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
              if (err) {
                res.json('Invalid Token');
              }else{
				existingItem = models.order_status_history.findAll();
                existingItem.then(function (value) {
                    //console.log(value.id)
                    res.status(200).send({ status:200,value:value });
		
             });
        }	
    });
}

exports.addOrder_status_history= function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];

        if(!id)
        {
            models.order_status_history.create({ 
                order_id: fields.order_id?fields.order_id[0]:null, 
                status: fields.status?fields.status[0]:null,
                remarks: fields.remarks?fields.remarks[0]:null, 
                is_customer_notified: fields.is_customer_notified?fields.is_customer_notified[0]:null, 
                customer_sms: fields.customer_sms?fields.customer_sms[0]:null,
            }).then(function(order_status_history) {

                req.flash('info','Successfully Created');	  
                return res.redirect('/superpos/order_status_history');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }else{
            models.order_status_history.update({ 
                order_id: fields.order_id?fields.order_id[0]:null, 
                status: fields.status?fields.status[0]:null,
                remarks: fields.remarks?fields.remarks[0]:null, 
                is_customer_notified: fields.is_customer_notified?fields.is_customer_notified[0]:null, 
                customer_sms: fields.customer_sms?fields.customer_sms[0]:null,
            },{where:{id:id}}).then(function(order_status_history) {
                req.flash('info','Successfully Updated');	  
                res.redirect('back');      
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });

    // var d = new Date();
    // var n = d.getTime();
    // var form = new multiparty.Form();
    // form.parse(req, function(err, fields, files) { 
	// //console.log(fields)
	// //return res.send(fields)
    //     var id = fields.update_id;
       
    //     if(!id){
    //         models.order_status_history.create({ 
			    
    //             order_id: fields.order_id ? fields.order_id[0] : null,
    //             status:fields.status ? fields.status[0] : null,
    //             remarks: fields.remarks ? fields.remarks[0] : null,
    //             is_customer_notified: fields.is_customer_notified ? fields.is_customer_notified[0] : null,
    //             customer_sms: fields.customer_sms ? fields.customer_sms[0] : null,
                
    //             }).then(function(customer) {  
                     
    //                 req.flash('info','Successfully Created');
    //                 return res.redirect('/superpos/order_status_history');
    //             })
    //             .catch(function(error) {
    //                 return res.send(error);
    //             });
    //     }
    //     else{
    //         models.order_status_history.update({ 

    //             order_id: fields.order_id ? fields.order_id[0] : null,
    //             status:fields.status ? fields.status[0] : null,
    //             remarks: fields.remarks ? fields.remarks[0] : null,
    //             is_customer_notified: fields.is_customer_notified ? fields.is_customer_notified[0] : null,
    //             customer_sms: fields.customer_sms ? fields.customer_sms[0] : null,
                
    //         },{where:{id:id}}).then(function(customer) {
    //             req.flash('info','Successfully Updated');
    //             return res.redirect('/superpos/order_status_history');
    //         })
    //         .catch(function(error) {
    //             return res.send(error);
    //         });
    //     }
    // });
};

exports.addeditOrder_status_history = function(req, res, next){
    
    var id = req.params.id;
	order = models.order.findAll();
	order.then(function (order) {	
        var existingItem = null;
        if(!id){	
            res.status(200).send({ status:200,arrData:order });
        }else{            
            existingItem = models.order_status_history.findOne({ where: {id:id} });
            existingItem.then(function (value) {
                res.status(200).send({ status:200,value: value,arrData:order });           
            });	
        }	
	});
};

exports.deleteOrder_status_history = function(req, res, next) {
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.order_status_history.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};


exports.order_status_historyMul= function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {      
                models.order_status_history.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 });
};


////////////////////////////////////////////Order Status History End////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
///////////////////////////////////Settings Profile API Start/////////////////////////////
//
exports.settings_profileList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
			existingItem = models.settings_profile.findAll({ where: {status: {$ne: 'archive'}} });  
            existingItem.then(function (value) {
                res.status(200).send({ status:200,value:value });
		    });
        }	
    });
};

exports.addSettings_profile = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
   
        var id = req.body.update_id;
        
        if(!id){
            models.settings_profile.create({ 
                username: req.body.username?req.body.username:null, 
                email: req.body.email?req.body.email:null,
				phone: req.body.phone?req.body.phone:null,
				skype: req.body.skype?req.body.skype:null,
				address	: req.body.address?req.body.address:null,
                status:req.body.status?req.body.status:null,
                }).then(function(settings_profile) {  
                     
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/settings_profile');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }else{
            models.settings_profile.update({ 
               username: req.body.username?req.body.username:null, 
                email: req.body.email?req.body.email:null,
				phone: req.body.phone?req.body.phone:null,
				skype: req.body.skype?req.body.skype:null,
				address	: req.body.address?req.body.address:null,
                status:req.body.status?req.body.status:null,
            },{where:{id:id}}).then(function(citi) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/settings_profile');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    
};


exports.addeditSettings_profile = function(req, res, next){
    
    var id = req.params.id;
	
    var existingItem = null;
    if(!id){	
        res.status(200).send({ status:200});
    }else{            
        existingItem = models.settings_profile.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
          
               res.status(200).send({ status:200,value: value });
           
        });	
    }	
	
};

exports.deleteSettings_profile = function(req, res, next) {
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.settings_profile.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};

exports.deleteSettings_profileMul = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {      
                models.settings_profile.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 });
};


///////////////////////////////////Settings profile API ends//////////////////////////////
//
////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/////////
///
///////////////////////////////////test by nil API Start/////////////////////////////
//
// exports.test_nilList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
// 			existingItem = models.test_by_nil.findAll({ });  
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
// 		    });
//         }	
//     });
// };

exports.test_nilList = function(req,res, next){
    if(err){
        res.status(200);
    }else{
        existingItem = models.test_by_nil.findAll({ });  
            existingItem.then(function (value) {
                res.status(200).send({ status:200,value:value });
		    });
    }
};

exports.addtest_nil = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
   
        var id = req.body.update_id;
        
        if(!id){
            models.test_by_nil.create({ 
                store_id: req.body.store_id?req.body.store_id:null,
                name: req.body.name?req.body.name:null, 
                email: req.body.email?req.body.email:null,
                content: req.body.content?req.body.content:null,
                address: req.body.address?req.body.address:null
               
				
                }).then(function(aboutus) {  
                     
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/test_by_nil');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.test_by_nil.update({ 
                store_id: req.body.store_id?req.body.store_id:null,
                name: req.body.name?req.body.name:null, 
                email: req.body.email?req.body.email:null,
                content: req.body.content?req.body.content:null,
                address: req.body.address?req.body.address:null

            },{where:{id:id}}).then(function(citi) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/test_by_nil');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    
};


exports.addedittest_nil = function(req, res, next){
    
    var id = req.params.id;
    stores = models.stores.findAll({ where: {status:'active'} });
	stores.then(function (stores) {	
		
    var existingItem = null;
    if(!id){	
        res.status(200).send({ status:200,arrData:stores });
    }else{            
        existingItem = models.test_by_nil.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
          
               res.status(200).send({ status:200,arrData:stores });
           
        });	
    }
    });	
	
};

exports.deleteNil = function(req, res, next) {
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.test_by_nil.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};

///////////////////////////////////test by nil API ends///////////////////////////////
//
///////////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/////////
///
//////////////////////////////////CMS API start/////////////////////////////////
//
exports.cmsList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
			existingItem = models.cms.findAll({ });  
            existingItem.then(function (value) {
                res.status(200).send({ status:200,value:value });
		    });
        }	
    });
};

exports.addCms = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
   
        var id = req.body.update_id;
        var str  = req.body.title ;
        var lower1 = str.toLowerCase();
        var lower = lower1.replace(" ", "_").replace(/[`~!@#$%^&*()|+\-=?;:'",.<>0-9\{\}\[\]\\\/]/gi, '');
        
    
       
        if(!id){
            var slug1= lower;
            models.cms.create({ 
                title: req.body.title?req.body.title:null, 
                slug: slug1,
                store_id: req.body.store_id?req.body.store_id:null,
				sequence: req.body.sequence?req.body.sequence:null,
				content_heading: req.body.content_heading?req.body.content_heading:null,
                content_description: req.body.content_description?req.body.content_description:null,
                meta_title: req.body.meta_title?req.body.meta_title:null,
                meta_key: req.body.meta_key?req.body.meta_key:null,
                meta_description: req.body.meta_description?req.body.meta_description:null,
                status:req.body.status?req.body.status:null,
                }).then(function(settings_profile) {  
                     
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/cms');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }else{
            models.cms.update({ 
                title: req.body.title?req.body.title:null, 
                //slug: req.body.slug?req.body.slug:null,
                store_id: req.body.store_id?req.body.store_id:null,
				sequence: req.body.sequence?req.body.sequence:null,
				content_heading: req.body.content_heading?req.body.content_heading:null,
                content_description: req.body.content_description?req.body.content_description:null,
                meta_title: req.body.meta_title?req.body.meta_title:null,
                meta_key: req.body.meta_key?req.body.meta_key:null,
                meta_description: req.body.meta_description?req.body.meta_description:null,
                status:req.body.status?req.body.status:null,
            },{where:{id:id}}).then(function(cms) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/cms');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    
};


exports.addeditCms = function(req, res, next){
    
    var id = req.params.id;
    stores = models.stores.findAll({ where: {status:'active'} });
    stores.then(function (stores) {
	
    var existingItem = null;
    if(!id){	
        res.status(200).send({ status:200,stores:stores});
    }else{            
        existingItem = models.cms.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
          
               res.status(200).send({ status:200,value: value,stores:stores });
           
        });	
    }
});	
	
};

exports.deleteCms = function(req, res, next) {
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.cms.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};

exports.deleteCmsMul = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {      
                models.cms.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 });
};


//
//////////////////////////////////CMS APi End///////////////////////////////////
//
///////////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/////////
///
//////////////////////////////////Notification API start/////////////////////////////////
//
exports.notificationList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
			existingItem = models.notification.findAll({ });  
            existingItem.then(function (value) {
                res.status(200).send({ status:200,value:value });
		    });
        }	
    });
};

exports.addNotification = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
   
        var id = req.body.update_id;
        var str  = req.body.title ;
       
        if(!id){
           
            models.notification.create({ 
                content: req.body.content?req.body.content:null, 
                device_id: req.body.device_id?req.body.device_id:null,
                status:req.body.status?req.body.status:null,
                }).then(function(settings_profile) {  
                     
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/notification');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.notification.update({ 
                content: req.body.content?req.body.content:null, 
                device_id: req.body.device_id?req.body.device_id:null,
                status:req.body.status?req.body.status:null,
                status:req.body.status?req.body.status:null,
            },{where:{id:id}}).then(function(cms) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/notification');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    
};


exports.addeditNotification = function(req, res, next){
    
    var id = req.params.id;
    
	
    var existingItem = null;
    if(!id){	
        res.status(200).send({ status:200});
    }else{            
        existingItem = models.notification.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
          
               res.status(200).send({ status:200,value: value});
           
        });	
    }

	
};

exports.deleteNotification = function(req, res, next) {
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.notification.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};


exports.deleteNotificationMul = function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {      
                models.notification.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
};

//
//////////////////////////////////Notification APi End///////////////////////////////////
//
///////////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/////////
///
//////////////////////////////////Notification Transaction API start/////////////////////////////////
//
exports.notification_transList = function(req, res, next){
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            sequelize.query("SELECT `notification_trans`.*, `notification`.`content`  FROM `notification_trans` INNER JOIN `notification` ON `notification_trans`.`notification_id`= `notification`.`id` WHERE notification_trans.status='active'",{ type: Sequelize.QueryTypes.SELECT })
			.then(function (value) {
                res.status(200).send({ status:200,value:value });
		    });
        }	
    });
};

exports.addNotification_trans = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
   
        var id = req.body.update_id;
        var str  = req.body.title ;
    
        if(!id){
           
            models.notification_trans.create({ 
                notification_id: req.body.notification_id?req.body.notification_id:null, 
                device_id: req.body.device_id?req.body.device_id:null,
                status:req.body.status?req.body.status:null,
                resend_status:req.body.resend_status?req.body.resend_status:null,
                }).then(function(settings_profile) {  
                     
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/notification_trans');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.notification_trans.update({ 
                notification_id: req.body.notification_id?req.body.notification_id:null, 
                device_id: req.body.device_id?req.body.device_id:null,
                status:req.body.status?req.body.status:null,
                resend_status:req.body.resend_status?req.body.resend_status:null,
            },{where:{id:id}}).then(function(cms) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/notification_trans');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    
};


exports.addeditNotification_trans = function(req, res, next){
    
    var id = req.params.id;
    notification = models.notification.findAll({ where: {status:'active'} });
    notification.then(function (notification) {
	
    var existingItem = null;
    if(!id){	
        res.status(200).send({ status:200,notification:notification});
    }else{            
        existingItem = models.notification_trans.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
          
               res.status(200).send({ status:200,value: value,notification:notification});
           
        });	
    }
});

	
};

exports.deleteNotification_trans = function(req, res, next) {
    var id = req.params.id;
    //console.log(req.headers)
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.notification_trans.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};


exports.deleteNotification_transMul= function(req, res, next) {
    var ch= req.body.check;
    var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {      
                models.notification_trans.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
};


//
//////////////////////////////////Notification_trans APi End///////////////////////////////////
//

/////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/////
//
////////////////////////////////////////////delivery_time_slot Start///////////////

// exports.addDelivery_time_slot = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     var logdetails = req.session.user 
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         if(!id)
//         {
//             models.delivery_time_slot.create({ 
//                 from_time: fields.from_time ? fields.from_time[0] : null,
//                 to_time: fields.to_time ? fields.to_time[0] : null,
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 status: fields.status ? fields.status[0] : null,
//                 createdBy : logdetails ? logdetails.id : '' 
//             }).then(function(delivery_time_slot) {
//                 // req.flash('info','Successfully Created');	  
//                 res.redirect('/superpos/delivery_time_slot');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }else{
//             models.delivery_time_slot.update({ 
//                 from_time: fields.from_time ? fields.from_time[0] : null,
//                 to_time: fields.to_time ? fields.to_time[0] : null,
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 status: fields.status ? fields.status[0] : null,
//                 updatedBy : logdetails ? logdetails.id : '' 
//             },{where:{id:id}}).then(function(delivery_time_slot) {
//                 // req.flash('info','Successfully Updated');	  
//                 res.redirect('/superpos/delivery_time_slot');      
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };


// exports.fileupload = function (req,res) {
// 	var formnew = new formidable.IncomingForm();
// 	formnew.parse(req);
// 	formnew.on('fileBegin', function (name, file) {
// 		if (file.name && file.name != '') {
// 			file.path = __dirname + '/../../public/superpos/myimages/' + file.name;
// 		}
// 	});	
// };


// exports.addeditDelivery_time_slot = function(req, res, next){
    
//     var id = req.params.id;
//     var existingItem = null;
//     var token= req.headers['token'];
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             if(!id){	
//                 res.status(200).send({ status:200 });
//             }else{            
//                 existingItem = models.delivery_time_slot.findOne({ where: {id:id} });
//                 existingItem.then(function (value) {                    
//                     res.status(200).send({ status:200,value: value });
                   	
//                 });
//             }
//         }
//     });
// };




// exports.delivery_time_slotList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             // existingCus = models.customer.findAll();
//             // existingCus.then(function (customer) {
//                 existingItem = models.delivery_time_slot.findAll({ where: {status: 'active'}});
//                 existingItem.then(function (value) {
//                     // console.log(value)
//                     res.status(200).send({ status:200,value:value });
                   
//                 });
//             //});    
//         }	
//     });
// };


// exports.deleteDelivery_time_slot = function(req, res, next) {    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.delivery_time_slot.update({ 
//                 status: 'archive',
//             },{where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
                
//             });
//         }           
//     });		
// };

exports.deleteDelivery_time_slotmul = function(req, res, next) {
    var ch= req.body.check;
   // var token= req.headers['token'];
            var i=0;
            ch.forEach(function(index,element) {      
                models.delivery_time_slot.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 });
};

       
////////////////////////////////////////////delivery_time_slot End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------////////////// 

////////////////////////////////////////////shipping_method Start///////////////

// exports.addShipping_method = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     var logdetails = req.session.user 
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         if(!id)
//         {
//             var slug=slugify(fields.name[0]);
//             models.shipping_method.create({ 
//                 name: fields.name ? fields.name[0] : null,
//                 slug: slug,
//                 price: fields.price ? fields.price[0] : null,
//                 from_time: fields.from_time ? fields.from_time[0] : null,
//                 to_time: fields.to_time ? fields.to_time[0] : null,
//                 status: fields.status ? fields.status[0] : null,
//                 createdBy : logdetails ? logdetails.id : '' 
//             }).then(function(shipping_method) {
//                 // req.flash('info','Successfully Created');	  
//                 res.redirect('/superpos/shipping_method');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }else{
//             models.shipping_method.update({ 
//                 name: fields.name ? fields.name[0] : null,
//                 price: fields.price ? fields.price[0] : null,
//                 from_time: fields.from_time ? fields.from_time[0] : null,
//                 to_time: fields.to_time ? fields.to_time[0] : null,
//                 status: fields.status ? fields.status[0] : null,
//                 updatedBy : logdetails ? logdetails.id : '' 
//             },{where:{id:id}}).then(function(shipping_method) {
//                 // req.flash('info','Successfully Updated');	  
//                 res.redirect('/superpos/shipping_method');      
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };


// exports.fileupload = function (req,res) {
// 	var formnew = new formidable.IncomingForm();
// 	formnew.parse(req);
// 	formnew.on('fileBegin', function (name, file) {
// 		if (file.name && file.name != '') {
// 			file.path = __dirname + '/../../public/superpos/myimages/' + file.name;
// 		}
// 	});	
// };


// exports.addeditShipping_method = function(req, res, next){
    
//     var id = req.params.id;
//     var existingItem = null;
//     var token= req.headers['token'];
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             if(!id){	
//                 res.status(200).send({ status:200 });
//             }else{            
//                 existingItem = models.shipping_method.findOne({ where: {id:id} });
//                 existingItem.then(function (value) {                    
//                     res.status(200).send({ status:200,value: value });
                   	
//                 });
//             }
//         }
//     });
// };




// exports.shipping_methodList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             // existingCus = models.customer.findAll();
//             // existingCus.then(function (customer) {
//                 existingItem = models.shipping_method.findAll({ where: {status: 'active'}});
//                 existingItem.then(function (value) {
//                     // console.log(value)
//                     res.status(200).send({ status:200,value:value });
                   
//                 });
//             //});    
//         }	
//     });
// };


// exports.deleteShipping_method = function(req, res, next) {    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.shipping_method.update({ 
//                 status: 'archive',
//             },{where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
                
//             });
//         }           
//     });		
// };

// exports.deleteShipping_methodmul = function(req, res, next) {
//     var ch= req.body.check;
//    // var token= req.headers['token'];
//             var i=0;
//             ch.forEach(function(index,element) {      
//                 models.shipping_method.destroy({
//                     where:{id:index} 
//                 })
//                 i++;
//             }, this);
//             res.status(200).send({ status:200,value:1 });
// };

       
////////////////////////////////////////////shipping_method End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------////////////// 


//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//////////////  
//
////////////////////////////////////////////wallet Start///////////////////////////////////

// exports.addWallet = function(req, res, next) {
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id ? fields.update_id[0] :  '';
//         if(!id)
//         {
//             models.wallet.create({ 
//                 cid	: fields.cid ? fields.cid[0] : null,
//                 amount : fields.amount ? fields.amount[0] : null,
//                 amount_type : fields.amount_type ? fields.amount_type[0] : null,
//                 status : fields.status ? fields.status[0] : null, 
//             }).then(function(wallet) {
//                 models.wallet_transaction.create({ 
//                     cid	: fields.cid ? fields.cid[0] : null,
//                     amount : fields.amount ? fields.amount[0] : null,
//                     amount_type : fields.amount_type ? fields.amount_type[0] : null,
//                     transcation_type : fields.transcation_type ? fields.transcation_type[0] : null,
//                     remarks : fields.remarks ? fields.remarks[0] : null,
//                     status : fields.status ? fields.status[0] : null, 
//                 }).then(function(wallet) {
//                     req.flash('info','Successfully Created');	  
//                     res.redirect('/superpos/wallet');
//                 })
//                 .catch(function(error) {
//                     return res.send(error);
//                 });                
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }else{
//             models.wallet.update({ 
//                 cid	: fields.cid ? fields.cid[0] : null,
//                 amount : fields.amount ? fields.amount[0] : null,
//                 amount_type : fields.amount_type ? fields.amount_type[0] : null,
//                 status : fields.status ? fields.status[0] : null, 
//             },{where:{id:id}}).then(function(wallet) {
//                 models.wallet_transaction.create({ 
//                     cid	: fields.cid ? fields.cid[0] : null,
//                     amount : fields.amount ? fields.amount[0] : null,
//                     amount_type : fields.amount_type ? fields.amount_type[0] : null,
//                     transcation_type : fields.transcation_type ? fields.transcation_type[0] : null,
//                     remarks : fields.remarks ? fields.remarks[0] : null,
//                     status : fields.status ? fields.status[0] : null, 
//                 }).then(function(wallet) {
//                     req.flash('info','Successfully Updated');	  
//                     res.redirect('/superpos/wallet');
//                 })
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };



// exports.addeditWallet = function(req, res, next){    
//     //var id = req.params.id;
//     var logdetails = req.session.user
//     var token= req.headers['token'];
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingCus = models.customer.findAll({ where: {status: 'active'} });
//             existingCus.then(function (customers) {   
//                     console.log(customers)                
//                 if(!logdetails.id){	
//                     res.status(200).send({ status:200, value: '', customers: customers });
//                 }else{            
                                    
//                     existingItem = models.wallet.findOne({ where: {id:logdetails.id} });
//                     existingItem.then(function (value) {                    
//                         res.status(200).send({ status:200, value: value, customers: customers });
//                     })
//                     .catch(function(error) {
//                         return res.send(error);
//                     });
                    
//                 }
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };


// exports.walletList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{

//             sequelize.query("SELECT `wallet`.*, `customer`.`first_name` as `customerName` FROM `wallet` INNER JOIN `customer` ON `wallet`.`cid`= `customer`.`id` WHERE wallet.status='active'",{ type: Sequelize.QueryTypes.SELECT })
//             .then(function (value) {
//                 // console.log(value)
//                 res.status(200).send({ status:200,value:value });
//             });     
//             // existingItem = models.wallet.findAll({ where: {status: 'active'}});
//             // existingItem.then(function (value) {
//             //     // console.log(value)
//             //     res.status(200).send({ status:200,value:value });
//             // });                
//         }	
//     });
// };


// exports.deleteWallet = function(req, res, next) {    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.wallet.update({ 
//                 status: 'archive',
//             },{where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };

// exports.deleteWalletMul = function(req, res, next) {
//     var ch= req.body.check;
//     //console.log(req.body);
//     // var token= req.headers['token'];  
//     var i=0;
//     ch.forEach(function(element,index) {  
//         console.log(index) 
//         console.log(element) 
//         models.wallet.update({ 
//             status: 'archive',
//         },{where:{id:element} });           
//         // models.dropdown_settings.destroy({
//         //     where:{id:index} 
//         // })
//         i++;
//     }, this);
//     res.status(200).send({ status:200,value:1 });
// };
       
////////////////////////////////////////////wallet End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
///////////////////////////// product upload start /////////////////////////////////////

// exports.addProduct_upload = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();

   
//     form.parse(req, function(err, fields, files) { 
//         //return res.send(files);
//        // return res.send(55555555555555555555555555);
        
//         var logdetails = req.session.user 
//         var id = fields.update_id[0];

//         var image=null;

        
//         var categoryArr=fields.optionValue;	
//         if(!id){
//             models.banner.create({ 
//                 title: fields.title ? fields.title[0] : null,
//                 category_id: fields.category_id ? fields.category_id[0] : null,
//                 store_id: fields.store_id ? fields.store_id[0] : null,
//                 short_description: fields.short_description ? fields.short_description[0]:null,
//                 image:image?image:null, 
//                 url: fields.url ? fields.url[0] : null,
//                 section: fields.section ? fields.section[0] : null,
//                 display_type: fields.display_type ? fields.display_type[0] : null,
//                 row: fields.row ? fields.row[0] : null,
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 createdBy: logdetails ? logdetails.id : null,
//                 status:fields.status ? fields.status[0] : null,
				
//                 }).then(function(banner) {  
//                     // if(categoryArr){
//                     //     var i=0;
//                     //     citiArr.forEach(function(element) {				
//                     //         models.citi_option.create({ 
//                     //         citi_id: citi.id,
//                     //         value: fields.optionValue[i],
//                     //         label: fields.optionLabel[i],
//                     //         position: fields.optionPosition[i],
//                     //     });
//                     //     i++;
//                     //     }, this);
//                     // }  
//                     req.flash('info','Successfully Created');
//                     return res.redirect('/superpos/banner');
//                 })
//                 .catch(function(error) {
//                     return res.send(error);
//                 });
//         }
//         else{
//             models.banner.update({ 
//                 title: fields.title ? fields.title[0] : null,
//                 category_id: fields.category_id ? fields.category_id[0] : null,
//                 store_id: fields.store_id ? fields.store_id[0] : null,
//                 short_description: fields.short_description ? fields.short_description[0]:null,
//                 image:image?image:null, 
//                 url: fields.url ? fields.url[0] : null,
//                 section: fields.section ? fields.section[0] : null,
//                 display_type: fields.display_type ? fields.display_type[0] : null,
//                 row: fields.row ? fields.row[0] : null,
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 updatedBy: logdetails ? logdetails.id : null,
//                 status:fields.status ? fields.status[0] : null,
//             },{where:{id:id}}).then(function(citi) {
//                 req.flash('info','Successfully Updated');
//                 return res.redirect('/superpos/banner');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };

/////////////////////////// product upload end ///////////////////////////////////////
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////banner Start//////////////////////////////////////
// exports.addBanner = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var logdetails = req.session.user 
//         var id = fields.update_id[0];

//         var image=null;

//         if(files.image[0].originalFilename!=''){	
//             image = req.app.locals.baseurl+'superpos/myimages/'+files.image[0].originalFilename;
//         }else{
//             image =fields.update_image[0];
//         }	


//         // if(fields.uploadHiddenCropImage && fields.uploadHiddenCropImage[0]!=''){
//         //     image = req.app.locals.baseurl+'superpos/myimages/'+fields.uploadHiddenCropImage[0];
//         // }else{
//         //     image =fields.update_image[0];
//         // }

//         var categoryArr=fields.optionValue;	
//         if(!id){
//             models.banner.create({ 
//                 title: fields.title ? fields.title[0] : null,
//                 category_id: fields.category_id ? fields.category_id[0] : null,
//                 store_id: fields.store_id ? fields.store_id[0] : null,
//                 short_description: fields.short_description ? fields.short_description[0]:null,
//                 image:image?image:null, 
//                 url: fields.url ? fields.url[0] : null,
//                 section: fields.section ? fields.section[0] : null,
//                 display_type: fields.display_type ? fields.display_type[0] : null,
//                 row: fields.row ? fields.row[0] : null,
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 createdBy: logdetails ? logdetails.id : null,
//                 status:fields.status ? fields.status[0] : null,
				
//                 }).then(function(banner) {
//                     req.flash('info','Successfully Created');
//                     return res.redirect('/superpos/banner');
//                 })
//                 .catch(function(error) {
//                     return res.send(error);
//                 });
//         }
//         else{
//             models.banner.update({ 
//                 title: fields.title ? fields.title[0] : null,
//                 category_id: fields.category_id ? fields.category_id[0] : null,
//                 store_id: fields.store_id ? fields.store_id[0] : null,
//                 short_description: fields.short_description ? fields.short_description[0]:null,
//                 image:image?image:null, 
//                 url: fields.url ? fields.url[0] : null,
//                 section: fields.section ? fields.section[0] : null,
//                 display_type: fields.display_type ? fields.display_type[0] : null,
//                 row: fields.row ? fields.row[0] : null,
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 updatedBy: logdetails ? logdetails.id : null,
//                 status:fields.status ? fields.status[0] : null,
//             },{where:{id:id}}).then(function(citi) {
//                 req.flash('info','Successfully Updated');
//                 return res.redirect('/superpos/banner');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };



// exports.addeditBanner = function(req, res, next){
    
//     var id = req.params.id;
// 	category = models.banner_section.findAll({ where: {status:'active'} });
// 	category.then(function (banner_section) {
//         category = models.banner_display.findAll({ where: {status:'active'} });
// 	    category.then(function (banner_display) {
//             category = models.stores.findAll({ where: {status:'active'} });
// 	        category.then(function (stores) {
//                 category = models.category.findAll({ where: {status:'active'} });
//                 category.then(function (category) {	
//                     var existingItem = null;
//                     if(!id){	
//                         res.status(200).send({ status:200, arrbanner_section: banner_section, arrbanner_display: banner_display, arrstore: stores, arrcategory: category});
//                     }else{            
//                         existingItem = models.banner.findOne({ where: {id:id} });
//                         existingItem.then(function (value) {	
//                             res.status(200).send({ status:200, value: value, arrbanner_section: banner_section, arrbanner_display: banner_display, arrstore: stores, arrcategory: category });
//                         });	
//                     }
//                 });
//             });    
//         });    
//     });		
// };


// exports.bannerList = function(req, res, next){

//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
// 			existingItem = models.banner.findAll();
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }	
//     });
// }

// exports.deleteBanner = function(req, res, next) {
    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.banner.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };

exports.deleteBannermul = function(req, res, next) {
    
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];
  
            var i=0;
            ch.forEach(function(index,element) {      
                //console.log('index:   '+index);
                //console.log('element:   '+element);
                         
                models.banner.destroy({
                    where:{id:index} 
                })
                i++;
            }, this);
            res.status(200).send({ status:200,value:1 });  
};


exports.appBannerList = function(req, res, next){
    
    existingItem = models.banner.findAll();
    existingItem.then(function (value) {
        res.status(200).send({ status:200,value:value });
    });
}

////////////////////////////////////////////banner End//////////////////////////////////////
//
////////////////////////////////////////////banner_section Start//////////////////////////////////banner_display

// exports.addBanner_section = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         var logdetails = req.session.user 

//         if(!id)
//         {
//             models.banner_section.create({ 
//                 title: fields.title ? fields.title[0] : null, 
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 status: fields.status ? fields.status[0] : null, 
//                 createdBy: logdetails ? logdetails.id : null,
//             }).then(function(banner_section) {

//                 req.flash('info','Successfully Created');	  
//                 res.redirect('/superpos/banner_section');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }else{
//             models.banner_section.update({ 
//                 title: fields.title ? fields.title[0] : null, 
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 status: fields.status ? fields.status[0] : null, 
//                 createdBy: logdetails ? logdetails.id : null, 
//             },{where:{id:id}}).then(function(banner_section) {
//                 req.flash('info','Successfully Updated');	  
//                 res.redirect('/superpos/banner_section');      
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };



// exports.addeditBanner_section = function(req, res, next){
    
//     var id = req.params.id;
//     var existingItem = null;
//     var existingPro = null;
//     var existingCat = null;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             //existingCus = models.customer.findAll({ where: {status: 'active'} });
//            // existingCus.then(function (customer) {
//                 //existingCat = models.category.findAll();
//                 //existingCat.then(function (category) {
//                     if(!id){	
//                         res.status(200).send({ status:200 });
//                     }else{            
//                         existingItem = models.banner_section.findOne({ where: {id:id} });
//                         existingItem.then(function (value) { 

//                             res.status(200).send({ status:200,value: value });
//                         });
//                     }
//                 //});    
//             //});	
//         }
//     });
// };



// exports.banner_sectionList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//            // existingCus = models.customer.findAll();
//            // existingCus.then(function (customer) {
//                 existingItem = models.banner_section.findAll();
//                 existingItem.then(function (value) {
//                     res.status(200).send({ status:200,value:value });
//                 });
//             //});    
//         }	
//     });
// };


// exports.deleteBanner_section = function(req, res, next) {
    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.banner_section.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };


exports.deleteBanner_sectionmul = function(req, res, next) {
    var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];  
        var i=0;
        ch.forEach(function(index,element) {               
            models.banner_section.destroy({
                where:{id:index} 
            })
            i++;
        }, this);
        res.status(200).send({ status:200,value:1 });
};

       
////////////////////////////////////////////banner_section End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//
////////////////////////////////////////////banner_display Start//////////////////////////////////

// exports.addBanner_display = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         var logdetails = req.session.user 

//         if(!id)
//         {
//             models.banner_display.create({ 
//                 title: fields.title ? fields.title[0] : null, 
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 status: fields.status ? fields.status[0] : null, 
//                 createdBy: logdetails ? logdetails.id : null,
//             }).then(function(banner_display) {

//                 req.flash('info','Successfully Created');	  
//                 res.redirect('/superpos/banner_display');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }else{
//             models.banner_display.update({ 
//                 title: fields.title ? fields.title[0] : null, 
//                 sequence: fields.sequence ? fields.sequence[0] : null,
//                 status: fields.status ? fields.status[0] : null, 
//                 createdBy: logdetails ? logdetails.id : null, 
//             },{where:{id:id}}).then(function(banner_display) {
//                 req.flash('info','Successfully Updated');	  
//                 res.redirect('/superpos/banner_display');      
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };


// exports.addeditBanner_display = function(req, res, next){
    
//     var id = req.params.id;
//     var existingItem = null;
//     var existingPro = null;
//     var existingCat = null;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             if(!id){	
//                 res.status(200).send({ status:200 });
//             }else{            
//                 existingItem = models.banner_display.findOne({ where: {id:id} });
//                 existingItem.then(function (value) { 

//                     res.status(200).send({ status:200,value: value });
//                 });
//             }
//         }
//     });
// };



// exports.banner_displayList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingItem = models.banner_display.findAll();
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
//             }); 
//         }	
//     });
// };


// exports.deleteBanner_display = function(req, res, next) {
    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.banner_display.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 res.status(200).send({ status:200,value:value });
//             });
//         }           
//     });		
// };


exports.deleteBanner_displaymul = function(req, res, next) {
  //  var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];  
        var i=0;
        ch.forEach(function(index,element) {               
            models.banner_display.destroy({
                where:{id:index} 
            })
            i++;
        }, this);
        res.status(200).send({ status:200,value:1 });
};

       
////////////////////////////////////////////banner_section End//////////////////////////////////////
//

////////////////////////////////////////////salesman Start//////////////////////////////////

// exports.addSalesman = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         var logdetails = req.session.user 

//         if(!id)
//         {
//             models.salesman.create({ 
//                 name: fields.name ? fields.name[0] : null, 
//                 email: fields.email ? fields.email[0] : null,
//                 phone: fields.phone ? fields.phone[0] : null,
//                 status: fields.status ? fields.status[0] : null, 
//                 createdBy: logdetails ? logdetails.id : null,
//             }).then(function(salesman) {

//                 req.flash('info','Successfully Created');	  
//                 res.redirect('/superpos/salesman');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }else{
//             models.salesman.update({ 
//                 name: fields.name ? fields.name[0] : null, 
//                 email: fields.email ? fields.email[0] : null,
//                 phone: fields.phone ? fields.phone[0] : null,
//                 status: fields.status ? fields.status[0] : null, 
//                 createdBy: logdetails ? logdetails.id : null, 
//             },{where:{id:id}}).then(function(salesman) {
//                 req.flash('info','Successfully Updated');	  
//                 res.redirect('/superpos/salesman');      
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };


// exports.addeditSalesman = function(req, res, next){
    
//     var id = req.params.id;
//     var existingItem = null;
//     var existingPro = null;
//     var existingCat = null;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             if(!id){	
//                 res.status(200).send({ status:200 });
//             }else{            
//                 existingItem = models.salesman.findOne({ where: {id:id} });
//                 existingItem.then(function (value) { 

//                     res.status(200).send({ status:200,value: value });
//                 });
//             }
//         }
//     });
// };



// exports.salesmanList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingItem = models.salesman.findAll();
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
//             }); 
//         }	
//     });
// };


exports.deleteSalesman = function(req, res, next) {
    
    var id = req.params.id;
    var token= req.headers['token'];
    //console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{    
            models.salesman.destroy({ 
                where:{id:id}
            }).then(function(value) {
                res.status(200).send({ status:200,value:value });
            });
        }           
    });		
};


exports.deleteSalesmanmul = function(req, res, next) {
  //  var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];  
        var i=0;
        ch.forEach(function(index,element) {               
            models.salesman.destroy({
                where:{id:index} 
            })
            i++;
        }, this);
        res.status(200).send({ status:200,value:1 });
};

       
////////////////////////////////////////////salesman End//////////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
////////////////////////////////////////////dropdown_settings start////////////////////////////////

// exports.addDropdown_settings = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         var dropdown_settingsArr=fields.option_value;	
//         if(!id){
//             models.dropdown_settings.create({ 
//                 name: fields.name?fields.name[0]:null, 
//                 identifier: fields.identifier?fields.identifier[0]:null,
//                 status:fields.status?fields.status[0]:null,
//                 }).then(function(dropdown_settings) {  
//                     if(dropdown_settingsArr){
//                         var i=0;
//                         dropdown_settingsArr.forEach(function(element) {				
//                             models.dropdown_settings_option.create({ 
//                             dropdown_id: dropdown_settings.id,
//                             option_value: fields.option_value[i],
//                             option_label: fields.option_label[i],
//                             option_order: fields.option_order[i],
//                         });
//                         i++;
//                         }, this);
//                     }  
//                     req.flash('info','Successfully Created');
//                     return res.redirect('/superpos/dropdown_settings');
//                 })
//                 .catch(function(error) {
//                     return res.send(error);
//                 });
//         }
//         else{
//             models.dropdown_settings.update({ 
//                 name: fields.name?fields.name[0]:null,
//                 status:fields.status?fields.status[0]:null,
//             },{where:{id:id}}).then(function(dropdown_settings) {

//                 models.dropdown_settings_option.destroy({	where:{dropdown_id: id}
// 				});
//                 if(dropdown_settingsArr){
//                     var i=0;
//                     dropdown_settingsArr.forEach(function(element) {				
//                         models.dropdown_settings_option.create({ 
//                         dropdown_id: id,
//                         option_value: fields.option_value[i],
//                         option_label: fields.option_label[i],
//                         option_order: fields.option_order[i],
//                     });
//                     i++;
//                     }, this);
//                 }  

//                 req.flash('info','Successfully Updated');
//                 return res.redirect('/superpos/dropdown_settings');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };


// exports.addeditDropdown_settings = function(req, res, next){
    
//     var id = req.params.id;
//     var existingItem = null;
//     if(!id){	
//         res.status(200).send({ status:200 });
//     }else{            
//         existingItem = models.dropdown_settings.findOne({ where: {id:id} });
//         existingItem.then(function (value) {	
//             existingItem = models.dropdown_settings_option.findAll({ where: {dropdown_id:id} });
// 			existingItem.then(function (opt_value) {	
//                res.status(200).send({ status:200,value: value,opt_value: opt_value });
//             })
//         });	
//     }	
// };



// exports.dropdown_settingsList = function(req, res, next){
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
// 			existingItem = models.dropdown_settings.findAll({ where: {status: {$ne: 'archive'}} });  
//             existingItem.then(function (value) {
//                 res.status(200).send({ status:200,value:value });
// 		    });
//         }	
//     });
// };



// exports.deleteDropdown_settings = function(req, res, next) {

//     var id = req.params.id;
//     //console.log(req.headers)
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.dropdown_settings.update({ 
//                 status:'archive'
// 				 },{where:{id:id}}).then(function(dropdown_settings) {
//                 models.dropdown_settings_option.destroy({ where: {dropdown_id:id} 
//                 }).then(function(opt_value){
//                     res.status(200).send({ status:200,opt_value:opt_value });
//                 })
//             });	
//         }
//     });
// };


exports.deleteDropdown_settingsmul = function(req, res, next) {
    var id = req.params.id;
    var ch= req.body.check;
    //console.log(req.body);
    var token= req.headers['token'];  
        var i=0;
        ch.forEach(function(index,element) {               
            models.dropdown_settings.destroy({
                where:{id:index} 
            })
            i++;
        }, this);
        res.status(200).send({ status:200,value:1 });
};

function slugify(text) {
    return text.toString().toLowerCase() .replace(/\s+/g, '_') 
    // Replace spaces with - .replace(/[^\w\-]+/g, '')
    // Remove all non-word chars .replace(/\-\-+/g, '-') // Replace multiple - with single - .replace(/^-+/, '') 
}

////////////////////////////////////////////dropdown_settings End///////////////////////////////////
//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
//---------------@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-------------------//
///////
exports.changePassword = function(req, res, next){
    //return res.send(req.body.data.email);
   // var token= req.headers['token'];
    //console.log(req.token)
    //console.log(token)
	var email=req.body.data.email;
   // return res.send(email);
				existingItem = models.users.findOne({ where: {email:email} });
                existingItem.then(function (value) {
					//return res.json(value.id);
					//return res.send(value.id);
					models.user_otp.create({ 
                    user_id:value.id, 
                    otpValue:123,
                    status:'active'
                   
                    }).then(function(team) {  
                    
                     res.status(200).send({ status:200,message: "otp send"});
                })
                .catch(function(error) {
                    return res.send(error);
        
                });
                    
		
             });
        	
   
}

exports.otpCheack = function(req, res, next){
   
  
   
   var today = new Date(Date.now() - ((1000*60*60*5)+(1000*60*30)+(1000*60*2)));
  var  formatted = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()+' '+today.getHours()+':'+today.getMinutes()+':'+today.getSeconds();
            
   var today1 = new Date(Date.now() - ((1000*60*60*5)+(1000*60*30)));
  var  formatted1 = today1.getFullYear() + '-' + (today1.getMonth() + 1) + '-' + today1.getDate()+' '+today1.getHours()+':'+today1.getMinutes()+':'+today1.getSeconds();
                
   
   
	var email=req.body.data.email;
	var otp=req.body.data.otp;
   existingItem = models.users.findOne({ where: {email:email} });
   existingItem.then(function (users) {
	   
	   otp= sequelize.query("SELECT * FROM user_otp WHERE user_id="+users.id+" and status='active' and (updatedAt BETWEEN '"+formatted+"' AND '"+formatted1+"') LIMIT 1",{ type: Sequelize.QueryTypes.SELECT }) 
otp.then(function (otp) {
             if(!otp){
				 res.status(200).send({ status:200,message: "otp not match"});
			 }else{
				 otp_update= sequelize.query("UPDATE user_otp set status='inactive' where user_id="+users.id+"",{ type: Sequelize.QueryTypes.UPDATE }) 
				var token =    jwt.sign({users}, SECRET, { expiresIn: 18000 });
               res.status(200).send({ status:200,message: "successfully login", token: token });
			 }
			  });  

   });   
   
}
// exports.register = function(req, res, next){
//     //return res.send(req.body.data);
//     models.users.findOne({ where: {$or: [{email: req.body.data.email}, {phone: req.body.data.phone}]}
//     }).then(function(users) { 
//         if(users){
//             if(users.email == req.body.data.email && users.phone == req.body.data.phone){
//                 res.status(200).send({ status:200, success: false,  message: "User already exists with this email and phone." });  
//             }else if(users.email != req.body.data.email && users.phone == req.body.data.phone){
//                 res.status(200).send({ status:200, success: false,  message: "User already exists with this phone." });  
//             }else if(users.email == req.body.data.email && users.phone != req.body.data.phone){
//                 res.status(200).send({ status:200, success: false,  message: "User already exists with this email." });  
//             }             
//         }else{
//             var hash = bcrypt.hashSync(req.body.data.password);	
//             models.users.create({ 
//                 firstName: req.body.data.firstName, 
//                 lastName:req.body.data.lastName,
//                 phone:req.body.data.phone,
//                 postCode:req.body.data.postCode,
//                 country: req.body.data.country, 
//                 location:req.body.data.location,
//                 address:req.body.data.address,
//                 email:req.body.data.email,
//                 password:hash        
//             }).then(function(users) {  
//                 var token = jwt.sign({users}, SECRET, { expiresIn: 18000 });
//                 res.status(200).send({ status:200, success: true, message: "success", token: token,userdetail:users });        
//             })
//             .catch(function(error) {
//                 return res.send( {success: false, error});
//             });
//             // res.status(200).send({ status:304, message: "failed", userdetail:'' });  
//         }     
//     })
//     .catch(function(error) {
//         return res.send({ success: false, error});
//     });
   
// }

/////////////////////front end registration 13.02.2019  start //////////////////

exports.register = function(req, res, next){
    //return res.send(req.body.data);
    //models.customer.findOne({ where: {customer_phone_no: req.body.data.phone, is_prime_phone:1}
    models.customer.findOne({ where: {$or: [{email: req.body.data.email}, {phone: req.body.data.phone}]}
    }).then(function(users) { 
        // if(customer_ph_no){
        //     if(customer_ph_no.customer_phone_no == req.body.data.email){
        //         res.status(200).send({ status:200, success: false,  message: "User already exists with this phone." });
        //     }   
        if(users){
            if(users.email == req.body.data.email && users.phone == req.body.data.phone){
                res.status(200).send({ status:200, success: false,  message: "User already exists with this email and phone." });  
            }else if(users.email != req.body.data.email && users.phone == req.body.data.phone){
                res.status(200).send({ status:200, success: false,  message: "User already exists with this phone." });  
            }else if(users.email == req.body.data.email && users.phone != req.body.data.phone){
                res.status(200).send({ status:200, success: false,  message: "User already exists with this email." });  
            }             
        }else{
            var hash = bcrypt.hashSync(req.body.data.password);	
            models.customer.create({ 
                first_name: req.body.data.firstName, 
                last_name:req.body.data.lastName,
                email:req.body.data.email,
                phone:req.body.data.phone,
                status:'active', 
                password:hash        
            }).then(function(customer) {
                models.customer_address.create({ 
                    customer_id: customer.id,
                    first_name:req.body.data.firstName,
                    last_name:req.body.data.lastName,
                    mobile:req.body.data.phone,
                    city:req.body.data.city,
                    state:req.body.data.state,
                    pin:req.body.data.postCode,
                    country: req.body.data.country, 
                    address:req.body.data.address,
                    is_prime_address: 1,
                }).then(async function(customer_address) {

                    var contact = await helpers.getEmailConfig();
                    var email =contact[1];
                    var phoneno =contact[0];
                    
                    // models.customer_phone.create({ 
                    // customer_id: customer.id,
                    // customer_phone_no:req.body.data.phone,
                    // is_prime_phone: 1,
                    // }).then(function(customer_phone) {
                        
                    // if(req.body.data.email){    
                    // models.customer_email.create({ 
                    // customer_id: customer.id,
                    // customer_email_id:req.body.data.email,
                    // is_prime_email: 1,
                    // }).then(function(customer_email) {      
                    var token = jwt.sign({customer}, SECRET, { expiresIn: 18000 });
					var smsContent = "Thank you for your registration with OTG. Please mail us at contactus@ssquaredigital.com for any concern."
                    sms_controller.sendsms(customer.phone,smsContent);
                    return new Promise((resolve, reject) => {
                        var data = {
                            from: 'Grocery User <me@samples.mailgun.org>',
                            // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                            to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                            subject: 'Registration',
                            html: '<!DOCTYPE html>'+
                            '<html>'+    
                            '<head>'+
                            '</head>'+    
                            '<body>'+
                                '<div>'+
                                    '<p>Hello Sir/Madam,</p>'+
                                    '<p>Welcome to One Tap Grocery.</p>'+
                                    '<p>Thank you for registering with us, here you will experience a hustle free service and quality product in reasonable price and many more offers. We are committed to serve you best quality and best price, from the comfort of your home.</p>'+                    
                                    '<p>Please feel free to reach us anytime.</p>'+
                                    '<p>We are available at: <br />'+
                                    'Mail: " '+email+' " <br />'+
                                    'Phone: " '+phoneno+' " <br />'+
                                    '<p>We expect a very happy and long relationship with you.</p>'+
                                    '<p>Have a nice shopping.</p>'+
                                    '<p>Thanks.</p>'+
                                    '<p>Regards,<br />'+
                                    'One Tap Grocery<br />'+
                                    'Midnapore</p>'+								
                                '</div>'+       
                            '</body>'+    
                            '</html>' 
                        };
                        mailgun.messages().send(data, function (error, body) {
                                         
                            
                            if (error) {
                                return reject(res.status(200).send({ status:200, success: true, message: "You are successfully signed up", token: token,userdetail:customer,useraddress:customer_address }));
                            }
                            return resolve(res.status(200).send({ status:200, success: true, message: "You are successfully signed up", token: token,userdetail:customer,useraddress:customer_address }));
                
                        });
                    });
                    
                    // })
                    // }else{
                    //     var token = jwt.sign({customer}, SECRET, { expiresIn: 18000 });
                    //     res.status(200).send({ status:200, success: true, message: "success", token: token,userdetail:customer,useremail:'',userphone:customer_phone,useraddress:customer_address });  
                    // }
                    //})
                })
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });
            // res.status(200).send({ status:304, message: "failed", userdetail:'' });  
        }     
    })
    .catch(function(error) {
        return res.send({ success: false, error});
    });   
}

exports.newRegister = function(req, res, next){
    models.customer.findOne({ where:  {phone: req.body.data.phone}
    }).then(function(users) { 
        if(users){
            res.status(200).send({ status:200, success: false,  message: "User already exists with this phone." });             
        }else{
            var random_sponsor_code = randomString();
            var random_otp = Math.floor(1000 + Math.random() * 9000);            
            var otp_date_time =  proper_Date_Time();

            models.customer.create({                 
                otp: random_otp,
                otp_created_at: otp_date_time,
                email:req.body.data.email ? req.body.data.email : null,
                phone:req.body.data.phone,
                sponsor_code: random_sponsor_code ? random_sponsor_code : '' ,
                status:'active', 
                is_approved:1
                //password:hash        
            }).then(function(customer) {
                value1= models.coupon_value.findOne({ where: {id:1} });
                value1.then(function(value){
                models.coupon.create({
                    coupon_type: 'amount',
                    coupon_value: value.coupon_value,                   
                    coupon_code: random_sponsor_code,
                    is_share: 'yes',
                    status: 'active',
                    createdBy : customer ? customer.id : '' 
                }).then(function(coupon) {                        
                    var smsContent1 = random_otp+" is your verification code. Code valid for 10 minutes only, one time use. Please DO NOT share this OTP with anyone to ensure account's security."
                    sms_controller.sendsms(customer.phone,smsContent1);   
                    return res.status(200).send({ status:200, success: true, message: "You are successfully signed up", userdetail:customer }) 
                    // var smsContent = "Thank you for your registration with OTG. Please mail us at contactus@ssquaredigital.com for any concern."
                    // sms_controller.sendsms(customer.phone,smsContent);   
                    // return new Promise((resolve, reject) => {
                    //     var data = {
                    //         from: 'Grocery User <me@samples.mailgun.org>',
                    //         to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                    //         subject: 'Registration',
                    //         html: '<!DOCTYPE html>'+
                    //         '<html>'+    
                    //         '<head>'+
                    //         '</head>'+    
                    //         '<body>'+
                    //             '<div>'+
                    //                 '<p>Hello Sir/Madam,</p>'+
                    //                 '<p>Welcome to One Tap Grocery.</p>'+
                    //                 '<p>Thank you for registering with us, here you will experience a hustle free service and quality product in reasonable price and many more offers. We are committed to serve you best quality and best price, from the comfort of your home.</p>'+                    
                    //                 '<p>Please feel free to reach us anytime.</p>'+
                    //                 '<p>We are available at: <br />'+
                    //                 'Mail: grocery@gmail.com<br />'+
                    //                 'Phone: +91 9232456754<br />'+
                    //                 '<p>We expect a very happy and long relationship with you.</p>'+
                    //                 '<p>Have a nice shopping.</p>'+
                    //                 '<p>Thanks.</p>'+
                    //                 '<p>Regards,<br />'+
                    //                 'One Tap Grocery<br />'+
                    //                 'Midnapore</p>'+								
                    //             '</div>'+       
                    //         '</body>'+    
                    //         '</html>' 
                    //     };
                    //     mailgun.messages().send(data, function (error, body) {                                         
                            
                    //         if (error) {
                    //             return reject(res.status(200).send({ status:200, success: true, message: "You are successfully signed up", userdetail:customer }));
                    //         }
                    //         return resolve(res.status(200).send({ status:200, success: true, message: "You are successfully signed up", userdetail:customer }));
                
                    //     });
                    // });                   
                    //res.status(200).send({ status:200, success: true, message: "You are successfully signed up", userdetail:customer })
                })
                .catch(function(error) {
                    return res.send( {status:200, success: false, message: "error occurred"});
                });
            })
        })
            .catch(function(error) {
                return res.send( {status:200, success: false, message: "error occurred"});
            });
        }     
    })
    .catch(function(error) {
        return res.send({status:200, success: false, message: "error occurred"});
    });   
}

// exports.newRegister = function(req, res, next){
//     models.customer.findOne({ where:  {phone: req.body.data.phone}
//     }).then(function(users) { 
//         if(users){
//             res.status(200).send({ status:200, success: false,  message: "User already exists with this phone." });             
//         }else{

//             // var exists_sponsorcode = true;

//             // do {
//             //     // $regNum      = rand(2200000, 2299999);
//             //     // $checkRegNum = "SELECT * FROM teachers WHERE teacherRegNum = '$regNum'";
//             //     // $result      = mysqli_query($connection, $checkRegNum);
            
//             //     // if (mysqli_num_rows($result) == 0) {
//             //     //     $regenerateNumber = false;
//             //     // }
//             //     var random_sponsor_code = randomString();
//             //     var random_otp = Math.floor(1000 + Math.random() * 9000);            
//             //     var otp_date_time =  proper_Date_Time();
//             //     // console.log(random_sponsor_code)
//             //     // console.log(random_otp)
//             //     // console.log(otp_date_time)
                 
//             //     models.customer.findOne({ where:  {sponsor_code: random_sponsor_code}
//             //     }).then(function(exists_users) { 
//             //         console.log(exists_users)
//             //         if(!exists_users ){
//             //             exists_sponsorcode =- false;
//             //             models.customer.create({ 
//             //                 otp: random_otp,
//             //                 otp_created_at: otp_date_time,
//             //                 email:req.body.data.email ? req.body.data.email : null,
//             //                 phone:req.body.data.phone,
//             //                 sponsor_code: random_sponsor_code ? random_sponsor_code : '' ,
//             //                 status:'active', 
//             //                 //password:hash        
//             //             }).then(function(customer) {
//             //                 models.coupon.create({
//             //                     coupon_type: 'amount',
//             //                     coupon_value: 50,                   
//             //                     coupon_code: random_sponsor_code,
//             //                     is_share: 'yes',
//             //                     status: 'active',
//             //                     createdBy : customer ? customer.id : '' 
//             //                 }).then(function(coupon) {      
//             //                     var smsContent = "Thank you for your registration with OTG. Please mail us at contactus@ssquaredigital.com for any concern."
//             //                     sms_controller.sendsms(customer.phone,smsContent);
//             //                     var smsContent1 = random_otp+" is your verification code. Code valid for 10 minutes only, one time use. Please DO NOT share this OTP with anyone to ensure account's security."
//             //                     sms_controller.sendsms(customer.phone,smsContent1);    
//             //                     return new Promise((resolve, reject) => {
//             //                         var data = {
//             //                             from: 'Grocery User <me@samples.mailgun.org>',
//             //                             to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
//             //                             subject: 'Registration',
//             //                             html: '<!DOCTYPE html>'+
//             //                             '<html>'+    
//             //                             '<head>'+
//             //                             '</head>'+    
//             //                             '<body>'+
//             //                                 '<div>'+
//             //                                     '<p>Hello Sir/Madam,</p>'+
//             //                                     '<p>Welcome to One Tap Grocery.</p>'+
//             //                                     '<p>Thank you for registering with us, here you will experience a hustle free service and quality product in reasonable price and many more offers. We are committed to serve you best quality and best price, from the comfort of your home.</p>'+                    
//             //                                     '<p>Please feel free to reach us anytime.</p>'+
//             //                                     '<p>We are available at: <br />'+
//             //                                     'Mail: grocery@gmail.com<br />'+
//             //                                     'Phone: +91 9232456754<br />'+
//             //                                     '<p>We expect a very happy and long relationship with you.</p>'+
//             //                                     '<p>Have a nice shopping.</p>'+
//             //                                     '<p>Thanks.</p>'+
//             //                                     '<p>Regards,<br />'+
//             //                                     'One Tap Grocery<br />'+
//             //                                     'Midnapore</p>'+								
//             //                                 '</div>'+       
//             //                             '</body>'+    
//             //                             '</html>' 
//             //                         };
//             //                         mailgun.messages().send(data, function (error, body) {                                                    
                                        
//             //                             if (error) {
//             //                                 return reject(res.status(200).send({ status:200, success: true, message: "You are successfully signed up", userdetail:customer }));
//             //                             }
//             //                             return resolve(res.status(200).send({ status:200, success: true, message: "You are successfully signed up", userdetail:customer }));
                            
//             //                         });
//             //                     });                          
//             //                     //res.status(200).send({ status:200, success: true, message: "You are successfully signed up", userdetail:customer })
//             //                 })
//             //                 .catch(function(error) {
//             //                     return res.send( {status:200, success: false, message: "error occurred"});
//             //                 });
//             //             })
//             //             .catch(function(error) {
//             //                 return res.send( {status:200, success: false, message: "error occurred"});
//             //             });
//             //         }

//             //     })
//             //     .catch(function(error) {
//             //        console.log("error occurred");
//             //     });   

//             // } while (exists_sponsorcode);

//         }     
//     })
//     .catch(function(error) {
//         return res.send({status:200, success: false, message: "error occurred"});
//     });   
// }


//////////////////// front end registration 13.02.2019 end ////////////////////

/////////////////// front end login 13.02.2019 start /////////////////////////

exports.login = function(req, res) {

    if(req.body.data.email && req.body.data.email!=''){
        // models.customer_email.findOne({ where: {'customer_email_id':req.body.data.email, 'is_prime_email': 1} })
        // .then(function(customer_email) {
            //console.log(customer_email);
            //if(customer_email!=null){
                //customer_email_data = customer_email.toJSON();  
                models.customer.findOne({ where: {'email':req.body.data.email} })
                .then(function(customer) {
                
                    if(customer){
                        if(!bcrypt.compareSync(req.body.data.password, customer.password)) {
                            res.status(200).send({success: false, message: "password wrong" });
                        } else {	  
                            if (req.body.data.email == customer.email) {              
                                var token =    jwt.sign({customer}, SECRET, { expiresIn: 18000 });
                                res.status(200).send({ status:200,success: true, message: "You are successfully logged in", token: token,userdetail:customer });                    
                            }else{				
                                res.status(200).send({ success: false, message: "No user found" });
                            }                
                        }//password check end 
                    }else{				
                        res.status(200).send({ success: false, message: "No user found" });
                    } 
                   
                })               
                
            // }else{				
            //     res.status(200).send({ success: false, message: "No user found" });
            // }  
        //})
   
        // .catch(function(error) {
        //     return res.send(error);
            
        // });
    }else if(req.body.data.phone && req.body.data.phone!=''){ 
        
        // models.customer_phone.findOne({ where: {'customer_phone_no':req.body.data.phone, 'is_prime_phone': 1} })
        // .then(function(customer_phone) {
            //console.log(customer_phone);
            //if(customer_phone!=null){
                //customer_phone_data = customer_phone.toJSON(); 
                
                models.customer.findOne({ where: {'phone':req.body.data.phone} })
                .then(function(customer) {

                    if(!bcrypt.compareSync(req.body.data.password, customer.password)) {
                        res.status(200).send({ success: false, message: "password wrong" });
                    } else {	  
                        if (req.body.data.phone == customer.phone) {              
                        var token =    jwt.sign({customer}, SECRET, { expiresIn: 18000 });
                        res.status(200).send({ status:200, success: true, message: "successfully login", token: token,userdetail: customer });                    
                        }else{				
                        res.status(200).send({ success: false, message: "No user found" });
                        }                
                    }//password check end   
                
                })
                
            // }else{				
            //     res.status(200).send({success: false, message: "No user found" });
            // } 
        // })
        // .catch(function(error) {
        //     return res.send(error);
            
        // });

    } 
};


exports.loginNew = function(req, res) {
    var otp_date_time =  proper_Date_Time();
    console.log(otp_date_time);
    if(req.body.data.phone && req.body.data.phone!=''){                
        models.customer.findOne({ where: {'phone':req.body.data.phone} })
        .then(function(customer) {
            if(customer!=null){
                var random_otp = Math.floor(1000 + Math.random() * 9000);
                //console.log(random_otp);
                models.customer.update({ 
                    otp: random_otp,
                    otp_created_at: otp_date_time,
                },{where:{id:customer.id}}).then(function(customer_otp) {
					var smsContent = random_otp+" is your verification code. Code valid for 10 minutes only, one time use. Please DO NOT share this OTP with anyone to ensure account's security."
                    sms_controller.sendsms(customer.phone,smsContent);        
                    // res.status(200).send({ status:200, success: true, message: "success", otp:random_otp, otp_created_at:otp_date_time, userDetails: customer });
                    res.status(200).send({ status:200, success: true, message: "successfully login" ,userdetail: customer});
                })
                .catch(function(error) {
                    return res.send( {status:200, success: false, message: "error happened"});
                });
            }else{				
                res.status(200).send({ status:204, success: false, message: "No customer found" });
            }
            // if (customer) {
            // res.status(200).send({ status:200, success: true, message: "successfully login", userdetail: customer });                    
            // }else{				
            // res.status(200).send({status:200, success: false, message: "No user found" });
            // }  
        
        })
    } else {
        res.status(200).send({status:200, success: false, message: "Phone no not found" });   
    }
};


///////////////////////////////////////////front end login 13.02.2019 end/////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////

// exports.login = function(req, res) {
//     if(req.body.data.phone && req.body.data.phone!=''){
//         models.users.findOne({ where: {'phone':req.body.data.phone} })
//         .then(function(users) {
//             //console.log(users);
//             if(users!=null){
//                 user = users.toJSON();        
//                 if(!bcrypt.compareSync(req.body.data.password, user.password)) {
//                     res.status(200).send({ success: false, message: "password wrong" });
//                 } else {	  
//                     if (req.body.data.phone == users.phone) {              
//                     var token =    jwt.sign({users}, SECRET, { expiresIn: 18000 });
//                     res.status(200).send({ status:200, success: true, message: "successfully login", token: token,userdetail:users });                    
//                     }else{				
//                     res.status(200).send({ success: false, message: "No user found" });
//                     }                
//                 }//password check end            
                
//             }else{				
//                 res.status(200).send({success: false, message: "No user found" });
//             }  

//             //////////////////////
//             // if(users!=null){
//             //     if (req.body.data.phone == users.phone && req.body.data.password == users.password) {
                    
//             //         var token =    jwt.sign({users}, SECRET, { expiresIn: 18000 });
//             //         res.status(200).send({ status:200,message: "successfully login", token: token });
                        
//             //     }else{
                    
//             //         res.status(200).send({ message: "No user found" });
//             //     }
//             // }else{                
//             //     res.status(200).send({ message: "No user found" });
//             // }
//         })
//         .catch(function(error) {
//             return res.send(error);
            
//         });
//     }else if(req.body.data.email && req.body.data.email!=''){
//         models.users.findOne({ where: {'email':req.body.data.email} })
//         .then(function(users) {
//             //console.log(users);
//             if(users!=null){
//                 user = users.toJSON();        
//                 if(!bcrypt.compareSync(req.body.data.password, user.password)) {
//                     res.status(200).send({success: false, message: "password wrong" });
//                 } else {	  
//                     if (req.body.data.email == users.email) {              
//                         var token =    jwt.sign({users}, SECRET, { expiresIn: 18000 });
//                         res.status(200).send({ status:200,success: true, message: "successfully login", token: token,userdetail:users });                    
//                     }else{				
//                         res.status(200).send({ success: false, message: "No user found" });
//                     }                
//                 }//password check end            
                
//             }else{				
//                 res.status(200).send({ success: false, message: "No user found" });
//             }  
//         })
//         .catch(function(error) {
//             return res.send(error);
            
//         });
//     }   
// };

///////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////

exports.addeditOderitem = function(req, res, next){    
    var id = req.params.id;
    var existingItem = null;
    var token= req.headers['token'];
    // console.log(req.token)
    // console.log(token)
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingStore = models.stores.findAll();
            existingStore.then(function (store) {			
                existingProduct = models.product.findAll();
                existingProduct.then(function (product) {
                    existingProduct = models.order.findAll();
                    existingProduct.then(function (order) {
                    
                        if(!id){	   
                            res.status(200).send({ status:200, store:store, product:product, order:order});
                        }else{            
                            existingItem = models.otderitem.findOne({ where: {id:id} });
                            existingItem.then(function (value) {                      
                                res.status(200).send({ status:200, value:value, store:store, product:product, order:order });
                            });
                        }
                    });    
                    
                });
            });				
        }
    });
};	


exports.addOrderitem = function(req, res, next){
    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        //return res.send(fields);	
        var id = fields.update_id[0];
        if(!id)
        {			
            models.orderitem.create({ 
                order_id: fields.order_id[0]?fields.order_id[0]:2,
                store_id: fields.store_id[0]?fields.store_id[0]:null,
                product_id: fields.product_id[0]?fields.product_id[0]:null,
                name: fields.name[0]?fields.name[0]:null,
                sku: fields.sku[0]?fields.sku[0]:null,
                description:fields.description[0]?fields.description[0]:null,
                discount_percent:fields.discount_percent[0]?fields.discount_percent[0]:null,
                discount_amount: fields.discount_amount[0]?fields.discount_amount[0]:null,
                qty: fields.qty[0]?fields.qty[0]:null,
                price: fields.price[0]?fields.price[0]:null,
                original_price:fields.original_price[0]?fields.original_price[0]:null,
                cgst:fields.cgst[0]?fields.cgst[0]:null,
                sgst:fields.sgst[0]?fields.sgst[0]:null,
                igst:fields.igst[0]?fields.igst[0]:null,
                row_total:fields.row_total[0]?fields.row_total[0]:null,                
            }).then(function(orderitem) {
                req.flash('info','Successfully Created');	  
                res.redirect('back');     
            })
            .catch(function(error) {
                return res.send(error);
            });
        }else{
            models.orderitem.update({ 
			
			
                
            },{where:{id:id}}).then(function(product) {
                req.flash('info','Successfully Updated');	  
                res.redirect('back');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    }); 
};

/////////////////////////////// app customer address start//////////////////////////////////

// exports.appCustomerAddressAdd = function(req, res, next) {
//     //var id = req.body.data.update_id;
//     if(req.body.data.customerId && req.body.data.customerId!=''){
//         var customerId= req.body.data.customerId ? req.body.data.customerId : '';
        
//         models.customer_address.findAll({ where: {customer_id : customerId } })
//         .then(function (value) {
//             if(value.length > 0 ){
//                 var xxx = req.body.data.is_prime_address ? req.body.data.is_prime_address:0;
//             }else{
//                 var xxx = 1;
//             }
//             models.customer_address.create({ 
//                 customer_id: customerId,
//                 first_name: req.body.data.firstName ? req.body.data.firstName:null,
//                 last_name: req.body.data.lastName ? req.body.data.lastName:null,
//                 mobile: req.body.data.mobile ? req.body.data.mobile:null,
//                 address: req.body.data.address ? req.body.data.address:null,
//                 city: req.body.data.city ? req.body.data.city:null,
//                 state: req.body.data.state ? req.body.data.state:null,
//                 pin: req.body.data.postCode ? req.body.data.postCode:null,
//                 country: req.body.data.country ? req.body.data.country:null,
//                 is_prime_address: xxx,
//             }).then(function(customer_address) {

//             res.status(200).send({ status:200, success: true, message: "success", customeraddress:customer_address });
//             })
//             .catch(function(error) {
//                 return res.send( {success: false, error});
//             });
//         }).catch(function(error) {
//             res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
//         });
//     }
// };

exports.appCustomerAddressAdd = function(req, res, next) {
    //var id = req.body.data.update_id;
    if(req.body.data.customerId && req.body.data.customerId!=''){
        var customerId= req.body.data.customerId ? req.body.data.customerId : '';
        
        models.customer_address.findAll({ where: {customer_id : customerId } })
        .then(function (value) {
            if(value.length > 0 ){
                var xxx = req.body.data.is_prime_address ? req.body.data.is_prime_address:0;
            }else{
                var xxx = 1;
            }
            models.customer_address.create({ 
                customer_id: customerId,
                first_name: req.body.data.firstName ? req.body.data.firstName:null,
                last_name: req.body.data.lastName ? req.body.data.lastName:null,
                mobile: req.body.data.mobile ? req.body.data.mobile:null,
                address: req.body.data.address ? req.body.data.address:null,
                city: req.body.data.city ? req.body.data.city:null,
                state: req.body.data.state ? req.body.data.state:null,
                pin: req.body.data.postCode ? req.body.data.postCode:null,
                country: req.body.data.country ? req.body.data.country:null,
                is_prime_address: xxx,
            }).then(function(customer_address) {


                models.customer.findOne({ where: {id:customerId} })
                .then(function (customerdetails) {

                    console.log(customerdetails.first_name);


                    if(customerdetails && (customerdetails.first_name ==null || customerdetails.first_name =='')){
                        console.log(11111111111111);
                        models.customer.update({
                            first_name: req.body.data.firstName ? req.body.data.firstName.trim():null,
                            last_name: req.body.data.lastName ? req.body.data.lastName.trim():null,
                        },{where:{id:customerId}}).then(function(customer) {
                            console.log(222222222222222222222); 
                        })
                        .catch(function(error) {
                            return res.send( {status:202, success: false, message: "error happened"});
                        });
                    }
                })
                .catch(function(error) {
                    return res.send( {status:202, success: false, message: "error happened"});
                });
                
                

            res.status(200).send({ status:200, success: true, message: "success", customeraddress:customer_address });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        });
    }
};

exports.appCustomerAddress = function(req, res, next){
    console.log(req.body.data);
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    existingItem = models.customer_address.findAll({ where: {customer_id : customerId } });
    existingItem.then(function (value) {
        //return res.send(value);
        if(value && value !=''){
            res.status(200).send({ status:200,success: true, value:value });
        }else{
            res.status(200).send({ status:205,success: false, message: "Address not found" });
        }
        
    }).catch(function(error) {
        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
    });       
}

exports.appCustomerAddressEdit = function(req, res, next) {
        
    var updateId= req.body.data.updateId ? req.body.data.updateId : '';
    //var customerId= req.body.data.customerId ? req.body.data.customerId : '';

    if(!updateId){
        res.status(200).send({ status:205,message: "updateId not found" });
    }else{
        models.customer_address.update({
            //customer_id: customerId,
            first_name:req.body.data.firstName ? req.body.data.firstName:null,
            last_name:req.body.data.lastName ? req.body.data.lastName:null,
            mobile:req.body.data.mobile ? req.body.data.mobile:null,
            city:req.body.data.city ? req.body.data.city:null,
            state:req.body.data.state ? req.body.data.state:null,
            pin:req.body.data.postCode ? req.body.data.postCode:null,
            country: req.body.data.country ? req.body.data.country:null, 
            address:req.body.data.address ? req.body.data.address:null,
            //is_prime_address: req.body.data.is_prime_address, 
        },{where:{id:updateId}}).then(function(value) {
            res.status(200).send({ status:200,value:value });
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        });
    }
};

exports.appCustomerAddressDelete = function(req, res, next) {
        
    var Id= req.body.data.id ? req.body.data.id : '';

    if(!Id){	
        res.status(200).send({ status:200,success: false, message: "Id not found", });
    }else{
        models.customer_address.destroy({ 
            where:{id:Id}
        }).then(function(value) {
            res.status(200).send({ status:200,value:value });
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        }); 
    }
};

exports.appCustomerAddressPrime = function(req, res, next){
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    var customerAddressId= req.body.data.customerAddressId ? req.body.data.customerAddressId : '';

    if(!customerId && customerId==''){
        return res.status(200).send({ status:200,success: false, message: "Customer Id not found", });
    }else{
        if(!customerAddressId && customerAddressId==''){
            return res.status(200).send({ status:200,success: false, message: "Customer Address Id not found", });
        }else{

            existingItem = models.customer_address.findOne({ where: {id : customerAddressId } });
            existingItem.then(function (value) {
                if(!value){
                    res.status(200).send({ status:205, success: true, message: "Customer Address not found" });
                }else{
                    models.customer_address.update({
                        is_prime_address:0        
                    },{where:{customer_id:customerId}}).then(function(customer) {

                        if(!customer || customer ==0 ){
                            return res.status(200).send({ status:200,success: false, message: "Customer not found", });
                        }else{
                            models.customer_address.update({
                                is_prime_address:1        
                            },{where:{id:customerAddressId}}).then(function(customer_address) {

                                return res.status(200).send({ status:200, success: true, message: "success", customerdetail:customer });        
                            })
                            .catch(function(error) {
                                return res.send( {success: false, error});
                            });
                        }
                    })
                    .catch(function(error) {
                        return res.send( {success: false, error});
                    });
                }
            });
        }
    }
}

/////////////////////////////// app customer address end//////////////////////////////////

/////////////////////////////// app add to cart start//////////////////////////////////
exports.appCartAdd = function(req, res, next) {
    if(req.body.data.customerId && req.body.data.customerId!=''){
        var customerId= req.body.data.customerId ? req.body.data.customerId : '';
        var productId= req.body.data.product_id ? req.body.data.product_id : '';

        existingItem = models.cart.findOne({ where: {customer_id : customerId, product_id : productId } });
        existingItem.then(function (cart_item) {
            if(!cart_item)
            {
                models.cart.create({ 
                    customer_id: customerId,
                    product_id: req.body.data.product_id ? req.body.data.product_id:null,
                    item_quantity: req.body.data.item_quantity ? req.body.data.item_quantity:null,
                }).then(function(cart) {
    
                res.status(200).send({ status:200, success: true, message: "Item successfully added", cart:cart });
                })
                .catch(function(error) {
                    return res.send( {success: false, error});
                });
            }else{
                //if(cart_item.item_quantity==req.body.data.item_quantity){

                    res.status(200).send({ status:205, success: true, message: "Item already added in the cart" });
                // }else{
                //     models.cart.update({ 
                //         //customer_id: customerId,
                //         //product_id: req.body.data.product_id ? req.body.data.product_id:null,
                //         item_quantity: req.body.data.item_quantity ? req.body.data.item_quantity:null,
                //     },{where:{customer_id:customerId, product_id:productId}}).then(function(cart) {
        
                //     res.status(200).send({ status:200, success: true, message: "success", cart:cart });
                //     })
                //     .catch(function(error) {
                //         return res.send( {success: false, error});
                //     });

                // }

            }
        });
    }
};

exports.appCartDelete = function(req, res, next) {
        
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    var productId= req.body.data.product_id ? req.body.data.product_id : '';

    models.cart.destroy({ 
        where:{customer_id:customerId,product_id:productId}
    }).then(function(value) {
        res.status(200).send({ status:200,value:value, message: "item successfully removed from your cart" });
    }).catch(function(error) {
        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
    }); 
};

exports.appCartListing = function(req, res, next){
    console.log(req.body.data);
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    if(!customerId){	
        res.status(200).send({ status:200,success: false, message: "customerId not found", });
    }else{  
        //existingItem = models.customer_address.findAll({ where: {customer_id : customerId } });
        //existingItem.then(function (value) {
        sequelize.query("SELECT cart.id as cart_id, cart.customer_id as customer_id, cart.product_id as product_id, cart.item_quantity as item_quantity, product.*, '5' AS max_pro_availability FROM `cart` LEFT JOIN `product` ON cart.product_id = product.id where `cart`.`customer_id` = "+customerId,{ type: Sequelize.QueryTypes.SELECT }) 
        .then(function (value) {

            if(value!=null && value!=''){	
                res.status(200).send({ status:200,success: true, value:value });

            }else{				
                res.status(200).send({status:200, success: false, value:[], message: "No cart data found" });
            }
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        });
        
        
    }       
}

exports.appCartItemAdd = function(req, res, next) {
    if(req.body.data.customerId && req.body.data.customerId!=''){
        var customerId= req.body.data.customerId ? req.body.data.customerId : '';
        var productId= req.body.data.product_id ? req.body.data.product_id : '';

        existingItem = models.cart.findOne({ where: {customer_id : customerId, product_id : productId } });
        existingItem.then(function (cart_item) {
            //existingItem = models.product.findOne({ where: { id : productId } });
            //existingItem.then(function (product) {

                //if(cart_item.item_quantity+req.body.data.item_quantity > product.inventory){	
                   // res.status(200).send({ status:200,success: true, message: "Item out of stock", });
               // }else{

                    //console.log(typeof cart_item.item_quantity);
                    //return res.send(typeof req.body.data.item_quantity);
                    models.cart.update({ 
                        item_quantity:cart_item.item_quantity+req.body.data.item_quantity,
                    },{where:{customer_id:customerId, product_id:productId}}).then(function(cart) {

                    res.status(200).send({ status:200, success: true, message: "success", cart:cart });
                    })
                    .catch(function(error) {
                        return res.send( {success: false, error});
                    });
                //} 
            //});
        });    
    }
};

exports.appCartItemRemove = function(req, res, next) {
    if(req.body.data.customerId && req.body.data.customerId!=''){
        var customerId= req.body.data.customerId ? req.body.data.customerId : '';
        var productId= req.body.data.product_id ? req.body.data.product_id : '';

        existingItem = models.cart.findOne({ where: {customer_id : customerId, product_id : productId } });
        existingItem.then(function (cart_item) {

            //console.log(typeof cart_item.item_quantity);
            //return res.send(typeof req.body.data.item_quantity);
            //return res.send(typeof Number(req.body.data.item_quantity));
            models.cart.update({
                item_quantity:cart_item.item_quantity-req.body.data.item_quantity,
            },{where:{customer_id:customerId, product_id:productId}}).then(function(cart) {

            res.status(200).send({ status:200, success: true, message: "success", cart:cart });
            })
            .catch(function(error) {
                return res.send( {success: false, error});
            });
        });
    }
};

exports.appMultipleCartItemAdd = function(req, res, next) {

var cartItem= req.body.data ? req.body.data : [];
    //console.log(cartItem.length);
    if(cartItem){
        // var i=0;
        cartItem.forEach(function(element) {
            //console.log(element);				
            models.cart.create({
                customer_id: element.customerId ? element.customerId:null,
                product_id: element.product_id ? element.product_id:null,
                item_quantity: element.item_quantity ? element.item_quantity:null,
            });
            //i++;
        }, this);
        res.status(200).send({ status:200, success: true, message: "Items successfully added" });
    }
};

/////////////////////////////// app add to cart end//////////////////////////////////
exports.appForgotPassword = function(req, res, next){
    console.log(req.body.data);
    //var email_id= req.body.data.email ? req.body.data.email : '';

    if(req.body.data.email && req.body.data.email!=''){
    //existingItem = models.customer_email.findOne({ where: {customer_email_id : email_id, is_prime_email : 1 } });
    //existingItem.then(function (customer_email) {

        //if(customer_email!=null){
            //customer_email_data = customer_email.toJSON();  
            models.customer.findOne({ where: {'email':req.body.data.email,'status':'active'} })
            .then(function(customer) {
                if(customer){
                    res.status(200).send({ success: true, message: "customer found", customer_data:customer.id });
                }else{
                    res.status(200).send({ success: false, message: "No customer found", customer_data:customer });
                }
            }).catch(function(error) {
                res.status(400).send({success: false, message: "No customer found",data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
            });               
            
        //}else{				
        //    res.status(200).send({ success: false, message: "No customer found" });
        //}
    }else{
        res.status(200).send({ success: false, message: "Please Provide Email Id" });
    }
    
    //});       
}

exports.appResetPassword = function(req, res, next){
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    var hash = bcrypt.hashSync(req.body.data.password);	
    models.customer.update({
        password:hash        
    },{where:{id:customerId}}).then(async function(customer) {

        var contact = await helpers.getEmailConfig();
        var email =contact[1];
        var phoneno =contact[0];

        return new Promise((resolve, reject) => {
            var data = {
                from: 'Grocery User <me@samples.mailgun.org>',
                // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                subject: 'Password Reset',
                html: '<!DOCTYPE html>'+
                '<html>'+    
                '<head>'+
                '</head>'+    
                '<body>'+
                    '<div>'+
                        '<p>Hello Sir/Madam,</p>'+
                        '<p>Thank you for using One Tap Grocery password reset wizard.</p>'+  
                        '<p>For any kind of query reach us at: <br />'+
                        'Mail: " '+email+' " <br />'+
                        'Phone: " '+phoneno+' " <br />'+
                        'Have a nice shopping.</p>'+
                        '<p>Thanks.</p>'+
                        '<p>Regards,<br />'+
                        'One Tap Grocery<br />'+
                        'Midnapore</p>'+								
                    '</div>'+       
                '</body>'+    
                '</html>' 
            };
            mailgun.messages().send(data, function (error, body) {
                if (error) {
                    return reject(res.status(200).send({ status:200, success: true, message: "success", customerdetail:customer }));
                }
                return resolve(res.status(200).send({ status:200, success: true, message: "success", customerdetail:customer }));
    
            });
        });
        
    })
    .catch(function(error) {
        return res.send( {success: false, error});
    });
}

exports.appChangePassword = function(req, res) { 

    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    var newPassword= req.body.data.password ? req.body.data.password : '';
    if(!newPassword && newPassword==''){
        res.status(200).send({ status:200, success: true, message: "Please enter new password" }); 
    }else{
        
        var hash = bcrypt.hashSync(req.body.data.password);

        models.customer.findOne({ where: {'id':customerId} })
        .then(function(users) {
            //res.send(users);
            if(users!=null){
                user = users.toJSON();	   
                if(!bcrypt.compareSync(req.body.data.oldPassword, user.password)) {		   
                    res.status(200).send({ status:200, success: true, message: "Old password does not match" });
                } else {                        
                    models.customer.update({
                        password:hash        
                    },{where:{id:customerId}}).then(async function(customer) {
                        
                        var contact = await helpers.getEmailConfig();
                        var email =contact[1];
                        var phoneno =contact[0];

                        return new Promise((resolve, reject) => {
                            var data = {
                                from: 'Grocery User <me@samples.mailgun.org>',
                                // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                subject: 'Password Reset',
                                html: '<!DOCTYPE html>'+
                                '<html>'+    
                                '<head>'+
                                '</head>'+    
                                '<body>'+
                                    '<div>'+
                                        '<p>Hello Sir/Madam,</p>'+
                                        '<p>Thank you for using One Tap Grocery password reset wizard.</p>'+  
                                        '<p>For any kind of query reach us at: <br />'+
                                        'Mail: " '+email+' " <br />'+
                                        'Phone: " '+phoneno+' "<br />'+
                                        'Have a nice shopping.</p>'+
                                        '<p>Thanks.</p>'+
                                        '<p>Regards,<br />'+
                                        'One Tap Grocery<br />'+
                                        'Midnapore</p>'+								
                                    '</div>'+       
                                '</body>'+    
                                '</html>' 
                            };
                            mailgun.messages().send(data, function (error, body) {
                                if (error) {
                                    return reject(res.status(200).send({ status:200, success: true, message: "Password successfully changed", customerdetail:customer }));
                                }
                                return resolve(res.status(200).send({ status:200, success: true, message: "Password successfully changed", customerdetail:customer }));
                    
                            });
                        });
                        
                    })
                    .catch(function(error) {
                        return res.send( {success: false, error});
                    });
                } 
            }else{                
                res.status(200).send({ message: "No customer found" });
            } 
        })
        .catch(function(error) {
            return res.send(error);        
        });
    }
   
};

///////////////////////////////////app customer profile 19 02 2019 start ////////////////////////


exports.appShowProfile = function(req, res, next){
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    var existingItem = null;
    var existingItem1 = null;
    if(!customerId){	
        res.status(200).send({ status:200,success: false, message: "customerId not found", });
    }else{            
        existingItem = models.customer.findOne({ where: {id:customerId} });
        existingItem.then(function (customer) {
            existingItem1 = models.customer_address.findOne({ where: {customer_id:customerId, is_prime_address: 1} });
            existingItem1.then(function (customer_address) {
                if(customer!=null){
                    res.status(200).send({ status:200,customer: customer,customer_address: customer_address});
                }else{				
                    res.status(200).send({ success: false, message: "No customer found" });
                }
            });	
        });	
    }
};

exports.appUpdateProfile = function(req, res, next){
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    var email= req.body.data.email ? req.body.data.email : '';
    var phone= req.body.data.phone ? req.body.data.phone : '';
    //var hash = bcrypt.hashSync(req.body.data.password);

    sequelize.query("SELECT * FROM customer WHERE id != "+customerId+" AND (`email` = '"+email+"' OR `phone` = "+phone+")",
    { type: Sequelize.QueryTypes.SELECT })
    .then(function (customer) {
//return res.send(customer);
        if(customer!=null && customer!=''){
           
            res.status(200).send({ success: false, message: "Phone or email already used" });
                
        }else{				
            models.customer.update({ 
            //first_name: req.body.data.firstName ? req.body.data.firstName:null,
            //last_name: req.body.data.lastName ? req.body.data.lastName:null,
            first_name: req.body.data.firstName ? req.body.data.firstName.trim():null,
            last_name: req.body.data.lastName ? req.body.data.lastName.trim():null,
            //password: hash,
            email: req.body.data.email ? req.body.data.email:null,
            phone: req.body.data.phone ? req.body.data.phone:null,
            },{where:{id:customerId}}).then(function(customer) {

               models.customer.findOne({ where: {id:customerId} })
                .then(function (customerdetails) {
                    res.status(200).send({ status:200, success: true, message: "success",customer:customerdetails });
                })
                .catch(function(error) {
                    return res.send( {status:202, success: false, message: "error happened"});
                });
    
                
            })
            .catch(function(error) {
                return res.send( {status:202, success: false, message: "error happened"});
            });
        }

    });
}
///////////////////////////////////app customer profile 19 02 2019 end ////////////////////////

// exports.appProductFilter = function(req, res, next){
//     var price= req.body.data.price ? req.body.data.price : '';
//     var categoryId= req.body.data.categoryId ? req.body.data.categoryId : [];
//     var existingItem = null;
//     console.log(querySql)
//         // console.log(price)
//         // console.log(categoryId)
//         // console.log(Array.isArray(categoryId))
//         // console.log(categoryId.length)
//     if(!price && !categoryId){
//         res.status(200).send({ success: false, message: "please select at least one" });
//     }else{

//         if(!Array.isArray(categoryId) || categoryId.length < 1){
//             var querySql="select * FROM `product` where price BETWEEN 1 AND "+price+"";
    
//         }else if(!price || price==''){
//             var querySql="select * FROM `product` where category_id IN("+categoryId+")";
    
//         }else{
//             var querySql="select * FROM `product` where price BETWEEN 1 AND "+price+" AND category_id IN("+categoryId+")";
//         } 
                        
//         sequelize.query(querySql,
//         { type: Sequelize.QueryTypes.SELECT })
//         .then(function (product) {
//             if(product!=null && product!=''){
//                 res.status(200).send({ status:200,product: product });
//             }else{				
//                 res.status(200).send({ success: false, message: "No product found" });
//             }
//         })
//         .catch(function(error) {
//             res.status(200).send({  success: false, message: "No product found"  });
//         });            
//     }    
// };

exports.appProductFilter = function(req, res, next){
    var price= req.body.data.price ? req.body.data.price : '';
    if(price !=''){
        if(Number(price) >= 5000)
        price = 1000000;
    }
    var categoryId= req.body.data.categoryId ? req.body.data.categoryId : [];
    var existingItem = null;
    //console.log(price)
    //console.log(categoryId)
    //console.log(Array.isArray(categoryId))
    //console.log(categoryId.length)
    var resultArray = [];
    if((!price || price=='') && (!categoryId || categoryId == '')){
        res.status(200).send({ status:200, product:resultArray, message: "please select at least one" });
    }else{
        if(!Array.isArray(categoryId) || categoryId.length < 1){
            // var querySql="select *, '5' AS max_pro_availability FROM `product` where is_configurable=0 and price BETWEEN 1 AND "+price+"";
            var querySql="select product.*, site_settings.max_pro_availability FROM `product` LEFT JOIN `site_settings` ON site_settings.id = 1 where product.is_configurable=0 and product.price BETWEEN 1 AND "+price+"";
    
        }else if(!price || price==''){
            // var querySql="select *, '5' AS max_pro_availability FROM `product` where is_configurable=0 and category_id IN("+categoryId+")";
            var querySql="select product.*, site_settings.max_pro_availability FROM `product` LEFT JOIN `site_settings` ON site_settings.id = 1 where product.is_configurable=0 and product.category_id IN("+categoryId+")";
    
        }else{
            // var querySql="select *, '5' AS max_pro_availability FROM `product` where is_configurable=0 and price BETWEEN 1 AND "+price+" AND category_id IN("+categoryId+")";
            var querySql="select product.*, site_settings.max_pro_availability FROM `product` LEFT JOIN `site_settings` ON site_settings.id = 1 where product.is_configurable=0 and product.price BETWEEN 1 AND "+price+" AND product.category_id IN("+categoryId+")";
        } 
        sequelize.query(querySql,{ type: Sequelize.QueryTypes.SELECT })
        .then(function (value) {           
            if(value.length > 0){
                var i = 0;
                value.forEach(function(element,index){ 
                    // sequelize.query("SELECT *,'5' AS max_pro_availability FROM product WHERE status='active' and is_configurable="+"'"+element.id+"'",{ type: Sequelize.QueryTypes.SELECT })  
                    sequelize.query("SELECT product.*,site_settings.max_pro_availability FROM product LEFT JOIN `site_settings` ON site_settings.id = 1 WHERE product.status='active' and product.is_configurable="+"'"+element.id+"'",{ type: Sequelize.QueryTypes.SELECT })  
                    // models.product.findAll({ where: {status:'active',is_configurable:element.id} })
                    .then(function(product_unit) {
                        console.log(product_unit)
                        resultArray.push({
                            "id":element.id,
                            "mainId":product_unit ? product_unit.id :'',
                            "title":element.title,
                            "short_description":element.short_description,
                            "description":element.description,
                            "type":element.type,
                            "size":element.weight,
                            "special_price":element.special_price,
                            "price":element.price,
                            "quantity":element.inventory,
                            "image":element.image,
                            "max_pro_availability":element.max_pro_availability,
                            "spice_level":element.spice_level,
                            "unit":product_unit ? product_unit.concat(value[index]) : value[index]
                        });    
                        i++;
                        if(i== value.length){
                            res.status(200).send({ status:200,value:resultArray });
                        }            
                    });                
                });           
            }else{
                res.status(200).send({ status:200,product:resultArray });
            }        
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        });   
    }    
};

/////////////////////// app order start ///////////////////////

exports.appOrder = function(req, res, next){

    var store_id = req.body.data.store_id;
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    var customerAddressId= req.body.data.customerAddressId ? req.body.data.customerAddressId : '';
    // console.log("pppppppppppppppppppppppppppppppppppp")
    // console.log(customerId)
    console.log(req.body.data);
    if(req.body.data.coupon_code && req.body.data.coupon_code!=''){
        models.coupon.findOne({where: {coupon_code : req.body.data.coupon_code, status:'active' }})
        .then(function (coupon) {
            if(coupon!=null && coupon!=''){
                if(coupon.is_share == 'yes'){
                    models.coupon_transaction.findOne({where: {customer_id : customerId, coupon_code: req.body.data.coupon_code, status:'active',is_share:'yes' }})
                    .then(function (coupon_code_check) {
                        if(coupon_code_check && coupon_code_check!=''){
                            models.coupon_transaction.create({ 
                                customer_id	: customerId	? customerId	 : '',
                                applied_amount	: req.body.data.coupon_amount	? req.body.data.coupon_amount	 : '',
                                coupon_type: coupon.coupon_type? coupon.coupon_type : '',
                                coupon_value	: coupon.coupon_value	? coupon.coupon_value	 : '',
                                date_from	: coupon.date_from	? coupon.date_from	 : '',
                                date_to	: coupon.date_to	? coupon.date_to	 : '',
                                time_from: coupon.time_from? coupon.time_from : '',
                                time_to: coupon.time_to? coupon.time_to : '',
                                coupon_code	: coupon.coupon_code	? coupon.coupon_code	 : '',
                                purchase_limit: coupon.purchase_limit? coupon.purchase_limit : '',
                                is_share:'no',
                                status: coupon.status? coupon.status : '',
                           
                            }).then(function(coupon_transaction) {
                                if(store_id){
                                    existingItem = models.customer.findOne({ where: {id:customerId} });
                                    existingItem.then(function (customer) {
                                        existingItem = models.customer_address.findOne({ where: {customer_id:customerId,is_prime_address:1} });
                                        existingItem.then(function (customer_address) {
                                            existingItem = models.customer_address.findOne({ where: {id:customerAddressId} });
                                            existingItem.then(function (customer_shipping_address) { 
                                                var shipping_address = '';
                                                if(customer_shipping_address && customer_shipping_address!==''){
                                                    shipping_address = customer_shipping_address.first_name+' '+customer_shipping_address.last_name+'; '+customer_shipping_address.address+'; '+customer_shipping_address.city+' '+customer_shipping_address.pin+'; '+customer_shipping_address.country+'; '+customer_shipping_address.mobile;   
                                                }                       
                                                models.order.create({                     
                                                    store_id: req.body.data.store_id ? req.body.data.store_id:null,
                                                    order_status: req.body.data.order_status ? req.body.data.order_status:'Processing',
                                                    shipping_method: req.body.data.shiping_method ? req.body.data.shiping_method:null,
                                                    shipping_description: req.body.data.shipping_description ? req.body.data.shipping_description:null,
                                                    payment_method: req.body.data.payment_method ? req.body.data.payment_method:null, 
                                                    customer_id: req.body.data.customerId ? req.body.data.customerId:null,
                                                    salesman_id: req.body.data.salesman_id ? req.body.data.salesman_id:null,
                                                    discount_percent: req.body.data.discount_percent ? req.body.data.discount_percent:null,
                                                    discount_amount: req.body.data.discount_amount ? req.body.data.discount_amount:null,
                                                    base_grand_total: req.body.data.base_grand_total ? req.body.data.base_grand_total:null,
                                                    shipping_amount: req.body.data.shipping_amount ? req.body.data.shipping_amount:null,
                                                    total_purchase: req.body.data.total_purchase ? req.body.data.total_purchase:null,
                                                    cgst: req.body.data.cgst ? req.body.data.cgst:null,
                                                    sgst:req.body.data.sgst ? req.body.data.sgst:null,
                                                    igst: req.body.data.igst ? req.body.data.igst:null,
                                                    total_tax: req.body.data.total_tax ? req.body.data.total_tax:null,
                                                    grand_total: req.body.data.grand_total ? req.body.data.grand_total:null,
                                                    amount_paid: req.body.data.amount_paid ? req.body.data.amount_paid:null,
                                                    coupon_amount: req.body.data.coupon_amount ? req.body.data.coupon_amount:null,
                                                    wallet_amount: req.body.data.wallet_amount ? req.body.data.wallet_amount:null,
                                                    coupon_code: req.body.data.coupon_code ? req.body.data.coupon_code:null,
                                                    customer_name:  customer && customer.first_name && customer.last_name ? customer.first_name+' '+customer.last_name : '',
                                                    customer_email	: customer.email,
                                                    customer_mobile: customer.phone,
                                                    street1: req.body.data.street1 ? req.body.data.street1:null,
                                                    street2: req.body.data.street2 ? req.body.data.street2:null,
                                                    address: customer_address.address ? customer_address.address:null,
                                                    city: customer_address.city ? customer_address.city:null,
                                                    state: req.body.data.state ? req.body.data.state:null,
                                                    pin: customer_address.pin ? customer_address.pin:null,
                                                    country: customer_address.country ? customer_address.country:null,
                                                    gift_message: req.body.data.gift_message ? req.body.data.gift_message:null,
                                                    delivery_time_slot: req.body.data.delivery_time_slot ? req.body.data.delivery_time_slot:null,
                                                    shipping_address: shipping_address ? shipping_address:null,
                                                    //cgst: req.body.data.cgst ? req.body.data.cgst:null,
                                                }).then(function(order) { 
                                                    var today =  dd_mm_yyyy();
                                                    var newOrder_id = 'OTG'+ today + order.id;
                                                    models.order.update({
                                                        order_id:newOrder_id ? newOrder_id : null,
                                                    },{where:{id:order.id}}).then(async function(order_update){

                                                        var contact = await helpers.getEmailConfig();
                                                        var email =contact[1];
                                                        var phoneno =contact[0];

                                                        if(req.body.data.wallet_amount && req.body.data.wallet_amount!='0'){
                                                            models.wallet.create({ 
                                                                cid	: customerId? customerId : '',
                                                                amount	: req.body.data.wallet_amount ? -Math.abs(req.body.data.wallet_amount) :'',
                                                                amount_type: 'debit',
                                                                description	: '',
                                                                status: 'active'                                                       
                                                            })                                                        
                                                        }
                                                        var productItem= req.body.data.products ? req.body.data.products : [];
                                                        if(productItem){
                                                            productItem.forEach(function(element) {
                            
                                                                if(element.special_price == 0.00){
                                                                    original_price = element.price ? element.price:null;
                                                                }else {
                                                                    original_price = element.special_price ? element.special_price:null;
                                                                }
                            
                                                                models.order_item.create({ 
                                                                    order_id: order.id,
                                                                    store_id: req.body.data.store_id ? req.body.data.store_id:null,
                                                                    product_id: element.product_id ? element.product_id:null,
                                                                    name: element.title ? element.title:null,
                                                                    description: element.description ? element.description:null,
                                                                    discount_percent: element.discount_percent ? element.discount_percent:null,
                                                                    discount_amount: element.discount_amount ? element.discount_amount:null,
                                                                    qty: element.item_quantity ? element.item_quantity:null,
                                                                    price: element.price ? element.price:null,
                                                                    original_price: original_price,
                                                                });
                                                            }, this);
                                                        }
                            
                            
                                                        if(customerId != ''){
                                                            models.cart.destroy({ 
                                                                where:{customer_id:customerId}
                                                            }).then(async function(cartdestroy) {

                                                                var contact = await helpers.getEmailConfig();
                                                                var email =contact[1];
                                                                var phoneno =contact[0];
                                                                
																if(newOrder_id){
																	var smsContent = 'Your order with the reference no. '+newOrder_id+' is successful. It will be delivered between '+ replaceDaysName(order.delivery_time_slot)+'.'
																	sms_controller.sendsms(customer.phone,smsContent);
																}
                                                                return new Promise((resolve, reject) => {
                                                                    var data = {
                                                                        from: 'Grocery User <me@samples.mailgun.org>',
                                                                        // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                                                        to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                                                        subject: 'Order',
                                                                        html: '<!DOCTYPE html>'+
                                                                        '<html>'+    
                                                                        '<head>'+
                                                                        '</head>'+    
                                                                        '<body>'+
                                                                            '<div>'+
                                                                                '<p>Hello Sir/Madam,</p>'+
                                                                                '<p>Thank you for your order with the reference no: '+newOrder_id+' . The total amount for your order is Rs. '+order.grand_total+' </p>'+  
                                                                                '<p>For any kind of query reach us at: <br />'+
                                                                                'Mail: " '+email+' " <br />'+
                                                                                'Phone: " '+phoneno+' " <br />'+
                                                                                'Have a nice shopping.</p>'+
                                                                                '<p>Thanks.</p>'+
                                                                                '<p>Regards,<br />'+
                                                                                'One Tap Grocery<br />'+
                                                                                'Midnapore</p>'+								
                                                                            '</div>'+       
                                                                        '</body>'+    
                                                                        '</html>' 
                                                                    };
                                                                    mailgun.messages().send(data, function (error, body) {
                                                                        if (error) {
                                                                            return reject(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                        }
                                                                        return resolve(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                    });
                                                                });
                                                                // res.status(200).send({ status:200, success: true, message: "success", });
                                                            }).catch(function(error) {
                                                                res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
                                                            }); 
                                                        }else{
                                                            return new Promise((resolve, reject) => {
                                                                var data = {
                                                                    from: 'Grocery User <me@samples.mailgun.org>',
                                                                    // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                                                    to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                                                    subject: 'Order',
                                                                    html: '<!DOCTYPE html>'+
                                                                    '<html>'+    
                                                                    '<head>'+
                                                                    '</head>'+    
                                                                    '<body>'+
                                                                        '<div>'+
                                                                            '<p>Hello Sir/Madam,</p>'+
                                                                            '<p>Thank you for your order with the reference no: '+newOrder_id+' . The total amount for your order is Rs. '+order.grand_total+' </p>'+  
                                                                            '<p>For any kind of query reach us at: <br />'+
                                                                            'Mail: " '+email+' " <br />'+
                                                                            'Phone: " '+phoneno+' " <br />'+
                                                                            'Have a nice shopping.</p>'+
                                                                            '<p>Thanks.</p>'+
                                                                            '<p>Regards,<br />'+
                                                                            'One Tap Grocery<br />'+
                                                                            'Midnapore</p>'+								
                                                                        '</div>'+       
                                                                    '</body>'+    
                                                                    '</html>' 
                                                                };
                                                                mailgun.messages().send(data, function (error, body) {
                                                                    if (error) {
                                                                        return reject(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                    }
                                                                    return resolve(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                });
                                                            });
                                                            // res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id});
                                                        }
                                                    })
                                                    .catch(function(error) {
                                                        res.status(200).send({ status:200, success: false,  message: "Sorry order failed" });
                                                    }); 
                                                
                                                })
                                                .catch(function(error) {
                                                    res.status(200).send({ status:200, success: false,  message: "Sorry order failed" });
                                                });
                                            })    
                            
                                        });
                                    });
                                } 
                            })
                            .catch(function(error) {
                                res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
                            });
                        }else{
                            models.coupon_transaction.create({ 
                                customer_id	: customerId	? customerId	 : '',
                                applied_amount	: req.body.data.coupon_amount	? req.body.data.coupon_amount	 : '',
                                coupon_type: coupon.coupon_type? coupon.coupon_type : '',
                                coupon_value	: coupon.coupon_value	? coupon.coupon_value	 : '',
                                date_from	: coupon.date_from	? coupon.date_from	 : '',
                                date_to	: coupon.date_to	? coupon.date_to	 : '',
                                time_from: coupon.time_from? coupon.time_from : '',
                                time_to: coupon.time_to? coupon.time_to : '',
                                coupon_code	: coupon.coupon_code	? coupon.coupon_code	 : '',
                                purchase_limit: coupon.purchase_limit? coupon.purchase_limit : '',
                                is_share:'yes',
                                status: coupon.status? coupon.status : '',                           
                            }).then(function(coupon_transaction) {
                                models.customer.findOne({ where: {sponsor_code:coupon.coupon_code, status:'active'} })
                                .then(function (customer_sponsor) {
                                    if(customer_sponsor && customer_sponsor!=''){
                                        models.wallet.create({ 
                                            cid	: customer_sponsor? customer_sponsor.id : '',
                                            amount	: coupon.coupon_value ? Math.abs(coupon.coupon_value) :'',
                                            amount_type: 'credit',
                                            description	: '',
                                            status: 'active'                                                       
                                        })   
                                    }
                                    if(store_id){
                                        existingItem = models.customer.findOne({ where: {id:customerId} });
                                        existingItem.then(function (customer) {
                                            existingItem = models.customer_address.findOne({ where: {customer_id:customerId,is_prime_address:1} });
                                            existingItem.then(function (customer_address) {
                                                existingItem = models.customer_address.findOne({ where: {id:customerAddressId} });
                                                existingItem.then(function (customer_shipping_address) { 
                                                    var shipping_address = '';
                                                    if(customer_shipping_address && customer_shipping_address!==''){
                                                        shipping_address = customer_shipping_address.first_name+' '+customer_shipping_address.last_name+'; '+customer_shipping_address.address+'; '+customer_shipping_address.city+' '+customer_shipping_address.pin+'; '+customer_shipping_address.country+'; '+customer_shipping_address.mobile;   
                                                    }                       
                                                    models.order.create({                     
                                                        store_id: req.body.data.store_id ? req.body.data.store_id:null,
                                                        order_status: req.body.data.order_status ? req.body.data.order_status:'Processing',
                                                        shipping_method: req.body.data.shiping_method ? req.body.data.shiping_method:null,
                                                        shipping_description: req.body.data.shipping_description ? req.body.data.shipping_description:null,
                                                        payment_method: req.body.data.payment_method ? req.body.data.payment_method:null, 
                                                        customer_id: req.body.data.customerId ? req.body.data.customerId:null,
                                                        salesman_id: req.body.data.salesman_id ? req.body.data.salesman_id:null,
                                                        discount_percent: req.body.data.discount_percent ? req.body.data.discount_percent:null,
                                                        discount_amount: req.body.data.discount_amount ? req.body.data.discount_amount:null,
                                                        base_grand_total: req.body.data.base_grand_total ? req.body.data.base_grand_total:null,
                                                        shipping_amount: req.body.data.shipping_amount ? req.body.data.shipping_amount:null,
                                                        total_purchase: req.body.data.total_purchase ? req.body.data.total_purchase:null,
                                                        cgst: req.body.data.cgst ? req.body.data.cgst:null,
                                                        sgst:req.body.data.sgst ? req.body.data.sgst:null,
                                                        igst: req.body.data.igst ? req.body.data.igst:null,
                                                        total_tax: req.body.data.total_tax ? req.body.data.total_tax:null,
                                                        grand_total: req.body.data.grand_total ? req.body.data.grand_total:null,
                                                        amount_paid: req.body.data.amount_paid ? req.body.data.amount_paid:null,
                                                        coupon_amount: req.body.data.coupon_amount ? req.body.data.coupon_amount:null,
                                                        wallet_amount: req.body.data.wallet_amount ? req.body.data.wallet_amount:null,
                                                        coupon_code: req.body.data.coupon_code ? req.body.data.coupon_code:null,
                                                        customer_name:  customer && customer.first_name && customer.last_name ? customer.first_name+' '+customer.last_name : '',
                                                        customer_email	: customer.email,
                                                        customer_mobile: customer.phone,
                                                        street1: req.body.data.street1 ? req.body.data.street1:null,
                                                        street2: req.body.data.street2 ? req.body.data.street2:null,
                                                        address: customer_address.address ? customer_address.address:null,
                                                        city: customer_address.city ? customer_address.city:null,
                                                        state: req.body.data.state ? req.body.data.state:null,
                                                        pin: customer_address.pin ? customer_address.pin:null,
                                                        country: customer_address.country ? customer_address.country:null,
                                                        gift_message: req.body.data.gift_message ? req.body.data.gift_message:null,
                                                        delivery_time_slot: req.body.data.delivery_time_slot ? req.body.data.delivery_time_slot:null,
                                                        shipping_address: shipping_address ? shipping_address:null,
                                                        //cgst: req.body.data.cgst ? req.body.data.cgst:null,
                                                    }).then(function(order) { 
                                                        var today =  dd_mm_yyyy();
                                                        var newOrder_id = 'OTG'+ today + order.id;
                                                        models.order.update({
                                                            order_id:newOrder_id ? newOrder_id : null,
                                                        },{where:{id:order.id}}).then(async function(order_update){

                                                            var contact = await helpers.getEmailConfig();
                                                            var email =contact[1];
                                                            var phoneno =contact[0];

                                                            if(req.body.data.wallet_amount && req.body.data.wallet_amount!='0'){
                                                                models.wallet.create({ 
                                                                    cid	: customerId? customerId : '',
                                                                    amount	: req.body.data.wallet_amount ? -Math.abs(req.body.data.wallet_amount) :'',
                                                                    amount_type: 'debit',
                                                                    description	: '',
                                                                    status: 'active'                                                       
                                                                })                                                        
                                                            }
                                                            var productItem= req.body.data.products ? req.body.data.products : [];
                                                            if(productItem){
                                                                productItem.forEach(function(element) {
                                
                                                                    if(element.special_price == 0.00){
                                                                        original_price = element.price ? element.price:null;
                                                                    }else {
                                                                        original_price = element.special_price ? element.special_price:null;
                                                                    }
                                
                                                                    models.order_item.create({ 
                                                                        order_id: order.id,
                                                                        store_id: req.body.data.store_id ? req.body.data.store_id:null,
                                                                        product_id: element.product_id ? element.product_id:null,
                                                                        name: element.title ? element.title:null,
                                                                        description: element.description ? element.description:null,
                                                                        discount_percent: element.discount_percent ? element.discount_percent:null,
                                                                        discount_amount: element.discount_amount ? element.discount_amount:null,
                                                                        qty: element.item_quantity ? element.item_quantity:null,
                                                                        price: element.price ? element.price:null,
                                                                        original_price: original_price,
                                                                    });
                                                                }, this);
                                                            }
                                
                                
                                                            if(customerId != ''){
                                                                models.cart.destroy({ 
                                                                    where:{customer_id:customerId}
                                                                }).then(async function(cartdestroy) {

                                                                    var contact = await helpers.getEmailConfig();
                                                                    var email =contact[1];
                                                                    var phoneno =contact[0];
                                                                    
                                                                    var smsContent = 'Your order with the reference no. '+newOrder_id+' is successful. It will be delivered between '+ replaceDaysName(order.delivery_time_slot)+'.'
																	sms_controller.sendsms(customer.phone,smsContent);
                                                                    return new Promise((resolve, reject) => {
                                                                        var data = {
                                                                            from: 'Grocery User <me@samples.mailgun.org>',
                                                                            // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                                                            to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                                                            subject: 'Order',
                                                                            html: '<!DOCTYPE html>'+
                                                                            '<html>'+    
                                                                            '<head>'+
                                                                            '</head>'+    
                                                                            '<body>'+
                                                                                '<div>'+
                                                                                    '<p>Hello Sir/Madam,</p>'+
                                                                                    '<p>Thank you for your order with the reference no: '+newOrder_id+' . The total amount for your order is Rs. '+order.grand_total+' </p>'+  
                                                                                    '<p>For any kind of query reach us at: <br />'+
                                                                                    'Mail: " '+email+' " <br />'+
                                                                                    'Phone: " '+phoneno+' " <br />'+
                                                                                    'Have a nice shopping.</p>'+
                                                                                    '<p>Thanks.</p>'+
                                                                                    '<p>Regards,<br />'+
                                                                                    'One Tap Grocery<br />'+
                                                                                    'Midnapore</p>'+								
                                                                                '</div>'+       
                                                                            '</body>'+    
                                                                            '</html>' 
                                                                        };
                                                                        mailgun.messages().send(data, function (error, body) {
                                                                            if (error) {
                                                                                return reject(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                            }
                                                                            return resolve(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                        });
                                                                    });
                                                                    // res.status(200).send({ status:200, success: true, message: "success", });
                                                                }).catch(function(error) {
                                                                    res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
                                                                }); 
                                                            }else{
                                                                return new Promise((resolve, reject) => {
                                                                    var data = {
                                                                        from: 'Grocery User <me@samples.mailgun.org>',
                                                                        // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                                                        to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                                                        subject: 'Order',
                                                                        html: '<!DOCTYPE html>'+
                                                                        '<html>'+    
                                                                        '<head>'+
                                                                        '</head>'+    
                                                                        '<body>'+
                                                                            '<div>'+
                                                                                '<p>Hello Sir/Madam,</p>'+
                                                                                '<p>Thank you for your order with the reference no: '+newOrder_id+' . The total amount for your order is Rs. '+order.grand_total+' </p>'+  
                                                                                '<p>For any kind of query reach us at: <br />'+
                                                                                'Mail: " '+email+' " <br />'+
                                                                                'Phone: " '+phoneno+' " <br />'+
                                                                                'Have a nice shopping.</p>'+
                                                                                '<p>Thanks.</p>'+
                                                                                '<p>Regards,<br />'+
                                                                                'One Tap Grocery<br />'+
                                                                                'Midnapore</p>'+								
                                                                            '</div>'+       
                                                                        '</body>'+    
                                                                        '</html>' 
                                                                    };
                                                                    mailgun.messages().send(data, function (error, body) {
                                                                        if (error) {
                                                                            return reject(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                        }
                                                                        return resolve(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                    });
                                                                });
                                                                // res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id});
                                                            }
                                                        })
                                                        .catch(function(error) {
                                                            res.status(200).send({ status:200, success: false,  message: "Sorry order failed" });
                                                        }); 
                                                    
                                                    })
                                                    .catch(function(error) {
                                                        res.status(200).send({ status:200, success: false,  message: "Sorry order failed" });
                                                    });
                                                })    
                                
                                            });
                                        });
                                    } 

                                }).catch(function(error) {
                                    res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                                });                                 
                            })
                            .catch(function(error) {
                                res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
                            });
                        }

                    }).catch(function(error) {
                        res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
                    });
                }else{
                    models.coupon_transaction.findOne({where: {customer_id : customerId, coupon_code: req.body.data.coupon_code, status:'active' }})
                    .then(function (coupon_code) {
                        if(coupon_code!=null && coupon_code!=''){
                            res.status(200).send({ status:200, success: false, message: "Coupon already used"});
                        }else{
                            models.coupon_transaction.create({ 
                                customer_id	: customerId	? customerId	 : '',
                                applied_amount	: req.body.data.coupon_amount	? req.body.data.coupon_amount	 : '',
                                coupon_type: coupon.coupon_type? coupon.coupon_type : '',
                                coupon_value	: coupon.coupon_value	? coupon.coupon_value	 : '',
                                date_from	: coupon.date_from	? coupon.date_from	 : '',
                                date_to	: coupon.date_to	? coupon.date_to	 : '',
                                time_from: coupon.time_from? coupon.time_from : '',
                                time_to: coupon.time_to? coupon.time_to : '',
                                coupon_code	: coupon.coupon_code	? coupon.coupon_code	 : '',
                                purchase_limit: coupon.purchase_limit? coupon.purchase_limit : '',
                                is_share:'no',
                                status: coupon.status? coupon.status : '',
                           
                            }).then(function(coupon_transaction) {
                                if(store_id){
                                    existingItem = models.customer.findOne({ where: {id:customerId} });
                                    existingItem.then(function (customer) {
                                        existingItem = models.customer_address.findOne({ where: {customer_id:customerId,is_prime_address:1} });
                                        existingItem.then(function (customer_address) {
                                            existingItem = models.customer_address.findOne({ where: {id:customerAddressId} });
                                            existingItem.then(function (customer_shipping_address) { 
                                                var shipping_address = '';
                                                if(customer_shipping_address && customer_shipping_address!==''){
                                                    shipping_address = customer_shipping_address.first_name+' '+customer_shipping_address.last_name+'; '+customer_shipping_address.address+'; '+customer_shipping_address.city+' '+customer_shipping_address.pin+'; '+customer_shipping_address.country+'; '+customer_shipping_address.mobile;   
                                                }                       
                                                models.order.create({                     
                                                    store_id: req.body.data.store_id ? req.body.data.store_id:null,
                                                    order_status: req.body.data.order_status ? req.body.data.order_status:'Processing',
                                                    shipping_method: req.body.data.shiping_method ? req.body.data.shiping_method:null,
                                                    shipping_description: req.body.data.shipping_description ? req.body.data.shipping_description:null,
                                                    payment_method: req.body.data.payment_method ? req.body.data.payment_method:null, 
                                                    customer_id: req.body.data.customerId ? req.body.data.customerId:null,
                                                    salesman_id: req.body.data.salesman_id ? req.body.data.salesman_id:null,
                                                    discount_percent: req.body.data.discount_percent ? req.body.data.discount_percent:null,
                                                    discount_amount: req.body.data.discount_amount ? req.body.data.discount_amount:null,
                                                    base_grand_total: req.body.data.base_grand_total ? req.body.data.base_grand_total:null,
                                                    shipping_amount: req.body.data.shipping_amount ? req.body.data.shipping_amount:null,
                                                    total_purchase: req.body.data.total_purchase ? req.body.data.total_purchase:null,
                                                    cgst: req.body.data.cgst ? req.body.data.cgst:null,
                                                    sgst:req.body.data.sgst ? req.body.data.sgst:null,
                                                    igst: req.body.data.igst ? req.body.data.igst:null,
                                                    total_tax: req.body.data.total_tax ? req.body.data.total_tax:null,
                                                    grand_total: req.body.data.grand_total ? req.body.data.grand_total:null,
                                                    amount_paid: req.body.data.amount_paid ? req.body.data.amount_paid:null,
                                                    coupon_amount: req.body.data.coupon_amount ? req.body.data.coupon_amount:null,
                                                    wallet_amount: req.body.data.wallet_amount ? req.body.data.wallet_amount:null,
                                                    coupon_code: req.body.data.coupon_code ? req.body.data.coupon_code:null,
                                                    customer_name:  customer && customer.first_name && customer.last_name ? customer.first_name+' '+customer.last_name : '',
                                                    customer_email	: customer.email,
                                                    customer_mobile: customer.phone,
                                                    street1: req.body.data.street1 ? req.body.data.street1:null,
                                                    street2: req.body.data.street2 ? req.body.data.street2:null,
                                                    address: customer_address.address ? customer_address.address:null,
                                                    city: customer_address.city ? customer_address.city:null,
                                                    state: req.body.data.state ? req.body.data.state:null,
                                                    pin: customer_address.pin ? customer_address.pin:null,
                                                    country: customer_address.country ? customer_address.country:null,
                                                    gift_message: req.body.data.gift_message ? req.body.data.gift_message:null,
                                                    delivery_time_slot: req.body.data.delivery_time_slot ? req.body.data.delivery_time_slot:null,
                                                    shipping_address: shipping_address ? shipping_address:null,
                                                    //cgst: req.body.data.cgst ? req.body.data.cgst:null,
                                                }).then(function(order) { 
                                                    var today =  dd_mm_yyyy();
                                                    var newOrder_id = 'OTG'+ today + order.id;
                                                    models.order.update({
                                                        order_id:newOrder_id ? newOrder_id : null,
                                                    },{where:{id:order.id}}).then(async function(order_update){

                                                        var contact = await helpers.getEmailConfig();
                                                        var email =contact[1];
                                                        var phoneno =contact[0];

                                                        if(req.body.data.wallet_amount && req.body.data.wallet_amount!='0'){
                                                            models.wallet.create({ 
                                                                cid	: customerId? customerId : '',
                                                                amount	: req.body.data.wallet_amount ? -Math.abs(req.body.data.wallet_amount) :'',
                                                                amount_type: 'debit',
                                                                description	: '',
                                                                status: 'active'                                                       
                                                            })                                                        
                                                        }
                                                        var productItem= req.body.data.products ? req.body.data.products : [];
                                                        if(productItem){
                                                            productItem.forEach(function(element) {
                            
                                                                if(element.special_price == 0.00){
                                                                    original_price = element.price ? element.price:null;
                                                                }else {
                                                                    original_price = element.special_price ? element.special_price:null;
                                                                }
                            
                                                                models.order_item.create({ 
                                                                    order_id: order.id,
                                                                    store_id: req.body.data.store_id ? req.body.data.store_id:null,
                                                                    product_id: element.product_id ? element.product_id:null,
                                                                    name: element.title ? element.title:null,
                                                                    description: element.description ? element.description:null,
                                                                    discount_percent: element.discount_percent ? element.discount_percent:null,
                                                                    discount_amount: element.discount_amount ? element.discount_amount:null,
                                                                    qty: element.item_quantity ? element.item_quantity:null,
                                                                    price: element.price ? element.price:null,
                                                                    original_price: original_price,
                                                                });
                                                            }, this);
                                                        }
                            
                            
                                                        if(customerId != ''){
                                                            models.cart.destroy({ 
                                                                where:{customer_id:customerId}
                                                            }).then(function(cartdestroy) {
                                                                var smsContent = 'Your order with the reference no. '+newOrder_id+' is successful. It will be delivered between '+ replaceDaysName(order.delivery_time_slot)+'.'
                                                                sms_controller.sendsms(customer.phone,smsContent);
                                                                return new Promise((resolve, reject) => {
                                                                    var data = {
                                                                        from: 'Grocery User <me@samples.mailgun.org>',
                                                                        // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                                                        to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                                                        subject: 'Order',
                                                                        html: '<!DOCTYPE html>'+
                                                                        '<html>'+    
                                                                        '<head>'+
                                                                        '</head>'+    
                                                                        '<body>'+
                                                                            '<div>'+
                                                                                '<p>Hello Sir/Madam,</p>'+
                                                                                '<p>Thank you for your order with the reference no: '+newOrder_id+' . The total amount for your order is Rs. '+order.grand_total+' </p>'+  
                                                                                '<p>For any kind of query reach us at: <br />'+
                                                                                'Mail: " '+email+' " <br />'+
                                                                                'Phone: " '+phoneno+' " <br />'+
                                                                                'Have a nice shopping.</p>'+
                                                                                '<p>Thanks.</p>'+
                                                                                '<p>Regards,<br />'+
                                                                                'One Tap Grocery<br />'+
                                                                                'Midnapore</p>'+								
                                                                            '</div>'+       
                                                                        '</body>'+    
                                                                        '</html>' 
                                                                    };
                                                                    mailgun.messages().send(data, function (error, body) {
                                                                        if (error) {
                                                                            return reject(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                        }
                                                                        return resolve(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                    });
                                                                });
                                                                // res.status(200).send({ status:200, success: true, message: "success", });
                                                            }).catch(function(error) {
                                                                res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
                                                            }); 
                                                        }else{
                                                            return new Promise((resolve, reject) => {
                                                                var data = {
                                                                    from: 'Grocery User <me@samples.mailgun.org>',
                                                                    // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                                                    to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                                                    subject: 'Order',
                                                                    html: '<!DOCTYPE html>'+
                                                                    '<html>'+    
                                                                    '<head>'+
                                                                    '</head>'+    
                                                                    '<body>'+
                                                                        '<div>'+
                                                                            '<p>Hello Sir/Madam,</p>'+
                                                                            '<p>Thank you for your order with the reference no: '+newOrder_id+' . The total amount for your order is Rs. '+order.grand_total+' </p>'+  
                                                                            '<p>For any kind of query reach us at: <br />'+
                                                                            'Mail: " '+email+' " <br />'+
                                                                            'Phone: " '+phoneno+' " <br />'+
                                                                            'Have a nice shopping.</p>'+
                                                                            '<p>Thanks.</p>'+
                                                                            '<p>Regards,<br />'+
                                                                            'One Tap Grocery<br />'+
                                                                            'Midnapore</p>'+								
                                                                        '</div>'+       
                                                                    '</body>'+    
                                                                    '</html>' 
                                                                };
                                                                mailgun.messages().send(data, function (error, body) {
                                                                    if (error) {
                                                                        return reject(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                    }
                                                                    return resolve(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                                });
                                                            });
                                                            // res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id});
                                                        }
                                                    })
                                                    .catch(function(error) {
                                                        res.status(200).send({ status:200, success: false,  message: "Sorry order failed" });
                                                    }); 
                                                
                                                })
                                                .catch(function(error) {
                                                    res.status(200).send({ status:200, success: false,  message: "Sorry order failed" });
                                                });
                                            })    
                            
                                        });
                                    });
                                } 
                            })
                            .catch(function(error) {
                                res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
                            });
                        }
    
                    }).catch(function(error) {
                        res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
                    });
                }
                
            }else{
                res.status(200).send({ status:200, success: false, message: "Coupon not found used"});
            }
        }).catch(function(error) {
            res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
        });
    }else{
        if(store_id){
            existingItem = models.customer.findOne({ where: {id:customerId} });
            existingItem.then(function (customer) {
                existingItem = models.customer_address.findOne({ where: {customer_id:customerId,is_prime_address:1} });
                existingItem.then(function (customer_address) {
                    existingItem = models.customer_address.findOne({ where: {id:customerAddressId} });
                    existingItem.then(function (customer_shipping_address) { 
                        var shipping_address = '';
                        if(customer_shipping_address && customer_shipping_address!==''){
                            shipping_address = customer_shipping_address.first_name+' '+customer_shipping_address.last_name+'; '+customer_shipping_address.address+'; '+customer_shipping_address.city+' '+customer_shipping_address.pin+'; '+customer_shipping_address.country+'; '+customer_shipping_address.mobile;   
                        }                       
                        models.order.create({                     
                            store_id: req.body.data.store_id ? req.body.data.store_id:null,
                            order_status: req.body.data.order_status ? req.body.data.order_status:'Processing',
                            shipping_method: req.body.data.shiping_method ? req.body.data.shiping_method:null,
                            shipping_description: req.body.data.shipping_description ? req.body.data.shipping_description:null,
                            payment_method: req.body.data.payment_method ? req.body.data.payment_method:null, 
                            customer_id: req.body.data.customerId ? req.body.data.customerId:null,
                            salesman_id: req.body.data.salesman_id ? req.body.data.salesman_id:null,
                            discount_percent: req.body.data.discount_percent ? req.body.data.discount_percent:null,
                            discount_amount: req.body.data.discount_amount ? req.body.data.discount_amount:null,
                            base_grand_total: req.body.data.base_grand_total ? req.body.data.base_grand_total:null,
                            shipping_amount: req.body.data.shipping_amount ? req.body.data.shipping_amount:null,
                            total_purchase: req.body.data.total_purchase ? req.body.data.total_purchase:null,
                            cgst: req.body.data.cgst ? req.body.data.cgst:null,
                            sgst:req.body.data.sgst ? req.body.data.sgst:null,
                            igst: req.body.data.igst ? req.body.data.igst:null,
                            total_tax: req.body.data.total_tax ? req.body.data.total_tax:null,
                            grand_total: req.body.data.grand_total ? req.body.data.grand_total:null,
                            amount_paid: req.body.data.amount_paid ? req.body.data.amount_paid:null,
                            coupon_amount: req.body.data.coupon_amount ? req.body.data.coupon_amount:null,
                            wallet_amount: req.body.data.wallet_amount ? req.body.data.wallet_amount:null,
                            coupon_code: req.body.data.coupon_code ? req.body.data.coupon_code:null,
                            customer_name:  customer && customer.first_name && customer.last_name ? customer.first_name+' '+customer.last_name : '',
                            customer_email	: customer.email,
                            customer_mobile: customer.phone,
                            street1: req.body.data.street1 ? req.body.data.street1:null,
                            street2: req.body.data.street2 ? req.body.data.street2:null,
                            address: customer_address.address ? customer_address.address:null,
                            city: customer_address.city ? customer_address.city:null,
                            state: req.body.data.state ? req.body.data.state:null,
                            pin: customer_address.pin ? customer_address.pin:null,
                            country: customer_address.country ? customer_address.country:null,
                            gift_message: req.body.data.gift_message ? req.body.data.gift_message:null,
                            delivery_time_slot: req.body.data.delivery_time_slot ? req.body.data.delivery_time_slot:null,
                            shipping_address: shipping_address ? shipping_address:null,
                            //cgst: req.body.data.cgst ? req.body.data.cgst:null,
                        }).then(function(order) { 
                            var today =  dd_mm_yyyy();
                            var newOrder_id = 'OTG'+ today + order.id;
                            models.order.update({
                                order_id:newOrder_id ? newOrder_id : null,
                            },{where:{id:order.id}}).then(async function(order_update){

                                var contact = await helpers.getEmailConfig();
                                var email =contact[1];
                                var phoneno =contact[0];

                                if(req.body.data.wallet_amount && req.body.data.wallet_amount!='0'){
                                    models.wallet.create({ 
                                        cid	: customerId? customerId : '',
                                        amount	: req.body.data.wallet_amount ? -Math.abs(req.body.data.wallet_amount) :'',
                                        amount_type: 'debit',
                                        description	: '',
                                        status: 'active'                                                       
                                    })                                                        
                                }
    
                                var productItem= req.body.data.products ? req.body.data.products : [];
                                if(productItem){
                                    productItem.forEach(function(element) {
    
                                        if(element.special_price == 0.00){
                                            original_price = element.price ? element.price:null;
                                        }else {
                                            original_price = element.special_price ? element.special_price:null;
                                        }
    
                                        models.order_item.create({ 
                                            order_id: order.id,
                                            store_id: req.body.data.store_id ? req.body.data.store_id:null,
                                            product_id: element.product_id ? element.product_id:null,
                                            name: element.title ? element.title:null,
                                            description: element.description ? element.description:null,
                                            discount_percent: element.discount_percent ? element.discount_percent:null,
                                            discount_amount: element.discount_amount ? element.discount_amount:null,
                                            qty: element.item_quantity ? element.item_quantity:null,
                                            price: element.price ? element.price:null,
                                            original_price: original_price,
                                        });
                                    }, this);
                                }    
    
                                if(customerId != ''){
                                    models.cart.destroy({ 
                                        where:{customer_id:customerId}
                                    }).then(function(cartdestroy) {
                                        if(newOrder_id){
                                            var smsContent = 'Your order with the reference no. '+newOrder_id+' is successful. It will be delivered between '+ replaceDaysName(order.delivery_time_slot)+'.'
                                            sms_controller.sendsms(customer.phone,smsContent);
                                        }
                                        return new Promise((resolve, reject) => {
                                            var data = {
                                                from: 'Grocery User <me@samples.mailgun.org>',
                                                // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                                to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                                subject: 'Order',
                                                html: '<!DOCTYPE html>'+
                                                '<html>'+    
                                                '<head>'+
                                                '</head>'+    
                                                '<body>'+
                                                    '<div>'+
                                                        '<p>Hello Sir/Madam,</p>'+
                                                        '<p>Thank you for your order with the reference no: '+newOrder_id+' . The total amount for your order is Rs. '+order.grand_total+' </p>'+  
                                                        '<p>For any kind of query reach us at: <br />'+
                                                        'Mail: " '+email+' " <br />'+
                                                        'Phone: " '+phoneno+' " <br />'+
                                                        'Have a nice shopping.</p>'+
                                                        '<p>Thanks.</p>'+
                                                        '<p>Regards,<br />'+
                                                        'One Tap Grocery<br />'+
                                                        'Midnapore</p>'+								
                                                    '</div>'+       
                                                '</body>'+    
                                                '</html>' 
                                            };
                                            mailgun.messages().send(data, function (error, body) {
                                                if (error) {
                                                    return reject(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                                }
                                                return resolve(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                            });
                                        });
                                        // res.status(200).send({ status:200, success: true, message: "success", });
                                    }).catch(function(error) {
                                        res.status(200).send({ status:200, success: false, message: "Sorry order failed" });
                                    }); 
                                }else{
                                    return new Promise((resolve, reject) => {
                                        if(newOrder_id){
                                            var smsContent = 'Your order with the reference no. '+newOrder_id+' is successful. It will be delivered between '+ replaceDaysName(order.delivery_time_slot)+'.'
                                            sms_controller.sendsms(customer.phone,smsContent);
                                        }
                                        var data = {
                                            from: 'Grocery User <me@samples.mailgun.org>',
                                            // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                            to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                            subject: 'Order',
                                            html: '<!DOCTYPE html>'+
                                            '<html>'+    
                                            '<head>'+
                                            '</head>'+    
                                            '<body>'+
                                                '<div>'+
                                                    '<p>Hello Sir/Madam,</p>'+
                                                    '<p>Thank you for your order with the reference no: '+newOrder_id+' . The total amount for your order is Rs. '+order.grand_total+' </p>'+  
                                                    '<p>For any kind of query reach us at: <br />'+
                                                    'Mail: " '+email+' " <br />'+
                                                    'Phone: " '+phoneno+' " <br />'+
                                                    'Have a nice shopping.</p>'+
                                                    '<p>Thanks.</p>'+
                                                    '<p>Regards,<br />'+
                                                    'One Tap Grocery<br />'+
                                                    'Midnapore</p>'+								
                                                '</div>'+       
                                            '</body>'+    
                                            '</html>' 
                                        };
                                        mailgun.messages().send(data, function (error, body) {
                                            if (error) {
                                                return reject(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                            }
                                            return resolve(res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id}));
                                        });
                                    });
                                    // res.status(200).send({ status:200, success: true, message: "success", order_id : newOrder_id});
                                }
                            })
                            .catch(function(error) {
                                res.status(200).send({ status:200, success: false,  message: "Sorry order failed" });
                            }); 
                        
                        })
                        .catch(function(error) {
                            res.status(200).send({ status:200, success: false,  message: "Sorry order failed" });
                        });
                    })    
    
                });
            });
        } 
    }
       
}

function dd_mm_yyyy () {  
    now = new Date(); 
    year = "" + now.getFullYear().toString().substr(-2);   
    month = "" + (now.getMonth() + 1); 
    day = "" + now.getDate();   
    return day + month + year;
}

exports.appCustomerOrder = function(req, res, next){
    console.log(req.body.data);
    if(req.body.data.customerId && req.body.data.customerId!=''){
        var customerId= req.body.data.customerId ? req.body.data.customerId : '';
        existingItem = models.order.findAll({ where: {customer_id : customerId },order: [['id', 'DESC']] });
        existingItem.then(function (value) {
            //return res.send(value);
            if(value && value !=''){
                console.log(value.length);
                var resultArray = [];
                if(value.length > 0){                    
                    var i = 0;
                    value.forEach(function(element,index){ 
                        console.log(element)
                        //models.order_item.findAll({ where: {order_id:element.id} })
                        //.then(function(order_unit) {
                        sequelize.query("select order_item.*, product.weight as productWeight FROM `order_item` LEFT JOIN `product` ON product.id = order_item.product_id where order_item.order_id ="+element.id,{ type: Sequelize.QueryTypes.SELECT })
                        .then(function(order_unit) {    
                            console.log(order_unit)
                            resultArray.push({
                                "id": element.id ? element.id : '',
                                "order_id": element.order_id ? element.order_id : '',
                                "store_id": element.store_id ? element.store_id : '',
                                "order_status": element.order_status ? element.order_status : '',
                                "shipping_method": element.shipping_method ? element.shipping_method : '',
                                "shipping_description": element.shipping_description ? element.shipping_description : '',
                                "payment_method": element.payment_method ? element.payment_method : '',
                                "coupon_code": element.coupon_code ? element.coupon_code : '',
                                "customer_id": element.customer_id ? element.customer_id : '',
                                "salesman_id": element.salesman_id ? element.salesman_id : '',
                                "discount_percent": element.discount_percent ? element.discount_percent : '',
                                "discount_amount": element.discount_amount ? element.discount_amount : '',
                                "base_grand_total": element.base_grand_total ? element.base_grand_total : '',
                                "shipping_amount": element.shipping_amount	 ? element.shipping_amount	 : '',
                                "cgst": element.cgst ? element.cgst : '',
                                "sgst": element.sgst ? element.sgst : '',
                                "igst": element.igst ? element.igst : '',
                                "total_tax": element.total_tax ? element.total_tax : '',
                                "grand_total": element.grand_total	 ? element.grand_total	 : '',
                                "amount_paid": element.amount_paid ? element.amount_paid : '',
                                "coupon_amount": element.coupon_amount ? element.coupon_amount : '',
                                "wallet_amount": element.wallet_amount ? element.wallet_amount : '',
                                "promotion": element.promotion ? element.promotion : '',
                                "customer_name": element.customer_name ? element.customer_name : '',
                                "customer_email": element.customer_email ? element.customer_email : '',
                                "customer_mobile": element.customer_mobile	 ? element.customer_mobile	 : '',
                                "address": element.address ? element.address : '',
                                "street1": element.street1 ? element.street1 : '',
                                "street2": element.street2	 ? element.street2	 : '',
                                "city": element.city	 ? element.city	 : '',
                                "state": element.state ? element.state : '',
                                "pin": element.pin	 ? element.pin	 : '',
                                "country": element.country ? element.country : '',
                                "lat": element.lat ? element.lat : '',
                                "long": element.long ? element.long : '',
                                "shipping_address": element.shipping_address ? element.shipping_address : '',
                                "gift_message": element.gift_message ? element.gift_message : '',
                                "remote_ip	": element.remote_ip	 ? element.remote_ip	 : '',
                                "medium": element.medium	 ? element.medium	 : '',
                                "delivery_time_slot": element.delivery_time_slot ? element.delivery_time_slot : '',
                                "createdAt": element.createdAt ? element.createdAt : '',
                                "createdBy	": element.createdBy	 ? element.createdBy	 : '',
                                "updatedAt": element.updatedAt ? element.updatedAt : '',
                                "updatedBy": element.updatedBy ? element.updatedBy : '',
                                "unit":order_unit ? order_unit : []
                            });    
                            i++;
                            if(i== value.length){
                                resultArray.sort(function(a,b){return b.id - a.id});
                                res.status(200).send({ status:200,success: true,value:resultArray });
                            }            
                        });                
                    });           
                }else{
                    res.status(200).send({ status:200,success: true,value:resultArray });
                } 
               
            }else{
                res.status(200).send({ status:205,success: false,message: "Order not found" });
            }
            
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        }); 
    }else{
        res.status(200).send({ status:200, success: false, message: "Customer Id not found", });  
    }  
          
}

//////////////////////////// app order end ///////////////////

//////////////////////////////// feedback api start/////////////////////
exports.appFeedback = function(req, res, next) {
    //var id = req.body.data.update_id;
    if(req.body.data.customerId && req.body.data.customerId!=''){
        var customerId= req.body.data.customerId ? req.body.data.customerId : '';
        
        models.feedback.create({ 
            customer_id: customerId,
            message: req.body.data.message ? req.body.data.message:null,
        }).then(function(feedback) {

        res.status(200).send({ status:200, success: true, message: "success", customerfeedback:feedback });
        })
        .catch(function(error) {
            return res.send( {success: false, error});
        });
    }
};

/////////////////////////////app feedback api end///////////////////////

/////////////////////////////// app global search start ///////////////////

// exports.appGlobalSearch = function(req, res, next){
//     //var price= req.body.data.price ? req.body.data.price : '';
//     var q= req.body.data.q ? req.body.data.q : '';
//     var existingItem = null;

//     if(!q){
//         res.status(200).send({ success: false, message: "No serch query recieved" });
//     }else{
//         var querySql="select * FROM `product` where title LIKE '%"+q+"%'";

//         sequelize.query(querySql,
//         { type: Sequelize.QueryTypes.SELECT })
//         .then(function (product) {
//             if(product!=null && product!=''){
//                 res.status(200).send({ status:200,success: true,product: product });
//             }else{				
//                 res.status(200).send({ status:200,success: false, message: "product not found" });
//             }
//         });
//     }
// };

exports.appGlobalSearch = function(req, res, next){


    //var price= req.body.data.price ? req.body.data.price : '';
    var q= req.body.data.q ? req.body.data.q : '';
    var existingItem = null;

    if(!q){
        res.status(200).send({ success: false, message: "No serch query recieved" });
    }else{
        // var querySql="select * FROM `product` where is_configurable=0 and title LIKE '%"+q+"%'";
        var querySql="select product.*, site_settings.max_pro_availability FROM `product` LEFT JOIN `site_settings` ON site_settings.id = 1 where product.is_configurable=0 and product.title LIKE '%"+q+"%' or product.keyword LIKE '%"+q+"%'";


        sequelize.query(querySql,
            { type: Sequelize.QueryTypes.SELECT }).then(function (value) {
            
                var resultArray = [];
            if(value.length > 0){
                var i = 0;
                value.forEach(function(element,index){ 
                    models.product.findAll({ where: {status:'active',is_configurable:element.id} })
                    .then(function(product_unit) {
                        console.log(product_unit)
                        resultArray.push({
                            "id":element.id,
                            "mainId":product_unit ? product_unit.id :'',
                            "title":element.title,
                            "short_description":element.short_description,
                            "description":element.description,
                            "type":element.type,
                            "size":element.weight,
                            "special_price":element.special_price,
                            "price":element.price,
                            "quantity":element.inventory,
                            "image":element.image,
                            // "max_pro_availability":"5",
                            "max_pro_availability":element.max_pro_availability,
                            "spice_level":element.spice_level,
                            "unit":product_unit ? product_unit.concat(value[index]) : value[index]
                        });    
                        i++;
                        if(i== value.length){
                            res.status(200).send({ status:200,product:resultArray });
                        }            
                    });                
                });           
            }else{
                res.status(200).send({ status:200,product:resultArray });
            }        
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        }); 

      
    }
};

exports.appGlobalSearchSuggestion = function(req, res, next){
    //var price= req.body.data.price ? req.body.data.price : '';
    //var q= req.body.data.q ? req.body.data.q : '';
    var existingItem = null;

   // if(!q){
        //res.status(200).send({ success: false, message: "No serch query recieved" });
    //}else{
        var querySql="select product.title FROM `product`";

        sequelize.query(querySql,
        { type: Sequelize.QueryTypes.SELECT })
        .then(function (product) {
            //if(product!=null && product!=''){
                res.status(200).send({ status:200,success: true,product: product });
            //}else{				
                //res.status(200).send({ status:200,success: false, message: "product not found" });
            //}
        });
    //}
};

//////////////////////// app global search end //////////////////////////

/////////////////////////////// app order cancel start ///////////////////

exports.appOrderCancel = function(req, res, next){   
    var orderId = req.params.orderId ? req.params.orderId : '';
    console.log(req.body.data);
    if(orderId !=''){
        models.order.findById(orderId).then(function(order_details) {
			models.customer.findById(order_details.customer_id).then(function(customer_details) {
                var smsContent = 'Your order with the reference no. '+order_details.order_id+' has been canceled. Thank you.'
                sms_controller.sendsms(customer_details.phone,smsContent);
				models.order.update({
					order_status:'Canceled',
				},{where:{id:orderId}}).then(async function(order_update){

                    var contact = await helpers.getEmailConfig();
                    var email =contact[1];
                    var phoneno =contact[0];

					return new Promise((resolve, reject) => {
						var data = {
							from: 'Grocery User <me@samples.mailgun.org>',
                            // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                            to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
							subject: 'Cancel Order',
							html: '<!DOCTYPE html>'+
							'<html>'+    
							'<head>'+
							'</head>'+    
							'<body>'+
								'<div>'+
									'<p>Hello Sir/Madam,</p>'+
									'<p>Your order with the reference no. '+order_details.order_id+' has been canceled.</p>'+  
									'<p>For any kind of query reach us at: <br />'+
									'Mail: " '+email+' " <br />'+
									'Phone: " '+phoneno+' " <br />'+
									'Have a nice shopping.</p>'+
									'<p>Thanks.</p>'+
									'<p>Regards,<br />'+
									'One Tap Grocery<br />'+
									'Midnapore</p>'+								
								'</div>'+       
							'</body>'+    
							'</html>' 
						};
						mailgun.messages().send(data, function (error, body) {
							if (error) {
								return reject(res.status(200).send({ status:200, success: true,  message: "Order canceled successfully" }));
							}
							return resolve(res.status(200).send({ status:200, success: true,  message: "Order canceled successfully" }));
						});
					});
					// res.status(200).send({ status:200, success: true,  message: "Order cancelled successfully" });
				})
				.catch(function(error) {
					res.status(200).send({ status:200, success: false,  message: "Order cancellation failed" });
				});
			})
            .catch(function(error) {
                res.status(200).send({ status:200, success: false,  message: "Customer Not Found" });
            });
        })
        .catch(function(error) {
            res.status(200).send({ status:200, success: false,  message: "Order cancellation failed" });
        });
           
    } else{
        res.status(200).send({ status:200, success: false,  message: "Order cancelation failed" });
    }           
}
/////////////////////////////// app order cancel end ///////////////////

///////////////////////////// app express shipping method api start//////////////////////

exports.appExpressShipping = function(req, res, next){
    //console.log(req.body.data);
    //var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    existingItem = models.shipping_method.findOne();
    existingItem.then(function (value) {
        //return res.send(value);
        if(value && value !=''){
            res.status(200).send({ status:200, success: true, value:value });
        }else{
            res.status(200).send({ status:205, success: false, message: "Shipping Express not found" });
        }
        
    }).catch(function(error) {
        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
    });       
}

//////////////////////// app express shipping methopd apiu end ////////////////////////////

///////////////////////////// app Site Settings api start//////////////////////

exports.appSiteSettings = function(req, res, next){
    //console.log(req.body.data);
    //var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    existingItem = models.site_settings.findOne();
    existingItem.then(function (value) {
        //return res.send(value);
        if(value && value !=''){
            res.status(200).send({ status:200, success: true, value:value });
        }else{
            res.status(200).send({ status:205, success: false, value:'', message: "Site Settings not found" });
        }
        
    }).catch(function(error) {
        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
    });       
}

//////////////////////// app Site Settings apiu end ////////////////////////////

///////////////////////// app otp start ///////////////////////////////

exports.appOtp = function(req, res, next) {

    //return res.send(val);
    var otp_date_time =  proper_Date_Time();
    console.log(otp_date_time);
    
    if(req.body.data.customerId && req.body.data.customerId!=''){
        var customerId= req.body.data.customerId ? req.body.data.customerId : '';

        existingItem = models.customer.findOne({ where: {id:customerId} });
        existingItem.then(function (customer) {
            if(customer!=null){
                var random_otp = Math.floor(1000 + Math.random() * 9000);
                //console.log(random_otp);
                models.customer.update({ 
                otp: random_otp,
                otp_created_at: otp_date_time,
                },{where:{id:customerId}}).then(function(customer_otp) {
					var smsContent = random_otp+" is your verification code. Code valid for 10 minutes only, one time use. Please DO NOT share this OTP with anyone to ensure account's security."
                    sms_controller.sendsms(customer.phone,smsContent);        
                    // res.status(200).send({ status:200, success: true, message: "success", otp:random_otp, otp_created_at:otp_date_time, userDetails: customer });
                    res.status(200).send({ status:200, success: true, message: "success" });
                })
                .catch(function(error) {
                    return res.send( {status:200, success: false, message: "error happened"});
                });

            }else{				
                res.status(200).send({ status:204, success: false, message: "No customer found" });
            }
        });    
    }else{
        res.status(200).send({ status:205, success: false, message: "Customer Id not found" });
    }
};

exports.appCheckOtp = function(req, res, next) {

    var PhoneNo = req.body.data.phone ? req.body.data.phone : '';
    var customerOtp = req.body.data.otp ? req.body.data.otp : '';
    //return res.send(val);

    if(req.body.data.phone && req.body.data.phone!=''){
        // var customerId= req.body.data.customerId ? req.body.data.customerId : '';

        existingItem = models.customer.findOne({ where: {phone:PhoneNo,status: 'active'} });
        existingItem.then(function (customer) {
            if(customer!=null){
                if(customerOtp!='' && customer.otp == customerOtp){
                    sequelize.query("SELECT id FROM `customer` WHERE `phone` = '"+PhoneNo+"' AND `otp` = "+customerOtp+" AND `otp_created_at` BETWEEN DATE_SUB(NOW(), INTERVAL 10 MINUTE) AND NOW()",{ type: Sequelize.QueryTypes.SELECT }) 
                    .then(function (value) {
                        //console.log("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL")
                        //console.log(value[0])
                        //console.log("pppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp")
                        // console.log(value[0].id)
                        if(value.length > 0 && value[0].id){
                            console.log(value[0])
                            if(customer.is_approved == 1){
                                models.customer.update({ 
                                    is_approved: null,
                                },{where:{id:customer.id}}).then(async function(customer_is_approved) {

                                    var contact = await helpers.getEmailConfig();
                                    var email =contact[1];
                                    var phoneno =contact[0];

                                    var smsContent = "Thank you for your registration with OTG. Please mail us at contactus@ssquaredigital.com for any concern."
                                    sms_controller.sendsms(customer.phone,smsContent);   
                                    return new Promise((resolve, reject) => {
                                        var data = {
                                            from: 'Grocery User <me@samples.mailgun.org>',
                                            // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                            to: ['tbiswa1993@gmail.com','subrata.adhikary110319@gmail.com'],
                                            subject: 'Registration',
                                            html: '<!DOCTYPE html>'+
                                            '<html>'+    
                                            '<head>'+
                                            '</head>'+    
                                            '<body>'+
                                                '<div>'+
                                                    '<p>Hello Sir/Madam,</p>'+
                                                    '<p>Welcome to One Tap Grocery.</p>'+
                                                    '<p>Thank you for registering with us, here you will experience a hustle free service and quality product in reasonable price and many more offers. We are committed to serve you best quality and best price, from the comfort of your home.</p>'+                    
                                                    '<p>Please feel free to reach us anytime.</p>'+
                                                    '<p>We are available at: <br />'+
                                                    'Mail: " '+email+' " <br />'+
                                                    'Phone:  " '+phoneno+' " <br />'+
                                                    '<p>We expect a very happy and long relationship with you.</p>'+
                                                    '<p>Have a nice shopping.</p>'+
                                                    '<p>Thanks.</p>'+
                                                    '<p>Regards,<br />'+
                                                    'One Tap Grocery<br />'+
                                                    'Midnapore</p>'+								
                                                '</div>'+       
                                            '</body>'+    
                                            '</html>' 
                                        };
                                        mailgun.messages().send(data, function (error, body) {                                         
                                            
                                            if (error) {
                                                return reject(res.status(200).send({ status:204, success: true, message: "Login Successful" }));
                                            }
                                            return resolve(res.status(200).send({ status:204, success: true, message: "Login Successful" }));
                                
                                        });
                                    }); 
                                    
                                }).catch(function(error) {
                                    return res.send( {status:200, success: false, message: "error happened"});
                                }); 

                            }else{
                                res.status(200).send({ status:204, success: true, message: "Login Successful" });
                            }                            

                        }else{
                            res.status(200).send({ status:204, success: false, message: "OTP expired" });
                        }
                        
                    }).catch(function(error) {
                        return res.send( {status:200, success: false, message: "error happened"});
                    }); 
                }else{
                    res.status(200).send({ status:204, success: false, message: "Invalid OTP" });
                }
            }else{				
                res.status(200).send({ status:204, success: false, message: "No customer found" });
            }

        });    
    }else{
        res.status(200).send({ status:205, success: false, message: "Phone no not found" });
    }
    
};

function proper_Date_Time () {  
    now = new Date(); 
    year = "" + now.getFullYear();   
    month = "" + (now.getMonth() + 1); 
    if (month.length == 1) { month = "0" + month; }   day = "" + now.getDate(); 
    if (day.length == 1) { day = "0" + day; }   hour = "" + now.getHours(); 
    if (hour.length == 1) { hour = "0" + hour; }   minute = "" + now.getMinutes(); 
    if (minute.length == 1) { minute = "0" + minute; }   second = "" + now.getSeconds(); 
    if (second.length == 1) { second = "0" + second; }   
    return year + "-" + month + "-" + day + "  "+ hour +":"+ minute +":"+ second ;

}

//////////////////////// app otp end /////////////////////////////

//////////////////////// app wallet api start /////////////////////

exports.appWalletDashboard = function(req, res, next){
    //console.log(req.body.data);
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    if(!customerId){	
        res.status(200).send({ status:200,success: false, message: "customerId not found", });
    }else{  
        //existingItem = models.customer_address.findAll({ where: {customer_id : customerId } });
        //existingItem.then(function (value) {
        sequelize.query("SELECT ABS(`wallet`.`amount`) as amount, `wallet`.`amount_type` as amount_type, `wallet`.`createdAt` as createdAt   FROM `wallet` WHERE wallet.cid = "+customerId+" ORDER BY `wallet`.`id` DESC LIMIT 5",{ type: Sequelize.QueryTypes.SELECT }) 
        .then(function (value) {
            if(value && value.length > 0){	
                sequelize.query("SELECT SUM(amount) as 'totalamount' FROM wallet WHERE cid = "+customerId,{ type: Sequelize.QueryTypes.SELECT }) 
                .then(function (total) { 
                    if(total && total.length > 0){           
                        //console.log(total[0].totalamount);
                        //console.log(total.length);
                        res.status(200).send({ status:200,success: true, value:value ,totalAmount: total[0].totalamount });
                    }else{
                        res.status(200).send({ status:200,success: true, value:value ,totalAmount:0.00 });
                    }
                    
                }).catch(function(error) {
                    res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                });    
            }else{				
                res.status(200).send({status:200, success: false, value:[], totalAmount:'', message: "No Wallet data found" });
            }
        }).catch(function(error) {
            res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        });       
        
    }       
}

exports.appWalletTransaction = function(req, res, next){
    console.log(req.body.data);
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    var fromDate= req.body.data.fromDate ? req.body.data.fromDate : '';
    var toDate= req.body.data.toDate ? req.body.data.toDate : '';

    if(!customerId){	
        res.status(200).send({ status:200,success: false, message: "customerId not found", });
    }else{  
        if(!fromDate || !toDate){	
            res.status(200).send({ status:200,success: false, message: "from date or to date not found", });
        }else{ 
            sequelize.query("SELECT ABS(`wallet`.`amount`) as amount, `wallet`.`amount_type` as amount_type, `wallet`.`createdAt` as createdAt FROM wallet where wallet.status='active' and wallet.cid ="+customerId+" and DATE(wallet.createdAt) BETWEEN '"+fromDate+"' AND '"+toDate+"' ORDER BY wallet.id DESC",{ type: Sequelize.QueryTypes.SELECT }) 
            //sequelize.query("select * FROM wallet where wallet.status='active' and wallet.cid ="+customerId+" and DATE(wallet.createdAt) BETWEEN '"+fromDate+"' AND '"+toDate+"'",{ type: Sequelize.QueryTypes.SELECT }) 
            .then(function (value) {

                if(value!=null && value!=''){	
                    res.status(200).send({ status:200,success: true, value:value });

                }else{				
                    res.status(200).send({status:200, success: false, value:[], message: "No Wallet data found" });
                }
            }).catch(function(error) {
                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
            });
        }
        
    }       
}


//////////////////////// app wallet api end /////////////////////

/////////////////////// app coupon api start ///////////////////

exports.appCouponCode = function(req, res, next){
    //console.log(req.body.data);
    var CouponCode= req.body.data.CouponCode ? req.body.data.CouponCode : '';
    var customerId= req.body.data.customerId ? req.body.data.customerId : '';
    var fromDate = null;
    var toDate = null;
    var fromTime = null;
    var toTime = null;
    var sqlQuery = null;
    var currentDate =  current_dd_mm_yyyy();
    var currentTime =  current_Time();
    if(!CouponCode){	
        res.status(200).send({ status:200,success: false, message: "Coupon Code not found", });
    }else{
        models.customer.findOne({where: {sponsor_code : CouponCode,id: customerId }})
        .then(function (customer) {
            if(customer!=null && customer!=''){
                res.status(200).send({status:200, success: false, message: "You can't used this Coupon" });
            }else{

                models.coupon.findOne({where: {coupon_code : CouponCode,status: 'active' }})
                .then(function (value) {

                    if(value!=null && value!=''){
                        //console.log("111111111111111111111111111111111111111111111111");
                        fromDate= value.date_from ? value.date_from:null;
                        toDate= value.date_to ? value.date_to:null;
                        fromTime= value.time_from ? value.time_from:null;
                        toTime= value.time_to ? value.time_to:null;	
                        //console.log(value.date_from);
                        console.log(typeof fromDate);
                        console.log(toDate);
                        //console.log(fromTime);
                        //console.log(toTime);
                        //console.log("2222222222222222222222222222222222222222222222222  0000-00-00");

                    
                        if(fromDate && fromDate !='0000-00-00' && toDate && toDate !='0000-00-00' && fromTime && toTime){
                            //console.log('44444444444444');
                        sqlQuery= "SELECT `coupon`.* FROM `coupon` where ('"+currentDate+"' BETWEEN '"+fromDate+"' and '"+toDate+"' ) and ('"+currentTime+"' BETWEEN '"+fromTime+"' and '"+toTime+"') and coupon_code = '"+CouponCode+"' and status='active'";
                        // }

                        }else if(fromDate && fromDate !='0000-00-00' && toDate && toDate !='0000-00-00' && (!fromTime || !toTime)){
                        sqlQuery= "SELECT `coupon`.* FROM `coupon` where ('"+currentDate+"' BETWEEN '"+fromDate+"' and '"+toDate+"' ) and coupon_code = '"+CouponCode+"' and status='active'";

                        }else if(((!fromDate || fromDate =='0000-00-00') || (!toDate || toDate =='0000-00-00')) && fromTime && toTime){
                        sqlQuery= "SELECT `coupon`.* FROM `coupon` where ('"+currentTime+"' BETWEEN '"+fromTime+"' and '"+toTime+"' ) and coupon_code = '"+CouponCode+"' and status='active'";
                        }else{
                            sqlQuery= null;
                        }
                        //console.log('44444444444444');
                        if(sqlQuery != null){
                            console.log(sqlQuery); 
                            sequelize.query(sqlQuery,{ type: Sequelize.QueryTypes.SELECT })
                            .then(function (valid_coupon_code) {
                                if(valid_coupon_code!=null && valid_coupon_code!=''){
                                    models.coupon_transaction.findOne({where: {customer_id : customerId, coupon_code: CouponCode, status:'active' }})
                                    .then(function (coupon_trans) {
                                        if(coupon_trans!=null && coupon_trans!=''){
                                            res.status(200).send({ status:200, success: false, message: "Coupon already used"});
                                        }else{
                                            res.status(200).send({ status:200,success: true, value:value });
                                        }
                                    }).catch(function(error) {
                                        //console.log(error); 
                                        res.status(200).send({status:200, success: false, message: "Error Happened1" });
                                    });

                                }else{
                                    res.status(200).send({status:200, success: false, message: "Invalid Coupon" });
                                }
                                //console.log(valid_coupon_code); 
                            }).catch(function(error) {
                                //console.log(error); 
                                res.status(200).send({status:200, success: false, message: "Error Happened2" });
                            });
                        }else{
                            models.coupon_transaction.findOne({where: {customer_id : customerId, coupon_code: CouponCode, status:'active' }})
                            .then(function (coupon_trans) {
                                if(coupon_trans!=null && coupon_trans!=''){
                                    res.status(200).send({ status:200, success: false, message: "Coupon already used"});
                                }else{
                                    res.status(200).send({ status:200,success: true, value:value });
                                }
                            }).catch(function(error) {
                                //console.log(error); 
                                res.status(200).send({status:200, success: false, message: "Error Happened3" });
                            });
                            // res.status(200).send({ status:201,success: true, value:value });

                        }
                        //res.status(200).send({ status:200,success: true, value:value });

                    }else{				
                        res.status(200).send({status:200, success: false, message: "Invalid Coupon" });
                    }
                }).catch(function(error) {
                    res.status(200).send({status:200, success: false, message: "Error Happened4" });
            
                }); 
            }    
                         
        }).catch(function(error) {
            res.status(200).send({status:200, success: false, message: "Error Happened5" });
        });        
    }       
}



exports.genarateChecksum = function(req, res, next){
    console.log(req.body.data);
    var merchantKey = req.body.data ? req.body.data.MERCHANT_KEY : '';
    var ver_param = {
        MID:  req.body.data.MID ? req.body.data.MID : 'nzOejH09061653206030',
        ORDER_ID: req.body.data.ORDER_ID ? req.body.data.ORDER_ID : 52,  //should be unique
        CUST_ID: req.body.data.CUST_ID ? req.body.data.CUST_ID : '298233',
        INDUSTRY_TYPE_ID: req.body.data.INDUSTRY_TYPE_ID ? req.body.data.INDUSTRY_TYPE_ID : 'Retail',         
        CHANNEL_ID: req.body.data.CHANNEL_ID ? req.body.data.CHANNEL_ID : 'WEB', 
        TXN_AMOUNT: req.body.data.MID ? req.body.data.MID : '1',        
        WEBSITE: req.body.data.WEBSITE ? req.body.data.WEBSITE : 'PaytmMktPlace',  
    };
    console.log(ver_param);
    genchecksum(ver_param,merchantKey, function(err, resData) {
        var data = {};
        if (!err) {            
            data.CHECKSUMHASH = resData.CHECKSUMHASH;
            data.ORDER_ID = resData.ORDER_ID;
            data.payt_STATUS = 1;
            console.log(data);
            res.status(200).send({status:200, success: true, data :data, message: "Success" });
            // res.status(200).send(         //Make sure your response be like this for mobile SDk.
            //     data
            // );
            // res.end();
        } else {
            console.log('data');
            res.status(200).send({status:200, success: false, data :data, message: "failed" });
            // res.status(200).send(         //Make sure your response be like this for mobile SDk.
            //     data
            // );
            // res.end();
            Logger.error("Error occured");            
        }
    });
};

function current_dd_mm_yyyy () { 
    now = new Date(); 
    year = "" + now.getFullYear();   
    month = "" + (now.getMonth() + 1); 
    if (month.length == 1) { month = "0" + month; }   day = "" + now.getDate(); 
    if (day.length == 1) { day = "0" + day; } 
    //return day + month + year;
    return year + "-" + month + "-" + day;
}

function current_Time () {  
    now = new Date(); 
    year = "" + now.getFullYear();   
    month = "" + (now.getMonth() + 1); 
    if (month.length == 1) { month = "0" + month; }   day = "" + now.getDate(); 
    if (day.length == 1) { day = "0" + day; }   hour = "" + now.getHours(); 
    if (hour.length == 1) { hour = "0" + hour; }   minute = "" + now.getMinutes(); 
    if (minute.length == 1) { minute = "0" + minute; }   second = "" + now.getSeconds(); 
    if (second.length == 1) { second = "0" + second; }   
    return hour +":"+ minute ;

}
//////////////////////// app coupon api end //////////////////////


function randomString() {
    var length = 4;
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

function  randomRecursiveString() {
    var length = 4;
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    console.log('result')
    console.log(result)
    models.customer.findAll({ attributes: ['sponsor_code'],where: { sponsor_code: {$ne: null,$ne: ''}}})
    .then(function (exists_coupon) {
        console.log('exists_coupon'); 
        console.log(exists_coupon); 
        return exists_coupon;

        // if(exists_coupon!=null && exists_coupon!=''){
        //     console.log('result1')
        //     return randomRecursiveString();
        // }else{
        //     console.log('result2')
        //     return result;
        // }
    }).catch(function(error) {
        console.log('result3')
        console.log(error); 
        return result;
    });    
};

var promise1 = new Promise(function(resolve, reject) {
    var length = 4;
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    models.customer.findAll({ attributes: ['sponsor_code'],where: { sponsor_code: {$ne: null,$ne: ''}}})
    .then(function (exists_coupon) {
        console.log('exists_coupon'); 
        console.log(exists_coupon); 
        // return exists_coupon;
        resolve(exists_coupon);
        // if(exists_coupon!=null && exists_coupon!=''){
        //     console.log('result1')
        //     return randomRecursiveString();
        // }else{
        //     console.log('result2')
        //     return result;
        // }
    }).catch(function(error) {
        console.log('result3')
        console.log(error); 
        resolve(result);
    }); 
    // }); 
    // resolve(result);
});


/////
function replaceDaysName(str) {
    var res = ''
    if(str.includes("Thatday")){
        res = str.replace(/Thatday/gi,"today")
        return res;
    }else if(str.includes("Nextday")){
        res = str.replace(/Nextday/gi,"tommorrow")
        return res;
    }else{
        return str;
    }    
}

exports.zipcodeList = function(req, res, next){

    existingItem = models.zipcode.findAll({ where: {status:  'active'} });  
    existingItem.then(function (value) {

        if(value && value !=''){
            res.status(200).send({ status:200,success: true, value:value });
        }else{
            res.status(200).send({ status:205,success: false, message: "Zip code not found" });
        }
        
    }).catch(function(error) {
        res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
    }); 
};