/*Project: superpos
 * 
 * 
 * 
 * */

var express = require('express');
var router = express.Router();
var authController = require('../controllers/auth/authController');
var usersController = require('../controllers/superpos/usersController');
var profileController = require('../controllers/superpos/profileController');
var account_settingController = require('../controllers/superpos/account_settingController');
var admin_userController = require('../controllers/superpos/admin_userController');
var sitesettingsController = require('../controllers/superpos/sitesettingsController');
var rolesController = require('../controllers/superpos/rolesController');
var modulesController = require('../controllers/superpos/modulesController');
var modulesroleController = require('../controllers/superpos/modulesroleController');
var countryController = require('../controllers/superpos/countryController');
var cityController = require('../controllers/superpos/cityController');
var call_to_actionController = require('../controllers/superpos/call_to_actionController');
var attributeController = require('../controllers/superpos/attributeController');
var citiController = require('../controllers/superpos/citiController');
var zipcodeController = require('../controllers/superpos/zipcodeController');
var storesController = require('../controllers/superpos/storesController');
var attribute_optionController = require('../controllers/superpos/attribute_optionController');
var categoryController = require('../controllers/superpos/categoryController');
var subcategoryController = require('../controllers/superpos/subcategoryController');
var productController = require('../controllers/superpos/productController');
var product_uploadController = require('../controllers/superpos/product_uploadController');
var category_productController = require('../controllers/superpos/category_productController');
var customerController = require('../controllers/superpos/customerController');
var customer_favController = require('../controllers/superpos/customer_favController');
var customer_groupController = require('../controllers/superpos/customer_groupController');
var call_to_actionController = require('../controllers/superpos/call_to_actionController');
//var customer_addressController = require('../controllers/superpos/customer_addressController');
var customer_addressController = require('../controllers/superpos/customer_addressController');
var orderController = require('../controllers/superpos/orderController');
var order_itemController = require('../controllers/superpos/order_itemController');
var order_status_historyController = require('../controllers/superpos/order_status_historyController');
var settings_profileController = require('../controllers/superpos/settings_profileController');
var testController = require('../controllers/superpos/testController');
var orderitemController = require('../controllers/superpos/orderitemController');
var cmsController = require('../controllers/superpos/cmsController');
var notificationController = require('../controllers/superpos/notificationController');
var notification_transController = require('../controllers/superpos/notification_transController');
// var delivary_time_slotController = require('../controllers/superpos/delivary_time_slotController');

var delivery_time_slotController = require('../controllers/superpos/delivery_time_slotController');
var walletController = require('../controllers/superpos/walletController');
var couponController = require('../controllers/superpos/couponController');
var coupon_transactionController = require('../controllers/superpos/coupon_transactionController');
var bannerController = require('../controllers/superpos/bannerController');
//var banner_siderController = require('../controllers/superpos/banner_siderController');
var banner_sectionController = require('../controllers/superpos/banner_sectionController');
var banner_subcategory= require('../controllers/superpos/banner_subcategoryController');
var banner_displayController = require('../controllers/superpos/banner_displayController');
var dropdown_settingsController = require('../controllers/superpos/dropdown_settingsController');
var salesmanController = require('../controllers/superpos/salesmanController');
var shipping_methodController = require('../controllers/superpos/shipping_methodController');
var smsController =require('../controllers/superpos/smsController');
var sale_reportsController = require('../controllers/superpos/sale_reportsController');
var customer_reportsController = require('../controllers/superpos/customer_reportsController');
var email_configcontroller = require('../controllers/superpos/email_configcontroller');
var models = require('../models');
var bcrypt = require('bcrypt-nodejs');
var csrf = require('csurf');
var csrfProtection = csrf({ cookie: true });




var app 				= express();
var url = require('url');

function checkAuthentication(req,res,next){
    if(req.session.token){
        //if user is looged in, req.isAuthenticated() will return true 
        next();
    } else{
        res.redirect("/superpos/admin");
    }
}


var expressValidator = require('express-validator');
router.use(expressValidator())


router.get('/',function(req, res) {
  //res.send(req.originalUrl);
  //console.log(req.originalUrl); // '/admin/new'
  ////console.log(req.baseUrl); // '/admin'
  //console.log(req.path); // '/new'
  //console.log(req.baseUrl, req.url);
      res.end();
});

function middleHandler(req,res,next){
    // models.admin_users.findOne({ where: {email: (req.session.user.email)} })
    models.users.findOne({ where: {email: (req.session.user.email)} })
    .then(function(user) {
        console.log( user);
        //res.cookie('firstname' , user.firstName);
        res.locals.username = user ? user.firstName+' '+user.lastName : '';
        res.locals.usercreatedBy =  user ? user.id : '';
        res.locals.userimage = user ? user.image: '';
        res.locals.userrole = user.role;
        // next();	  
    });

    models.site_settings.findAll().then(function(footer) {
	  //console.log( footer);
		//res.cookie('firstname' , user.firstName);
        res.locals.footerText = footer.length > 0 ? footer[0].footerText : '';       
        next();	  
    });
     
}


function checkAuthifLogin(req,res,next){
    // if(!req.isAuthenticated()){
    //     //if user is looged in, req.isAuthenticated() will return true 
    //     next();
    // } else{
        res.redirect("/superpos/dashboard");
    // }
}


router.get('/admin',csrfProtection,authController.signinview);

router.get('/logout', function(req, res){
    req.logout();
    req.session.destroy();
    return res.redirect('/superpos/admin');
    // res.redirect('/superpos/admin');
});


router.post('/signin', authController.signin);
router.post('/signup', authController.signup);




router.get('/dashboard',checkAuthentication,middleHandler,admin_userController.dashboard);
//
///////////////admin_user///////////////////////
router.get('/admin/list',checkAuthentication,middleHandler,admin_userController.adminList);
router.get('/admin/addedit/:id?',checkAuthentication,middleHandler,admin_userController.addeditAdmin);
//router.post('/admin/add',checkAuthentication,middleHandler,admin_userController.add);
router.get('/admin/delete/:id?',checkAuthentication,middleHandler,admin_userController.deleteAdmin);
///////////////End admin_user///////////////////////
//
//
///////////////Attribute///////////////////////
//router.post('/attribute/add',checkAuthentication,middleHandler,attributeController.add);
router.get('/attribute',checkAuthentication,middleHandler,attributeController.attributeList);
router.get('/attribute/addedit/:id?',checkAuthentication,middleHandler,attributeController.addeditAttribute);
router.get('/attribute/delete/:id?',checkAuthentication,middleHandler,attributeController.deleteAttribute);
//////////////End Attribute/////////////////////////
//
//
///////////////Citi///////////////////////
router.post('/citi/add',checkAuthentication,middleHandler,citiController.addCiti);
router.get('/citi',checkAuthentication,middleHandler,citiController.citiList);
router.get('/citi/addedit/:id?',checkAuthentication,middleHandler,citiController.addeditCiti);
router.get('/citi/delete/:id?',checkAuthentication,middleHandler,citiController.deleteCiti);
//////////////End Citi/////////////////////////
//

router.get('/sms',checkAuthentication,middleHandler,smsController.send);
     
router.post('/sms/send',checkAuthentication,middleHandler,smsController.sendSms);   







//
///////////////Zipcode///////////////////////
router.post('/zipcode/add',checkAuthentication,middleHandler,zipcodeController.addZipcode);
router.get('/zipcode',checkAuthentication,middleHandler,zipcodeController.zipcodeList);
router.get('/zipcode/addedit/:id?',checkAuthentication,middleHandler,zipcodeController.addeditZipcode);
router.get('/zipcode/delete/:id?',checkAuthentication,middleHandler,zipcodeController.deleteZipcode);
//////////////End Zipcode/////////////////////////
//
//
///////////////stores///////////////////////
router.post('/stores/add',checkAuthentication,middleHandler,storesController.addStores);
router.get('/stores',checkAuthentication,middleHandler,storesController.storesList);
router.get('/stores/addedit/:id?',checkAuthentication,middleHandler,storesController.addeditStores);
router.get('/stores/delete/:id?',checkAuthentication,middleHandler,storesController.deleteStores);
//////////////End stores/////////////////////////
//
//
///////////////Category///////////////////////
router.post('/category/fileupload',checkAuthentication,middleHandler,categoryController.fileupload);
router.post('/category/add',checkAuthentication,middleHandler,categoryController.addCategory);
router.get('/category',checkAuthentication,middleHandler,categoryController.categoryList);
router.get('/category/addedit/:id?',checkAuthentication,middleHandler,categoryController.addeditCategory);
router.get('/category/delete/:id?',checkAuthentication,middleHandler,categoryController.deleteCategory);
router.get('/category/download',checkAuthentication,middleHandler,categoryController.downloadCategoryList);
//////////////End Category/////////////////////////
//
//
///////////////SubCategory///////////////////////
//router.post('/category/fileupload',checkAuthentication,middleHandler,categoryController.fileupload);
//router.post('/category/add',checkAuthentication,middleHandler,categoryController.add);
router.get('/subcategory',checkAuthentication,middleHandler,subcategoryController.subcategoryList);
router.get('/subcategory/addedit/:id?',checkAuthentication,middleHandler,subcategoryController.addeditSubcategory);
router.get('/subcategory/delete/:id?',checkAuthentication,middleHandler,subcategoryController.deleteSubcategory);
router.get('/subcategory/download',checkAuthentication,middleHandler,subcategoryController.downloadSubCategoryList);
//////////////End SubCategory/////////////////////////
//
//
///////////////Banner Subcategory//////////////////////
router.get('/banner/subcategory',checkAuthentication,middleHandler,banner_subcategory.subcategoryBannerList);
router.get('/banner/subcategory/addedit/:id?',checkAuthentication,middleHandler,banner_subcategory.subcategoryBannerAddedit);
router.post('/banner/subcategory/add/:id?',checkAuthentication,middleHandler,banner_subcategory.add);
router.get('/banner/subcategory/delete/:id?',checkAuthentication,middleHandler,banner_subcategory.delete);

//////////////End Banner Subcategory/////////////////////
//

//
///////////////Product///////////////////////
router.post('/product/fileupload',checkAuthentication,middleHandler,productController.fileupload);
router.post('/product/add', checkAuthentication,middleHandler,productController.addProduct);
//router.post('/product/add',checkAuthentication,middleHandler,productController.add);
router.get('/product',checkAuthentication,middleHandler,productController.productList);
router.get('/product/addedit/:id?/:page?',checkAuthentication,middleHandler,productController.addeditProduct);
router.get('/product/delete/:id?',checkAuthentication,middleHandler,productController.deleteProduct);
router.post('/product/subcategory/list/:id?',checkAuthentication,middleHandler,productController.productSubcategory);
//router.post('/productmul/delete',checkAuthentication,middleHandler,productController.deleteProductmul);
//////////////End Product/////////////////////////

router.get('/product/exclereport',checkAuthentication,middleHandler,productController.downloadList);
//
///////////////product_upload start///////////////////////
router.get('/product_upload',checkAuthentication,middleHandler,product_uploadController.product_upload);
router.post('/product_upload/add',checkAuthentication,middleHandler, product_uploadController.addProduct_upload);
//////////////End product_upload/////////////////////////
//
///////////////category_product///////////////////////
router.post('/category_product/add',checkAuthentication,middleHandler,category_productController.add);
router.get('/category_product',checkAuthentication,middleHandler,category_productController.category_productList);
router.get('/category_product/addedit/:id?',checkAuthentication,middleHandler,category_productController.addedit);
router.get('/category_product/delete/:id?',checkAuthentication,middleHandler,category_productController.deleteCategory_Product);
//////////////End Category_Product/////////////////////////
//
//
///////////////Attribute_option///////////////////////
router.post('/attribute_option/add',checkAuthentication,middleHandler,attribute_optionController.add);
router.get('/attribute_option',checkAuthentication,middleHandler,attribute_optionController.attributeList);
router.get('/attribute_option/addedit/:id?',checkAuthentication,middleHandler,attribute_optionController.addedit);
//////////////Attribute_option/////////////////////////
//
//
///////////////Customer///////////////////////////////
// router.post('/customer/add',checkAuthentication,middleHandler,customerController.addCustomer);
router.get('/customer',checkAuthentication,middleHandler,customerController.customerList);
router.get('/customer/addedit/:id?',checkAuthentication,middleHandler,customerController.addeditCustomer);
router.get('/customer/delete/:id?',checkAuthentication,middleHandler,customerController.deleteCustomer);
router.get('/customer/download',checkAuthentication,middleHandler,customerController.downloadCustomerList);
//////////////End Customer////////////////////////////
//
//
///////////////Customer_fav///////////////////////////////
//router.post('/customer/add',checkAuthentication,middleHandler,customerController.add);
router.get('/customer_fav',checkAuthentication,middleHandler,customer_favController.customer_favList);
router.get('/customer_fav/addedit/:id?',checkAuthentication,middleHandler,customer_favController.addeditCustomer_fav);
router.get('/customer_fav/delete/:id?',checkAuthentication,middleHandler,customer_favController.deleteCustomer_fav);
//////////////End Customer_fav////////////////////////////
//
///////////////Customer_group///////////////////////////////
//router.post('/customer/add',checkAuthentication,middleHandler,customerController.add);
router.get('/customer_group',checkAuthentication,middleHandler,customer_groupController.customer_groupList);
router.get('/customer_group/addedit/:id?',checkAuthentication,middleHandler,customer_groupController.addeditCustomer_group);
router.get('/customer_group/delete/:id?',checkAuthentication,middleHandler,customer_groupController.deleteCustomer_group);
//////////////End Customer_group////////////////////////////
//
//
///////////////Customer Address///////////////////////////////
//router.get('/customer_address',checkAuthentication,middleHandler,customer_addressController.customer_addressList);
//////////////End Customer Address////////////////////////////
//
//
///////////////Customer Address///////////////////////////////
router.get('/customer_address',checkAuthentication,middleHandler,customer_addressController.customer_addressList);
router.get('/customer_address/addedit/:id?',checkAuthentication,middleHandler,customer_addressController.addeditCustomer_address);
router.get('/customer_address/delete/:id?',checkAuthentication,middleHandler,customer_addressController.deleteCustomer_address);
//////////////End Customer Address////////////////////////////
//
//
///////////////Order///////////////////////////////
router.get('/order',checkAuthentication,middleHandler,orderController.orderList);
router.get('/order/addedit/:id?/:page?',checkAuthentication,middleHandler,orderController.addeditOrder);
router.get('/order/delete/:id?',checkAuthentication,middleHandler,orderController.deleteOrder);

router.get('/order/StatusChange/:id?/:data?',orderController.orderStatusChange);
// router.get('/order/searchorder/:data?',checkAuthentication,middleHandler,orderController.orderSearch);
router.post('/order/add',checkAuthentication,middleHandler,orderController.updateOrder);


router.get('/order/otgInvoice/:id?',checkAuthentication,middleHandler,orderController.otgInvoice);
//////////////End Order////////////////////////////
//
router.post('/order/update/:id?',checkAuthentication,middleHandler,orderController.updateOrder);
//////////////End Order////////////////////////////
//
///////////////Order Item///////////////////////////////
router.get('/order_item',checkAuthentication,middleHandler,order_itemController.order_itemList);
//////////////End Order Item////////////////////////////
//
//
///////////////Order Status///////////////////////////////
router.get('/order_status_history',checkAuthentication,middleHandler,order_status_historyController.order_status_historyList);
router.get('/order_status_history/addedit/:id?',checkAuthentication,middleHandler,order_status_historyController.addeditOrder_status_history);
router.get('/order_status_history/delete/:id?',checkAuthentication,middleHandler,order_status_historyController.deleteOrder_status_history);
//////////////End Order Status////////////////////////////
//
//
///////////////Settings///////////////////////
router.get('/sitesettings',checkAuthentication,middleHandler,sitesettingsController.addedit);
router.post('/sitesettings/add',checkAuthentication,middleHandler,sitesettingsController.add);
///////////////End Settings///////////////////////
//
//
///////////////Roles///////////////////////
router.get('/roles',checkAuthentication,middleHandler,rolesController.rolesList);
router.get('/roles/addedit/:id?',checkAuthentication,middleHandler,rolesController.addeditRoles);
router.post('/roles/add',checkAuthentication,middleHandler,rolesController.addRoles);
router.get('/roles/delete/:id?',checkAuthentication,middleHandler,rolesController.deleteRole);
///////////////End Roles///////////////////////
//
//
///////////////Permission///////////////////////
router.get('/modules',checkAuthentication,middleHandler,modulesController.modulesList);
router.get('/modules/addedit/:id?',checkAuthentication,middleHandler,modulesController.addedit);
router.post('/modules/add',checkAuthentication,middleHandler,modulesController.add);
router.get('/modules/delete/:id?',checkAuthentication,middleHandler,modulesController.deleteModules);
///////////////End Permission///////////////////////
//
//
///////////////Permission Role///////////////////////
router.get('/modulesrole',checkAuthentication,middleHandler,modulesroleController.modulesroleList);
router.get('/modulesrole/addedit/:id?',checkAuthentication,middleHandler,modulesroleController.addedit);
router.post('/modulesrole/add',checkAuthentication,middleHandler,modulesroleController.add);
router.get('/modulesrole/delete/:id?',checkAuthentication,middleHandler,modulesroleController.deleteModulesrole);
///////////////End Permission Role///////////////////////
//
//
///////////////users///////////////////////
router.get('/users',checkAuthentication,middleHandler,usersController.usersList);
router.get('/users/addedit/:id?',checkAuthentication,middleHandler,usersController.addeditUser);
router.post('/users/add', checkAuthentication,middleHandler,usersController.addUser);
router.post('/users/fileupload',checkAuthentication,middleHandler,usersController.fileupload);
router.post('/usersUqEmail', checkAuthentication,middleHandler,usersController.usersUqEmail);
router.get('/users/delete/:id?',checkAuthentication,middleHandler,usersController.deleteUsers);
///////////////End users///////////////////////
//
///////////////////////////////////////profile start////////////////////////////////////////
//
router.get('/profile/addedit/:id?',checkAuthentication,middleHandler,profileController.addedit);
//
///////////////////////////////////////profile ends////////////////////////////////////
//
//////////////////////////////////////account_setting start////////////////////////////////////////
//
//router.get('/account_setting',checkAuthentication,middleHandler,account_settingController.account_settingList);
router.get('/account_setting/addedit/:id?',checkAuthentication,middleHandler,account_settingController.addeditAccount_setting);
router.post('/account_setting/fileupload',checkAuthentication,middleHandler,account_settingController.fileupload);
//router.get('/account_setting/delete/:id?',checkAuthentication,middleHandler,account_settingController.deleteAccount_setting);
//
///////////////////////////////////////account_setting ends////////////////////////////////////
///
//
///////////////Country ///////////////////////
router.get('/country/:id?',checkAuthentication,middleHandler,countryController.countryList);
router.get('/country/addedit/:id?',checkAuthentication,middleHandler,countryController.countryList);
router.post('/country/add',checkAuthentication,middleHandler,countryController.add);
router.get('/country/delete/:id?',checkAuthentication,middleHandler,countryController.deleteCountry);
///////////////End Country ///////////////////////
//
//
///////////////City ///////////////////////
router.get('/city/:id?/:cid?',checkAuthentication,middleHandler,cityController.cityList);
//router.get('/city/addedit/:id?/:cid?',checkAuthentication,middleHandler,cityController.cityList);
router.post('/city/add',checkAuthentication,middleHandler,cityController.add);
router.get('/city/delete/:id?/:cid?',checkAuthentication,middleHandler,cityController.deleteCity);
///////////////End City ///////////////////////
//
//
///////////////Call to Actione///////////////////////
router.get('/call_to_action',checkAuthentication,middleHandler,call_to_actionController.call_to_actionList);
router.get('/call_to_action/addedit/:id?',middleHandler,checkAuthentication,call_to_actionController.addedit);
router.post('/call_to_action/add',checkAuthentication,middleHandler,call_to_actionController.add);
router.get('/call_to_action/delete/:id?',checkAuthentication,middleHandler,call_to_actionController.deletecall_to_action);
///////////////End Call to Actione///////////////////////
//
//
///////////////settings_profile start ///////////////////////
router.get('/settings_profile',checkAuthentication,middleHandler,settings_profileController.settings_profileList);
router.get('/settings_profile/addedit/:id?',checkAuthentication,middleHandler,settings_profileController.addeditSettings_profile);
router.get('/settings_profile/delete/:id?',checkAuthentication,middleHandler,settings_profileController.deleteSettings_profile);
///////////////End settings profile ///////////////////////
//
//
///////////////CMS start ///////////////////////
router.get('/cms',checkAuthentication,middleHandler,cmsController.cmsList);
router.get('/cms/addedit/:id?',checkAuthentication,middleHandler,cmsController.addeditCms);
router.get('/cms/delete/:id?',checkAuthentication,middleHandler,cmsController.deleteCms);
///////////////End CMS ///////////////////////
//
//
///////////////Notification start ///////////////////////
router.get('/notification',checkAuthentication,middleHandler,notificationController.notificationList);
router.get('/notification/addedit/:id?',checkAuthentication,middleHandler,notificationController.addeditNotification);
router.get('/notification/delete/:id?',checkAuthentication,middleHandler,notificationController.deleteNotification);
///////////////End Notification ///////////////////////
//
//
///////////////Notification_trans start ///////////////////////
router.get('/notification_trans',checkAuthentication,middleHandler,notification_transController.notification_transList);
router.get('/notification_trans/addedit/:id?',checkAuthentication,middleHandler,notification_transController.addeditNotification_trans);
router.get('/notification_trans/delete/:id?',checkAuthentication,middleHandler,notification_transController.deleteNotification_trans);
///////////////End Notification_trans ///////////////////////
//
//
//
///////////////start delivery_time_slot///////////////////////////////
router.get('/delivery_time_slot',checkAuthentication,middleHandler,delivery_time_slotController.delivery_time_slotList);
router.get('/delivery_time_slot/addedit/:id?',checkAuthentication,middleHandler,delivery_time_slotController.addeditDelivery_time_slot);
router.post('/delivery_time_slot/add', checkAuthentication,middleHandler,delivery_time_slotController.addDelivery_time_slot);
router.get('/delivery_time_slot/delete/:id?',checkAuthentication,middleHandler,delivery_time_slotController.deleteDelivery_time_slot);
//////////////End delivery_time_slot////////////////////////////
//
//
///////////////start coupon///////////////////////////////
router.get('/coupon',checkAuthentication,middleHandler,couponController.couponList);
router.get('/coupon/addedit/:id?',checkAuthentication,middleHandler,couponController.addeditCoupon);
router.post('/coupon/add', checkAuthentication,middleHandler,couponController.addCoupon);
router.get('/coupon/delete/:id?',checkAuthentication,middleHandler,couponController.deleteCoupon);
//////////////End coupon////////////////////////////

////////start coupon value//////
//

router.get('/coupon-value',checkAuthentication,middleHandler,couponController.addCouponvalue);
router.post('/coupon-value/add', checkAuthentication,middleHandler,couponController.updateCouponvalue);


//
///////////////start coupon_transaction///////////////////////////////
router.get('/coupon_transaction',checkAuthentication,middleHandler,coupon_transactionController.coupon_transactionList);
// router.get('/coupon_transaction/addedit/:id?',checkAuthentication,middleHandler,coupon_transactionController.addeditCoupon_transaction);
// router.post('/coupon_transaction/add', checkAuthentication,middleHandler,coupon_transactionController.addCoupon_transaction);
router.get('/coupon_transaction/delete/:id?',checkAuthentication,middleHandler,coupon_transactionController.deleteCoupon_transaction);
//////////////End coupon_transaction////////////////////////////
//
//
/////start email configaration/////
//router.get('/emailconfig',checkAuthentication,middleHandler,checkAuthentication,middleHandler,email_configcontroller.email_config);
router.get('/emailconfig',checkAuthentication,middleHandler,checkAuthentication,middleHandler,email_configcontroller.email_configadd);
router.post('/emailconfig/add',checkAuthentication,middleHandler,checkAuthentication,middleHandler,email_configcontroller.add);
//
//
///////////////start wallet///////////////////////////////
router.get('/wallet',checkAuthentication,middleHandler,walletController.walletList);
router.get('/wallet/addedit/:id?',checkAuthentication,middleHandler,walletController.addeditwallet);
router.post('/wallet/add', checkAuthentication,middleHandler,walletController.addWallet);
router.get('/wallet/delete/:id?',checkAuthentication,middleHandler,walletController.deleteWallet);
//////////////End wallet////////////////////////////
//
//
///////////////banner start///////////////////////
router.post('/banner/fileupload',checkAuthentication,middleHandler,bannerController.fileupload);
router.post('/banner/add',checkAuthentication,middleHandler,bannerController.addBanner);
router.get('/banner',checkAuthentication,middleHandler,bannerController.bannerList);
router.get('/banner/addedit/:id?',checkAuthentication,middleHandler,bannerController.addeditBanner);
router.get('/banner/delete/:id?',checkAuthentication,middleHandler,bannerController.deleteBanner);
//////////////End banner/////////////////////////
//
//
///////////////banner_section Start ///////////////////////////////
router.get('/banner_section',checkAuthentication,middleHandler,banner_sectionController.banner_sectionList);
router.get('/banner_section/addedit/:id?',checkAuthentication,middleHandler,banner_sectionController.addeditBanner_section);
router.post('/banner_section/add', checkAuthentication,middleHandler,banner_sectionController.addBanner_section);
router.get('/banner_section/delete/:id?',checkAuthentication,middleHandler,banner_sectionController.deleteBanner_section);
//////////////End banner_section ////////////////////////////
//
//
///////////////banner_display Start ///////////////////////////////
router.get('/banner_display',checkAuthentication,middleHandler,banner_displayController.banner_displayList);
router.get('/banner_display/addedit/:id?',checkAuthentication,middleHandler,banner_displayController.addeditBanner_display);
router.post('/banner_display/add', checkAuthentication,middleHandler,banner_displayController.addBanner_display);
router.get('/banner_display/delete/:id?',checkAuthentication,middleHandler,banner_displayController.deleteBanner_display);
//////////////End banner_display ////////////////////////////
//
//
///////////////dropdown_settings///////////////////////
router.post('/dropdown_settings/add',checkAuthentication,middleHandler,dropdown_settingsController.addDropdown_settings);
router.get('/dropdown_settings',checkAuthentication,middleHandler,dropdown_settingsController.dropdown_settingsList);
router.get('/dropdown_settings/addedit/:id?',checkAuthentication,middleHandler,dropdown_settingsController.addeditDropdown_settings);
router.get('/dropdown_settings/delete/:id?',checkAuthentication,middleHandler,dropdown_settingsController.deleteDropdown_settings);
//////////////End Attribute/////////////////////////
//
///////////////salesman Start ///////////////////////////////
router.get('/salesman',checkAuthentication,middleHandler,salesmanController.salesmanList);
router.get('/salesman/addedit/:id?',checkAuthentication,middleHandler,salesmanController.addeditSalesman);
router.post('/salesman/add', checkAuthentication,middleHandler,salesmanController.addSalesman);
router.get('/salesman/delete/:id?',checkAuthentication,middleHandler,salesmanController.deleteSalesman);
//////////////End salesman ////////////////////////////
//
//
///////////////start shipping_method///////////////////////////////
router.get('/shipping_method',checkAuthentication,middleHandler,shipping_methodController.shipping_methodList);
router.get('/shipping_method/addedit/:id?',checkAuthentication,middleHandler,shipping_methodController.addeditShipping_method);
router.post('/shipping_method/add', checkAuthentication,middleHandler,shipping_methodController.addShipping_method);
router.get('/shipping_method/delete/:id?',checkAuthentication,middleHandler,shipping_methodController.deleteShipping_method);
//////////////End shipping_method////////////////////////////
//

//-----------test--------------------/
router.get('/test',checkAuthentication,middleHandler,testController.testList);
router.get('/test/addedit/:id?',checkAuthentication,middleHandler,testController.addeditTeam);
router.post('/test/add',checkAuthentication,middleHandler,testController.addTest);
router.get('/test/delete/:id?',checkAuthentication,middleHandler,testController.deleteTest);
//-----------test end--------------------/

//------------orderitem-------------------------/
router.get('/orderitem',checkAuthentication,middleHandler,orderitemController.orderitemList);
router.get('/orderitem/addedit/:id?',checkAuthentication,middleHandler,orderitemController.addeditOrderitem);
//------------end orderitem-------------------------/

//------------sale reports-------------------------/
router.post('/sale_reports/list',checkAuthentication,middleHandler,sale_reportsController.sale_reportsList);
router.get('/sale_reports',checkAuthentication,middleHandler,sale_reportsController.sale_reports);
//------------sale reports-------------------------/

//----------------------customer report-------------//
router.get('/customer_reports',checkAuthentication,middleHandler,customer_reportsController.customer_reportsList);
//--------------------customer report---------------//

router.get('/checkvalidation', function(req, res){  
    return res.render('checkvalidation');
});

module.exports = router;
