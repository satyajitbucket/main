/*Project: Node Scratch
 * Author: Bibekananda Jana
 * 
 * 
* */

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.redirect('/superpos/admin');
});

module.exports = router;
